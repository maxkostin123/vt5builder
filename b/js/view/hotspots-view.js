/**
 * Created by Max Kostin on 07/02/2017.
 */


"use strict";


const fs = require("fs-extra");
const path = require('path');

const logger = require("./../util/logger");

let div;

let mapHolder;
let imgMap;
let hotspots;
let sType = "label";
let selectedEl;
let canCreate = true;

let rectHolder;
let rectSpot;

let diffX = 0;
let diffY = 0;

let txtId, txtLabel, bgColor, btnSetSpot, gui, hotspotsTxt;
let hasInstructions = true;


exports.init = function ($gui) {

    gui = $gui;

    div = document.getElementById("hotspots");

    div.innerHTML = `
              <div class="hotspots-controls">
                <button id="hotspots-loadImage">Load image</button><input id="hotspot-img-open" type="file" hidden/>
                <div id="hotspots-type-selector">
                        <span>
                            <label>Circle <input type="radio" name="s-type" data-id="circle"></label>
                            <label>Rect <input type="radio" name="s-type" data-id="rect"></label>
                            <label>Label <input type="radio" name="s-type" data-id="label" checked></label>
                        </span>
                        <span>
                            <label>id: <input type="text" id="hotspots-id"></label>
                            <label>description: <input type="text" id="hotspots-label"></label>
                            <label>Colour <input type="color" id="hotspots-color" value="#3399cc"></label>
                        </span>
                    </div>
              </div>
              <button id="hotspots-save">Get</button> <button id="hotspots-set">Set</button> <input type="text" id="hotspots-output">
              <div class="hotspots-imgHolder">
                <div class="imageMap" id="hotspots-map">
                    <img id="hotspots-img" src="">
                    <div class="hotspots" id="hotspots-spots"><div class="hotspots-instructions">Instructions:<br><ol>
                        <li>Load image.</li>
                        <li>Select hotspot type to add (cirle and rect are for Test questions).</li>
                        <li>Click on image. You can drag, resize and delete selected hotspots. Labels can't be resized.</li>
                        <li>To delete, select a hotspot and hit Backspace.</li>
                        <li>Click Get to generate code. It will be copied to your clipboard automatically. Paste to your data file.</li>
                        <li>To edit existing hotspots: load image, copy array into the textfield next to Set button and click Set.</li>
                        <li>You can change IDs, Descriptions and Background colours at any time.</li>
                        <li>Only Labels can have descriptions and background colours.</li>
                        <li>Descriptions should not be longer than 80 characters.</li>
                    </ol></div></div>
                </div>
              </div>
              `;


    mapHolder = document.getElementById("map-holder");

    imgMap = document.getElementById("hotspots-map");
    hotspots = document.getElementById("hotspots-spots");
    txtId = document.getElementById("hotspots-id");
    txtLabel = document.getElementById("hotspots-label");
    bgColor = document.getElementById("hotspots-color");
    btnSetSpot = document.getElementById("set-spot");
    hotspotsTxt = document.getElementById("hotspots-output");

    imgMap.addEventListener("click", mapClickHandler);
    imgMap.addEventListener("mousedown", mDown);

    window.addEventListener("keyup", keyUp);

    document.getElementById("hotspots-type-selector").addEventListener("click", changeType);

    document.getElementById("hotspots-id").addEventListener("input", setSpotValue);
    document.getElementById("hotspots-label").addEventListener("input", setSpotValue);
    document.getElementById("hotspots-color").addEventListener("change", setBackground);

    document.getElementById("hotspots-loadImage").addEventListener("click", () => document.getElementById("hotspot-img-open").click());
    document.getElementById("hotspot-img-open").addEventListener("change", fileSelectHandler);

    document.getElementById("hotspots-save").addEventListener("click", copyToClipboard);
    document.getElementById("hotspots-set").addEventListener("click", () => setHotspots(hotspotsTxt.value));

};


function fileSelectHandler(e) {

    let file = e.target.files[0];

    if (! file.path.match(/bin\Wres\Wassets\W/i)) {
        logger.log(`File ${file.name} does not appear to be in the project's directory.`, "warning");
    } else {
        logger.log(file.name, "ok");
    }
    document.getElementById("hotspots-img").src = "file:///" + file.path;
    if (hasInstructions) {
        let instr = document.querySelector(".hotspots-instructions");
        instr.parentNode.removeChild(instr);
        hasInstructions = false;
    }

}


let hcount = 1;


function mapClickHandler(e) {

    if (!e.target.classList.contains("hspot") && !e.target.classList.contains("resizer") && canCreate) {
        if (selectedEl) {
            selectedEl.classList.remove("selected");
        }
        createHotspot(e);
    }
}

function createHotspot(e) {
    let h = document.createElement("div");

    rectHolder = imgMap.getBoundingClientRect();

    let x = e.offsetX * 100 / rectHolder.width;
    let y = e.offsetY * 100 / rectHolder.height;


    if (sType === "circle") {
        h.classList.add("hspot");
        h.classList.add("circle");
        h.innerHTML = `<span class='el-id'>${hcount}</span><div class='resizer'></div>`;

    } else if (sType === "rect") {
        h.classList.add("hspot");
        h.classList.add("rect");
        h.style.width = "10%";
        h.style.height = "10%";
        h.innerHTML = `<span class='el-id'>${hcount}</span><div class='resizer'></div>`;

    } else if (sType === "label") {
        h.classList.add("hspot");
        h.classList.add("circle");
        h.classList.add("label");
        h.setAttribute("data-s-color", "#3399cc");
        h.innerHTML = `<span class='el-id'>${hcount}</span>`;
    }

    h.style.left = x + "%";
    h.style.top = y + "%";
    hotspots.appendChild(h);

    h.classList.add("selected");
    selectedEl = h;

    h.setAttribute("data-s-id", hcount);
    h.setAttribute("data-s-type", sType);
    h.setAttribute("data-s-x", x.toFixed(1) + "");
    h.setAttribute("data-s-y", y.toFixed(1) + "");

    if (sType !== "label") {
        rectHolder = imgMap.getBoundingClientRect();
        let rect = h.getBoundingClientRect();
        let w = rect.width * 100 / rectHolder.width;
        let hi = rect.height * 100 / rectHolder.height;
        h.setAttribute("data-s-width", w.toFixed(1) + "");
        h.setAttribute("data-s-height", hi.toFixed(1) + "");
    }

    setLabels();
    hcount++;

}


function setHotspots(str) {

    if (str.length < 5) return;

    str = str.replace(/,$/, "");

    let arr;

    try {
        arr = eval(str)
    } catch (err) {
        logger.log(err, "error");
    }

    let data = "";

    for (let i = 0; i < arr.length; i++) {

        let h = arr[i];

        let type = (h.type === "label") ? "circle label" : h.type;
        let id = `<span class='el-id'>${h.id}</span>`;
        let size = h.width? `width: ${h.width}%; height: ${h.height}%;` : "";
        let description = h.description? `data-s-description="${h.description}"` : "";
        let color = h.color? `background: ${h.color};` : "";

        let x = h.x;
        let y = h.y;
        let width = h.width? `data-s-width="${h.width}"` : "";
        let height = h.width? `data-s-height="${h.height}"` : "";
        let colorData = h.color? `data-s-color="${h.color}"` : "";

        let resizer = h.type !== "label" ? "<div class='resizer'></div>" : "";

        data += `<div 
                data-s-id="${h.id}"
                data-s-type="${h.type}"
                data-s-x="${x}"
                data-s-y="${y}" ${width} ${height}
                ${colorData}
                ${description}
                class="hspot ${type}" 
                style="left: ${h.x}%; top: ${h.y}%; ${size}; ${color}">${id}${resizer}</div>`;

    }

    data = data.replace(/(;;)|(; ;)/g, ";");

    hotspots.innerHTML = data;

}


function changeType(e) {
    let t = e.target.getAttribute("data-id");
    if (!t) return;
    sType = t;
}


function mDown(e) {

    canCreate = true;
    let el = e.target;

    if (selectedEl) {
        selectedEl.classList.remove("selected");
        selectedEl = undefined;
    }

    rectHolder = imgMap.getBoundingClientRect();
    diffX = rectHolder.left + e.offsetX;
    diffY = rectHolder.top + e.offsetY;


    if (el.classList.contains("hspot")) {
        selectedEl = el;
        rectSpot = selectedEl.getBoundingClientRect();
        imgMap.addEventListener("mousemove", mMove);
        imgMap.addEventListener("mouseup", mUp);
        canCreate = false;

    } else if (el.classList.contains("resizer")) {
        canCreate = false;
        selectedEl = el.parentNode;
        rectSpot = selectedEl.getBoundingClientRect();
        imgMap.addEventListener("mousemove", resize);
        imgMap.addEventListener("mouseup", mUp);
    }

    if (selectedEl) {
        selectedEl.classList.add("selected");
        setLabels();
    } else {

    }

}

function mMove(e) {
    canCreate = false;
    if (selectedEl) {
        let cX = 0;
        let cY = 0;
        if (selectedEl.classList.contains("circle")) {
            cX = rectSpot.width / 2;
            cY = rectSpot.width / 2;
        }
        let x = (e.clientX - diffX + cX) * 100 / rectHolder.width;
        let y = (e.clientY - diffY + cY) * 100 / rectHolder.height;
        selectedEl.style.left = x + "%";
        selectedEl.style.top = y + "%";
        selectedEl.setAttribute("data-s-x", x.toFixed(1) + "");
        selectedEl.setAttribute("data-s-y", y.toFixed(1) + "");
    }
}


function resize(e) {

    if (selectedEl) {
        let x = (e.clientX - rectHolder.left - (rectSpot.left - rectHolder.left)) * 100 / rectHolder.width;
        let y = (e.clientY - rectHolder.top - (rectSpot.top - rectHolder.top)) * 100 / rectHolder.height;
        selectedEl.style.width = x + "%";
        selectedEl.style.height = y + "%";
        selectedEl.setAttribute("data-s-width", x.toFixed(1) + "");
        selectedEl.setAttribute("data-s-height", y.toFixed(1) + "");
    }
}


function mUp(e) {
    imgMap.removeEventListener("mousemove", mMove);
    imgMap.removeEventListener("mouseup", mUp);
    imgMap.removeEventListener("mousemove", resize);
}


function keyUp(e) {
    if (e.key === "Backspace" && selectedEl) {
        if (document.activeElement === txtLabel || document.activeElement === txtId || document.activeElement === hotspotsTxt) return;
        hotspots.removeChild(selectedEl);
        if (hcount > 1) hcount--;
    }
}

function setLabels() {
    if (!selectedEl) return;
    txtId.value = selectedEl.getAttribute("data-s-id");
    txtLabel.value = selectedEl.getAttribute("data-s-description");

    let color = selectedEl.getAttribute("data-s-color");
    if (color) {
        bgColor.value = color;
    } else {
        bgColor.value = "#3399cc";
    }
}

function setSpotValue() {
    if (!selectedEl) return;
    selectedEl.setAttribute("data-s-id", txtId.value);
    selectedEl.querySelector(".el-id").textContent = txtId.value;
    let str = txtLabel.value;
    if (str) {
        selectedEl.setAttribute("data-s-description", str.replace(/"/g, "'"));
    }
}

function setBackground() {
    if (selectedEl.classList.contains("label")) {
        selectedEl.style.backgroundColor = bgColor.value;
        selectedEl.setAttribute("data-s-color", bgColor.value);
    }
}


function zoomIn() {
    mapHolder.classList.add("zoomed-in");
    mapHolder.style.width = "100%";
    imgMap.addEventListener("mousedown", mapMoveStart);
}


let mapDiffX = 0;
let mapDiffY = 0;

function mapMoveStart(e) {
    mapDiffX = e.offsetX;
    mapDiffY = e.offsetY;
    imgMap.addEventListener("mousemove", mapMoveProgress);
    imgMap.addEventListener("mouseup", mapMoveEnd);
}

function mapMoveProgress(e) {
    let x = (e.clientX - mapDiffX) * 100 / rectHolder.width;
    let y = (e.clientY - mapDiffY) * 100 / rectHolder.height;
    imgMap.style.left = x + "%";
    imgMap.style.top = y + "%";
}

function mapMoveEnd(e) {
    imgMap.removeEventListener("mousemove", mapMoveProgress);
}

function clear() {
    hotspots.innerHtml = "";
    hcount = 1;
}


function copyToClipboard() {


    let nodes = hotspots.querySelectorAll(".hspot");

    let str = "hotspots: [";

    for (let i = 0; i < nodes.length; i++) {

        let n = nodes[i];

        let id = n.getAttribute("data-s-id");
        let type = n.getAttribute("data-s-type");
        let x = n.getAttribute("data-s-x");
        let y = n.getAttribute("data-s-y");
        let width = n.getAttribute("data-s-width");
        let height = n.getAttribute("data-s-height");
        let description = n.getAttribute("data-s-description");
        let color = n.getAttribute("data-s-color");
        if (color === "#3399cc") color = undefined;

        str += `{id:${id}, type:'${type}', x:${x}, y:${y}`;

        if (width) str += `, width:${width}`;
        if (height) str += `, height:${height}`;
        if (description) str += `, description:"${description}"`;
        if (color) str += `, color:'${color}'`;

        let comma = i < nodes.length - 1 ? ", " : "";

        str += `}${comma}`;
    }

    str += "]";

    let clipboard = gui.Clipboard.get();
    clipboard.set(str, 'text');
    let out = document.getElementById("hotspots-output");
    out.value = str;
    out.select();


}

/*

let hs = [{id:1, type:'circle', x:31, y:18.8, width:18.8, height:13.1}, {id:2, type:'label', x:39.3, y:13.4}]

let h0 = hs[0];
let h1 = hs[1];

console.log(insideEllipse(h0.x, h0.y, h0.width/2, h0.height/2, h1.x, h1.y))

function insideEllipse(h, k, rx, ry, x, y) {
    return ((x-h)*(x-h))/(rx*rx) + ((y-k)*(y-k))/(ry*ry) <= 1;
}

*/


/*
 type: string; //circle, rect, label
 id: string;
 x: number;
 y: number;
 width?: number;
 height?: number;
 description?: string;
 color?: string;
 */




