/**
 * Created by Max Kostin on 26/04/2016
 */

"use strict";


const fs = require("fs-extra");
const walker = require("klaw");
const path = require('path');
const rjs = require("requirejs");
const gulp = require("gulp");
const zip = require("gulp-zip");
const util = require('util');

const logger = require("./../util/logger");
const compressor = require("../util/compressor");
const scormBuilder = require("../util/scorm-builder");

let div;
let inpNew;
let dirIn;

let projName;

let isScormRequired = false;
let isVT5Required = false;
let isOSX64Required = false;
let isWin64Required = false;
let currentTime = 0;

let optVT5;

let dirOut;


exports.init = function () {

    div = document.getElementById("build");

    div.innerHTML = `<div class="opt-nav">
		<div class="panel">
		    <label>Targets:</label>
			<label>VT5<input type="checkbox" data-id="include-vt5"></label>
			<label>SCORM<input type="checkbox" data-id="include-scorm"></label>
			<label>win64<input type="checkbox" data-id="include-win64"></label>
			<label>osx64<input type="checkbox" data-id="include-osx64"></label>
		</div>
		<div class="panel">
            <input id="scorm-open" type="file" nwdirectory/><span class="btn" data-id="open">Select bin folder</span>
            <span data-id="scorm-selected"></span>
            <p></p>
            <div><span class="btn action" data-id="start-scorm">Build</span></div>
        </div>
	</div>`;

    inpNew = div.querySelector("#scorm-open");
    inpNew.addEventListener("change", onDirSelect);
    div.querySelector("span[data-id='open']").addEventListener("click", onOpen);
    div.querySelector("span[data-id='start-scorm']").addEventListener("click", onStart);

    optVT5 = div.querySelector("input[data-id='include-vt5']");


};


function onOpen() {
    inpNew.click();
}

function onDirSelect(e) {
    lockVT5(false);
    dirIn = e.path[0].files[0].path;
    div.querySelector("span[data-id='scorm-selected']").textContent = dirIn;
    inpNew.value = "";

    if (!fs.existsSync(dirIn + "/res/data/data_en-gb.js")) {
        logger.log("INVALID PROJECT FOLDER.", "error");
    } else {
        /// Check if stats.json exists
        let stats = path.resolve(dirIn, "../", "stats.json");
        if (!fs.existsSync(stats)) {
            lockVT5(true);
            getStructure(dirIn)
                .then((result)=> {
                    fs.writeFileSync(stats, JSON.stringify(result), "utf8");
                })
        } else {
            /// compare stats
            let arr1 = JSON.parse(fs.readFileSync(stats).toString()).stats;
            getStructure(dirIn)
                .then((result)=> {
                    let st2 = JSON.parse(JSON.stringify(result));
                    if (!compareDir(arr1, st2.stats)) {
                        lockVT5(true);
                        getStructure(dirIn)
                            .then((result)=> {
                                fs.writeFileSync(stats, JSON.stringify(result), "utf8");
                            })
                    }
                })

        }
    }

}

function onStart() {

    if (!dirIn) {
        logger.log("You need to select the bin folder first.", "error");
        return;
    }


    isVT5Required = optVT5.checked;
    isScormRequired = div.querySelector("input[data-id='include-scorm']").checked;
    isWin64Required = div.querySelector("input[data-id='include-win64']").checked;
    isOSX64Required = div.querySelector("input[data-id='include-osx64']").checked;

    start();
}

function lockVT5(bool) {
    optVT5.checked = bool;
    isVT5Required = bool;
    optVT5.disabled = bool;

}


/*==============================================================
 START
 ===============================================================*/

function start() {

    currentTime = Date.now();

    div.classList.add("disabled");

    logger.log("Starting build process. This may take several minutes. Or hours.", "warning");

    let dir = path.basename(dirIn);
    dirOut = dirIn.substr(0, dirIn.lastIndexOf(dir)) + "release/";

    if (!fs.existsSync(dirOut)) {
        fs.mkdirSync(dirOut);
    }

    if (isScormRequired && !fs.existsSync(dirOut + "scorm")) {
        fs.mkdirSync(dirOut + "scorm");
    }

    let prevRelease = fs.existsSync(dirOut + "vt5/res/css");


    if (!prevRelease) {
        isVT5Required = true;
    }


    let data = rjs(dirIn + "/res/data/data_en-gb.js");
    projName = data.product.courseCode + " / " + data.product.title;


    makeVT5()
        .then(()=> {
            return makeSCORM();
        })
        .then(()=> {
            return makeStandalone();
        })
        .then(()=> {
            let tm = ((Date.now() - currentTime) / 1000).toFixed(2);
            logger.log(`Build complete after ${tm}s.`, "ok");
            div.classList.remove("disabled");
            lockVT5(false);
        });


}


function makeVT5() {

    return new Promise((resolve) => {

        if (!isVT5Required) {
            resolve();
        }
        else {

            logger.log("Copying files...");
            let copyArray = [
                dirIn + "/**/*",
                `!${dirIn}/**/*.mp4`,
                `!${dirIn}/**/*.MP4`,
                `!${dirIn}/**/*.mp3`,
                `!${dirIn}/**/*.MP3`
            ];
            gulp.src(copyArray)
                .pipe(gulp.dest(dirOut + "vt5"))
                .on("end", ()=> {
                    compressor.compress(dirIn, dirOut + "/vt5/")
                        .then(()=> {

                            let json = JSON.parse(fs.readFileSync("./package.json").toString());
                            let targetServices = json.services;
                            let indexTemplate = fs.readFileSync("./app/index_template.html").toString();

                            for (let i = 0; i < targetServices.length; i++) {

                                let obj = targetServices[i];

                                let indexHtml = indexTemplate;
                                indexHtml = indexHtml.replace("{{service}}", obj.src);
                                indexHtml = indexHtml.replace("{{lang}}", "en-gb");
                                fs.writeFileSync(dirOut + "vt5/" + obj.fname, indexHtml, "utf8");
                            }

                            resolve();
                        });
                });
        }

    });
}


function makeSCORM() {
    return new Promise((resolve) => {

        if (!isScormRequired) {
            resolve();
        } else {
            scormBuilder.build(dirOut, dirIn)
                .then(()=> {
                    resolve();
                })
        }

    });
}


function makeStandalone() {

    return new Promise((resolve) => {
        if (isWin64Required && isOSX64Required) {

            makePlatform("osx64")
                .then(()=> {
                    return convertMedia(path.normalize(dirOut + `/osx64/content/`));
                })
                .then(()=> {
                    return makePlatform("win64");
                })
                .then(()=> {
                    let pMedia = path.normalize(dirOut + `/osx64/content/`);
                    let media = [
                        pMedia + "**/*.webm",
                        pMedia + "**/*.ogg"
                    ];
                    gulp.src(media)
                        .pipe(gulp.dest(path.normalize(dirOut + `/win64/content/`)))
                        .on("end", ()=> {
                            resolve();
                        })
                })

        } else if (isOSX64Required) {

            makePlatform("osx64")
                .then(()=> {
                    return convertMedia(path.normalize(dirOut + `/osx64/content/`));
                })
                .then(()=> {
                    resolve();
                })
        }

        else if (isWin64Required) {

            makePlatform("win64")
                .then(()=> {
                    return convertMedia(path.normalize(dirOut + `/win64/content/`));
                })
                .then(()=> {
                    resolve();
                })
        }

        else {
            resolve();
        }
    });
}


function makePlatform(target) {

    return new Promise((resolve) => {

        logger.log(`creating ${target} target`);
        let dir = path.normalize(dirOut + `/${target}/`);
        let vt5Dir = path.normalize(dirOut + "/vt5/");


        if (fs.existsSync(dir)) {
            fs.removeSync(dir);
        }
        fs.mkdirSync(dir);

        fs.copy(`./x/${target}/`, dir, ()=> {

            let copyArray = [
                vt5Dir + "**/*",
                `!${vt5Dir}**/*.mp4`,
                `!${vt5Dir}**/*.MP4`,
                `!${vt5Dir}**/*.mp3`,
                `!${vt5Dir}**/*.MP3`
            ];

            gulp.src(copyArray)
                .pipe(gulp.dest(dir + "/content/"))
                .on("end", ()=> {
                    let json = JSON.parse(fs.readFileSync("./x/package.json").toString());
                    json.name = "vt5app";
                    json.window.title = "vt5app";
                    fs.writeFileSync(path.normalize(dir + "/package.json"), JSON.stringify(json), "utf8");

                    walker(dir + "content/res/")
                        .on("data", (f)=> {
                            if (f.path.match(/(.html|.json|.js)/i) && (f.path.indexOf("res/data/") > -1 || f.path.indexOf("res/assets/") > -1) && ! f.stats.isDirectory()) {

                                let str = fs.readFileSync(f.path).toString();

                                let regMp3 = /((res\/assets\/)([\s\w\/'-_])+)\.mp3/ig;
                                let regMp4 = /((res\/assets\/)([\s\w\/'-_])+)\.mp4/ig;

                                str = str.replace(regMp3, "$1" + ".ogg");
                                str = str.replace(regMp4, "$1" + ".webm");
                                fs.writeFileSync(f.path, str, "utf8");
                            }
                        })
                        .on("end", ()=> {
                            resolve();
                        });

                });

        })

    });
}


function convertMedia(fout) {
    return new Promise((resolve) => {
        compressor.getMediaFiles(dirIn, fout)
            .then(()=> {
                return compressor.convertMP3s();
            })
            .then(()=> {
                return compressor.convertMP4s();
            })
            .then(()=> {
                resolve();
            });
    });
}


function compareDir(arr1, arr2) {

    if (arr1.length !== arr2.length) {
        return false;
    }

    for (let i = 0; i < arr1.length; i++) {
        let f1 = arr1[i];
        let f2 = arr2[i];
        if (f1.stats.mtime !== f2.stats.mtime) {
            return false;
        }
    }

    return true;

}

function getStructure(dir) {

    let arr = [];
    let obj = {};

    return new Promise((resolve) => {

        walker(dir)
            .on("data", (f)=> {
                if (f.path.indexOf(".DS_Store") === -1) {
                    arr.push(f);
                }
            })
            .on("end", ()=> {
                obj.stats = arr;
                resolve(obj);
            })

    });
}




















