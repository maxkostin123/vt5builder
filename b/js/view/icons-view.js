/**
 * Created by Max Kostin on 09/11/2016.
 */


const fs = require("fs-extra");

let div;
let html = "";

exports.init = function () {

    div = document.getElementById("icons");

    let str = fs.readFileSync("./app/styles/vt-icon-font.css").toString();


    let arr = str.match(/vti_[a-zA-Z0-9-]+/gi);

    for (let i = 0; i < arr.length; i++) {

        let glyph = arr[i];

        html += `<div><span class="${glyph}"></span><span>${glyph}</span></div>`

    }

    div.innerHTML = html;

};

