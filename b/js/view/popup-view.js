/**
 * Created by Max Kostin on 25/04/2016
 */


var updDiv, div;

exports.show = function (body) {

    div = document.createElement("div");
    div.id = "modal";
    div.innerHTML = `<div class="popup">
                        <div class="close"><span class="btn" id="btnClose">Close</span></div>
                        ${body}
                        <div id="upd"></div>
                    </div>`;
    document.body.appendChild(div);
    updDiv = document.getElementById("upd");

    document.getElementById("btnClose").addEventListener("click", ()=> {
        document.body.removeChild(div);
    })

};

exports.showUpdate = function (txt) {
    updDiv.innerHTML = txt;
};

