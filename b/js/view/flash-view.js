/**
 * Created by Max Kostin on 27/04/2016
 */

"use strict";

const fs = require("fs");
const logger = require("./../util/logger");


let div,
    inpIn,
    inpOut,
    srcPath,
    destPath;

exports.init = function () {

    div = document.getElementById("flash");

    div.innerHTML = `<div class="panel">
        <div>Select input/output folders and press "Convert".</div>
        <p></p>
        <input id="flash-in" type="file" nwdirectory/><span class="btn" data-id="open-in">Select input folder</span>
        <span data-id="selected-in"></span>
        <p></p>
        <input id="flash-out" type="file" nwdirectory/><span class="btn" data-id="open-out">Select output folder</span>
        <span data-id="selected-out"></span>
        <p></p>
        <p>
            <label>Flash 2016 <input type="radio" data-id="f2016" name="g"></label>
            <label>Flash 2017 <input type="radio" data-id="f2017" name="g" checked></label>
        </p>
        <div><span class="btn action" data-id="start-flash">Convert</span></div>
    </div>`;

    inpIn = div.querySelector("#flash-in");
    inpIn.addEventListener("change", onDirSelectIn);
    div.querySelector("span[data-id='open-in']").addEventListener("click", onOpenIn);

    inpOut = div.querySelector("#flash-out");
    inpOut.addEventListener("change", onDirSelectOut);
    div.querySelector("span[data-id='open-out']").addEventListener("click", onOpenOut);

    div.querySelector("span[data-id='start-flash']").addEventListener("click", onStart);

};


function onOpenIn() {
    inpIn.click();
}

function onDirSelectIn(e) {
    srcPath = e.path[0].files[0].path + "/";
    div.querySelector("span[data-id='selected-in']").textContent = srcPath;
    inpIn.value = "";
}


function onOpenOut() {
    inpOut.click();
}

function onDirSelectOut(e) {
    destPath = e.path[0].files[0].path + "/";
    div.querySelector("span[data-id='selected-out']").textContent = destPath;
    inpOut.value = "";
}

function onStart() {

    // let tmp_parser = require("./../util/flash2017");
    // tmp_parser.parse("/Users/vthome/projects/tmp/flash/2017", "/Users/vthome/projects/tmp/flash_converted/2017");
    // return;

    if (!srcPath || !destPath) {
        logger.log("You need to select in/out folders first.", "error");
        return;
    }

    let parser;

   if (document.querySelector("input[data-id='f2016']").checked) {
       parser = require("./../util/flash2016");
   } else {
       parser = require("./../util/flash2017");
   }
    parser.parse(srcPath, destPath);

}

/*

 function parse() {


 let canvasStr = "canvas = document.createElement('canvas');\n" +
 "canvas.width = lib.properties.width;\n" +
 "canvas.height = lib.properties.height;\n" +
 "lib.canvas = canvas;\n";


 function parseHtml(file) {
 "use strict";
 let str = fs.readFileSync(file, "utf8");

 if (str.indexOf("spritesheet") > -1) {
 logger.log("ERROR. Disable 'Export all bitmaps as Spritesheets' in Flash IDE's Publish options. File: " + file, "error");
 return;
 }

 let begin = str.indexOf("function");
 let end = str.lastIndexOf("<\/script>") - begin;
 str = str.substr(begin, end);

 str = str.replace("canvas = document.getElementById(\"canvas\");", canvasStr);

 return "let canvas, stage, exportRoot;\n" + str + "\n";
 }

 function parseJS(file, folderName) {
 "use strict";
 let str = fs.readFileSync(file, "utf8");
 str = str.replace(/src:\"images\//g, "src:\"assets/animations/" + folderName + "/");
 let initStr;
 let endStr1;
 let endStr2;
 if (str.indexOf("lib, img, cjs, ss") > -1) {
 initStr = "(function (lib, img, cjs, ss) {";
 endStr1 = "let lib, images, createjs, ss;";
 endStr2 = "})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{});";
 } else {
 initStr = "(function (lib, img, cjs) {";
 endStr1 = "let lib, images, createjs";
 endStr2 = "})(lib = lib||{}, images = images||{}, createjs = createjs||{});";
 }
 str = str.replace(initStr, "define([], function(){\n" +
 "let cjs = createjs;\n" +
 "let lib = {}; let img = {}; let images = {};\n");
 str = str.replace(endStr1, "");
 str = str.replace(endStr2, "");

 str = str.replace("this.stop();", 'createjs.Ticker.removeEventListener("tick", stage);\nlib.stage = stage;\nthis.stop();\n');


 return str;
 }

 function combineFiles(htmlStr, jsStr, jsFile) {
 "use strict";
 jsStr += htmlStr + "for(let i= 0, k = lib.properties.manifest.length; i < k; i++){\n" +
 "let obj = lib.properties.manifest[i];\n" +
 "obj.src = 'res/' + obj.src;\n" +
 "img[obj.id] = obj.src;}\n" +
 "init();\n" +

 "lib.dispose = function(){\n" +
 "createjs.Ticker.removeEventListener(\"tick\", stage);\n" +
 "createjs.Tween.removeAllTweens();\n" +
 "let evt = document.createEvent(\"Event\");\nevt.initEvent(\"clearCreateJS\", true, false);\n" +
 "canvas.dispatchEvent(evt);\n" +
 "if (stage) stage.removeAllChildren();\n" +
 "stage = null;\n" +
 "exportRoot = null;\n" +
 "};\n" +

 "return lib;});";

 let dir = jsFile.split("/")[0];
 if (!fs.existsSync(destPath + dir)) {
 fs.mkdirSync(destPath + dir);
 writeFile(jsStr, destPath + jsFile);
 } else {
 writeFile(jsStr, destPath + jsFile);
 }

 }


 let okFiles = 0;

 function writeFile(jsStr, path) {
 "use strict";
 let wstream = fs.createWriteStream(path);
 wstream.write(jsStr);
 wstream.end();
 okFiles++;
 logger.log("OK " + path, "ok");
 }

 function start() {
 fs.readdir(srcPath, function (err, files) {
 "use strict";
 if (err) throw err;
 files.forEach(function (file) {
 fs.stat(srcPath + file, function (err, stats) {
 if (stats && stats.isDirectory()) {
 fs.readdir(srcPath + file, function (err, files) {
 if (err) throw err;
 let htmlStr, jsStr, jsFile;
 files.forEach(function (file2) {
 if (file2.indexOf(".html") != -1) {
 htmlStr = parseHtml(srcPath + file + "/" + file2);
 }
 else if (file2.indexOf(".js") != -1) {
 jsStr = parseJS(srcPath + file + "/" + file2, file);
 jsFile = file + "/" + file2;
 } else {
 let imageDir = srcPath + file + "/" + file2;
 fs.stat(imageDir, function (err, stats2) {
 if (err) throw  err;
 /// folder contains images, copy them to dest folder
 if (stats2.isDirectory()) {
 fs.readdir(imageDir, function (err, files) {
 if (err) throw err;
 files.forEach(function (file3) {
 fs.createReadStream(imageDir + "/" + file3).pipe(fs.createWriteStream(destPath + file + "/" + file3));
 })
 });
 }
 });
 }
 });
 if (htmlStr && jsStr && jsFile) {
 combineFiles(htmlStr, jsStr, jsFile);
 htmlStr = null;
 jsStr = null;
 jsFile = null;
 }
 });
 }
 });
 })

 });
 }

 start();
 }

 */







