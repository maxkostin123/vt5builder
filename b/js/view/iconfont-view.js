/**
 * Created by vthome on 14/02/2017.
 */


"use strict";

const fs = require("fs-extra");
const gulp = require('gulp');
const iconFont = require('gulp-iconfont');
const path = require('path');
const logger = require("./../util/logger");

let iconsPath;
let fontName = "CustomIcons";
let prefix;


let div,
    destPath,
    inPath;



exports.init = function () {

    div = document.getElementById("iconfont");

    div.innerHTML = `<div class="panel">
        <div>Select input/output folders and press "Build".</div>
        <p></p>
        <div><input type="text" data-id="fontname" placeholder="Font name (singe word)"> <input type="text" data-id="prefix" placeholder="prefix" style="width: 4em;"> access your icons using the prefix.</div><p></p>
        <input id="iconfont-in" type="file" nwdirectory/><span class="btn" data-id="open-in">Select icons folder</span>
        <span data-id="selected-in">Your .svg icons folder</span>
        <p></p>
        <input id="iconfont-out" type="file" nwdirectory/><span class="btn" data-id="open-out">Select destination (addon) folder</span>
        <span data-id="selected-out">e.g. bin/res/addon</span>
        <p></p>
        <div><span class="btn action" data-id="build">Build</span></div>
        <div>
        <p></p>
        Add the following entry in your data file:
        <code id="iconfont-instruction"></code>
        </div>
    </div>`;

    let inpIn = div.querySelector("#iconfont-in");
    let inpOut = div.querySelector("#iconfont-out");

    div.querySelector(".btn[data-id='open-in']").addEventListener("click", ()=> inpIn.click());
    div.querySelector(".btn[data-id='open-out']").addEventListener("click", ()=> inpOut.click());
    div.querySelector(".btn[data-id='build']").addEventListener("click", build);

    inpIn.addEventListener("change", onDirSelectIn);
    inpOut.addEventListener("change", onDirSelectOut);


};


function onDirSelectIn(e) {
    iconsPath = e.path[0].files[0].path + "/*.svg";
    inPath = e.path[0].files[0].path + "/";
    div.querySelector("span[data-id='selected-in']").textContent = iconsPath;
    e.target.value = "";
}



function onDirSelectOut(e) {
    destPath = e.path[0].files[0].path + "/";
    div.querySelector("span[data-id='selected-out']").textContent = destPath;
    e.target.value = "";
}


function build() {


    fontName = document.querySelector("input[data-id='fontname']").value;
    prefix = document.querySelector("input[data-id='prefix']").value;

    if (fontName.length < 2 || prefix.length < 2 || ! iconsPath || ! destPath) {
        logger.log("You have to select font name, prefix, icons folder and output folder.", "error");
        return;
    }

    rename();

    let runTimestamp = Math.round(Date.now() / 1000);
    gulp.src([iconsPath])
        .pipe(iconFont({
            fontName: fontName, // required
            prependUnicode: true, // recommended option
            formats: ['ttf', 'woff'], // default, 'woff2' and 'svg' are available
            timestamp: runTimestamp, // recommended to get consistent builds when watching files
            normalize: true,
            fontHeight: 1001,
            centerhorizontally: true
        }))
        .on('glyphs', function (glyphs, options) {
            onGlyphs(glyphs);
        })
        .pipe(gulp.dest(destPath + "fonts"));


    document.getElementById("iconfont-instruction").innerHTML = `addon: {css: "res/assets/addon/${fontName}.css"},`

}


function onGlyphs(glyphs) {

    let str = "";
    let html = "";
    for (let i = 0; i < glyphs.length; i++) {
        let g = glyphs[i];
        let char = "\\" + g.unicode[0].charCodeAt(0).toString(16).toUpperCase();
        let name = g.name;
        str += `.${name}:before {content: "${char}";}\n`;
    }

    let cssStr = getCSSString(fontName, "fonts/", str);

    fs.writeFileSync(destPath + `${fontName}.css`, cssStr, "utf8");

    logger.log("font built.", "ok");

}

function rename() {
    let files = fs.readdirSync(inPath);
    for (let i = 0; i < files.length; i++) {
        let fOld = files[i];
        let oldPath = inPath + fOld;
        let newName = prefix + fOld.substring(fOld.indexOf("_"));
        fs.renameSync(oldPath, inPath + newName);
    }
}




function getCSSString(fontName, fontPath, fonts) {
    return `@font-face {
  font-family: "${fontName}";
  src: url('${fontPath}${fontName}.woff') format('woff'),
  url('${fontPath}${fontName}.ttf') format('truetype');
}

[class*="${prefix}_"] {
	font-family: "${fontName}";
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
	font-style: normal;
	font-variant: normal;
	font-weight: normal;
	text-decoration: none;
	text-transform: none;
}

${fonts}

`;
}


function getHtml(str) {
    return `<html>
<head>
<link rel="stylesheet" href="styles/vt-icon-font.css">
<style>
[class*="vti_"] {
            font-size: 32px;
            display: inline-block;
            float: left;
        }
        .icon {
            line-height: 32px;
            vertical-align: middle;
            margin: 5px;
            border-bottom: 1px solid rgba(0,0,0, .2);
            height: 42px;
        }

        .description {
            margin-left: 30px;
        }
</style>
</head>
<body>
${str}
</body>
</html>`;
}

