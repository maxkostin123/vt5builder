/**
 * Created by maxk on 04/02/2016
 */

let html = "";
let document;
let cssDiv;
let projPath;
let isListenerSet = false;

const gulp = require("gulp");
const less = require("gulp-less");
const rename = require("gulp-rename");
const autoprefixer = require("gulp-autoprefixer");
const path = require("path");

let pickerColor, pickerHex, pickerRGB, colorSelector;


const logger = require("../../../b/js/util/logger");
const fs = require("fs");


exports.getHtml = function () {
    return html;
};

exports.init = function (doc) {
    document = doc;
    cssDiv = document.getElementById("css");
};


exports.setPath = function (value) {
    projPath = value;
    createView();
    setListeners();
};

exports.compile = function ($path) {

    return new Promise((resolve)=> {
        writeTheme("light");
        compileCSS("theme-light.css", $path)
            .then(()=> {
                writeTheme("dark");
                return compileCSS("theme-dark.css", $path);
            })
            .then(()=> {
                resolve();
            });
    });

};



let setListeners = function () {

    cssDiv.addEventListener("change", changeHandler);
    cssDiv.addEventListener("click", colorClickHandler);
    cssDiv.querySelector(".btn[data-id='save']").addEventListener("click", save);
    colorSelector = cssDiv.querySelector(".css-color-picker");
    colorSelector.addEventListener("change", colorPickerHandler);

    pickerColor = cssDiv.querySelector("input[data-type='css-picker']");
    pickerHex = cssDiv.querySelector("input[data-type='css-hex']");
    pickerRGB = cssDiv.querySelector("input[data-type='css-rgb']");

};

function createView() {
    let strLight, strDark;

    try {
        strLight = fs.readFileSync(projPath + "/res/css/_light.less").toString();
        strDark = fs.readFileSync(projPath + "/res/css/_dark.less").toString();
    } catch (er) {
        logger.log(er, "error");
    }

    strLight = strLight.replace(/\s/g, "");
    strDark = strDark.replace(/\s/g, "");

    let arrLight = strLight.split(";");
    let arrDark = strDark.split(";");


    let valuesLightStr = getValuesStr(arrLight, "LIGHT");
    let valuesDarkStr = getValuesStr(arrDark, "DARK");

    cssDiv.innerHTML = `<div class="nav-secondary">
                            <span class="btn" data-id="save">Save</span>
                            <span class="css-color-picker">Colour picker 
                                <input type="color" data-type="css-picker"> 
                                HEX:<input type="text" data-type="css-hex"> 
                                RGB:<input type="text" data-type="css-rgb">
                            </span>
                        </div>
                        <div class="content-inner">
                            ${valuesLightStr}
                            ${valuesDarkStr}
                        </div>
                        `;
}


function getValuesStr(arr, title) {

    let dark = title === "DARK" ? "class='css-dark'" : "class='css-light'";
    let str = `<div ${dark}><h4>${title}</h4>`;

    for (let i = 0; i < arr.length; i++) {

        let pair = arr[i].split(":");
        let fieldName = pair[0];
        let val = pair[1];
        let field;
        let disabled = "";
        if (!fieldName || !val) {
            continue;
        }

        let dataType = "";
        let preview = "";

        if (fieldName.indexOf("@color") > -1) {
            dataType = "data-type='color'";
            preview = `style="background: ${val};"`;
        } else {
            disabled = "disabled"
        }

        str += `<div class="css-field"><span>${fieldName}</span>
                    <span ${preview}>
                        <input class="css-inp" type="text" value="${val}" ${dataType} ${disabled}>
                    </span>
                </div>`;

    }

    return str + "</div>"
}


exports.updateOldValues = function($oldPath) {

    let oldVals = getValuesArrs("./tmp");
    //let newVals = getValuesArrs("./app/struct/app/trunk/bin/res/css");
    let newVals = getValuesArrs("./app/");

    overwirteNewVals(oldVals.light, newVals.light);
    overwirteNewVals(oldVals.dark, newVals.dark);

    let strDark = newVals.dark.join(";\n");
    let strLight = newVals.light.join(";\n");

    fs.writeFileSync($oldPath + "/res/css/_light.less", strLight);
    fs.writeFileSync($oldPath + "/res/css/_dark.less", strDark);

};



function getValuesArrs($path) {
    let strLight, strDark;

    try {
        strLight = fs.readFileSync($path + "/_light.less").toString();
        strDark = fs.readFileSync($path + "/_dark.less").toString();
    } catch (er) {
        logger.log(er, "error");
    }

    strLight = strLight.replace(/\s/g, "");
    strDark = strDark.replace(/\s/g, "");

    let arrLight = strLight.split(";");
    let arrDark = strDark.split(";");

    return {
        light: arrLight,
        dark: arrDark
    }

}

function overwirteNewVals(arrOld, arrNew) {


    for (let i = 0; i < arrNew.length; i++) {

        let fieldNew = arrNew[i].split(":")[0];

        for (let j = 0; j < arrOld.length; j++) {

            let fieldOld = arrOld[j].split(":")[0];
            if (fieldNew === fieldOld) {
                arrNew[i] = arrOld[j];
                //console.log(arrNew[i])
            }
        }
    }
}


function changeHandler(e) {
    let targ = e.target;
    let val = targ.value;

    let type = targ.getAttribute("data-type");

    switch (type) {

        case "color" :
            let parent = targ.parentNode;
            parent.style.background = val;
            break;

    }
}


function colorClickHandler(e) {

    let targ = e.target;
    let val = targ.value;

    let type = targ.getAttribute("data-type");

    switch (type) {

        case "color" :

            if (val.indexOf("#") === 0) {
                pickerHex.value = val.substr(1, val.length);
                pickerColor.value = val;
                pickerRGB.value = hexToRgb(val);

            }
            break;

    }


}


function colorPickerHandler(e) {

    let type = e.target.getAttribute("data-type");
    let value = e.target.value;

    switch (type) {

        case "css-hex" :
            let rgbs = hexToRgb(value);
            pickerRGB.value = rgbs.join(",");
            pickerColor.value = "#" + value;
            break;

        case "css-rgb" :
            let a = rgbStrToArray(value);
            let v = rgbToHex(a[0], a[1], a[2]);
            pickerHex.value = v;
            pickerColor.value = "#" + v;
            break;

        case "css-picker" :
            let rgbs2 = hexToRgb(value);
            pickerRGB.value = rgbs2.join(",");
            pickerHex.value = value;
            break;
    }
}


function save() {


    let strLight = getThemeStr("css-light");
    let strDark = getThemeStr("css-dark");

    fs.writeFileSync("./app/styles/_light.less", strLight);
    fs.writeFileSync("./app/styles/_dark.less", strDark);

    fs.writeFileSync(projPath + "/res/css/_light.less", strLight);
    fs.writeFileSync(projPath + "/res/css/_dark.less", strDark);


    writeTheme("light");
    compileCSS("theme-light.css", projPath)
        .then(()=> {
            writeTheme("dark");
            return compileCSS("theme-dark.css", projPath);
        })
        .then(()=> {
            logger.log("Themes created!", "ok");
        });

}


function getThemeStr(themeClass) {

    let divs = cssDiv.querySelectorAll(`.${themeClass} > .css-field`);

    let str = "";

    for (let i = 0; i < divs.length; i++) {
        let d = divs[i];
        let fieldName = d.children[0].textContent;
        let value = d.children[1].children[0].value;
        str += `${fieldName}:${value};\n`;
    }
    return str;
}


function compileCSS(theme, $path) {
    "use strict";

    return new Promise((resolve)=> {
        gulp.src("./app/styles/main.less")
            .pipe(less())
            .on("error", function (err) {
                logger.log(err.toString(), "error");
                this.emit("end");
            })
            .pipe(autoprefixer({
                browsers: ['last 2 versions'],
                cascade: false
            }))
            .pipe(rename(theme))
            .pipe(gulp.dest(path.normalize($path + '/res/css')))
            .on("end", ()=> {
                logger.log(`${theme} compiled`);
                resolve();
                /*if (theme === "theme-light.css") {
                    writeTheme("dark");
                    compileCSS("theme-dark.css", $path);
                } else {
                    logger.log("CSS theme created!", "ok");
                    resolve();
                }*/
            })
    });


}


function writeTheme(theme) {
    let str = theme === "dark" ? '@theme: "_dark";\n@import "@{theme}";' : '@theme: "_light";\n@import "@{theme}";';
    fs.writeFileSync("./app/styles/theme.less", str);
}


function rgbToHex(r, g, b) {
    return "" + componentToHex(r) + componentToHex(g) + componentToHex(b);
}

function componentToHex(c) {
    let hex = c.toString(16);
    return hex.length == 1 ? "0" + hex : hex;
}


function hexToRgb(hex) {
    let result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? [
        parseInt(result[1], 16),
        parseInt(result[2], 16),
        parseInt(result[3], 16)
    ] : null;
}

function rgbStrToArray(str) {
    let cols = str.match(/[0-9.]+/g);
    cols.forEach(function (v, n) {
        cols[n] = parseFloat(v)
    });
    return cols;
}








