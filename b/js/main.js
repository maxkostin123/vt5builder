/**
 * Created by maxk on 04/02/2016
 */

let document;
let nav;

let gui; // nw.gui
let projPath;

let sample_window;

const server = require("../../b/js/server/server");
const fs = require("fs-extra");
const gulp = require("gulp");
const path = require("path");

let divCSS,
    divContent,
    divBuild,
    divFlash,
    divIcons,
    divIconfont,
    divHotspots,
    buildView,
    inpOpen,
    inpNew,
    inpUpdate,
    logger,
    cssView,
    iconsView,
    iconfontView,
    hotspotsView,
    flashView;

exports.init = function(doc, $gui) {
    document = doc;
    gui = $gui;

    logger = require("../../b/js/util/logger");
    logger.init(doc.querySelector("footer"));

    divIcons = document.getElementById("icons");
    divIconfont = document.getElementById("iconfont");
    divCSS = document.getElementById("css");
    divContent = document.getElementById("content");
    divBuild = document.getElementById("build");
    divFlash = document.getElementById("flash");
    divHotspots = document.getElementById("hotspots");

    nav = document.getElementById("mainNav");
    nav.addEventListener("click", navHandler);

    inpNew = document.getElementById("inp-new");
    inpOpen = document.getElementById("inp-open");
    inpUpdate = document.getElementById("inp-update");

    inpNew.addEventListener("change", makeNewProjHandler);
    inpOpen.addEventListener("change", startServer);
    inpUpdate.addEventListener("change", updateCourse);

    cssView = require("../../b/js/view/css-view");
    buildView = require("../../b/js/view/build-view");
    flashView = require("../../b/js/view/flash-view");
    iconsView = require("../../b/js/view/icons-view");
    iconfontView = require("../../b/js/view/iconfont-view");
    hotspotsView = require("../../b/js/view/hotspots-view");

    cssView.init(document);
    buildView.init();
    flashView.init();
    iconsView.init();
    iconfontView.init();
    hotspotsView.init(gui);

    //divIcons.innerHTML = iconsView.getHtml();
    //divCSS.innerHTML = cssView.getHtml();

    nw.App.clearCache();
    checkUpdates();



};


function navHandler(e) {
    let id = e.target.getAttribute("data-id");
    switch (id) {

        case "new" :
            inpNew.value = null;
            inpNew.click();
            break;

        case "open" :
            inpOpen.value = null;
            inpOpen.click();
            break;

        case "update" :
            inpUpdate.value = null;
            inpUpdate.click();
            break;

        case "doc" :
            let doc = path.resolve("./doc/VT5_AUTHORS_GUIDE.docx");
            nw.Shell.openItem(doc);
            break;

        case "sample-project" :
            sample_window = gui.Window.open("./sample_app/index.html",
                {width: 1200, height: 750});
            break;

        case "mode-css" :
            hideAll();
            divCSS.classList.remove("hidden");
            break;

        case "mode-content" :
            hideAll();
            divContent.classList.remove("hidden");
            break;

        case "mode-build" :
            hideAll();
            divBuild.classList.remove("hidden");
            break;

        case "mode-flash" :
            hideAll();
            divFlash.classList.remove("hidden");
            break;

        case "mode-icons" :
            hideAll();
            divIcons.classList.remove("hidden");
            break;

        case "mode-iconfont" :
            hideAll();
            divIconfont.classList.remove("hidden");
            break;

        case "mode-hotspots" :
            hideAll();
            divHotspots.classList.remove("hidden");
            break;


    }
}

function hideAll() {
    divContent.classList.add("hidden");
    divCSS.classList.add("hidden");
    divBuild.classList.add("hidden");
    divFlash.classList.add("hidden");
    divIcons.classList.add("hidden");
    divIconfont.classList.add("hidden");
    divHotspots.classList.add("hidden");
}


function startServer(e) {
    projPath = e.path[0].files[0].path;
    doStartServer();
}

function makeNewProjHandler(e){
    projPath = e.path[0].files[0].path;

    let dir = fs.readdirSync(projPath);
    let n = 0;
    for (let i = 0; i < dir.length; i++) {
        let f = dir[i].toLowerCase();
        if(f.indexOf("ds_store") === -1) {
            n++;
            logger.log("Directory is not empty.", "error");
            break;
        }
    }

    if (n === 0) {

        gulp.src("app/struct/**/*")
            .pipe(gulp.dest(projPath))
            .on("end", ()=> {
                projPath = projPath + "/app/trunk/bin/";
                let indexTemplate = fs.readFileSync("./app/index_template.html").toString();
                indexTemplate = indexTemplate.replace("{{service}}", "vt5service");
                indexTemplate = indexTemplate.replace("{{lang}}", "en-gb");
                fs.writeFileSync(projPath + "index.html", indexTemplate, "utf8");
                fs.copySync("app/language_list.js", path.normalize(projPath + "/res/data/language_list.js"));

                fs.copySync("app/_light.less", path.normalize(projPath + "/res/css/_light.less"));
                fs.copySync("app/_dark.less", path.normalize(projPath + "/res/css/_dark.less"));

                logger.log("new project created");
                doStartServer();
            });
    }
}

function doStartServer() {
    server.start(projPath);
    updatePaths();
    logger.log("path set: " + projPath);
    logger.log('created server on http://127.0.0.1:' + server.getPort());
    logger.log('Listening on port '  + server.getPort());
    gui.Shell.openExternal("http://127.0.0.1:" + server.getPort());
}


function updatePaths() {
    cssView.setPath(projPath);
}



function checkUpdates() {
    let updater = require("./util/updater");
    updater.checkUpdates();

}

function updateCourse(e) {

    nw.App.clearCache();
    
    let f = e.path[0].files[0].path + "/";
    let data = path.normalize(f + "/res/data/");

    if( ! fs.existsSync(data)) {
        logger.log("INVALID PROJECT FOLDER.", "error");
    } else {

        let assets = "app/struct/app/trunk/bin/res/";

        fs.copySync(path.normalize(f + "/res/css/_light.less"), "./tmp/_light.less");
        fs.copySync(path.normalize(f + "/res/css/_dark.less"), "./tmp/_dark.less");

        fs.copySync("sample_app/index.html", path.normalize(f + "index.html"));

        fs.copySync("sample_app/res/assets/media/logos/videotel.svg", path.normalize(f + "res/assets/media/logos/videotel.svg"));
        fs.copySync("sample_app/res/assets/media/logos/videotel_grey.svg", path.normalize(f + "res/assets/media/logos/videotel_grey.svg"));
        fs.copySync("sample_app/res/assets/default/", path.normalize(f + "res/assets/default"));


        if (! fs.existsSync(path.normalize(f + "/res/data/language_list.js"))){
            fs.copySync("app/language_list.js", path.normalize(f + "/res/data/language_list.js"));
        }

        fs.removeSync(path.normalize(f + "/res/assets/fonts"));
        fs.removeSync(path.normalize(f + "/res/js"));
        fs.removeSync(path.normalize(f + "/res/css"));

       fs.copy(assets + "css", f + "res/css", ()=> {
           fs.copy(assets + "js", f + "res/js", ()=> {
               cssView.updateOldValues(f);
               fs.removeSync("./tmp/_light.less");
               fs.removeSync("./tmp/_dark.less");

               cssView.compile(f)
                   .then(()=> {
                       logger.log(`Project @ ${f} updated.`, "ok");
                   });

           });
       });

    }
}









