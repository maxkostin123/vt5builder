const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const logger = require("../util/logger");
const fs = require("fs");


var port = 8000;
var app;
var server;
var db;


exports.start = function (dir) {

    if (server) {
        server.close();
    }

    loadUserData();

    app = express();
    app.use(express.static(dir)); // Current directory is root
    app.use(bodyParser.json());
    server = app.listen(++port);
    //console.log(users)
    app.post('/api', function (req, res) {
        'use strict';
        var reqType = req.body.request;
        logger.log("request from client received: " + reqType, "ok" );
        var reply;
        var user;
        switch (reqType) {

            case "isTokenValid" :
                var token = db.tokens[req.body.token];
                if (token) {
                    user = db.users[token.id];
                    reply = {status: 1, user: user};
                } else {
                    reply = {status: 0};
                }
                break;

            case "login" :
                user = db.users[req.body.trainingId];
                if (user && user.password === req.body.password) {
                    switch (user.status) {
                        case "active" :
                            reply = {status:1, token: "3247987238945", user:user};
                            break;
                        case "expired" :
                            reply = {status:0, error:"expired", user:user};
                            break;
                    }

                } else {
                    reply = {status:0, error:"incorrect_login"}
                }
                break;

        }

        if (reply) {
            res.setHeader('Content-Type', 'application/json');
            res.json(reply);
        }

    });

};

exports.getPort = function () {
    return port;
};



function loadUserData(){
    fs.readFileSync(path.resolve(__dirname, 'users.json'), 'UTF-8', function (er, f) {
        if (er) {
            console.log(er)
        } else {
            db = JSON.parse(f);
        }
    });

}

var responseStruct = {
    status: 1, //int 1 or 0
    error: "expired", //string
    token: "",
    user: {} // user object

};

