/**
 * Created by vthome on 19/12/2016.
 */
"use strict";

const fs = require("fs-extra");
const logger = require("./../util/logger");

var srcPath, destPath;

exports.parse = function (src, dest) {

    srcPath = src;
    destPath = dest;


    var canvasStr = "canvas = document.createElement('canvas');\n" +
        "canvas.width = lib.properties.width;\n" +
        "canvas.height = lib.properties.height;\n" +
        "lib.canvas = canvas;\n";


    function parseHtml(file) {
        "use strict";
        var str = fs.readFileSync(file, "utf8");

        if (str.indexOf("spritesheet") > -1) {
            logger.log("ERROR. Disable 'Export all bitmaps as Spritesheets' in Flash IDE's Publish options. File: " + file, "error");
            return;
        }

        var begin = str.indexOf("function");
        var end = str.lastIndexOf("<\/script>") - begin;
        str = str.substr(begin, end);

        str = str.replace("canvas = document.getElementById(\"canvas\");", canvasStr);

        return "var canvas, stage, exportRoot;\n" + str + "\n";
    }

    function parseJS(file, folderName) {
        "use strict";
        var str = fs.readFileSync(file, "utf8");
        str = str.replace(/src:\"images\//g, "src:\"assets/animations/" + folderName + "/");
        var initStr;
        var endStr1;
        var endStr2;
        if (str.indexOf("lib, img, cjs, ss") > -1) {
            initStr = "(function (lib, img, cjs, ss) {";
            endStr1 = "var lib, images, createjs, ss;";
            endStr2 = "})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{});";
        } else {
            initStr = "(function (lib, img, cjs) {";
            endStr1 = "var lib, images, createjs";
            endStr2 = "})(lib = lib||{}, images = images||{}, createjs = createjs||{});";
        }
        str = str.replace(initStr, "define([], function(){\n" +
            "var cjs = createjs;\n" +
            "var lib = {}; var img = {}; var images = {};\n");
        str = str.replace(endStr1, "");
        str = str.replace(endStr2, "");

        str = str.replace("this.stop();", 'createjs.Ticker.removeEventListener("tick", stage);\nlib.stage = stage;\nthis.stop();\n');


        return str;
    }

    function combineFiles(htmlStr, jsStr, jsFile) {
        "use strict";
        jsStr += htmlStr + "for(var i= 0, k = lib.properties.manifest.length; i < k; i++){\n" +
            "var obj = lib.properties.manifest[i];\n" +
            "obj.src = 'res/' + obj.src;\n" +
            "img[obj.id] = obj.src;}\n" +
            "init();\n" +

            "lib.dispose = function(){\n" +
            "createjs.Ticker.removeEventListener(\"tick\", stage);\n" +
            "createjs.Tween.removeAllTweens();\n" +
            "var evt = document.createEvent(\"Event\");\nevt.initEvent(\"clearCreateJS\", true, false);\n" +
            "canvas.dispatchEvent(evt);\n" +
            "if (stage) stage.removeAllChildren();\n" +
            "stage = null;\n" +
            "exportRoot = null;\n" +
            "};\n" +

            "return lib;});";

        var dir = jsFile.split("/")[0];
        if (!fs.existsSync(destPath + dir)) {
            fs.mkdirSync(destPath + dir);
            writeFile(jsStr, destPath + jsFile);
        } else {
            writeFile(jsStr, destPath + jsFile);
        }

    }


    var okFiles = 0;

    function writeFile(jsStr, path) {
        "use strict";
        var wstream = fs.createWriteStream(path);
        wstream.write(jsStr);
        wstream.end();
        okFiles++;
        logger.log("OK " + path, "ok");
    }

    function start() {
        fs.readdir(srcPath, function (err, files) {
            if (err) throw err;
            files.forEach(function (file) {
                fs.stat(srcPath + file, function (err, stats) {
                    if (stats && stats.isDirectory()) {
                        fs.readdir(srcPath + file, function (err, files) {
                            if (err) {
                                throw err;
                            }
                            var htmlStr, jsStr, jsFile;
                            files.forEach(function (file2) {
                                if (file2.indexOf(".html") != -1) {
                                    htmlStr = parseHtml(srcPath + file + "/" + file2);
                                }
                                else if (file2.indexOf(".js") != -1) {
                                    jsStr = parseJS(srcPath + file + "/" + file2, file);
                                    jsFile = file + "/" + file2;
                                } else {
                                    var imageDir = srcPath + file + "/" + file2;
                                    fs.stat(imageDir, function (err, stats2) {
                                        if (err) throw  err;
/// folder contains images, copy them to dest folder
                                        if (stats2.isDirectory()) {
                                            fs.readdir(imageDir, function (err, files) {
                                                if (err) throw err;
                                                files.forEach(function (file3) {
                                                    //fs.createReadStream(imageDir + "/" + file3).pipe(fs.createWriteStream(destPath + file + "/" + file3));
                                                    fs.copy(imageDir + "/" + file3, destPath + file + "/" + file3);
                                                })
                                            });
                                        }
                                    });
                                }
                            });

                            if (htmlStr && jsStr && jsFile) {
                                combineFiles(htmlStr, jsStr, jsFile);
                                htmlStr = null;
                                jsStr = null;
                                jsFile = null;
                            }
                        });
                    }
                });
            })

        });
    }

    start();
};