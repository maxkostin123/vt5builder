/**
 * Created by Max Kostin on 28/04/2016
 */

"use strict";
const gulp = require("gulp");
const uglify = require("gulp-uglify");
const logger = require("./logger");
const fs = require("fs-extra");
const walker = require("klaw");
const path = require("path");
const spawn = require("child_process").spawn;
const ffm = require("fluent-ffmpeg");

let inDir = "";
let outDir = "";

let arrPng = [];
let arrJpg = [];
let arrVid = [];
let arrMp3 = [];
let os = require("os");
let platform;

let pngQuantExe;
let jpgTranExe;


exports.compress = function ($in, $out) {

    return new Promise((resolve) => {

        getMediaFiles($in, $out)
            .then(()=> {
                compressPngs()
                    .then(()=> {
                        return compressJepgs();
                    })
                    .then(()=> {
                        return compressMP3s(arrMp3);
                    })
                    .then(()=> {
                        return compressMP4s(arrVid);
                    })
                    .then(()=> {
                        return minifyJS();
                    })
                    .then(()=> {
                        resolve();
                    })
            })
    });
};


function getMediaFiles($in, $out) {

    arrJpg = [];
    arrPng = [];
    arrVid = [];
    arrMp3 = [];
    inDir = $in;
    outDir = path.normalize($out);
    platform = os.platform(); /// osx='darwin', win64='win64'

    return new Promise((resolve) => {
        walker($in)
            .on("data", (f)=> {

                var p = f.path;
                var ext = path.extname(p).toLowerCase();

                var relOut = p.substr($in.length, p.length);
                var fileOut = path.normalize($out + "/" + relOut);
                var obj = {
                    in: p,
                    out: fileOut,
                    name: path.basename(p),
                    ext: path.extname(p)
                };

                switch (ext) {

                    case ".jpg":
                    case ".jpeg":
                        arrJpg.push(obj);
                        break;

                    case ".png":
                        arrPng.push(obj);
                        break;

                    case ".mp4":
                        arrVid.push(obj);
                        break;

                    case ".mp3":
                        arrMp3.push(obj);
                        break;

                }

            })
            .on("end", ()=> {
                initCompressors();
                resolve();
            });
    });
}


function initCompressors() {

    if (platform === "darwin") {

        ffm.setFfmpegPath("./platform/osx/ffmpeg");
        pngQuantExe = "platform/osx/pngquant";
        jpgTranExe = "platform/osx/jpegtran";

    } else {

        ffm.setFfmpegPath("./platform/win/ffmpeg.exe");
        pngQuantExe = "platform/win/pngquant.exe";
        jpgTranExe = "platform/win/jpegtran.exe";
    }


}


function compressPngs() {

    return new Promise((resolve) => {

        function doCompress() {
            if (arrPng.length > 0) {
                var obj = arrPng.pop();

                var process = spawn(pngQuantExe, [obj.out, "--quality=65-80", "--force", `--ext=${obj.ext}`]);

                process.stderr.on("data", (data)=> {
                    logger.log(data.toString(), "error");
                });

                process.on("exit", ()=> {
                    logger.log("optimising " + obj.in);
                    doCompress();
                });
            } else {
                resolve();
            }
        }

        doCompress();
    });
}


function compressJepgs() {

    return new Promise((resolve) => {

        function doCompress() {
            if (arrJpg.length > 0) {
                var obj = arrJpg.pop();
                var process = spawn(jpgTranExe, ["-progressive", "-outfile", obj.out, obj.out]);

                process.stderr.on("data", (data)=> {
                    logger.log(data.toString(), "error");
                });

                process.on("exit", ()=> {
                    logger.log("optimising " + obj.in);
                    doCompress();
                });
            } else {
                resolve();
            }
        }

        doCompress();
    });
}


function compressMP4s(arr) {

    var tmpArr = arr.concat();

    return new Promise((resolve) => {

        function doCompress() {

            if (tmpArr.length > 0) {
                var f = tmpArr.pop();
                logger.log("optimising " + f.in);
                ffm({source: f.in})
                //.format("webm")
                    .videoCodec('libx264')
                    .videoBitrate('1000k')
                    .audioQuality(5)
                    .audioCodec("libmp3lame")
                    .saveToFile(f.out)
                    .on("error", (er)=> {
                        logger.log(er.toString(), "error");
                    })
                    .on("end", ()=> {
                        doCompress();
                    });
                    // .on("progress", (progress)=> {
                    //     for (var p in progress) {
                    //         logger.log(p + " : " + progress[p])
                    //     }
                    // });
            } else {
                resolve();
            }
        }

        doCompress();
    });

}

function compressMP3s(arr) {

    var tmpArr = arr.concat();

    return new Promise((resolve) => {

        function doCompress() {

            if (tmpArr.length > 0) {
                var f = tmpArr.pop();
                logger.log("optimising " + f.in);
                ffm({source: f.in})
                    .format("mp3")
                    .audioQuality(5)
                    .audioCodec("libmp3lame")
                    .saveToFile(f.out)
                    .on("error", (er)=> {
                        logger.log(er.toString(), "error");
                    })
                    .on("end", ()=> {
                        doCompress();
                    });
            } else {
                resolve();
            }
        }

        doCompress();
    });

}


function minifyJS() {


    return new Promise((resolve) => {

        var files = [
            inDir + "/res/**/**.js",
            "!" + inDir + "/res/js/libs/3d/**/*",
            "!" + inDir + "/res/js/libs/pdf/**/*",
            "!" + inDir + "/res/js/libs/createjs.*"
        ];

        logger.log("Minifying JS source files......");

        gulp.src(files)
            .pipe(uglify())
            .pipe(gulp.dest(path.normalize(outDir + "/res/")))
            .on("end", ()=> {
                resolve();
            });

    });


}


exports.convertMP4s = function () {

    var tmpArr = arrVid.concat();

    return new Promise((resolve) => {

        function doCompress() {

            if (tmpArr.length > 0) {
                var f = tmpArr.pop();
                var fout = f.out.replace(/.mp4/i, ".webm");
                logger.log("converting " + f.in);
                ffm({source: f.in})
                    .format("webm")
                    .videoCodec('libvpx')
                    .audioCodec("vorbis")
                    .videoBitrate('1000k')
                    .saveToFile(fout)
                    .on("error", (er)=> {
                        logger.log(er.toString(), "error");
                    })
                    .on("end", ()=> {
                        doCompress();
                    });
            } else {
                resolve();
            }
        }

        doCompress();
    });
};

exports.convertMP3s = function () {

    var tmpArr = arrMp3.concat();

    return new Promise((resolve) => {

        function doCompress() {

            if (tmpArr.length > 0) {
                var f = tmpArr.pop();
                var fout = f.out.replace(/.mp3/i, ".ogg");
                logger.log("converting " + f.in);
                ffm({source: f.in})
                    .format("ogg")
                    //.audioCodec("vorbis")
                    .saveToFile(fout)
                    .on("error", (er)=> {
                        logger.log(er.toString(), "error");
                    })
                    .on("end", ()=> {
                        doCompress();
                    });
            } else {
                resolve();
            }
        }

        doCompress();
    });
};

exports.getMediaArray = function () {
    return arrMp3.concat(arrVid);
};




exports.getMediaFiles = getMediaFiles;



