/**
 * Created by maxk on 04/02/2016
 */

var footer;
var arr = [];

exports.init = function($footer) {
    footer = $footer.querySelector("div");
};

exports.log = function(str, color) {
    if (arr.length > 1000) {
        arr.splice(0, 500);
    }
    if (footer) {
        color = color || "info";
        arr.push(`<br><span class='${color}'>* ${str}</span>`);
        footer.innerHTML = arr.join("");
        var lc = footer.lastChild;
        if (lc) {
            lc.scrollIntoView(false);
        }

    }
};

