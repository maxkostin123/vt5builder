/**
 * Created by Max Kostin on 10/05/2016
 */
"use strict";


const fs = require("fs-extra");
const walker = require("klaw");
const path = require('path');
const rjs = require("requirejs");
const gulp = require("gulp");
const zip = require("gulp-zip");
const util = require('util');

const logger = require("./../util/logger");


let scormId,
    passScore,
    scormTitle,
    zipName,
    zipDir,
    outDir,
    projPath;




exports.build = function ($outDir, $projPath) {

    outDir = $outDir;
    projPath = $projPath;

    var data = rjs(projPath + "/res/data/data_en-gb.js");

    if (data.scorm) {
        scormId = data.scorm.id;
        passScore = data.scorm.passScore;
        scormTitle = data.scorm.title;
    } else { // In case there's no SCORM entry in the data file.
        scormId = data.product.title.replace(/\W+/g, "");
        passScore = 0;
        scormTitle = data.product.title;
    }


    /*
     type: "Advanced Gas Tanker Training Course",
     courseCode: "1268",
     title: "Advanced Gas Tanker Training Course",
     appPath: "0000/",
     dm: 101
     */

    zipName = scormId + "_" + getTimeStr();

    zipDir = outDir + zipName;
    if (!fs.existsSync(zipDir)) {
        fs.mkdirSync(zipDir);
    }
    logger.log("indexing files....");


    return new Promise((resolve) => {


        createManifest(outDir + "vt5")
            .then(()=> {
                return onScormFilesCopied();
            })
            .then(()=> {
                return zipFiles();
            })
            .then(()=> {
                resolve();
            })




    });




};





/*==============================================================
 CREATE MANIFEST
 ===============================================================*/
function createManifest(dir) {
    "use strict";
    return new Promise((resolve) => {

        var str = "";
        walker(dir)
            .on("data", file => {
                if(! file.stats.isDirectory()){
                    var p = file.path;
                    var fstr = p.substr((outDir+"vt5").length+1, p.length);
                    fstr = fstr.split(path.sep).join("/");
                    str += `<file href="${fstr}"/>\n`;
                }
            })
            .on("end", ()=> {
                var man = getManifest(scormId, scormTitle, str);
                fs.writeFileSync(zipDir + "/imsmanifest.xml", man, "utf8");
                gulp.src("./build/scorm/xsd/**/*")
                    .pipe(gulp.dest(zipDir))
                    .on("end", ()=> {
                        resolve();
                    });
            });


    });


}



function onScormFilesCopied() {

    logger.log("Copying SCORM files......");

    return new Promise((resolve) => {

        gulp.src(outDir + "vt5/**/*")
            .pipe(gulp.dest(zipDir))
            .on("end", ()=> {

                var fStr = fs.readFileSync(projPath + "/index.html").toString();
                fStr = fStr.replace("vt5service", "scorm1.2");
                fs.writeFileSync(zipDir + "/index.html", fStr, "utf8");
                resolve();
            });
    });

}



/* ==================================================================================
 ZIP
 ====================================================================================*/
function zipFiles() {

    logger.log("Creating archive......");

    return new Promise((resolve) => {

        gulp.src(zipDir + "/**/*")
            .pipe(zip(zipName + ".zip"))
            .pipe(gulp.dest(outDir + "scorm"))
            .on("end", () => {
                fs.removeSync(zipDir);
                resolve();
            });


    });


}


function getTimeStr() {
    var d = new Date();
    var month = d.getMonth() + 1 < 10 ? "0" + (d.getMonth() + 1) : d.getMonth() + 1;
    var day = d.getDate() < 10 ? "0" + d.getDate() : d.getDate();
    var hours = d.getHours() < 10 ? "0" + d.getHours() : d.getHours();
    var minutes = d.getMinutes() < 10 ? "0" + d.getMinutes() : d.getMinutes();
    var secs = d.getSeconds() < 10 ? "0" + d.getSeconds() : d.getSeconds();
    return `${d.getFullYear()}_${month}_${day}_${hours}${minutes}${secs}`;
}



function getManifest(id, title, resource) {
    return `<?xml version="1.0" standalone="no" ?>

<manifest identifier="com.scorm.golfsamples.runtime.basicruntime.12" version="1"
         xmlns="http://www.imsproject.org/xsd/imscp_rootv1p1p2"
         xmlns:adlcp="http://www.adlnet.org/xsd/adlcp_rootv1p2"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://www.imsproject.org/xsd/imscp_rootv1p1p2 imscp_rootv1p1p2.xsd
                             http://www.imsglobal.org/xsd/imsmd_rootv1p2p1 imsmd_rootv1p2p1.xsd
                             http://www.adlnet.org/xsd/adlcp_rootv1p2 adlcp_rootv1p2.xsd">

  <metadata>
    <schema>ADL SCORM</schema>
    <schemaversion>1.2</schemaversion>
  </metadata>
	<organizations default="${id}">
		<organization identifier="${id}">
			<title>${title}</title>
			<item identifier="item_1" identifierref="resource_1">
				<title>${title}</title>
			</item>
		</organization>
	</organizations>
	<resources>
		<resource identifier="resource_1" type="webcontent" adlcp:scormtype="sco" href="index.html">
            ${resource}
		</resource>
	</resources>
</manifest>`;
}




