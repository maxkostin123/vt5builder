/**
 * Created by Max Kostin on 25/04/2016
 */
"use strict";

const fs = require("fs");
const logger = require("./logger");
const extract = require("extract-zip");
const popup = require("../view/popup-view");
const req = require("request");

let currentVersion, newVersion, remoteJson, callbackFunc;
let que = [];
let newJsonStr;


exports.checkUpdates = function (callback) {


    callbackFunc = callback;
    let myJSON = JSON.parse(fs.readFileSync("./package.json").toString());

    currentVersion = myJSON.version;

    download(myJSON.updates.json, JSONloaded);

};


function JSONloaded(file) {

    newJsonStr = file.toString("utf-8");
    remoteJson = JSON.parse(newJsonStr);
    newVersion = remoteJson.version;

    if (currentVersion !== newVersion) {
        let arrOld = currentVersion.split(".");
        let arrNew = newVersion.split(".");
        /// If the 1st number of the new JSON version is greater, make user download the new package (.nw)
        if (parseInt(arrNew[0]) > parseInt(arrOld[0])) {
            logger.log(`NEW VERSION ${arrNew[0]} IS AVAILABLE.`, "error");
            popup.show(`<div><h3>${remoteJson.updates.message}.</h3></div>`);

        } else if (parseInt(arrNew[0]) < parseInt(arrOld[0])) {
            logger.log("--");

        }
        /// If the last digit is greater, read the list of new files and download them.
        else if (parseInt(arrNew[2]) > parseInt(arrOld[2])) {
            popup.show(`<div><h3>${remoteJson.updates.message}.</h3><p>Downloading update...</p></div>`);
            que = remoteJson.updates.files.concat();
            for (let i = 0; i < que.length; i++) {
                que[i].src = remoteJson.updates.path + que[i].src;
            }
            loadQue();
        }
    } else {
        logger.log(`vt-5 builder V ${currentVersion} is up to date.`);
    }

}


function download(src, callback, obj) {

    let encoding = src.indexOf(".zip") > 0 ? null : "utf8";

    req({
        url:src,
        encoding: encoding
    }, (err, resp, file)=> {
        if (! err && resp.statusCode == 200) {
            //console.log(resp)
            callback(file, obj);
        } else if (err) {
            logger.log(err, "error");
        } else {
            logger.log(`Updater: ${resp.statusCode} ${resp.statusMessage}`, "error");
        }
        //console.log(resp)
    });
}



function loadQue() {

    if (que.length > 0) {
        let obj = que.shift();
        popup.showUpdate("loading file " + obj.dest);
        download(obj.src, fileFetched, obj);
    } else {
        fs.writeFileSync("./package.json", newJsonStr, "utf8");
        popup.showUpdate("vt-5 builder is now up to date.<p class='important'>Please restart the app.</p>");
    }
}



function fileFetched(file, obj) {

    fs.writeFileSync(obj.dest, file);


    if (obj.src.indexOf(".zip") > -1) {

        let dest = obj.dest.substr(0, obj.dest.lastIndexOf("/"));
        extract(obj.dest, {dir:dest}, function (err) {
            if (err) {
                console.log(err);
            } else {
                fs.unlinkSync(obj.dest);
                loadQue();
            }

        })
    } else {
        loadQue();
    }
}








