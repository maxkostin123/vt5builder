/**
 * Created by Max Kostin on 19/12/2016.
 */
"use strict";

const fs = require("fs-extra");
const logger = require("./../util/logger");
const path = require("path");

let srcPath, destPath;

exports.parse = function (src, dest) {

    srcPath = path.resolve(src);
    destPath = path.resolve(dest);

    let folders = fs.readdirSync(srcPath);
    for (let i = 0; i < folders.length; i++) {
        let f = srcPath + "/" + folders[i];
        if(fs.lstatSync(f).isDirectory()) {
            convert(f, folders[i]);
        }
    }


};


function convert(folder, dir) {

    let files = fs.readdirSync(folder);
    for (let i = 0; i < files.length; i++) {

        let f = folder + "/" + files[i];

        //console.log(files[i])

        /// Copy assets --------------------------
        if(fs.lstatSync(f).isDirectory()) {
            let ss = getAtlas(f);
            if (ss) {
                //fs.ensureFileSync(fname);
                fs.copySync(f + "/" + ss , destPath + "/" + dir + "/" + ss);
            } else {
                fs.copySync(f, destPath + "/" + dir);
            }

        } else if (files[i].indexOf(".html") > -1) {

            let str = parseHtml(f, dir);
            let fname = destPath + "/" + dir + "/" + files[i].replace(".html", ".js");
            fs.ensureFileSync(fname);
            fs.writeFileSync(fname, str, "utf8");
            logger.log(`${f} OK`, "ok");

        } else if (files[i].indexOf(".js") > -1) {
            logger.log(`${f} found in the path. Go to Adobe Animate publish settings and make sure option "Include JavaScript in HTML is selected."`, "error");
        }

    }
}


function getAtlas(dir) {
    let files = fs.readdirSync(dir);
    for (let i = 0; i < files.length; i++) {
        if (files[i].indexOf("_atlas_") > 0) {
            return files[i];
        }
    }
    return null;
}


function parseHtml(f, dir) {

    let str = fs.readFileSync(f, "utf8");

    let meta = "";
    let startMeta = str.indexOf("lib.ssMetadata");
    if(startMeta > -1) {
       let endMeta = str.indexOf("lib.updateListCache = function");
        meta = str.substring(startMeta, endMeta);
    }



    // content
    let start = str.indexOf("// symbols:");
    let end = str.indexOf("// library properties:");
    let content = str.substring(start, end);


    //library properties
    let startLib = str.indexOf("// library properties:");
    let endLib = str.indexOf("})(lib = lib||{},");
    let lib = str.substring(startLib, endLib);
    lib = lib.replace(/images\//gim, `assets/animations/${dir}/`);

    let js = header + meta + content + lib + getFooter(dir);

    //console.log(js)
    return js;

}


const header = `define([], function () {
    var cjs = createjs;
    var lib = {};
    var img = {};
    var images = {};
    var p;
`;


function getFooter(libName) {

    return `
    var canvas, stage, exportRoot, ss;

    function init() {
        canvas = document.createElement('canvas');
        canvas.width = lib.properties.width;
        canvas.height = lib.properties.height;
        lib.canvas = canvas;

        images = images || {};
        ss = ss||{};

         if (lib.properties.manifest.length > 0) {
            var loader = new createjs.LoadQueue(false);
            loader.addEventListener("fileload", handleFileLoad);
            loader.addEventListener("complete", handleComplete);
            loader.loadManifest(lib.properties.manifest);
        } else {
        	handleComplete();
		}
    }

    function handleFileLoad(evt) {
        if (evt.item.type == "image") {
            images[evt.item.id] = evt.result;
        }
    }

    function handleComplete(evt) {
    
        if (evt) {
            var queue = evt.target;
            var ssMetadata = lib.ssMetadata;
            for (i = 0; i < ssMetadata.length; i++) {
                ss[ssMetadata[i].name] = new createjs.SpriteSheet({
                    "images": [queue.getResult(ssMetadata[i].name)],
                    "frames": ssMetadata[i].frames
                })
            }
        }
    
        var exportRoot = lib.hasOwnProperty("_${libName}") ? new lib._${libName}() : new lib.${libName}();
        
       

        stage = new createjs.Stage(canvas);
        stage.addChild(exportRoot);
        stage.update();
        lib.stage = stage;

        createjs.Ticker.setFPS(lib.properties.fps);
        createjs.Ticker.addEventListener("tick", stage);
    }

    for (var i = 0, k = lib.properties.manifest.length; i < k; i++) {
        var obj = lib.properties.manifest[i];
        obj.src = 'res/' + obj.src;
        img[obj.id] = obj.src;
    }
    
    init();
    lib.dispose = function () {
        createjs.Ticker.removeEventListener("tick", stage);
        createjs.Tween.removeAllTweens();
        var evt = document.createEvent("Event");
        evt.initEvent("clearCreateJS", true, false);
        canvas.dispatchEvent(evt);
        if (stage) stage.removeAllChildren();
        stage = null;
        exportRoot = null;
    };
    return lib;
});

`;
}













