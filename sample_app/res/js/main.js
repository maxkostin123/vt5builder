(function(){
	"use strict";

	requirejs.config({
		shim: {
			easeljs: {
				exports: 'createjs'
			},
			tweenjs: {
				deps: ['easeljs'],
				exports: 'Tween'
			},
			movieclip: {
				deps: ['easeljs', 'tweenjs']
			},
			three: {
                exports: "THREE"
            },
            OrbitControls: {
                deps: ["three"]
            },
			babylon: {
				exports: "BABYLON"
			},
			pep: {
				deps: ["babylon"]
			}


		},
		paths: {
			createjs: 'libs/createjs-2015.11.26.min',
            "pdfjs-dist": "libs/pdf/pdfjs-dist/",
            three: "libs/3d/threejs/three.min",
            OrbitControls: "libs/3d/threejs/OrbitControls",
            babylon: "libs/3d/babylon/babylon",
            pep: "libs/3d/babylon/pep"
		}
	});

}());
