/**
 * Created by Max Kostin on 17/02/2016
 *
 * This is a wrapper class for communicating with servers/services. You can add any variables or methods,
 * but the ones defined below are required: if any of these are missing, the application will throw an Error.
 *
 * If any of these methods are not needed, just leave their body empty.
 *
 * In the most simple scenario, you will (1) add a variable with the server address and use this address later to
 * send or receive data. This should probably be done inside the init() method.
 *
 * (2) Obtain user name from the server and change value of the variable userName (also inside init method).
 *
 * (3) Obtain sessionId if needed.
 *
 * (4) Test results is a an Object with many properties (example is at the end of this file). You can grab any value needed,
 * construct your own object/json/xml and send it on to your endpoint.
 */
"use strict";

define({

    minPassScore: 0,
    userName: "VT5",
    server: "",
    tid: "",
    courseId: "",
    loginSuccessCallback: undefined,
    loginFailCallback: undefined,
    serviceReadyCallback: undefined,

    /**
     * Called when App is initialized
     */
    init: function () {
        console.log("[Service] init");
        var href = window.location.href;
        if (href.indexOf("://www.") > -1) {
            this.server = "http://www.videoteltraining.com/courses/webservice/courses.cfc?WSDL";
        } else {
            this.server = "http://videoteltraining.com/courses/webservice/courses.cfc?WSDL";
        }

        this.sendVar = this.sendVar.bind(this);
        this.sendTestResults = this.sendTestResults.bind(this);
        this.loginResponseHandler = this.loginResponseHandler.bind(this);
        this.userActiveResponseHandler = this.userActiveResponseHandler.bind(this);
    },

    /**
     * Sets minimum pass score
     */
    setMinPassScore: function (value) {
        this.minPassScore = value;
    },

    /**
     * Sends test result to the service.
     * @param result
     */
    sendTestResults: function (result) {
        var str = this.server + "&method=insertDetailedResult&tid=" + this.tid + "&courseid=" + this.courseId + "&themodule=" + result.testId + "&timetaken=" + result.durationString + "&score=" + result.scorePercent;
        this.sendVar(str, null);
        console.log(result);
    },


    sendVar: function (str, callback) {

        var xhr = new XMLHttpRequest();
        xhr.open("GET", str, true);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.onload = function () {
            if (callback) {
                callback(this.responseText);
            }
        };

        xhr.send();
    },

    /**
     * Called when Window is closed
     */
    onExit: function () {

    },


    /**
     * Tells VT5 app if the user needs to log in.
     * @returns {boolean}
     */
    requiresAuth: function () {
        return true;
    },

    /**
     * Returns an Array of label/type Objects to be used as fields in the Login Modal.
     * This method is required if requireAuth() is set to return boolean 'true'.
     * @returns {{title: string, fields: *[]}}
     */
    getAuthFields: function () {
        return {
            type: "login",
            title: "Login",
            fields: [
                {
                    label: "First Name",
                    type: "text",
                    id: "firstname"
                },
                {
                    label: "Last Name",
                    type: "text",
                    id: "lastname"
                },
                {
                    label: "Training ID",
                    type: "password",
                    id: "tid"
                }
            ]
        };
    },

    login: function (argsArray, callback) {

        var strUser = this.server + "&method=checkLogin";
        this.tid = argsArray[2].value;
        this.userName = argsArray[0].value + " " + argsArray[1].value;

        for (var i = 0; i < argsArray.length; i++) {
            strUser += "&" + argsArray[i].name + "=" + argsArray[i].value;
        }

        this.sendVar(strUser, this.loginResponseHandler);

    },

    loginResponseHandler: function (result) {

        var xml = new DOMParser().parseFromString(result, 'text/xml');
        var active = xml.querySelector("field[name='ACTIVE']").firstChild;

        if (active) {
            var num = active.firstChild.nodeValue;
            if (num.indexOf("1.") > -1) {
                // user is active, check if he can use this course
                var str = this.server + "&method=checkStatus&tid=" + this.tid + "&ccode=" + this.courseId;
                this.sendVar(str, this.userActiveResponseHandler);
            } else {
                this.loginFailCallback();
            }
        } else {
            this.loginFailCallback();
        }

    },

    userActiveResponseHandler: function(result) {
        var xml = new DOMParser().parseFromString(result, 'text/xml');
        var active = xml.querySelector("field[name='ACTIVE']").firstChild;

        if (active) {
            var num = active.firstChild.nodeValue;
            if (num.indexOf("1.") > -1) {
                // user is active, login OK
                this.loginSuccessCallback();
                this.serviceReadyCallback();
            } else {
                this.loginFailCallback();
            }
        } else {
            this.loginFailCallback();
        }
    },


    receiveVar: function (name, callback) {

    },

    /**
     * Returns user name
     * @returns {string}
     */
    getUserName: function () {
        return this.userName;
    },

    /**
     * Returns training ID
     * @returns {string}
     */
    getTrainingId: function () {
        return this.tid;
    }


});

/*
 TestResultData {
 score: 3,
 maxScore: 11,
 scorePercent: 27,
 duration: 10071,
 userName: "VT5"}
 breakdown: Array[2]0: Object
 maxScore: 7
 poolId: "p1"
 poolName: "Section 1"
 userScore: 3
 Object
 maxScore: 4
 poolId: "p2"
 poolName: undefined
 userScore: 0
 certificateType: "final"
 courseName: "VT5"
 date: "12/07/2016"
 duration: 10071
 durationString: "00:00:10"
 maxScore: 11
 passed: true
 score: 3
 scorePercent: 27
 testName: "Final Test"
 trainingId: "12345"
 userName: "VT5"
 }
 */


