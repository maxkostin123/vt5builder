/**
 * Created by Max Kostin on 17/02/2016
 *
 * This is a wrapper class for communicating with servers/services. You can add any variables or methods,
 * but the ones defined below are required: if any of these are missing, the application will throw an Error.
 *
 * If any of these methods are not needed, just leave their body empty.
 *
 * In the most simple scenario, you will (1) add a variable with the server address and use this address later to
 * send or receive data. This should probably be done inside the init() method.
 *
 * (2) Obtain user name from the server and change value of the variable userName (also inside init method).
 *
 * (3) Obtain sessionId if needed.
 *
 * (4) Test results is a an Object with many properties (example is at the end of this file). You can grab any value needed,
 * construct your own object/json/xml and send it on to your endpoint.
 *
 * http://192.168.16.168/products/course/9999/VOD_index.html?sessionID=262c0b55-042e-46be-ad24-ad54b78f8203&envType=nvodServer&catalogCode=9999#1
 */
"use strict";

define ({

    minPassScore:0,
    userName: "-|-",
    firstName: "",
    lastName: "",
    server: "",
    tid: "01000011110111",
    courseId: "",
    sessionId: "",

    vtUserId: "",

    /**
     * This method should be invoked when user has successfully logged in. It is defined by VT5 App.
     */
    loginSuccessCallback: undefined,

    /**
     * This method should be invoked when user login has failed. It is defined by VT5 App.
     */
    loginFailCallback: undefined,

    /** This method should be invoked when
     * a) user name/surname is available.
     * b) training ID is available.
     * c) user is logged in (if required).
     * It can be called from within any method of this file where it makes sense and when all required fields are available.
     * This method is defined by VT5 app.
     */
    serviceReadyCallback: undefined,

    /**
     * Called when App is initialized
     */
    init:function(){
        console.log("[Service] init");

        this.sendGetRequest = this.sendGetRequest.bind(this);
        this.setSessionVars = this.setSessionVars.bind(this);

        var href = window.location.href;
        this.server = href.substr(0, href.indexOf("/products/"));

        var sessionIdRes = href.match(/(sessionId=)([a-zA-Z0-9_-]+)/i);
        if (sessionIdRes) {
            this.sessionId = sessionIdRes[2];
        }

        var catalogCodeRes =  href.match(/(catalogCode=)([a-zA-Z0-9_-]+)/i);
        if (catalogCodeRes) {
            this.courseId = catalogCodeRes[2];
        }

        this.sendGetRequest("session/" + this.sessionId, this.setSessionVars);


    },


    sendGetRequest:function (param, callback) {
        var xhr = new XMLHttpRequest();
        xhr.open("GET", this.server + "/nvod/training/external/" + param, true);
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.setRequestHeader("Accept", "application/json");
        xhr.onload = function () {
            callback(JSON.parse(this.responseText));
        };

        xhr.send();

    },

    setSessionVars: function (sessionResponse) {

        if(sessionResponse.completed) {

            this.userName = sessionResponse.firstName + " " + sessionResponse.lastName;
            this.tid = sessionResponse.tId;
            this.vtUserId = sessionResponse.vtUserId;
            this.sessionId = sessionResponse.sessionId;
            this.firstName = sessionResponse.firstName;
            this.lastName = sessionResponse.lastName;

            var host = this.server + "/nvod/training/external/test/start";
            var xhttp = new XMLHttpRequest();
            xhttp.open("POST", host, true);
            xhttp.setRequestHeader("Content-type","application/json");
            xhttp.setRequestHeader("Accept", "application/json");
            xhttp.onload = function () {
                var obj = JSON.parse(this.responseText);
                if (! obj.completed) {
                    alert(obj.msg);
                }
            };
            xhttp.send(
                JSON.stringify({
                    sessionId: this.sessionId,
                    vtUserId: this.vtUserId,
                    trainingId: this.trainingId,
                    catalogCode: this.courseId
                }));

            if (this.serviceReadyCallback) {
                this.serviceReadyCallback();
            }
        } else {
            alert(sessionResponse.msg);
        }



    },



    /**
     * Sets minimum pass score
     */
    setMinPassScore: function (value) {
        this.minPassScore = value;
    },

    /**
     * Sends test result to the service.
     * @param result
     */
    sendTestResults: function (result) {

        console.log(result);
        var obj = {};

        obj.firstName = this.firstName;
        obj.lastName = this.lastName;
        obj.catalogCode = this.courseId;
        obj.trainingId = this.tid;
        obj.sessionId = this.sessionId;
        obj.vtUserId = this.vtUserId;

        obj.moduleCode = result.testId;
        obj.score = result.score;
        obj.scorePercentage = result.scorePercent;
        var dateArr = result.date.split("/");
        var dateStr = dateArr[2] + "-" + dateArr[1] + "-" + dateArr[0];
        obj.startDate = dateStr;
        obj.endDate = dateStr;
        obj.timeTaken = result.duration;

        var jsonObj = JSON.stringify(obj);

        var host = this.server + "/nvod/training/external/test/record";
        var xhttp = new XMLHttpRequest();
        xhttp.open("POST", host, true);
        xhttp.setRequestHeader("Content-type","application/json");
        xhttp.setRequestHeader("Accept", "application/json");
        xhttp.onload = function () {
            console.log(this.responseText)
        };
        xhttp.send(jsonObj);
    },


    sendVar:function(){

    },

    /**
     * Called when Window is closed
     */
    onExit: function () {

    },



    /**
     * Tells VT5 app if the user needs to log in.
     * @returns {boolean}
     */
    requiresAuth:function() {
        return false;
    },

    /**
     * Returns an Array of label/type Objects to be used as fields in the Login Modal.
     * This method is required if requireAuth() is set to return boolean 'true'.
     * @returns {{title: string, fields: *[]}}
     */
    getAuthFields:function() {
        return {
            type: "login",
            title: "Login",
            fields: [
                {
                    label: "First Name",
                    type: "text",
                    id: "firstname"
                },
                {
                    label: "Last Name",
                    type: "text",
                    id: "lastname"
                },
                {
                    label: "Training ID",
                    type: "password",
                    id: "tid"
                }
            ]
        };
    },


    login:function (argsArray, callback) {
        this.loginSuccessCallback()
    },


    receiveVar: function(val, callback) {

    },

    /**
     * Returns user name
     * @returns {string}
     */
    getUserName: function() {
        return this.userName;
    },

    /**
     * Returns training ID
     * @returns {string}
     */
    getTrainingId: function() {
        return this.tid;
    }


});

/*
 TestResultData {
 score: 3,
 maxScore: 11,
 scorePercent: 27,
 duration: 10071,
 userName: "VT5"}
 breakdown: Array[2]0: Object
 maxScore: 7
 poolId: "p1"
 poolName: "Section 1"
 userScore: 3
 Object
 maxScore: 4
 poolId: "p2"
 poolName: undefined
 userScore: 0
 certificateType: "final"
 courseName: "VT5"
 date: "12/07/2016"
 duration: 10071
 durationString: "00:00:10"
 maxScore: 11
 passed: true
 score: 3
 scorePercent: 27
 testName: "Final Test"
 trainingId: "12345"
 userName: "VT5"
 }
 */


