/**
 * Created by Max Kostin on 04/05/2016
 */
define([], function () {

    "use strict";

    return new function () {

        var div;
        var selected;
        var audio;

        /**
         * Passes DIV media element to the js. Required.
         * @param $div HTML content defined in a separate file (in this instance, in body.html) and passed wrapped in a div.
         */
        this.init = function ($div) {

            div = $div;
            audio = div.querySelector("audio");

            var soundDivs = div.querySelectorAll(".sound-wave");
            for (var i = 0; i < soundDivs.length; i++) {
                var soundDiv = soundDivs[i];
                soundDiv.innerHTML = getSoundWaveStr();
            }

            div.addEventListener("click", clickHandler);
            audio.addEventListener("ended", audioFinished);

        };

        /**
         * Clears this component. Required.
         */
        this.clear = function () {

            if (div) {
                div.removeEventListener("click", clickHandler);
                audio.removeEventListener("ended", audioFinished);
            }

            div = null;
            selected = null;
            audio = null;
        };



        function getSoundWaveStr(div) {
            var str = "<span class='button vti_play'></span><div class='sound-wave_inner'>";
            for (var i = 0; i < 20; i++) {
                str += "<div class='bar'></div>";
            }
            return str + "</div>";
        }



       function clickHandler(e) {

           var sound = e.target.getAttribute("data-sound");
           if (! sound) {
               return;
           }

           audio.pause();
           audio.time = 0;
           if (selected) {
               selected.querySelector(".sound-wave_inner").classList.remove("animated");
           }
           selected = e.target;
           selected.querySelector(".sound-wave_inner").classList.add("animated");
           audio.src = sound;
           audio.play();
       }


        function audioFinished() {
            selected.querySelector(".sound-wave_inner").classList.remove("animated");
        }
    };


});







