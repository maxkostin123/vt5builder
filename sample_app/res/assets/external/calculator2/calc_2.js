/**
 * Created by Max Kostin on 04/05/2016
 */
define([], function () {

    "use strict";

    return new function () {
        var div;
        var bg;
        var canvas;
        var ctx;

        var inpDensity,
            inpDensityAtRef,
            inpTemp,
            divDiameter;

        var density = 15;
        var densityAtRef = 0.84;
        var temperature = 40;
        var diameter = 0;

        var ratioX = 9; // 10 steps on the graphic represent 90px, hence 90 / 10 = 9.
        var ratioY = 1900;

        var left = 110; /// offset on the graphic's left.
        var right = 920;
        var bottom = 700 - 1;

        var lastInp;

        /**
         * Required.
         * @param $div HTML content defined in a separate file (in this instance, in body.html) and passed wrapped in a div.
         */
        this.init = function ($div) {

            div = $div;
            canvas = div.querySelector("canvas");
            ctx = canvas.getContext("2d");

            inpDensity = div.querySelector("input[data-id='inp-density']");
            inpDensityAtRef = div.querySelector("input[data-id='inp-densityAtRef']");
            inpTemp = div.querySelector("input[data-id='inp-temp']");
            divDiameter = div.querySelector("span[data-id='calc-result']");

            bg = document.createElement("img");
            bg.addEventListener("load", bgLoadedHandler);
            bg.src = "res/assets/external/calculator2/calc_141_bg.png";

        };

        this.clear = function () {
            div.removeEventListener("click", changeValue);
            div.removeEventListener("change", inpChangeHandler);
            div = null;
        };



        function bgLoadedHandler() {

            setListeners();
            updateInputs();
            calculate();


        }


        function setListeners() {
            div.addEventListener("click", changeValue);
            div.addEventListener("change", inpChangeHandler);
        }

        function changeValue(e) {

            var type = e.target.getAttribute("data-id");

            switch (type) {

                case "-density" :
                    density -= 1;
                    lastInp = "density";
                    break;
                case "+density" :
                    density += 1;
                    lastInp = "density";
                    break;


                case "-densityAtRef" :
                    densityAtRef -= .005;
                    lastInp = "ref";
                    break;
                case "+densityAtRef" :
                    densityAtRef += .005;
                    lastInp = "ref";
                    break;

                case "-temp" :
                    temperature -= 1;
                    lastInp = "temp";
                    break;
                case "+temp" :
                    temperature += 1;
                    lastInp = "temp";
                    break;

                default :
                    return;
            }

            updateInputs();
            calculate();

        }

        function inpChangeHandler(e) {

            var type = e.target.getAttribute("data-id");

            switch (type) {

                case "inp-density" :
                    density = parseInt(inpDensity.value);
                    lastInp = "density";
                    break;

                case "inp-densityAtRef" :
                    densityAtRef = parseFloat(inpDensityAtRef.value);
                    lastInp = "ref";
                    break;

                case "inp-temp" :
                    temperature = parseInt(inpTemp.value);
                    lastInp = "temp";
                    break;

            }
            updateInputs();
            calculate();

        }


        function updateInputs() {

            if (density < 15) density = 15;
            if (density > 100) density = 100;

            if (densityAtRef < .75) densityAtRef = .75;
            if (densityAtRef > 1) densityAtRef = 1;

            if (temperature < 15) temperature = 15;
            if (temperature > 100) temperature = 100;

            if (lastInp === "density" && temperature - density < 2) {
                density = temperature -2;
            } else if (temperature - density < 2){
                temperature = density + 2;
            }


            inpDensity.value = "" + density;
            inpDensityAtRef.value = densityAtRef.toFixed(3);
            inpTemp.value = "" + temperature;


        }

        function calculate() {


            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.drawImage(bg, 0,0);
            ctx.lineWidth = 8;


            var x1 = left + (density - 10) * ratioX;
            var x2 = left + (temperature - 10) * ratioX;

            var y1 = 2028 - (densityAtRef) * ratioY;
            var y2 = y1 + 45;

            /// LEFT LINE -----------------------
            ctx.beginPath();
            ctx.strokeStyle = "#545454";
            ctx.moveTo(x1, y1);
            ctx.lineTo(x1, bottom);
            ctx.stroke();

            /// RIGHT LINE -----------------------
            ctx.beginPath();
            ctx.strokeStyle = "#34A614";
            ctx.moveTo(x2, y2);
            ctx.lineTo(x2, bottom);
            ctx.stroke();

            /// TOP LINE -----------------------
            ctx.beginPath();
            ctx.strokeStyle = "#ff0000";
            ctx.moveTo(left, y1);
            ctx.lineTo(x1, y1);
            ctx.lineTo(x2, y2);
            ctx.lineTo(right, y2);
            ctx.stroke();

            // SHOULD BE REPLACED WITH PROPER CALCULATION!
            diameter = density + temperature;
            divDiameter.textContent = parseInt(diameter) + "";

        }


    };



});







