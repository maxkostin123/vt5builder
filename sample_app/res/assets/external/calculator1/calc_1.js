/**
 * Created by Max Kostin on 04/05/2016
 */
define([], function () {

    "use strict";

    return new function () {
        var div;
        var resGross, resNet, resWater, resAsh, resSulphur, resCalc;
        var inpDensity, inpWater, inpAsh, inpSulphur;

        var density = 0,
            water = 0,
            ash = 0,
            sulphur = 0,
            step = 1;

        /**
         * Required.
         * @param $div HTML content defined in a separate file (in this instance, in body.html) and passed wrapped in a div.
         */
        this.init = function ($div) {
            div = $div;

            resGross = div.querySelector("span[data-id='res-gross']");
            resNet = div.querySelector("span[data-id='res-net']");
            resWater = div.querySelector("span[data-id='res-water']");
            resAsh = div.querySelector("span[data-id='res-ash']");
            resSulphur = div.querySelector("span[data-id='res-sulphur']");
            resCalc = div.querySelector("span[data-id='res-calc']");

            inpDensity = div.querySelector("input[data-id='inp-density']");
            inpWater = div.querySelector("input[data-id='inp-water']");
            inpAsh = div.querySelector("input[data-id='inp-ash']");
            inpSulphur = div.querySelector("input[data-id='inp-sulphur']");

            density = parseFloat(inpDensity.value);
            water = parseFloat(inpWater.value);
            ash = parseFloat(inpAsh.value);
            sulphur = parseFloat(inpSulphur.value);


            setListeners();
            updateInputs();
            calculate();
        };

        this.clear = function () {
            div.removeEventListener("click", changeValue);
            div.removeEventListener("change", inpChangeHandler);
            div = null;
        };


        function setListeners() {
            div.addEventListener("click", changeValue);
            div.addEventListener("change", inpChangeHandler);
        }

        function changeValue(e) {

            var type = e.target.getAttribute("data-id");

            switch (type) {

                case "-density" :
                    density -= step;
                    break;
                case "+density" :
                    density += step;
                    break;

                case "-water" :
                    water -= step;
                    break;
                case "+water" :
                    water += step;
                    break;

                case "-ash" :
                    ash -= step;
                    break;
                case "+ash" :
                    ash += step;
                    break;

                case "-sulphur" :
                    sulphur -= step;
                    break;
                case "+sulphur" :
                    sulphur += step;
                    break;

                case "calc-x":
                    step = parseInt(e.target.getAttribute("data-value"));
                    break;

                default :
                    return;
            }

            updateInputs();
            calculate();

        }

        function inpChangeHandler(e) {

            var type = e.target.getAttribute("data-id");

            switch (type) {

                case "inp-density" :
                    density = parseFloat(inpDensity.value);
                    break;

                case "inp-water" :
                    water = parseFloat(inpWater.value);
                    break;

                case "inp-ash" :
                    ash = parseFloat(inpAsh.value);
                    break;

                case "inp-sulphur" :
                    sulphur = parseFloat(inpSulphur.value);
                    break;
            }

            calculate();

        }


        function updateInputs() {

            inpDensity.value = "" + density;
            inpWater.value = "" + water;
            inpAsh.value = "" + ash;
            inpSulphur.value = "" + sulphur;

        }

        function calculate() {

            // These are all fake calculations; please updtate with real formulae.

            var gross = parseFloat(inpDensity.value) + parseFloat(inpAsh.value) + parseFloat(inpWater.value) + parseFloat(inpSulphur.value);
            var net = (gross - parseFloat(inpAsh.value))/2;

            resGross.textContent = gross.toFixed(2);
            resNet.textContent = net.toFixed(2);
            resAsh.textContent = Math.sqrt(net).toFixed(2);
            resWater.textContent = (Math.sqrt(net)/10).toFixed(2);
            resSulphur.textContent = (Math.sqrt(net)/10).toFixed(2);
            resCalc.textContent = (net + net/10).toFixed(2);


        }


    };



});







