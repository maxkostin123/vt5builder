define(
    {

        background: 0x666666,

        camera: {
            x: 0,
            y: 3,
            z: 10
        },

        scene: {
            rotation: {
                y: 0.87 // In radians
            },
            scale: 1,
            autoRotateY: 0
        }


    }
);