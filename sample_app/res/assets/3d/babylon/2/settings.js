define(
    {

      src: {
          root: "res/assets/3d/babylon/2/",
          file: "pump.json"
      },

        camera: {
            upperRadiusLimit: 45, // how far the camera can move away from Scene
            lowerRadiusLimit: 3, // how close the camera can move into the Scene
            position: [10, 6, -6], // camera position
            copyPositionFromSceneCamera: false,
            useSceneCamera: false

        },

        scene: {
            clearColor: [.3, .3, .4],
            ambientColor: [.1, .1, .1],
            scale: 1,
            useOptimizer: false, // Should be set to false if there're Tweaks or Animations.
            antialias: true,


        },


        init: function (scene) {
            "use strict";
            var cam = scene.activeCamera;

            T._(cam, 2, [{prop: "radius", to: 15, from: 45}, {prop: "alpha", to: 5.5, from: 4}, {prop: "beta", to: 1.2, from: 2}]);

            var coverTop = scene.getMeshByName("coverTop");
            var coverBottom = scene.getMeshByName("coverBottom");
            var frame = scene.getMeshByName("frame");
            var r1 = scene.getMeshByName("ring1");
            var r2 = scene.getMeshByName("ring2");
            var washer = scene.getMeshByName("washerSquare");
            var b1 = scene.getMeshByName("black1");
            var b2 = scene.getMeshByName("black2");
            var disk = scene.getMeshByName("discTop");

            function assemble() {
                T._(coverTop.position, 1, [{prop:"y", to:0}], {delay:2.5});
                T._(frame.position, 1, [{prop:"y", to:0}], {delay:2.5});
                T._(coverBottom.position, 1, [{prop:"y", to:.06}], {delay:2.5, call:dissemble});
                T._(r1.position, 1, [{prop:"x", to:-.2}], {delay:2});
                T._(r2.position, 1, [{prop:"x", to:.2}], {delay:2});
                T._(washer.position, 1, [{prop:"y", to:2.65}], {delay:2});
                T._(b1.position, 1, [{prop:"y", to:2.83}], {delay:2});
                T._(b2.position, 1, [{prop:"y", to:3.55}], {delay:2});
                T._(disk.position, 1, [{prop:"y", to:3.65}], {delay:2});
            }

            function dissemble() {
                T._(coverTop.position, 1, [{prop:"y", to:.71}], {delay:2});
                T._(frame.position, 1, [{prop:"y", to:.5}], {delay:2});
                T._(coverBottom.position, 1, [{prop:"y", to:-1}], {delay:2});
                T._(r1.position, 1, [{prop:"x", to:-1.5}], {delay:2.6});
                T._(r2.position, 1, [{prop:"x", to:1.5}], {delay:2.6, call:assemble});

                T._(washer.position, 1, [{prop:"y", to:3}], {delay:2.6});
                T._(b1.position, 1, [{prop:"y", to:3.5}], {delay:2.6});
                T._(b2.position, 1, [{prop:"y", to:5}], {delay:2.6});
                T._(disk.position, 1, [{prop:"y", to:6}], {delay:2.6});
            }


            dissemble();

        },



        destroy: function () {
            T.killAll();
        },

        update: function () {
            //console.log(this.meshes[0])
        }


    }
);