define(
    {

        src: {
            root: "res/assets/3d/babylon/1/",
            file: "scene2.json"
        },

        babylonScene: null,

        camera: {
            upperRadiusLimit: 45, // how far the camera can move away from Scene
            lowerRadiusLimit: 3, // how close the camera can move into the Scene
            position: [10, 6, -6], // camera position
            copyPositionFromSceneCamera: false,
            orbit: .2 // if not 0, the camera will auto-orbit the scene
        },

        scene: {
            clearColor: [.3, .3, .4],
            ambientColor: [.2, .2, .2],
            scale: 1

        },


        init: function (scene) {
            this.babylonScene = scene;
        },

        destroy: function () {
            this.babylonScene = null;
        },

        update: function () {
            this.babylonScene.activeCamera.alpha += .01;
        }


    }
);