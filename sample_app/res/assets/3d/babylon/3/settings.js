define(
    {
        src: {
            root: "res/assets/3d/babylon/3/",
            file: "cam_inside.json"
        },

        camera: {
            fov: 35,
            angularSensibilityX: 3500,
            panningSensibility: 200,
            wheelPrecision: 80,
            pinchPrecision: 70,
            upperRadiusLimit: 20, // how far the camera can move away from Scene
            lowerRadiusLimit: 1, // how close the camera can move into the Scene
            lowerBetaLimit: 70, // min pitch
            upperBetaLimit: 108, // max pitch
            position: [1, 1, 1], // camera position
            alpha: 0, // yaw
            beta: 85, // pitch
            target: [0, 1, 0],
            copyPositionFromSceneCamera: false,
            useSceneCamera: false
        },

        scene: {
            clearColor: [.3, .3, .4],
            ambientColor: [0, 0, 0],
            scale: 1,
            useOptimizer: false, // Should set to false if there're Tweaks or Animations.
            antialias: true,
            skybox: {
                src: "res/assets/3d/babylon/3/sky/sky",
                infiniteDistance: true,
                size: 1500.0
            }
        },




        init: function () {
            "use strict";
        },


        destroy: function () {

        },

        update: function () {

        }


    }
);