define([], function () {
    var cjs = createjs;
    var lib = {};
    var img = {};
    var images = {};


    var p; // shortcut to reference prototypes

// library properties:
    lib.properties = {
        width: 1024,
        height: 470,
        fps: 60,
        color: "#CCCCCC",
        manifest: [
            {src: "assets/animations/14a/_0633_14_04.png", id: "_0633_14_04"},
            {src: "assets/animations/14a/Screw_Horizontal.png", id: "Screw_Horizontal"},
            {src: "assets/animations/14a/screw_overlay.png", id: "screw_overlay"},
            {src: "assets/animations/14a/thread.png", id: "thread"}
        ]
    };


// symbols:


    (lib._0633_14_04 = function () {
        this.initialize(img._0633_14_04);
    }).prototype = p = new cjs.Bitmap();
    p.nominalBounds = new cjs.Rectangle(0, 0, 559, 317);


    (lib.Screw_Horizontal = function () {
        this.initialize(img.Screw_Horizontal);
    }).prototype = p = new cjs.Bitmap();
    p.nominalBounds = new cjs.Rectangle(0, 0, 767, 154);


    (lib.screw_overlay = function () {
        this.initialize(img.screw_overlay);
    }).prototype = p = new cjs.Bitmap();
    p.nominalBounds = new cjs.Rectangle(0, 0, 744, 151);


    (lib.thread = function () {
        this.initialize(img.thread);
    }).prototype = p = new cjs.Bitmap();
    p.nominalBounds = new cjs.Rectangle(0, 0, 25, 313);


    (lib.hand = function () {
        this.initialize();

        // Layer 4
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#000000").s().p("AingfIFPAfIlPAfg");
        this.shape.setTransform(18.6, 0.1);

        // Layer 3
        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#000000").s().p("AgoAnQgQgQAAgXQAAgXAQgRQASgQAWAAQAXAAAQAQQASARAAAXQAAAXgSAQQgQASgXAAQgWAAgSgSg");
        this.shape_1.setTransform(0, 0.1);

        this.addChild(this.shape_1, this.shape);
    }).prototype = p = new cjs.Container();
    p.nominalBounds = new cjs.Rectangle(-5.7, -5.7, 41.2, 11.5);


    (lib.Tween9 = function () {
        this.initialize();

        // Layer 1
        this.instance = new lib.Screw_Horizontal();
        this.instance.setTransform(-383.5, -77);

        this.addChild(this.instance);
    }).prototype = p = new cjs.Container();
    p.nominalBounds = new cjs.Rectangle(-383.5, -77, 767, 154);


    (lib.Tween7 = function () {
        this.initialize();

        // Layer 1
        this.instance = new lib.Screw_Horizontal();
        this.instance.setTransform(-383.5, -77);

        this.addChild(this.instance);
    }).prototype = p = new cjs.Container();
    p.nominalBounds = new cjs.Rectangle(-383.5, -77, 767, 154);


    (lib.Tween5copy = function () {
        this.initialize();

        // Layer 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FFFFFF").s().p("EjXKAAbQkzg4IHAEMGszAABQg3AEAAANQAwARjsARg");
        this.shape.setTransform(0.3, -68.3);

        this.addChild(this.shape);
    }).prototype = p = new cjs.Container();
    p.nominalBounds = new cjs.Rectangle(-1388.2, -70.9, 2777.1, 5.4);


    (lib.Tween4 = function () {
        this.initialize();

        // Layer 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FFFFFF").s().p("EjXKAAbQkzg4IHAEMGszAABQg3AEAAANQAwARjsARg");
        this.shape.setTransform(0.3, -83.1);

        this.addChild(this.shape);
    }).prototype = p = new cjs.Container();
    p.nominalBounds = new cjs.Rectangle(-1388.2, -85.8, 2777.1, 5.4);


    (lib.Tween3 = function () {
        this.initialize();

        // Layer 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FFFFFF").s().p("EjhrABvQAAjpIgANMG3wAAAQgwANCZA9QDTBIkHBKg");
        this.shape.setTransform(9.6, -82.2);

        this.addChild(this.shape);
    }).prototype = p = new cjs.Container();
    p.nominalBounds = new cjs.Rectangle(-1434.8, -93.3, 2888.9, 22.3);


    (lib.Tween2 = function () {
        this.initialize();

        // Layer 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FFFFFF").s().p("EjUIABbQDeiAFRgfQDegZDjADMGVsAAaQAuACBUAMIAzAEQg8Amh/AiQibAiifAfg");
        this.shape.setTransform(8.5, -88.6);

        this.addChild(this.shape);
    }).prototype = p = new cjs.Container();
    p.nominalBounds = new cjs.Rectangle(-1349.3, -97.8, 2715.6, 18.3);


    (lib.Tween1copy4 = function () {
        this.initialize();

        // Layer 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FFFFFF").s().p("EjREABmQB4g2C6gyQFzhpFIAGMGScAAgQg1ARjJBHQifA7hlAYg");
        this.shape.setTransform(-8.2, 277.2);

        this.addChild(this.shape);
    }).prototype = p = new cjs.Container();
    p.nominalBounds = new cjs.Rectangle(-1346.4, 267, 2676.4, 20.6);


    (lib.Tween1copy3 = function () {
        this.initialize();

        // Layer 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FFFFFF").s().p("EjREABWQB4guC6gqQFzhZFIAGMGScAAgQj3BLkLBAg");
        this.shape.setTransform(-8.2, 275.6);

        this.addChild(this.shape);
    }).prototype = p = new cjs.Container();
    p.nominalBounds = new cjs.Rectangle(-1346.4, 267, 2676.4, 17.4);


    (lib.Tween1copy2 = function () {
        this.initialize();

        // Layer 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FFFFFF").s().p("EjREABWQB4guC6gqQFzhZFIAGMGScAAgQj3BLkLBAg");
        this.shape.setTransform(-8.2, 275.6);

        this.addChild(this.shape);
    }).prototype = p = new cjs.Container();
    p.nominalBounds = new cjs.Rectangle(-1346.4, 267, 2676.4, 17.4);


    (lib.Tween1copy = function () {
        this.initialize();

        // Layer 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FFFFFF").s().p("EjREAAmQB4gWC6gSQFzgpFIAGMGScAAgQhYAZimgBQiyAAhSATg");
        this.shape.setTransform(-8.2, 270.8);

        this.addChild(this.shape);
    }).prototype = p = new cjs.Container();
    p.nominalBounds = new cjs.Rectangle(-1346.4, 266.9, 2676.4, 7.8);


    (lib.Tween1 = function () {
        this.initialize();

        // Layer 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FFFFFF").s().p("EjREABWQHhi1IMAKMGScAAgQj7BMkHA/g");
        this.shape.setTransform(8.2, -83.7);

        this.addChild(this.shape);
    }).prototype = p = new cjs.Container();
    p.nominalBounds = new cjs.Rectangle(-1329.9, -92.3, 2676.4, 17.3);


    (lib._0633_14_Tween11 = function () {
        this.initialize();

        // Layer 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#D5EEF5").s().p("A+GP+IAA/7MA8NAAAIAAf7g");

        this.addChild(this.shape);
    }).prototype = p = new cjs.Container();
    p.nominalBounds = new cjs.Rectangle(-192.7, -102.2, 385.6, 204.5);


    (lib._0633_14_Tween05 = function () {
        this.initialize();

        // Layer 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#999999").ss(1, 1, 1).p("AhRmfQhLAAAABMIAAKnQAABLBLAAICiAAQBMAAAAhLIAAqnQAAhMhMAAg");
        this.shape.setTransform(0, -41.4);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.lf(["#818181", "#E8E8E8", "#B7B7B7"], [0.082, 0.49, 0.992], 0, 42.8, 0, -42.7).s().p("AhRGgQhLAAAAhNIAAqlQAAhNBLAAICiAAQBMAAAABNIAAKlQAABNhMAAg");
        this.shape_1.setTransform(0, -41.4);

        this.shape_2 = new cjs.Shape();
        this.shape_2.graphics.f().s("#999999").ss(1, 1, 1).p("AhRmeQhLAAAABLIAAKnQAABLBLAAICiAAQBMAAAAhLIAAqnQAAhLhMAAg");
        this.shape_2.setTransform(0, 41.5);

        this.shape_3 = new cjs.Shape();
        this.shape_3.graphics.lf(["#B7B7B7", "#E8E8E8", "#7F7F7F"], [0.18, 0.49, 0.91], -82.8, 41.9, -82.9, -41.7).s().p("AhRGfQhLAAAAhLIAAqnQAAhLBLAAICiAAQBMAAAABLIAAKnQAABLhMAAg");
        this.shape_3.setTransform(0, 41.5);

        this.addChild(this.shape_3, this.shape_2, this.shape_1, this.shape);
    }).prototype = p = new cjs.Container();
    p.nominalBounds = new cjs.Rectangle(-16.7, -84, 33.6, 168.1);


    (lib._0633_14_Tween3copy2 = function () {
        this.initialize();

        // Layer 2
        this.instance = new lib.thread();
        this.instance.setTransform(-12.8, -148.2);

        this.addChild(this.instance);
    }).prototype = p = new cjs.Container();
    p.nominalBounds = new cjs.Rectangle(-13.7, -165.4, 27.4, 330.8);


    (lib._0633_14_Tween3copy = function () {
        this.initialize();

        // Layer 2
        this.instance = new lib.thread();
        this.instance.setTransform(-12.8, -147.6);

        this.addChild(this.instance);
    }).prototype = p = new cjs.Container();
    p.nominalBounds = new cjs.Rectangle(-13.7, -165.4, 27.4, 330.8);


    (lib._0633_14_Tween3 = function () {
        this.initialize();

        // Layer 2
        this.instance = new lib.thread();
        this.instance.setTransform(-12.8, -163.8);

        this.addChild(this.instance);
    }).prototype = p = new cjs.Container();
    p.nominalBounds = new cjs.Rectangle(-13.7, -165.4, 27.4, 330.8);


    (lib.gauge1 = function (mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {});

        // timeline functions:
        this.frame_0 = function () {
            var scope = this;

            var initAngle = -200;
            var numDigits = 10;
            var stepValue = 5;
            var minValue = null;
            var maxValue = null;
            var targetAngle = initAngle;
            var userMin = 0;
            var r = 55;
            var HALF_PI = Math.PI / 180;
            var speed = -2;
            var allowRun = false;
            var dist = -2;
            var degStep;
            var hasStopped = true;
            var valuesArray = null;
            var valuesIndex = 0;
            var isAnimated = false;
            var hasStarted = false;

            var rpmMin, rpmMax, initMin, initMax;

            /* ============================================================================
             PUBLIC FUNCTIONS
             ==============================================================================*/

            /**
             * Sets oscillation parameters for this component.
             * @param $degree angular distance to move back from original position
             * @param $speed velocity of rotaion
             */
            this.setOscillate = function ($degree, $speed) {
                minValue = maxValue - $degree;
                speed = $speed || -1;
            }

            /**
             * Starts oscillating the hand of this component.
             * If setOscillate(..) is not envoked, default speed of 2 is set,
             * and the oscillation degree will be based on angular distance between two neighbouring digits.
             */
            this.start = function () {
                if (hasStarted) return;
                isAnimated = true;
                allowRun = true;
                hasStarted = true;
                run();
            }

            /**
             * Stops this component.
             * Should be called every time to reset all values back to 0.
             */
            this.stop = function () {
                hasStopped = false;
                speed = -Math.abs(speed);
                allowRun = false;
                //slowDown();
                setRevsText(0);
                valuesIndex = 0;
                targetAngle = initAngle;
                scope.hand.rotation = initAngle;
                hasStarted = false;
            };

            /**
             * Checks if the hand has reached zero.
             * @returns {boolean}
             */
            this.hasStopped = function () {
                return scope.hand.rotation <= initAngle;
            }

            /**
             * Increases the value
             */
            this.up = function () {
                if (valuesArray) {
                    if (valuesIndex < valuesArray.length - 1) {
                        valuesIndex++;
                        scope.hand.rotation = getAngleFromValue(valuesArray[valuesIndex]);
                        setRevsText(valuesArray[valuesIndex]);
                        targetAngle = getAngleFromValue(valuesArray[valuesIndex]);
                    }
                }
                else {
                    var mAng = (isAnimated) ? maxValue - degStep : maxValue
                    if (scope.hand.rotation < mAng) {
                        valuesIndex++;
                        scope.hand.rotation += degStep;
                        setRevsText(valuesIndex * stepValue);
                        targetAngle += degStep;
                    }
                }
            };

            /**
             * Decreases the value
             */
            this.down = function () {
                if (valuesArray) {
                    if (valuesIndex > 0) {
                        valuesIndex--;
                        scope.hand.rotation = getAngleFromValue(valuesArray[valuesIndex]);
                        setRevsText(valuesArray[valuesIndex]);
                        targetAngle = getAngleFromValue(valuesArray[valuesIndex]);
                    }
                }
                else {
                    if (scope.hand.rotation > initAngle) {
                        valuesIndex--;
                        scope.hand.rotation -= degStep;
                        setRevsText(valuesIndex * stepValue);
                        targetAngle -= degStep;
                    }
                }
            }

            /**
             * Allows to pass an array of values to move to when up or down buttons are pressed.
             * It's a good idea to try to match the number of actual animation steps with the array length.
             * Array should always start with a 0.
             * @param {Array} array of values
             */
            this.setValues = function (value) {
                valuesArray = value;
            }

            /**
             * Initializes this component.
             * @param $numDigits number of digits on the dial
             * @param $unitValue unit value. E.g. if set to 5, the dial will display 0 5 10 15 20...
             */
            this.init = function ($numDigits, $unitValue) {
                numDigits = $numDigits || 7;
                stepValue = $unitValue || 5;
                var s = new cjs.Shape();
                var g = s.graphics;
                g.setStrokeStyle(.2);
                g.beginStroke("#000000");

                degStep = 220 / (numDigits - 1);
                var angle = initAngle - degStep;

                var outter = 14;
                var inner = 30;

                for (var i = 0; i < numDigits; i++) {
                    angle += degStep;
                    var tf = new cjs.Text("" + (i * stepValue), "12px 'Arial'", "#333333");
                    tf.textAlign = "center";
                    var rad = angle * HALF_PI;
                    var x = Math.cos(rad);
                    var y = Math.sin(rad);
                    tf.x = x * r + dist;
                    tf.y = y * r + dist - 5;
                    scope.addChild(tf);

                    g.moveTo(x * (r - outter) + dist, y * (r - outter) + dist);
                    g.lineTo(x * (r - inner) + dist, y * (r - inner) + dist);
                }
                maxValue = angle;
                minValue = degStep;
                setRevsText(0);
                scope.hand.rotation = initAngle;
                scope.addChild(s);
            };


            /* ============================================================================
             PRIVATE FUNCTIONS
             ==============================================================================*/

            function getAngleFromValue(v) {
                if (v === 0) return initAngle;
                return (v / stepValue) * degStep + initAngle;
            }


            function setRevsText(value) {
                scope.tfRevs.text = value.toFixed(1);
            };


            function run() {
                hasStopped = false;
                if (speed > 0 && scope.hand.rotation <= targetAngle) {
                    speed = Math.abs(speed);
                }
                else if (speed > 0 && scope.hand.rotation >= targetAngle) {
                    speed = -Math.abs(speed);
                }
                else if (speed < 0 && scope.hand.rotation <= targetAngle - minValue) {
                    speed = Math.abs(speed);
                }
                else if (speed < 0 && scope.hand.rotation <= initAngle) {
                    speed = Math.abs(speed);
                }

                scope.hand.rotation += speed;
                if (allowRun) {
                    setTimeout(run, 10);
                }
            }


            function slowDown() {
                if (!hasStopped) scope.hand.rotation += speed;
                if (scope.hand.rotation > initAngle) {
                    setTimeout(slowDown, 10);
                } else {
                    hasStopped = true;
                }
            }
        }

        // actions tween:
        this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

        // Layer 3
        this.hand = new lib.hand();
        this.hand.setTransform(-0.2, -1.4, 1, 1, 0, 0, 0, -0.3, 0.5);

        this.tfRevs = new cjs.Text("0.0", "10px 'Arial'", "#333333");
        this.tfRevs.name = "tfRevs";
        this.tfRevs.textAlign = "center";
        this.tfRevs.lineHeight = 12;
        this.tfRevs.lineWidth = 36;
        this.tfRevs.setTransform(-0.8, 41, 1.375, 1.375);

        this.tfRM = new cjs.Text("Discharge Flow", "10px 'Arial'", "#333333");
        this.tfRM.name = "tfRM";
        this.tfRM.textAlign = "center";
        this.tfRM.lineHeight = 12;
        this.tfRM.lineWidth = 87;
        this.tfRM.setTransform(-0.1, 26, 1.375, 1.375);

        this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.tfRM}, {t: this.tfRevs}, {t: this.hand}]}).wait(1));

        // Layer 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#000000").ss(1, 1, 1).p("AqdqdIU7AAIAAU7I07AAg");
        this.shape.setTransform(0, -6);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#FFFFFF").s().p("AqcKeIAA06IU5AAIAAU6g");
        this.shape_1.setTransform(0, -6);

        this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_1}, {t: this.shape}]}).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = new cjs.Rectangle(-68, -74, 136, 136);


    (lib.gaugecopy = function (mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {});

        // timeline functions:
        this.frame_0 = function () {
            var scope = this;

            var initAngle = -200;
            var numDigits = 10;
            var stepValue = 5;
            var minValue = null;
            var maxValue = null;
            var targetAngle = initAngle;
            var userMin = 0;
            var r = 55;
            var HALF_PI = Math.PI / 180;
            var speed = -2;
            var allowRun = false;
            var dist = -2;
            var degStep;
            var hasStopped = true;
            var valuesArray = null;
            var valuesIndex = 0;
            var isAnimated = false;
            var hasStarted = false;

            var rpmMin, rpmMax, initMin, initMax;

            /* ============================================================================
             PUBLIC FUNCTIONS
             ==============================================================================*/

            /**
             * Sets oscillation parameters for this component.
             * @param $degree angular distance to move back from original position
             * @param $speed velocity of rotaion
             */
            this.setOscillate = function ($degree, $speed) {
                minValue = maxValue - $degree;
                speed = $speed || -1;
            }

            /**
             * Starts oscillating the hand of this component.
             * If setOscillate(..) is not envoked, default speed of 2 is set,
             * and the oscillation degree will be based on angular distance between two neighbouring digits.
             */
            this.start = function () {
                if (hasStarted) return;
                isAnimated = true;
                allowRun = true;
                hasStarted = true;
                run();
            }

            /**
             * Stops this component.
             * Should be called every time to reset all values back to 0.
             */
            this.stop = function () {
                hasStopped = false;
                speed = -Math.abs(speed);
                allowRun = false;
                //slowDown();
                setRevsText(0);
                valuesIndex = 0;
                targetAngle = initAngle;
                scope.hand.rotation = initAngle;
                hasStarted = false;
            };

            /**
             * Checks if the hand has reached zero.
             * @returns {boolean}
             */
            this.hasStopped = function () {
                return scope.hand.rotation <= initAngle;
            }

            /**
             * Increases the value
             */
            this.up = function () {
                if (valuesArray) {
                    if (valuesIndex < valuesArray.length - 1) {
                        valuesIndex++;
                        scope.hand.rotation = getAngleFromValue(valuesArray[valuesIndex]);
                        setRevsText(valuesArray[valuesIndex]);
                        targetAngle = getAngleFromValue(valuesArray[valuesIndex]);
                    }
                }
                else {
                    var mAng = (isAnimated) ? maxValue - degStep : maxValue
                    if (scope.hand.rotation < mAng) {
                        valuesIndex++;
                        scope.hand.rotation += degStep;
                        setRevsText(valuesIndex * stepValue);
                        targetAngle += degStep;
                    }
                }
            };

            /**
             * Decreases the value
             */
            this.down = function () {
                if (valuesArray) {
                    if (valuesIndex > 0) {
                        valuesIndex--;
                        scope.hand.rotation = getAngleFromValue(valuesArray[valuesIndex]);
                        setRevsText(valuesArray[valuesIndex]);
                        targetAngle = getAngleFromValue(valuesArray[valuesIndex]);
                    }
                }
                else {
                    if (scope.hand.rotation > initAngle) {
                        valuesIndex--;
                        scope.hand.rotation -= degStep;
                        setRevsText(valuesIndex * stepValue);
                        targetAngle -= degStep;
                    }
                }
            }

            /**
             * Allows to pass an array of values to move to when up or down buttons are pressed.
             * It's a good idea to try to match the number of actual animation steps with the array length.
             * Array should always start with a 0.
             * @param {Array} array of values
             */
            this.setValues = function (value) {
                valuesArray = value;
            }

            /**
             * Initializes this component.
             * @param $numDigits number of digits on the dial
             * @param $unitValue unit value. E.g. if set to 5, the dial will display 0 5 10 15 20...
             */
            this.init = function ($numDigits, $unitValue) {
                numDigits = $numDigits || 7;
                stepValue = $unitValue || 5;
                var s = new cjs.Shape();
                var g = s.graphics;
                g.setStrokeStyle(.2);
                g.beginStroke("#000000");

                degStep = 220 / (numDigits - 1);
                var angle = initAngle - degStep;

                var outter = 14;
                var inner = 30;

                for (var i = 0; i < numDigits; i++) {
                    angle += degStep;
                    var tf = new cjs.Text("" + (i * stepValue), "12px 'Arial'", "#333333");
                    tf.textAlign = "center";
                    var rad = angle * HALF_PI;
                    var x = Math.cos(rad);
                    var y = Math.sin(rad);
                    tf.x = x * r + dist;
                    tf.y = y * r + dist - 5;
                    scope.addChild(tf);

                    g.moveTo(x * (r - outter) + dist, y * (r - outter) + dist);
                    g.lineTo(x * (r - inner) + dist, y * (r - inner) + dist);
                }
                maxValue = angle;
                minValue = degStep;
                setRevsText(0);
                scope.hand.rotation = initAngle;
                scope.addChild(s);
            };


            /* ============================================================================
             PRIVATE FUNCTIONS
             ==============================================================================*/

            function getAngleFromValue(v) {
                if (v === 0) return initAngle;
                return (v / stepValue) * degStep + initAngle;
            }


            function setRevsText(value) {
                scope.tfRevs.text = value.toFixed(1);
            };


            function run() {
                hasStopped = false;
                if (speed > 0 && scope.hand.rotation <= targetAngle) {
                    speed = Math.abs(speed);
                }
                else if (speed > 0 && scope.hand.rotation >= targetAngle) {
                    speed = -Math.abs(speed);
                }
                else if (speed < 0 && scope.hand.rotation <= targetAngle - minValue) {
                    speed = Math.abs(speed);
                }
                else if (speed < 0 && scope.hand.rotation <= initAngle) {
                    speed = Math.abs(speed);
                }

                scope.hand.rotation += speed;
                if (allowRun) {
                    setTimeout(run, 10);
                }
            }


            function slowDown() {
                if (!hasStopped) scope.hand.rotation += speed;
                if (scope.hand.rotation > initAngle) {
                    setTimeout(slowDown, 10);
                } else {
                    hasStopped = true;
                }
            }
        }

        // actions tween:
        this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

        // Layer 3
        this.hand = new lib.hand();
        this.hand.setTransform(-0.2, -1.4, 1, 1, 0, 0, 0, -0.3, 0.5);

        this.tfRevs = new cjs.Text("0.0", "10px 'Arial'", "#333333");
        this.tfRevs.name = "tfRevs";
        this.tfRevs.textAlign = "center";
        this.tfRevs.lineHeight = 12;
        this.tfRevs.lineWidth = 36;
        this.tfRevs.setTransform(-0.8, 37, 1.375, 1.375);

        this.tfRM = new cjs.Text("rev/min", "10px 'Arial'", "#333333");
        this.tfRM.name = "tfRM";
        this.tfRM.textAlign = "center";
        this.tfRM.lineHeight = 12;
        this.tfRM.lineWidth = 36;
        this.tfRM.setTransform(-0.8, 22, 1.375, 1.375);

        this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.tfRM}, {t: this.tfRevs}, {t: this.hand}]}).wait(1));

        // Layer 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#000000").ss(1, 1, 1).p("AqdqdIU7AAIAAU7I07AAg");
        this.shape.setTransform(0, -6);

        this.shape_1 = new cjs.Shape();
        this.shape_1.graphics.f("#FFFFFF").s().p("AqcKeIAA06IU5AAIAAU6g");
        this.shape_1.setTransform(0, -6);

        this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.shape_1}, {t: this.shape}]}).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = new cjs.Rectangle(-68, -74, 136, 136);


    (lib._0633_14_Symbol2 = function (mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {});

        // Layer 3
        this.instance = new lib.Tween1copy2("synched", 0);
        this.instance.setTransform(-32.6, -80.8);
        this.instance.alpha = 0.199;

        this.instance_1 = new lib.Tween1copy3("synched", 0);
        this.instance_1.setTransform(-32.6, -112.8);
        this.instance_1.alpha = 0.199;
        this.instance_1._off = true;

        this.instance_2 = new lib.Tween1copy4("synched", 0);
        this.instance_2.setTransform(-32.6, -152.8);
        this.instance_2.alpha = 0.199;
        this.instance_2._off = true;

        this.instance_3 = new lib.Tween1copy("synched", 0);
        this.instance_3.setTransform(-32.6, -216.8);
        this.instance_3.alpha = 0.199;
        this.instance_3._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance).to({_off: true, y: -112.8}, 10).wait(32));
        this.timeline.addTween(cjs.Tween.get(this.instance_1).to({_off: false}, 10).to({
            _off: true,
            y: -152.8
        }, 11).wait(21));
        this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(10).to({_off: false}, 11).to({
            _off: true,
            y: -216.8
        }, 11).wait(10));
        this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(21).to({_off: false}, 11).to({
            y: -256.8,
            alpha: 0.051
        }, 9).wait(1));

        // Layer 2
        this.instance_4 = new lib.Tween1("synched", 0);
        this.instance_4.setTransform(-32.6, -96.8);
        this.instance_4.alpha = 0.199;

        this.instance_5 = new lib.Tween2("synched", 0);
        this.instance_5.setTransform(-54.4, -72.4);
        this.instance_5.alpha = 0.199;
        this.instance_5._off = true;

        this.instance_6 = new lib.Tween3("synched", 0);
        this.instance_6.setTransform(-146.4, 4.8);
        this.instance_6.alpha = 0.199;
        this.instance_6._off = true;

        this.instance_7 = new lib.Tween4("synched", 0);
        this.instance_7.setTransform(-32.4, 63.7);
        this.instance_7.alpha = 0.199;
        this.instance_7._off = true;

        this.instance_8 = new lib.Tween5copy("synched", 0);
        this.instance_8.setTransform(-32.4, 80.9);
        this.instance_8.alpha = 0.051;

        this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_4}]}).to({state: [{t: this.instance_5}]}, 10).to({state: [{t: this.instance_6}]}, 11).to({state: [{t: this.instance_7}]}, 11).to({state: [{t: this.instance_8}]}, 9).wait(1));
        this.timeline.addTween(cjs.Tween.get(this.instance_4).to({_off: true, x: -54.4, y: -72.4}, 10).wait(32));
        this.timeline.addTween(cjs.Tween.get(this.instance_5).to({_off: false}, 10).to({
            _off: true,
            x: -146.4,
            y: 4.8
        }, 11).wait(21));
        this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(10).to({_off: false}, 11).to({
            _off: true,
            x: -32.4,
            y: 63.7
        }, 11).wait(10));
        this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(21).to({_off: false}, 11).to({
            _off: true,
            y: 80.9,
            alpha: 0.051
        }, 9).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = new cjs.Rectangle(-1379, -189.1, 2692.8, 392.7);


    (lib.pump = function (mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {end: 234});

        // Label
        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#43B4DF").ss(1.5, 1, 1).p("AAAgBIAAAD");
        this.shape.setTransform(667.5, 234.5);

        this.timeline.addTween(cjs.Tween.get(this.shape).wait(235));

        // wheel (mask)
        var mask = new cjs.Shape();
        mask._off = true;
        mask.graphics.p("AhBFPQg9AAAAg9IAAolQAAg0AsgIIARACICDAAIARgCQAsAIAAA0IAAIlQAAA9g9AAg");
        mask.setTransform(305.9, 394.5);

        // rotation
        this.instance = new lib._0633_14_Tween3("synched", 0);
        this.instance.setTransform(307, 449, 1.211, 0.95);

        this.instance.mask = mask;

        this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX: 1.25, x: 306.9, y: 332}, 234).wait(1));

        // wheel (mask)
        var mask_1 = new cjs.Shape();
        mask_1._off = true;
        mask_1.graphics.p("AhBFQQg9AAAAg+IAAokQAAg1AsgHIARAAICDAAIARAAQAsAHAAA1IAAIkQAAA+g9AAg");
        mask_1.setTransform(305.9, 327.5);

        // Layer 5
        this.instance_1 = new lib._0633_14_Tween3copy("synched", 0);
        this.instance_1.setTransform(307, 205, 1.211, 0.95);

        this.instance_2 = new lib._0633_14_Tween3copy2("synched", 0);
        this.instance_2.setTransform(306.9, 332, 1.25, 0.95);

        this.instance_1.mask = this.instance_2.mask = mask_1;

        this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_1}]}).to({state: [{t: this.instance_2}]}, 234).wait(1));
        this.timeline.addTween(cjs.Tween.get(this.instance_1).to({
            _off: true,
            scaleX: 1.25,
            x: 306.9,
            y: 332
        }, 234).wait(1));

        // wheel
        this.instance_3 = new lib._0633_14_Tween05("synched", 0);
        this.instance_3.setTransform(306.7, 360.8, 0.81, 0.81);

        this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(235));

        // Diagram
        this.instance_4 = new lib._0633_14_04();
        this.instance_4.setTransform(218.3, 190);

        this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(235));

        // Layer 4 (mask)
        var mask_2 = new cjs.Shape();
        mask_2._off = true;
        var mask_2_graphics_0 = new cjs.Graphics().p("Eg6HAKZMB0PAAAIAAXmMh0PAAAg");
        var mask_2_graphics_1 = new cjs.Graphics().p("Eg6GAKZMB0NAAAIAAXmMh0NAAAg");
        var mask_2_graphics_2 = new cjs.Graphics().p("Eg6GAKZMB0NAAAIAAXmMh0NAAAg");
        var mask_2_graphics_3 = new cjs.Graphics().p("Eg6GAKZMB0NAAAIAAXmMh0NAAAg");
        var mask_2_graphics_4 = new cjs.Graphics().p("Eg6GAKZMB0NAAAIAAXmMh0NAAAg");
        var mask_2_graphics_5 = new cjs.Graphics().p("Eg6GAKZMB0NAAAIAAXmMh0NAAAg");
        var mask_2_graphics_6 = new cjs.Graphics().p("Eg6GAKZMB0NAAAIAAXmMh0NAAAg");
        var mask_2_graphics_7 = new cjs.Graphics().p("Eg6GAKZMB0NAAAIAAXmMh0NAAAg");
        var mask_2_graphics_8 = new cjs.Graphics().p("Eg6GAKZMB0NAAAIAAXmMh0NAAAg");
        var mask_2_graphics_9 = new cjs.Graphics().p("Eg6GAKZMB0NAAAIAAXmMh0NAAAg");
        var mask_2_graphics_10 = new cjs.Graphics().p("Eg6GAKZMB0NAAAIAAXmMh0NAAAg");
        var mask_2_graphics_11 = new cjs.Graphics().p("Eg6GAKZMB0NAAAIAAXmMh0NAAAg");
        var mask_2_graphics_12 = new cjs.Graphics().p("Eg6GAKZMB0NAAAIAAXmMh0NAAAg");
        var mask_2_graphics_13 = new cjs.Graphics().p("Eg6GAKZMB0NAAAIAAXmMh0NAAAg");
        var mask_2_graphics_14 = new cjs.Graphics().p("Eg6GAKZMB0NAAAIAAXmMh0NAAAg");
        var mask_2_graphics_15 = new cjs.Graphics().p("Eg6GAKZMB0NAAAIAAXmMh0NAAAg");
        var mask_2_graphics_16 = new cjs.Graphics().p("Eg6GAKZMB0NAAAIAAXmMh0NAAAg");
        var mask_2_graphics_17 = new cjs.Graphics().p("Eg6FAKZMB0NAAAIAAXmMh0NAAAg");
        var mask_2_graphics_18 = new cjs.Graphics().p("Eg59AKZMB0NAAAIAAXmMh0NAAAg");
        var mask_2_graphics_19 = new cjs.Graphics().p("Eg50AKZMB0MAAAIAAXmMh0MAAAg");
        var mask_2_graphics_20 = new cjs.Graphics().p("Eg5sAKZMB0MAAAIAAXmMh0MAAAg");
        var mask_2_graphics_21 = new cjs.Graphics().p("Eg5kAKZMB0MAAAIAAXmMh0MAAAg");
        var mask_2_graphics_22 = new cjs.Graphics().p("Eg5cAKZMB0NAAAIAAXmMh0NAAAg");
        var mask_2_graphics_23 = new cjs.Graphics().p("Eg5TAKZMB0MAAAIAAXmMh0MAAAg");
        var mask_2_graphics_24 = new cjs.Graphics().p("Eg5LAKZMB0MAAAIAAXmMh0MAAAg");
        var mask_2_graphics_25 = new cjs.Graphics().p("Eg5DAKZMB0MAAAIAAXmMh0MAAAg");
        var mask_2_graphics_26 = new cjs.Graphics().p("Eg47AKZMB0MAAAIAAXmMh0MAAAg");
        var mask_2_graphics_27 = new cjs.Graphics().p("Eg4yAKZMB0LAAAIAAXmMh0LAAAg");
        var mask_2_graphics_28 = new cjs.Graphics().p("Eg4qAKZMB0MAAAIAAXmMh0MAAAg");
        var mask_2_graphics_29 = new cjs.Graphics().p("Eg4iAKZMB0MAAAIAAXmMh0MAAAg");
        var mask_2_graphics_30 = new cjs.Graphics().p("Eg4ZAKZMB0LAAAIAAXmMh0LAAAg");
        var mask_2_graphics_31 = new cjs.Graphics().p("Eg4RAKZMB0LAAAIAAXmMh0LAAAg");
        var mask_2_graphics_32 = new cjs.Graphics().p("Eg4JAKZMB0LAAAIAAXmMh0LAAAg");
        var mask_2_graphics_33 = new cjs.Graphics().p("Eg4BAKZMB0LAAAIAAXmMh0LAAAg");
        var mask_2_graphics_34 = new cjs.Graphics().p("Eg34AKZMB0LAAAIAAXmMh0LAAAg");
        var mask_2_graphics_35 = new cjs.Graphics().p("Eg3wAKZMB0LAAAIAAXmMh0LAAAg");
        var mask_2_graphics_36 = new cjs.Graphics().p("Eg3oAKZMB0LAAAIAAXmMh0LAAAg");
        var mask_2_graphics_37 = new cjs.Graphics().p("Eg3gAKZMB0LAAAIAAXmMh0LAAAg");
        var mask_2_graphics_38 = new cjs.Graphics().p("Eg3XAKZMB0KAAAIAAXmMh0KAAAg");
        var mask_2_graphics_39 = new cjs.Graphics().p("Eg3PAKZMB0LAAAIAAXmMh0LAAAg");
        var mask_2_graphics_40 = new cjs.Graphics().p("Eg3HAKZMB0LAAAIAAXmMh0LAAAg");
        var mask_2_graphics_41 = new cjs.Graphics().p("Eg2/AKZMB0LAAAIAAXmMh0LAAAg");
        var mask_2_graphics_42 = new cjs.Graphics().p("Eg22AKZMB0KAAAIAAXmMh0KAAAg");
        var mask_2_graphics_43 = new cjs.Graphics().p("Eg2uAKZMB0KAAAIAAXmMh0KAAAg");
        var mask_2_graphics_44 = new cjs.Graphics().p("Eg2mAKZMB0KAAAIAAXmMh0KAAAg");
        var mask_2_graphics_45 = new cjs.Graphics().p("Eg2eAKZMB0LAAAIAAXmMh0LAAAg");
        var mask_2_graphics_46 = new cjs.Graphics().p("Eg2VAKZMB0KAAAIAAXmMh0KAAAg");
        var mask_2_graphics_47 = new cjs.Graphics().p("Eg2NAKaMB0KAAAIAAXmMh0KAAAg");
        var mask_2_graphics_48 = new cjs.Graphics().p("Eg2FAKaMB0KAAAIAAXmMh0KAAAg");
        var mask_2_graphics_49 = new cjs.Graphics().p("Eg19AKaMB0KAAAIAAXmMh0KAAAg");
        var mask_2_graphics_50 = new cjs.Graphics().p("Eg10AKaMB0JAAAIAAXmMh0JAAAg");
        var mask_2_graphics_51 = new cjs.Graphics().p("Eg1sAKaMB0KAAAIAAXmMh0KAAAg");
        var mask_2_graphics_52 = new cjs.Graphics().p("Eg1kAKaMB0KAAAIAAXmMh0KAAAg");
        var mask_2_graphics_53 = new cjs.Graphics().p("Eg1cAKaMB0KAAAIAAXmMh0KAAAg");
        var mask_2_graphics_54 = new cjs.Graphics().p("Eg1TAKaMB0JAAAIAAXmMh0JAAAg");
        var mask_2_graphics_55 = new cjs.Graphics().p("Eg1LAKaMB0JAAAIAAXmMh0JAAAg");
        var mask_2_graphics_56 = new cjs.Graphics().p("Eg1DAKaMB0JAAAIAAXmMh0JAAAg");
        var mask_2_graphics_57 = new cjs.Graphics().p("Eg07AKaMB0KAAAIAAXmMh0KAAAg");
        var mask_2_graphics_58 = new cjs.Graphics().p("Eg0yAKaMB0JAAAIAAXmMh0JAAAg");
        var mask_2_graphics_59 = new cjs.Graphics().p("Eg0qAKaMB0JAAAIAAXmMh0JAAAg");
        var mask_2_graphics_60 = new cjs.Graphics().p("Eg0iAKaMB0JAAAIAAXmMh0JAAAg");
        var mask_2_graphics_61 = new cjs.Graphics().p("Eg0ZAKaMB0IAAAIAAXmMh0IAAAg");
        var mask_2_graphics_62 = new cjs.Graphics().p("Eg0RAKaMB0IAAAIAAXmMh0IAAAg");
        var mask_2_graphics_63 = new cjs.Graphics().p("Eg0JAKaMB0JAAAIAAXmMh0JAAAg");
        var mask_2_graphics_64 = new cjs.Graphics().p("Eg0BAKaMB0JAAAIAAXmMh0JAAAg");
        var mask_2_graphics_65 = new cjs.Graphics().p("Egz4AKaMB0IAAAIAAXmMh0IAAAg");
        var mask_2_graphics_66 = new cjs.Graphics().p("EgzwAKaMB0IAAAIAAXmMh0IAAAg");
        var mask_2_graphics_67 = new cjs.Graphics().p("EgzoAKaMB0IAAAIAAXmMh0IAAAg");
        var mask_2_graphics_68 = new cjs.Graphics().p("EgzgAKaMB0JAAAIAAXmMh0JAAAg");
        var mask_2_graphics_69 = new cjs.Graphics().p("EgzXAKaMB0IAAAIAAXmMh0IAAAg");
        var mask_2_graphics_70 = new cjs.Graphics().p("EgzPAKaMB0IAAAIAAXmMh0IAAAg");
        var mask_2_graphics_71 = new cjs.Graphics().p("EgzHAKaMB0IAAAIAAXmMh0IAAAg");
        var mask_2_graphics_72 = new cjs.Graphics().p("Egy/AKaMB0IAAAIAAXmMh0IAAAg");
        var mask_2_graphics_73 = new cjs.Graphics().p("Egy2AKaMB0HAAAIAAXmMh0HAAAg");
        var mask_2_graphics_74 = new cjs.Graphics().p("EgyuAKaMB0IAAAIAAXmMh0IAAAg");
        var mask_2_graphics_75 = new cjs.Graphics().p("EgymAKaMB0IAAAIAAXmMh0IAAAg");
        var mask_2_graphics_76 = new cjs.Graphics().p("EgyeAKaMB0IAAAIAAXmMh0IAAAg");
        var mask_2_graphics_77 = new cjs.Graphics().p("EgyVAKaMB0HAAAIAAXmMh0HAAAg");
        var mask_2_graphics_78 = new cjs.Graphics().p("EgyNAKaMB0HAAAIAAXmMh0HAAAg");
        var mask_2_graphics_79 = new cjs.Graphics().p("EgyFAKaMB0HAAAIAAXmMh0HAAAg");
        var mask_2_graphics_80 = new cjs.Graphics().p("Egx9AKaMB0IAAAIAAXmMh0IAAAg");
        var mask_2_graphics_81 = new cjs.Graphics().p("Egx0AKaMB0HAAAIAAXmMh0HAAAg");
        var mask_2_graphics_82 = new cjs.Graphics().p("EgxsAKaMB0HAAAIAAXmMh0HAAAg");
        var mask_2_graphics_83 = new cjs.Graphics().p("EgxkAKaMB0HAAAIAAXmMh0HAAAg");
        var mask_2_graphics_84 = new cjs.Graphics().p("EgxcAKaMB0HAAAIAAXmMh0HAAAg");
        var mask_2_graphics_85 = new cjs.Graphics().p("EgxTAKaMB0GAAAIAAXmMh0GAAAg");
        var mask_2_graphics_86 = new cjs.Graphics().p("EgxLAKaMB0HAAAIAAXmMh0HAAAg");
        var mask_2_graphics_87 = new cjs.Graphics().p("EgxDAKaMB0HAAAIAAXmMh0HAAAg");
        var mask_2_graphics_88 = new cjs.Graphics().p("Egw7AKaMB0HAAAIAAXmMh0HAAAg");
        var mask_2_graphics_89 = new cjs.Graphics().p("EgwyAKaMB0GAAAIAAXmMh0GAAAg");
        var mask_2_graphics_90 = new cjs.Graphics().p("EgwqAKaMB0GAAAIAAXmMh0GAAAg");
        var mask_2_graphics_91 = new cjs.Graphics().p("EgwiAKaMB0HAAAIAAXmMh0HAAAg");
        var mask_2_graphics_92 = new cjs.Graphics().p("EgwZAKaMB0GAAAIAAXmMh0GAAAg");
        var mask_2_graphics_93 = new cjs.Graphics().p("EgwRAKaMB0GAAAIAAXmMh0GAAAg");
        var mask_2_graphics_94 = new cjs.Graphics().p("EgwJAKaMB0GAAAIAAXmMh0GAAAg");
        var mask_2_graphics_95 = new cjs.Graphics().p("EgwBAKaMB0GAAAIAAXmMh0GAAAg");
        var mask_2_graphics_96 = new cjs.Graphics().p("Egv4AKaMB0FAAAIAAXmMh0FAAAg");
        var mask_2_graphics_97 = new cjs.Graphics().p("EgvwAKaMB0GAAAIAAXmMh0GAAAg");
        var mask_2_graphics_98 = new cjs.Graphics().p("EgvoAKaMB0GAAAIAAXmMh0GAAAg");
        var mask_2_graphics_99 = new cjs.Graphics().p("EgvgAKaMB0GAAAIAAXmMh0GAAAg");
        var mask_2_graphics_100 = new cjs.Graphics().p("EgvXAKaMB0FAAAIAAXmMh0FAAAg");
        var mask_2_graphics_101 = new cjs.Graphics().p("EgvPAKaMB0FAAAIAAXmMh0FAAAg");
        var mask_2_graphics_102 = new cjs.Graphics().p("EgvHAKaMB0FAAAIAAXmMh0FAAAg");
        var mask_2_graphics_103 = new cjs.Graphics().p("Egu/AKaMB0GAAAIAAXmMh0GAAAg");
        var mask_2_graphics_104 = new cjs.Graphics().p("Egu2AKaMB0FAAAIAAXmMh0FAAAg");
        var mask_2_graphics_105 = new cjs.Graphics().p("EguuAKaMB0FAAAIAAXmMh0FAAAg");
        var mask_2_graphics_106 = new cjs.Graphics().p("EgumAKaMB0FAAAIAAXmMh0FAAAg");
        var mask_2_graphics_107 = new cjs.Graphics().p("EgueAKaMB0FAAAIAAXmMh0FAAAg");
        var mask_2_graphics_108 = new cjs.Graphics().p("EguVAKaMB0EAAAIAAXmMh0EAAAg");
        var mask_2_graphics_109 = new cjs.Graphics().p("EguNAKaMB0FAAAIAAXmMh0FAAAg");
        var mask_2_graphics_110 = new cjs.Graphics().p("EguFAKaMB0FAAAIAAXmMh0FAAAg");
        var mask_2_graphics_111 = new cjs.Graphics().p("Egt9AKaMB0FAAAIAAXmMh0FAAAg");
        var mask_2_graphics_112 = new cjs.Graphics().p("Egt0AKaMB0EAAAIAAXmMh0EAAAg");
        var mask_2_graphics_113 = new cjs.Graphics().p("EgtsAKaMB0EAAAIAAXmMh0EAAAg");
        var mask_2_graphics_114 = new cjs.Graphics().p("EgtkAKaMB0EAAAIAAXmMh0EAAAg");
        var mask_2_graphics_115 = new cjs.Graphics().p("EgtcAKaMB0FAAAIAAXmMh0FAAAg");
        var mask_2_graphics_116 = new cjs.Graphics().p("EgtTAKaMB0EAAAIAAXmMh0EAAAg");
        var mask_2_graphics_117 = new cjs.Graphics().p("EgtLAKaMB0EAAAIAAXmMh0EAAAg");
        var mask_2_graphics_118 = new cjs.Graphics().p("EgtDAKaMB0EAAAIAAXmMh0EAAAg");
        var mask_2_graphics_119 = new cjs.Graphics().p("Egs6AKaMB0DAAAIAAXmMh0DAAAg");
        var mask_2_graphics_120 = new cjs.Graphics().p("EgsyAKaMB0EAAAIAAXmMh0EAAAg");
        var mask_2_graphics_121 = new cjs.Graphics().p("EgsqAKaMB0EAAAIAAXmMh0EAAAg");
        var mask_2_graphics_122 = new cjs.Graphics().p("EgsiAKaMB0EAAAIAAXmMh0EAAAg");
        var mask_2_graphics_123 = new cjs.Graphics().p("EgsZAKaMB0DAAAIAAXmMh0DAAAg");
        var mask_2_graphics_124 = new cjs.Graphics().p("EgsRAKaMB0DAAAIAAXmMh0DAAAg");
        var mask_2_graphics_125 = new cjs.Graphics().p("EgsJAKaMB0DAAAIAAXmMh0DAAAg");
        var mask_2_graphics_126 = new cjs.Graphics().p("EgsBAKaMB0EAAAIAAXmMh0EAAAg");
        var mask_2_graphics_127 = new cjs.Graphics().p("Egr4AKaMB0DAAAIAAXmMh0DAAAg");
        var mask_2_graphics_128 = new cjs.Graphics().p("EgrwAKaMB0DAAAIAAXmMh0DAAAg");
        var mask_2_graphics_129 = new cjs.Graphics().p("EgroAKaMB0DAAAIAAXmMh0DAAAg");
        var mask_2_graphics_130 = new cjs.Graphics().p("EgrgAKaMB0DAAAIAAXmMh0DAAAg");
        var mask_2_graphics_131 = new cjs.Graphics().p("EgrXAKaMB0CAAAIAAXmMh0CAAAg");
        var mask_2_graphics_132 = new cjs.Graphics().p("EgrPAKaMB0DAAAIAAXmMh0DAAAg");
        var mask_2_graphics_133 = new cjs.Graphics().p("EgrHAKaMB0DAAAIAAXmMh0DAAAg");
        var mask_2_graphics_134 = new cjs.Graphics().p("Egq/AKaMB0DAAAIAAXmMh0DAAAg");
        var mask_2_graphics_135 = new cjs.Graphics().p("Egq2AKaMB0CAAAIAAXmMh0CAAAg");
        var mask_2_graphics_136 = new cjs.Graphics().p("EgquAKaMB0CAAAIAAXmMh0CAAAg");
        var mask_2_graphics_137 = new cjs.Graphics().p("EgqmAKaMB0CAAAIAAXmMh0CAAAg");
        var mask_2_graphics_138 = new cjs.Graphics().p("EgqeAKaMB0DAAAIAAXmMh0DAAAg");
        var mask_2_graphics_139 = new cjs.Graphics().p("EgqVAKaMB0CAAAIAAXmMh0CAAAg");
        var mask_2_graphics_140 = new cjs.Graphics().p("EgqNAKaMB0CAAAIAAXmMh0CAAAg");
        var mask_2_graphics_141 = new cjs.Graphics().p("EgqFAKbMB0CAAAIAAXmMh0CAAAg");
        var mask_2_graphics_142 = new cjs.Graphics().p("Egp9AKbMB0CAAAIAAXmMh0CAAAg");
        var mask_2_graphics_143 = new cjs.Graphics().p("Egp0AKbMB0CAAAIAAXmMh0CAAAg");
        var mask_2_graphics_144 = new cjs.Graphics().p("EgpsAKbMB0CAAAIAAXmMh0CAAAg");
        var mask_2_graphics_145 = new cjs.Graphics().p("EgpkAKbMB0CAAAIAAXmMh0CAAAg");
        var mask_2_graphics_146 = new cjs.Graphics().p("EgpcAKbMB0CAAAIAAXmMh0CAAAg");
        var mask_2_graphics_147 = new cjs.Graphics().p("EgpTAKbMB0BAAAIAAXmMh0BAAAg");
        var mask_2_graphics_148 = new cjs.Graphics().p("EgpLAKbMB0BAAAIAAXmMh0BAAAg");
        var mask_2_graphics_149 = new cjs.Graphics().p("EgpDAKbMB0CAAAIAAXmMh0CAAAg");
        var mask_2_graphics_150 = new cjs.Graphics().p("Ego6AKbMB0BAAAIAAXmMh0BAAAg");
        var mask_2_graphics_151 = new cjs.Graphics().p("EgoyAKbMB0BAAAIAAXmMh0BAAAg");
        var mask_2_graphics_152 = new cjs.Graphics().p("EgoqAKbMB0BAAAIAAXmMh0BAAAg");
        var mask_2_graphics_153 = new cjs.Graphics().p("EgoiAKbMB0BAAAIAAXmMh0BAAAg");
        var mask_2_graphics_154 = new cjs.Graphics().p("EgoZAKbMB0AAAAIAAXmMh0AAAAg");
        var mask_2_graphics_155 = new cjs.Graphics().p("EgoRAKbMB0BAAAIAAXmMh0BAAAg");
        var mask_2_graphics_156 = new cjs.Graphics().p("EgoJAKbMB0BAAAIAAXmMh0BAAAg");
        var mask_2_graphics_157 = new cjs.Graphics().p("EgoBAKbMB0BAAAIAAXmMh0BAAAg");
        var mask_2_graphics_158 = new cjs.Graphics().p("Egn4AKbMB0AAAAIAAXmMh0AAAAg");
        var mask_2_graphics_159 = new cjs.Graphics().p("EgnwAKbMB0AAAAIAAXmMh0AAAAg");
        var mask_2_graphics_160 = new cjs.Graphics().p("EgnoAKbMB0AAAAIAAXmMh0AAAAg");
        var mask_2_graphics_161 = new cjs.Graphics().p("EgngAKbMB0BAAAIAAXmMh0BAAAg");
        var mask_2_graphics_162 = new cjs.Graphics().p("EgnXAKbMB0AAAAIAAXmMh0AAAAg");
        var mask_2_graphics_163 = new cjs.Graphics().p("EgnPAKbMB0AAAAIAAXmMh0AAAAg");
        var mask_2_graphics_164 = new cjs.Graphics().p("EgnHAKbMB0AAAAIAAXmMh0AAAAg");
        var mask_2_graphics_165 = new cjs.Graphics().p("Egm/AKbMB0AAAAIAAXmMh0AAAAg");
        var mask_2_graphics_166 = new cjs.Graphics().p("Egm2AKbMBz/AAAIAAXmMhz/AAAg");
        var mask_2_graphics_167 = new cjs.Graphics().p("EgmuAKbMB0AAAAIAAXmMh0AAAAg");
        var mask_2_graphics_168 = new cjs.Graphics().p("EgmmAKbMB0AAAAIAAXmMh0AAAAg");
        var mask_2_graphics_169 = new cjs.Graphics().p("EgmeAKbMB0AAAAIAAXmMh0AAAAg");
        var mask_2_graphics_170 = new cjs.Graphics().p("EgmVAKbMBz/AAAIAAXmMhz/AAAg");
        var mask_2_graphics_171 = new cjs.Graphics().p("EgmNAKbMBz/AAAIAAXmMhz/AAAg");
        var mask_2_graphics_172 = new cjs.Graphics().p("EgmFAKbMB0AAAAIAAXmMh0AAAAg");
        var mask_2_graphics_173 = new cjs.Graphics().p("Egl9AKbMB0AAAAIAAXmMh0AAAAg");
        var mask_2_graphics_174 = new cjs.Graphics().p("Egl0AKbMBz/AAAIAAXmMhz/AAAg");
        var mask_2_graphics_175 = new cjs.Graphics().p("EglsAKbMBz/AAAIAAXmMhz/AAAg");
        var mask_2_graphics_176 = new cjs.Graphics().p("EglkAKbMBz/AAAIAAXmMhz/AAAg");
        var mask_2_graphics_177 = new cjs.Graphics().p("EglbAKbMBz+AAAIAAXmMhz+AAAg");
        var mask_2_graphics_178 = new cjs.Graphics().p("EglTAKbMBz/AAAIAAXmMhz/AAAg");
        var mask_2_graphics_179 = new cjs.Graphics().p("EglLAKbMBz/AAAIAAXmMhz/AAAg");
        var mask_2_graphics_180 = new cjs.Graphics().p("EglDAKbMBz/AAAIAAXmMhz/AAAg");
        var mask_2_graphics_181 = new cjs.Graphics().p("Egk6AKbMBz+AAAIAAXmMhz+AAAg");
        var mask_2_graphics_182 = new cjs.Graphics().p("EgkyAKbMBz+AAAIAAXmMhz+AAAg");
        var mask_2_graphics_183 = new cjs.Graphics().p("EgkqAKbMBz+AAAIAAXmMhz+AAAg");
        var mask_2_graphics_184 = new cjs.Graphics().p("EgkiAKbMBz/AAAIAAXmMhz/AAAg");
        var mask_2_graphics_185 = new cjs.Graphics().p("EgkZAKbMBz+AAAIAAXmMhz+AAAg");
        var mask_2_graphics_186 = new cjs.Graphics().p("EgkRAKbMBz+AAAIAAXmMhz+AAAg");
        var mask_2_graphics_187 = new cjs.Graphics().p("EgkJAKbMBz+AAAIAAXmMhz+AAAg");
        var mask_2_graphics_188 = new cjs.Graphics().p("EgkBAKbMBz+AAAIAAXmMhz+AAAg");
        var mask_2_graphics_189 = new cjs.Graphics().p("Egj4AKbMBz9AAAIAAXmMhz9AAAg");
        var mask_2_graphics_190 = new cjs.Graphics().p("EgjwAKbMBz+AAAIAAXmMhz+AAAg");
        var mask_2_graphics_191 = new cjs.Graphics().p("EgjoAKbMBz+AAAIAAXmMhz+AAAg");
        var mask_2_graphics_192 = new cjs.Graphics().p("EgjgAKbMBz+AAAIAAXmMhz+AAAg");
        var mask_2_graphics_193 = new cjs.Graphics().p("EgjXAKbMBz9AAAIAAXmMhz9AAAg");
        var mask_2_graphics_194 = new cjs.Graphics().p("EgjPAKbMBz9AAAIAAXmMhz9AAAg");
        var mask_2_graphics_195 = new cjs.Graphics().p("EgjHAKbMBz+AAAIAAXmMhz+AAAg");
        var mask_2_graphics_196 = new cjs.Graphics().p("Egi/AKbMBz+AAAIAAXmMhz+AAAg");
        var mask_2_graphics_197 = new cjs.Graphics().p("Egi2AKbMBz9AAAIAAXmMhz9AAAg");
        var mask_2_graphics_198 = new cjs.Graphics().p("EgiuAKbMBz9AAAIAAXmMhz9AAAg");
        var mask_2_graphics_199 = new cjs.Graphics().p("EgimAKbMBz9AAAIAAXmMhz9AAAg");
        var mask_2_graphics_200 = new cjs.Graphics().p("EgieAKbMBz9AAAIAAXmMhz9AAAg");
        var mask_2_graphics_201 = new cjs.Graphics().p("EgiVAKbMBz9AAAIAAXmMhz9AAAg");
        var mask_2_graphics_202 = new cjs.Graphics().p("EgiNAKbMBz9AAAIAAXmMhz9AAAg");
        var mask_2_graphics_203 = new cjs.Graphics().p("EgiFAKbMBz9AAAIAAXmMhz9AAAg");
        var mask_2_graphics_204 = new cjs.Graphics().p("Egh9AKbMBz9AAAIAAXmMhz9AAAg");
        var mask_2_graphics_205 = new cjs.Graphics().p("Egh0AKbMBz8AAAIAAXmMhz8AAAg");
        var mask_2_graphics_206 = new cjs.Graphics().p("EghsAKbMBz8AAAIAAXmMhz8AAAg");
        var mask_2_graphics_207 = new cjs.Graphics().p("EghkAKbMBz9AAAIAAXmMhz9AAAg");
        var mask_2_graphics_208 = new cjs.Graphics().p("EghbAKbMBz8AAAIAAXmMhz8AAAg");
        var mask_2_graphics_209 = new cjs.Graphics().p("EghTAKbMBz8AAAIAAXmMhz8AAAg");
        var mask_2_graphics_210 = new cjs.Graphics().p("EghLAKbMBz8AAAIAAXmMhz8AAAg");
        var mask_2_graphics_211 = new cjs.Graphics().p("EghDAKbMBz8AAAIAAXmMhz8AAAg");
        var mask_2_graphics_212 = new cjs.Graphics().p("Egg6AKbMBz7AAAIAAXmMhz7AAAg");
        var mask_2_graphics_213 = new cjs.Graphics().p("EggyAKbMBz8AAAIAAXmMhz8AAAg");
        var mask_2_graphics_214 = new cjs.Graphics().p("EggqAKbMBz8AAAIAAXmMhz8AAAg");
        var mask_2_graphics_215 = new cjs.Graphics().p("EggiAKbMBz8AAAIAAXmMhz8AAAg");
        var mask_2_graphics_216 = new cjs.Graphics().p("EggZAKbMBz7AAAIAAXmMhz7AAAg");
        var mask_2_graphics_217 = new cjs.Graphics().p("EggRAKbMBz7AAAIAAXmMhz7AAAg");
        var mask_2_graphics_218 = new cjs.Graphics().p("EggJAKbMBz7AAAIAAXmMhz7AAAg");
        var mask_2_graphics_219 = new cjs.Graphics().p("EggBAKbMBz8AAAIAAXmMhz8AAAg");
        var mask_2_graphics_220 = new cjs.Graphics().p("A/4KbMBz7AAAIAAXmMhz7AAAg");
        var mask_2_graphics_221 = new cjs.Graphics().p("A/wKbMBz7AAAIAAXmMhz7AAAg");
        var mask_2_graphics_222 = new cjs.Graphics().p("A/oKbMBz7AAAIAAXmMhz7AAAg");
        var mask_2_graphics_223 = new cjs.Graphics().p("A/gKbMBz7AAAIAAXmMhz7AAAg");
        var mask_2_graphics_224 = new cjs.Graphics().p("A/XKbMBz7AAAIAAXmMhz7AAAg");
        var mask_2_graphics_225 = new cjs.Graphics().p("A/PKbMBz7AAAIAAXmMhz7AAAg");
        var mask_2_graphics_226 = new cjs.Graphics().p("A/HKbMBz7AAAIAAXmMhz7AAAg");
        var mask_2_graphics_227 = new cjs.Graphics().p("A+/KbMBz7AAAIAAXmMhz7AAAg");
        var mask_2_graphics_228 = new cjs.Graphics().p("A+2KbMBz6AAAIAAXmMhz6AAAg");
        var mask_2_graphics_229 = new cjs.Graphics().p("A+uKbMBz6AAAIAAXmMhz6AAAg");
        var mask_2_graphics_230 = new cjs.Graphics().p("A+mKbMBz7AAAIAAXmMhz7AAAg");
        var mask_2_graphics_231 = new cjs.Graphics().p("A+eKbMBz7AAAIAAXmMhz7AAAg");
        var mask_2_graphics_232 = new cjs.Graphics().p("A+VKbMBz6AAAIAAXmMhz6AAAg");
        var mask_2_graphics_233 = new cjs.Graphics().p("A+NKbMBz6AAAIAAXmMhz6AAAg");
        var mask_2_graphics_234 = new cjs.Graphics().p("A+FKcMBz6AAAIAAXmMhz6AAAg");

        this.timeline.addTween(cjs.Tween.get(mask_2).to({
            graphics: mask_2_graphics_0,
            x: 344.3,
            y: 217.6
        }).wait(1).to({graphics: mask_2_graphics_1, x: 345.9, y: 217.6}).wait(1).to({
            graphics: mask_2_graphics_2,
            x: 347.5,
            y: 217.6
        }).wait(1).to({graphics: mask_2_graphics_3, x: 349.2, y: 217.6}).wait(1).to({
            graphics: mask_2_graphics_4,
            x: 350.8,
            y: 217.6
        }).wait(1).to({graphics: mask_2_graphics_5, x: 352.4, y: 217.6}).wait(1).to({
            graphics: mask_2_graphics_6,
            x: 354.1,
            y: 217.6
        }).wait(1).to({graphics: mask_2_graphics_7, x: 355.7, y: 217.6}).wait(1).to({
            graphics: mask_2_graphics_8,
            x: 357.4,
            y: 217.6
        }).wait(1).to({graphics: mask_2_graphics_9, x: 359, y: 217.6}).wait(1).to({
            graphics: mask_2_graphics_10,
            x: 360.6,
            y: 217.6
        }).wait(1).to({graphics: mask_2_graphics_11, x: 362.3, y: 217.6}).wait(1).to({
            graphics: mask_2_graphics_12,
            x: 363.9,
            y: 217.6
        }).wait(1).to({graphics: mask_2_graphics_13, x: 365.6, y: 217.6}).wait(1).to({
            graphics: mask_2_graphics_14,
            x: 367.2,
            y: 217.6
        }).wait(1).to({graphics: mask_2_graphics_15, x: 368.8, y: 217.6}).wait(1).to({
            graphics: mask_2_graphics_16,
            x: 370.5,
            y: 217.6
        }).wait(1).to({graphics: mask_2_graphics_17, x: 372, y: 217.6}).wait(1).to({
            graphics: mask_2_graphics_18,
            x: 372.8,
            y: 217.6
        }).wait(1).to({graphics: mask_2_graphics_19, x: 373.7, y: 217.6}).wait(1).to({
            graphics: mask_2_graphics_20,
            x: 374.5,
            y: 217.6
        }).wait(1).to({graphics: mask_2_graphics_21, x: 375.3, y: 217.6}).wait(1).to({
            graphics: mask_2_graphics_22,
            x: 376.1,
            y: 217.6
        }).wait(1).to({graphics: mask_2_graphics_23, x: 376.9, y: 217.6}).wait(1).to({
            graphics: mask_2_graphics_24,
            x: 377.7,
            y: 217.6
        }).wait(1).to({graphics: mask_2_graphics_25, x: 378.6, y: 217.6}).wait(1).to({
            graphics: mask_2_graphics_26,
            x: 379.4,
            y: 217.6
        }).wait(1).to({graphics: mask_2_graphics_27, x: 380.2, y: 217.6}).wait(1).to({
            graphics: mask_2_graphics_28,
            x: 381,
            y: 217.6
        }).wait(1).to({graphics: mask_2_graphics_29, x: 381.8, y: 217.6}).wait(1).to({
            graphics: mask_2_graphics_30,
            x: 382.6,
            y: 217.6
        }).wait(1).to({graphics: mask_2_graphics_31, x: 383.5, y: 217.6}).wait(1).to({
            graphics: mask_2_graphics_32,
            x: 384.3,
            y: 217.6
        }).wait(1).to({graphics: mask_2_graphics_33, x: 385.1, y: 217.6}).wait(1).to({
            graphics: mask_2_graphics_34,
            x: 385.9,
            y: 217.6
        }).wait(1).to({graphics: mask_2_graphics_35, x: 386.7, y: 217.6}).wait(1).to({
            graphics: mask_2_graphics_36,
            x: 387.5,
            y: 217.6
        }).wait(1).to({graphics: mask_2_graphics_37, x: 388.4, y: 217.6}).wait(1).to({
            graphics: mask_2_graphics_38,
            x: 389.2,
            y: 217.6
        }).wait(1).to({graphics: mask_2_graphics_39, x: 390, y: 217.6}).wait(1).to({
            graphics: mask_2_graphics_40,
            x: 390.8,
            y: 217.6
        }).wait(1).to({graphics: mask_2_graphics_41, x: 391.6, y: 217.6}).wait(1).to({
            graphics: mask_2_graphics_42,
            x: 392.5,
            y: 217.6
        }).wait(1).to({graphics: mask_2_graphics_43, x: 393.3, y: 217.6}).wait(1).to({
            graphics: mask_2_graphics_44,
            x: 394.1,
            y: 217.6
        }).wait(1).to({graphics: mask_2_graphics_45, x: 394.9, y: 217.6}).wait(1).to({
            graphics: mask_2_graphics_46,
            x: 395.7,
            y: 217.6
        }).wait(1).to({graphics: mask_2_graphics_47, x: 396.5, y: 217.6}).wait(1).to({
            graphics: mask_2_graphics_48,
            x: 397.4,
            y: 217.6
        }).wait(1).to({graphics: mask_2_graphics_49, x: 398.2, y: 217.6}).wait(1).to({
            graphics: mask_2_graphics_50,
            x: 399,
            y: 217.6
        }).wait(1).to({graphics: mask_2_graphics_51, x: 399.8, y: 217.6}).wait(1).to({
            graphics: mask_2_graphics_52,
            x: 400.6,
            y: 217.6
        }).wait(1).to({graphics: mask_2_graphics_53, x: 401.4, y: 217.6}).wait(1).to({
            graphics: mask_2_graphics_54,
            x: 402.3,
            y: 217.6
        }).wait(1).to({graphics: mask_2_graphics_55, x: 403.1, y: 217.6}).wait(1).to({
            graphics: mask_2_graphics_56,
            x: 403.9,
            y: 217.6
        }).wait(1).to({graphics: mask_2_graphics_57, x: 404.7, y: 217.6}).wait(1).to({
            graphics: mask_2_graphics_58,
            x: 405.5,
            y: 217.6
        }).wait(1).to({graphics: mask_2_graphics_59, x: 406.3, y: 217.6}).wait(1).to({
            graphics: mask_2_graphics_60,
            x: 407.2,
            y: 217.6
        }).wait(1).to({graphics: mask_2_graphics_61, x: 408, y: 217.6}).wait(1).to({
            graphics: mask_2_graphics_62,
            x: 408.8,
            y: 217.6
        }).wait(1).to({graphics: mask_2_graphics_63, x: 409.6, y: 217.6}).wait(1).to({
            graphics: mask_2_graphics_64,
            x: 410.4,
            y: 217.6
        }).wait(1).to({graphics: mask_2_graphics_65, x: 411.3, y: 217.6}).wait(1).to({
            graphics: mask_2_graphics_66,
            x: 412.1,
            y: 217.6
        }).wait(1).to({graphics: mask_2_graphics_67, x: 412.9, y: 217.6}).wait(1).to({
            graphics: mask_2_graphics_68,
            x: 413.7,
            y: 217.6
        }).wait(1).to({graphics: mask_2_graphics_69, x: 414.5, y: 217.6}).wait(1).to({
            graphics: mask_2_graphics_70,
            x: 415.3,
            y: 217.6
        }).wait(1).to({graphics: mask_2_graphics_71, x: 416.2, y: 217.6}).wait(1).to({
            graphics: mask_2_graphics_72,
            x: 417,
            y: 217.6
        }).wait(1).to({graphics: mask_2_graphics_73, x: 417.8, y: 217.6}).wait(1).to({
            graphics: mask_2_graphics_74,
            x: 418.6,
            y: 217.6
        }).wait(1).to({graphics: mask_2_graphics_75, x: 419.4, y: 217.6}).wait(1).to({
            graphics: mask_2_graphics_76,
            x: 420.2,
            y: 217.6
        }).wait(1).to({graphics: mask_2_graphics_77, x: 421.1, y: 217.6}).wait(1).to({
            graphics: mask_2_graphics_78,
            x: 421.9,
            y: 217.6
        }).wait(1).to({graphics: mask_2_graphics_79, x: 422.7, y: 217.6}).wait(1).to({
            graphics: mask_2_graphics_80,
            x: 423.5,
            y: 217.6
        }).wait(1).to({graphics: mask_2_graphics_81, x: 424.3, y: 217.6}).wait(1).to({
            graphics: mask_2_graphics_82,
            x: 425.1,
            y: 217.6
        }).wait(1).to({graphics: mask_2_graphics_83, x: 426, y: 217.6}).wait(1).to({
            graphics: mask_2_graphics_84,
            x: 426.8,
            y: 217.6
        }).wait(1).to({graphics: mask_2_graphics_85, x: 427.6, y: 217.6}).wait(1).to({
            graphics: mask_2_graphics_86,
            x: 428.4,
            y: 217.6
        }).wait(1).to({graphics: mask_2_graphics_87, x: 429.2, y: 217.6}).wait(1).to({
            graphics: mask_2_graphics_88,
            x: 430,
            y: 217.6
        }).wait(1).to({graphics: mask_2_graphics_89, x: 430.9, y: 217.6}).wait(1).to({
            graphics: mask_2_graphics_90,
            x: 431.7,
            y: 217.6
        }).wait(1).to({graphics: mask_2_graphics_91, x: 432.5, y: 217.6}).wait(1).to({
            graphics: mask_2_graphics_92,
            x: 433.3,
            y: 217.6
        }).wait(1).to({graphics: mask_2_graphics_93, x: 434.1, y: 217.6}).wait(1).to({
            graphics: mask_2_graphics_94,
            x: 435,
            y: 217.7
        }).wait(1).to({graphics: mask_2_graphics_95, x: 435.8, y: 217.7}).wait(1).to({
            graphics: mask_2_graphics_96,
            x: 436.6,
            y: 217.7
        }).wait(1).to({graphics: mask_2_graphics_97, x: 437.4, y: 217.7}).wait(1).to({
            graphics: mask_2_graphics_98,
            x: 438.2,
            y: 217.7
        }).wait(1).to({graphics: mask_2_graphics_99, x: 439, y: 217.7}).wait(1).to({
            graphics: mask_2_graphics_100,
            x: 439.9,
            y: 217.7
        }).wait(1).to({graphics: mask_2_graphics_101, x: 440.7, y: 217.7}).wait(1).to({
            graphics: mask_2_graphics_102,
            x: 441.5,
            y: 217.7
        }).wait(1).to({graphics: mask_2_graphics_103, x: 442.3, y: 217.7}).wait(1).to({
            graphics: mask_2_graphics_104,
            x: 443.1,
            y: 217.7
        }).wait(1).to({graphics: mask_2_graphics_105, x: 443.9, y: 217.7}).wait(1).to({
            graphics: mask_2_graphics_106,
            x: 444.8,
            y: 217.7
        }).wait(1).to({graphics: mask_2_graphics_107, x: 445.6, y: 217.7}).wait(1).to({
            graphics: mask_2_graphics_108,
            x: 446.4,
            y: 217.7
        }).wait(1).to({graphics: mask_2_graphics_109, x: 447.2, y: 217.7}).wait(1).to({
            graphics: mask_2_graphics_110,
            x: 448,
            y: 217.7
        }).wait(1).to({graphics: mask_2_graphics_111, x: 448.8, y: 217.7}).wait(1).to({
            graphics: mask_2_graphics_112,
            x: 449.7,
            y: 217.7
        }).wait(1).to({graphics: mask_2_graphics_113, x: 450.5, y: 217.7}).wait(1).to({
            graphics: mask_2_graphics_114,
            x: 451.3,
            y: 217.7
        }).wait(1).to({graphics: mask_2_graphics_115, x: 452.1, y: 217.7}).wait(1).to({
            graphics: mask_2_graphics_116,
            x: 452.9,
            y: 217.7
        }).wait(1).to({graphics: mask_2_graphics_117, x: 453.8, y: 217.7}).wait(1).to({
            graphics: mask_2_graphics_118,
            x: 454.6,
            y: 217.7
        }).wait(1).to({graphics: mask_2_graphics_119, x: 455.4, y: 217.7}).wait(1).to({
            graphics: mask_2_graphics_120,
            x: 456.2,
            y: 217.7
        }).wait(1).to({graphics: mask_2_graphics_121, x: 457, y: 217.7}).wait(1).to({
            graphics: mask_2_graphics_122,
            x: 457.8,
            y: 217.7
        }).wait(1).to({graphics: mask_2_graphics_123, x: 458.7, y: 217.7}).wait(1).to({
            graphics: mask_2_graphics_124,
            x: 459.5,
            y: 217.7
        }).wait(1).to({graphics: mask_2_graphics_125, x: 460.3, y: 217.7}).wait(1).to({
            graphics: mask_2_graphics_126,
            x: 461.1,
            y: 217.7
        }).wait(1).to({graphics: mask_2_graphics_127, x: 461.9, y: 217.7}).wait(1).to({
            graphics: mask_2_graphics_128,
            x: 462.7,
            y: 217.7
        }).wait(1).to({graphics: mask_2_graphics_129, x: 463.6, y: 217.7}).wait(1).to({
            graphics: mask_2_graphics_130,
            x: 464.4,
            y: 217.7
        }).wait(1).to({graphics: mask_2_graphics_131, x: 465.2, y: 217.7}).wait(1).to({
            graphics: mask_2_graphics_132,
            x: 466,
            y: 217.7
        }).wait(1).to({graphics: mask_2_graphics_133, x: 466.8, y: 217.7}).wait(1).to({
            graphics: mask_2_graphics_134,
            x: 467.6,
            y: 217.7
        }).wait(1).to({graphics: mask_2_graphics_135, x: 468.5, y: 217.7}).wait(1).to({
            graphics: mask_2_graphics_136,
            x: 469.3,
            y: 217.7
        }).wait(1).to({graphics: mask_2_graphics_137, x: 470.1, y: 217.7}).wait(1).to({
            graphics: mask_2_graphics_138,
            x: 470.9,
            y: 217.7
        }).wait(1).to({graphics: mask_2_graphics_139, x: 471.7, y: 217.7}).wait(1).to({
            graphics: mask_2_graphics_140,
            x: 472.5,
            y: 217.7
        }).wait(1).to({graphics: mask_2_graphics_141, x: 473.4, y: 217.7}).wait(1).to({
            graphics: mask_2_graphics_142,
            x: 474.2,
            y: 217.7
        }).wait(1).to({graphics: mask_2_graphics_143, x: 475, y: 217.7}).wait(1).to({
            graphics: mask_2_graphics_144,
            x: 475.8,
            y: 217.7
        }).wait(1).to({graphics: mask_2_graphics_145, x: 476.6, y: 217.7}).wait(1).to({
            graphics: mask_2_graphics_146,
            x: 477.5,
            y: 217.7
        }).wait(1).to({graphics: mask_2_graphics_147, x: 478.3, y: 217.7}).wait(1).to({
            graphics: mask_2_graphics_148,
            x: 479.1,
            y: 217.7
        }).wait(1).to({graphics: mask_2_graphics_149, x: 479.9, y: 217.7}).wait(1).to({
            graphics: mask_2_graphics_150,
            x: 480.7,
            y: 217.7
        }).wait(1).to({graphics: mask_2_graphics_151, x: 481.5, y: 217.7}).wait(1).to({
            graphics: mask_2_graphics_152,
            x: 482.4,
            y: 217.7
        }).wait(1).to({graphics: mask_2_graphics_153, x: 483.2, y: 217.7}).wait(1).to({
            graphics: mask_2_graphics_154,
            x: 484,
            y: 217.7
        }).wait(1).to({graphics: mask_2_graphics_155, x: 484.8, y: 217.7}).wait(1).to({
            graphics: mask_2_graphics_156,
            x: 485.6,
            y: 217.7
        }).wait(1).to({graphics: mask_2_graphics_157, x: 486.4, y: 217.7}).wait(1).to({
            graphics: mask_2_graphics_158,
            x: 487.3,
            y: 217.7
        }).wait(1).to({graphics: mask_2_graphics_159, x: 488.1, y: 217.7}).wait(1).to({
            graphics: mask_2_graphics_160,
            x: 488.9,
            y: 217.7
        }).wait(1).to({graphics: mask_2_graphics_161, x: 489.7, y: 217.7}).wait(1).to({
            graphics: mask_2_graphics_162,
            x: 490.5,
            y: 217.7
        }).wait(1).to({graphics: mask_2_graphics_163, x: 491.3, y: 217.7}).wait(1).to({
            graphics: mask_2_graphics_164,
            x: 492.2,
            y: 217.7
        }).wait(1).to({graphics: mask_2_graphics_165, x: 493, y: 217.7}).wait(1).to({
            graphics: mask_2_graphics_166,
            x: 493.8,
            y: 217.7
        }).wait(1).to({graphics: mask_2_graphics_167, x: 494.6, y: 217.7}).wait(1).to({
            graphics: mask_2_graphics_168,
            x: 495.4,
            y: 217.7
        }).wait(1).to({graphics: mask_2_graphics_169, x: 496.3, y: 217.7}).wait(1).to({
            graphics: mask_2_graphics_170,
            x: 497.1,
            y: 217.7
        }).wait(1).to({graphics: mask_2_graphics_171, x: 497.9, y: 217.7}).wait(1).to({
            graphics: mask_2_graphics_172,
            x: 498.7,
            y: 217.7
        }).wait(1).to({graphics: mask_2_graphics_173, x: 499.5, y: 217.7}).wait(1).to({
            graphics: mask_2_graphics_174,
            x: 500.3,
            y: 217.7
        }).wait(1).to({graphics: mask_2_graphics_175, x: 501.2, y: 217.7}).wait(1).to({
            graphics: mask_2_graphics_176,
            x: 502,
            y: 217.7
        }).wait(1).to({graphics: mask_2_graphics_177, x: 502.8, y: 217.7}).wait(1).to({
            graphics: mask_2_graphics_178,
            x: 503.6,
            y: 217.7
        }).wait(1).to({graphics: mask_2_graphics_179, x: 504.4, y: 217.7}).wait(1).to({
            graphics: mask_2_graphics_180,
            x: 505.2,
            y: 217.7
        }).wait(1).to({graphics: mask_2_graphics_181, x: 506.1, y: 217.7}).wait(1).to({
            graphics: mask_2_graphics_182,
            x: 506.9,
            y: 217.7
        }).wait(1).to({graphics: mask_2_graphics_183, x: 507.7, y: 217.7}).wait(1).to({
            graphics: mask_2_graphics_184,
            x: 508.5,
            y: 217.7
        }).wait(1).to({graphics: mask_2_graphics_185, x: 509.3, y: 217.7}).wait(1).to({
            graphics: mask_2_graphics_186,
            x: 510.1,
            y: 217.7
        }).wait(1).to({graphics: mask_2_graphics_187, x: 511, y: 217.7}).wait(1).to({
            graphics: mask_2_graphics_188,
            x: 511.8,
            y: 217.8
        }).wait(1).to({graphics: mask_2_graphics_189, x: 512.6, y: 217.8}).wait(1).to({
            graphics: mask_2_graphics_190,
            x: 513.4,
            y: 217.8
        }).wait(1).to({graphics: mask_2_graphics_191, x: 514.2, y: 217.8}).wait(1).to({
            graphics: mask_2_graphics_192,
            x: 515,
            y: 217.8
        }).wait(1).to({graphics: mask_2_graphics_193, x: 515.9, y: 217.8}).wait(1).to({
            graphics: mask_2_graphics_194,
            x: 516.7,
            y: 217.8
        }).wait(1).to({graphics: mask_2_graphics_195, x: 517.5, y: 217.8}).wait(1).to({
            graphics: mask_2_graphics_196,
            x: 518.3,
            y: 217.8
        }).wait(1).to({graphics: mask_2_graphics_197, x: 519.1, y: 217.8}).wait(1).to({
            graphics: mask_2_graphics_198,
            x: 520,
            y: 217.8
        }).wait(1).to({graphics: mask_2_graphics_199, x: 520.8, y: 217.8}).wait(1).to({
            graphics: mask_2_graphics_200,
            x: 521.6,
            y: 217.8
        }).wait(1).to({graphics: mask_2_graphics_201, x: 522.4, y: 217.8}).wait(1).to({
            graphics: mask_2_graphics_202,
            x: 523.2,
            y: 217.8
        }).wait(1).to({graphics: mask_2_graphics_203, x: 524, y: 217.8}).wait(1).to({
            graphics: mask_2_graphics_204,
            x: 524.9,
            y: 217.8
        }).wait(1).to({graphics: mask_2_graphics_205, x: 525.7, y: 217.8}).wait(1).to({
            graphics: mask_2_graphics_206,
            x: 526.5,
            y: 217.8
        }).wait(1).to({graphics: mask_2_graphics_207, x: 527.3, y: 217.8}).wait(1).to({
            graphics: mask_2_graphics_208,
            x: 528.1,
            y: 217.8
        }).wait(1).to({graphics: mask_2_graphics_209, x: 528.9, y: 217.8}).wait(1).to({
            graphics: mask_2_graphics_210,
            x: 529.8,
            y: 217.8
        }).wait(1).to({graphics: mask_2_graphics_211, x: 530.6, y: 217.8}).wait(1).to({
            graphics: mask_2_graphics_212,
            x: 531.4,
            y: 217.8
        }).wait(1).to({graphics: mask_2_graphics_213, x: 532.2, y: 217.8}).wait(1).to({
            graphics: mask_2_graphics_214,
            x: 533,
            y: 217.8
        }).wait(1).to({graphics: mask_2_graphics_215, x: 533.8, y: 217.8}).wait(1).to({
            graphics: mask_2_graphics_216,
            x: 534.7,
            y: 217.8
        }).wait(1).to({graphics: mask_2_graphics_217, x: 535.5, y: 217.8}).wait(1).to({
            graphics: mask_2_graphics_218,
            x: 536.3,
            y: 217.8
        }).wait(1).to({graphics: mask_2_graphics_219, x: 537.1, y: 217.8}).wait(1).to({
            graphics: mask_2_graphics_220,
            x: 537.9,
            y: 217.8
        }).wait(1).to({graphics: mask_2_graphics_221, x: 538.8, y: 217.8}).wait(1).to({
            graphics: mask_2_graphics_222,
            x: 539.6,
            y: 217.8
        }).wait(1).to({graphics: mask_2_graphics_223, x: 540.4, y: 217.8}).wait(1).to({
            graphics: mask_2_graphics_224,
            x: 541.2,
            y: 217.8
        }).wait(1).to({graphics: mask_2_graphics_225, x: 542, y: 217.8}).wait(1).to({
            graphics: mask_2_graphics_226,
            x: 542.8,
            y: 217.8
        }).wait(1).to({graphics: mask_2_graphics_227, x: 543.7, y: 217.8}).wait(1).to({
            graphics: mask_2_graphics_228,
            x: 544.5,
            y: 217.8
        }).wait(1).to({graphics: mask_2_graphics_229, x: 545.3, y: 217.8}).wait(1).to({
            graphics: mask_2_graphics_230,
            x: 546.1,
            y: 217.8
        }).wait(1).to({graphics: mask_2_graphics_231, x: 546.9, y: 217.8}).wait(1).to({
            graphics: mask_2_graphics_232,
            x: 547.7,
            y: 217.8
        }).wait(1).to({graphics: mask_2_graphics_233, x: 548.6, y: 217.8}).wait(1).to({
            graphics: mask_2_graphics_234,
            x: 549.4,
            y: 217.8
        }).wait(1));

        // lines movt.
        this.instance_5 = new lib._0633_14_Symbol2("synched", 0);
        this.instance_5.setTransform(547.1, 362.7, 0.114, 0.4, 0, 0, 0, 18.1, 16.4);
        this.instance_5.alpha = 0.461;

        this.instance_5.mask = mask_2;

        this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(235));

        // Mask (mask)
        var mask_3 = new cjs.Shape();
        mask_3._off = true;
        mask_3.graphics.p("EAGdAh/IAA33MAwQAAAIAAX3g");
        mask_3.setTransform(350.2, 217.6);

        // Layer 2
        this.instance_6 = new lib.Tween7("synched", 0);
        this.instance_6.setTransform(348.5, 360);

        this.instance_7 = new lib.Tween9("synched", 0);
        this.instance_7.setTransform(732.3, 360);

        this.instance_6.mask = this.instance_7.mask = mask_3;

        this.timeline.addTween(cjs.Tween.get({}).to({state: [{t: this.instance_6}]}).to({state: [{t: this.instance_7}]}, 234).wait(1));
        this.timeline.addTween(cjs.Tween.get(this.instance_6).to({_off: true, x: 732.3}, 234).wait(1));

        // Blue colour
        this.instance_8 = new lib._0633_14_Tween11("synched", 0);
        this.instance_8.setTransform(544.6, 358.5, 0.81, 0.81);

        this.instance_8.mask = mask_3;

        this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(235));

    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = new cjs.Rectangle(-41.5, 189, 819.8, 318.8);


    (lib.main = function () {
        this.initialize();

        // gauge
        this.gauge1 = new lib.gauge1();
        this.gauge1.setTransform(68, 366.5);

        this.gauge = new lib.gaugecopy();
        this.gauge.setTransform(68, 223.5);

        // Screw Pump
        this.pump = new lib.pump();
        this.pump.setTransform(-82.4, -53.5, 1.083, 1.083);

        this.addChild(this.pump, this.gauge, this.gauge1);
    }).prototype = p = new cjs.Container();
    p.nominalBounds = new cjs.Rectangle(-120.3, 16.5, 879.6, 660.9);


// stage content:
    (lib._14a = function (mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {});

        // timeline functions:
        this.frame_1 = function () {
            /// set the scope for functions:
            /// in javascript, 'this' refers to a function's/object's scope.
            var scope = this;

            var delay = 60; /// delay in milliseconds
            var allowPlay;
            var frameStep = 1;


            /// set up Gauge
            var gauge = this.main.gauge;
            gauge.init(7, 5); /// number of digits, digit unit
            gauge.setOscillate(10, .5); /// if the Gauge needs to be oscillating, set the values (degrees, speed)

            /// array of Gauge values
            gauge.setValues([0, 2.3, 2.4, 2.5, 2.6, 2.8, 3, 3.2, 3.5, 3.8, 4.1, 4.5, 5.0, 5.6, 6.4, 7.5, 9.0, 11.3, 15, 22.5]);


            // set up Gauge2
            var gauge1 = this.main.gauge1;
            gauge1.init(11, 2); /// number of digits, digit unit
            gauge1.setOscillate(10, .2); /// if the Gauge needs to be oscillating, set the values (degrees, speed)

            /// array of Gauge values
            gauge1.setValues([0, 1.2, 1.3, 1.3, 1.3, 1.3, 1.4, 1.4, 2, 2, 2.1, 2.2, 2.3, 2.4, 3, 4, 4.4, 5.4, 7.1, 11]);


            /// list of actors that have timeline
            var actors = [
                this.main.pump
            ];

            var numActors = actors.length;

            //	main loop --------------------------------------------------------------------
            //	all Actors with timelines have Labels at the end with flag "end"
            //
            function run() {
                for (var i = 0; i < numActors; i++) {
                    var actor = actors[i];
                    actor.gotoAndStop(actor.currentFrame + frameStep)

                    if (actor.getCurrentLabel() == "end") {
                        actor.gotoAndStop(0);
                    }
                }
                if (allowPlay) {
                    setTimeout(run, delay);
                }

            }

            //scope.main.cPanel.btnStart.bstop.visible = false;

            /// Events for control panel buttons --------------------------------------------
            //scope.main.cPanel.btnStart.addEventListener("mousedown", btnStartHandler);

            function startHandler(e) {
                if (allowPlay) return;
                allowPlay = true;
                run();
                gauge.up();
                gauge.start(); // starts oscillation
                gauge1.up();
                gauge1.start(); // starts oscillation
                if (e) e.preventDefault();
            }


            function stopHandler(e) {
                allowPlay = false;
                gauge.stop();
                gauge1.stop();
                delay = 100;
                frameStep = 1;
                //scope.main.cPanel.btnStart.bstop.visible = allowPlay;
                if (e) e.preventDefault();
            }


            //scope.main.cPanel.btnUp.addEventListener("mousedown", btnUpHandler);

            function increaseHandler(e) {
                if (!allowPlay) {
                    return;
                }

                if (delay > 11) {
                    delay = Math.round(delay * .7);
                }
                if (delay < 30) frameStep = 2;

                gauge.up();
                gauge1.up();
                e.preventDefault();
            }


            //scope.main.cPanel.btnDown.addEventListener("mousedown", btnDownHandler);


            function decreaseHandler(e) {
                if (!allowPlay) {
                    return;
                }
                if (delay < 100) {
                    delay += 5;
                }
                if (delay >= 30) frameStep = 1;

                gauge.down();
                gauge1.down();
                if (gauge.hasStopped()) {
                    btnStartHandler(null);
                }

                e.preventDefault();
            }


            ///--------------------------------------------------------------------------------


            /// stop all default animations
            for (var i = 0; i < numActors; i++) {
                actors[i].gotoAndStop(0);
            }


            if (canvas) {
                canvas.addEventListener("clearCreateJS", clearCJS);
                canvas.addEventListener("start", startHandler);
                canvas.addEventListener("stop", stopHandler);
                canvas.addEventListener("increase", increaseHandler);
                canvas.addEventListener("decrease", decreaseHandler);

            }


            function clearCJS() {
                canvas.removeEventListener("start", startHandler);
                canvas.removeEventListener("stop", stopHandler);
                canvas.removeEventListener("increase", increaseHandler);
                canvas.removeEventListener("decrease", decreaseHandler);
                canvas.removeEventListener("clearCreateJS", clearCJS);
                allowPlay = false;
                gauge.stop();
                gauge1.stop();
                scope.main.removeAllEventListeners();
                //scope.main.cPanel.btnStart.removeEventListener("mousedown", btnStartHandler);
                //scope.main.cPanel.btnUp.removeEventListener("mousedown", btnUpHandler);
                //scope.main.cPanel.btnDown.removeEventListener("mousedown", btnDownHandler);
            }

            createjs.Ticker.removeEventListener("tick", stage);
            lib.stage = stage;
            this.stop();

        }

        // actions tween:
        this.timeline.addTween(cjs.Tween.get(this).wait(1).call(this.frame_1).wait(1));

        // main
        this.main = new lib.main();
        this.main.setTransform(-1.3, -203.1, 1.349, 1.349);

        this.timeline.addTween(cjs.Tween.get(this.main).wait(2));

    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = new cjs.Rectangle(348.4, 54.2, 1186.8, 768.5);


    var canvas, stage, exportRoot;

    function init() {
        canvas = document.createElement('canvas');
        canvas.width = lib.properties.width;
        canvas.height = lib.properties.height;
        lib.canvas = canvas;

        images = images || {};

        var loader = new createjs.LoadQueue(false);
        loader.addEventListener("fileload", handleFileLoad);
        loader.addEventListener("complete", handleComplete);
        loader.loadManifest(lib.properties.manifest);
    }

    function handleFileLoad(evt) {
        if (evt.item.type == "image") {
            images[evt.item.id] = evt.result;
        }
    }

    function handleComplete(evt) {
        exportRoot = new lib._14a();

        stage = new createjs.Stage(canvas);
        stage.addChild(exportRoot);
        stage.update();

        createjs.Ticker.setFPS(lib.properties.fps);
        createjs.Ticker.addEventListener("tick", stage);
    }

    for (var i = 0, k = lib.properties.manifest.length; i < k; i++) {
        var obj = lib.properties.manifest[i];
        obj.src = 'res/' + obj.src;
        img[obj.id] = obj.src;
    }
    init();
    lib.dispose = function () {
        createjs.Ticker.removeEventListener("tick", stage);
        createjs.Tween.removeAllTweens();
        var evt = document.createEvent("Event");
        evt.initEvent("clearCreateJS", true, false);
        canvas.dispatchEvent(evt);
        if (stage) stage.removeAllChildren();
        stage = null;
        exportRoot = null;
    };
    return lib;
});