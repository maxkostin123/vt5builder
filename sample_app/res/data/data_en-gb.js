define({
    // product info
    product: {
        type: "cbt", // Course type. Other types: cobalt, interactive
        courseCode: "1001",
        title: "VT5",
        appPath: "1001/"
    },
    //scorm info
    scorm: {
        id: "VT5_SCORM_TEST_2", // no illegal characters or spaces!
        passScore: 0, // minimum pass scrore
        title: "VT5 SCORM TEST 3"
    },

    addon: {
        css: "res/assets/addon/pengineering.css"
    },

    //introLogo: "res/assets/media/images/sample_logo.png", // If present, custom logo will be displayed at loading screen instead of VT logo.
    // UI
    ui: {
        mainNav: {
            useCaps: false, //Nav titles will be converted to UPPER CASE.
            showAllPagesInNav: false, // If set to true, all pages will be accessible from the Menu. Leave as 'false'.
            soundControls: [
                {
                    type: "toggle",
                    cssClass: "btn-icon",
                    state1: {
                        type: "button",
                        event: "SoundEvent.pause",
                        icon: "vti_pause"
                    },
                    state2: {
                        type: "button",
                        event: "SoundEvent.play",
                        icon: "vti_play"
                    }
                },
                {
                    cssClass: "btn-icon",
                    type: "button",
                    event: "SoundEvent.replay",
                    icon: "vti_play-step-back"
                }
            ],

            menu: {
                main: "Main Menu",
                back: "Back",
                close: "Close Menu",
                search: "Search..."
            },

            toolButtons: [
                {
                    type: "tool",
                    label: "Help",
                    icon: "vti_help",
                    event: "UserEvent.showModal",
                    payload: "system-help",
                    show: true
                },
                {
                    type: "tool",
                    label: "About",
                    icon: "vti_info",
                    event: "UserEvent.showModal",
                    payload: "system-about",
                    show: true
                },
                {
                    type: "tool",
                    label: "Glossary",
                    icon: "vti_glossary",
                    event: "UserEvent.showModal",
                    payload: "glossary",
                    show: true
                },
                {
                    type: "tool",
                    label: "Feedback",
                    icon: "vti_feedback",
                    event: "UserEvent.feedback",
                    show: true
                },
                {
                    type: "settings",
                    label: "Settings",
                    icon: "vti_settings",
                    event: "UserEvent.showModal",
                    show: true,
                    payload: "system-settings",
                    theme: {
                        label: "Theme",
                        theme1: "Light",
                        theme2: "Dark"
                    },
                    language: "Language"

                }

            ]
        }
    },
    /// Data =========================================================================================================
    data: {
        content: [
            {
                type: "page",
                CID: "m0_0", // Content ID. This is used as a unique identifier to be used with hyper links. Optional.
                nav_item: true, // Optional. If set to true, the item will appear on main Menu if it's the 1st level.
                nav_icon: "vti_home", // Menu item can have an icon. Optional.
                nav_title: "Home", // Optional. If present, title will appear as menu item. Only relevant for top-level items.
                hideHeader: true, // Optional. If true, title 'breadcrumb' on this page will not appear.
                /*
                 Layout of the Page. The following are currently supported:
                 'splash' - for splash pages
                 'page-cols' (columns)
                 'none'
                 */
                layout: "splash",
                notCountable: true, // the page will not be counted in the Paginator and will not display page numbers.
                backgroundImage: "res/assets/media/images/home_splash.jpg", // background image
                backgroundOverlay: "rgba(0,0,0,.1), rgba(0,0,0,.4)", //background gradient overlay
                body: [ // Any element with property 'body' is treated as a Page. This is what will be rendered in the browser.

                    {
                        type: "group",
                        items: [
                            {
                                type: "text", // text element
                                text: "<h1 class='big-num'>1</h1>" +
                                "<h1>COURSE TITLE</h1>" +
                                "<h2>SPLASH PAGE SUB-HEADER</h2>"
                            },
                            {
                                type: "media",
                                kind: "image",
                                src: "res/assets/media/images/sample_logo.png",
                                background: "-1"
                            }
                        ]
                    }
                ]
            },

            /* MAIN ======================================================================== */
            {
                title: "Main",
                nav_title: "Main",
                content: [

                    {
                        type: "page",
                        title: "Why is this training important?",
                        nav_title: "Why is this training important?",
                        nav_icon: "vti_vr",
                        nav_item: true,
                        layout: "page-cols",
                        body: [
                            {
                                type: "group",
                                style: "panel-border",
                                items: [
                                    {
                                        type: "text",
                                        text: "<h1>Actions</h1>" +
                                        "<p>" +
                                        "<p>If you discover a fire, the quality of your actions might determine whether it is extinguished quickly or whether it engulfs the ship.</p>" +
                                        "<p>Fires begin <strong>suddenly and without warning</strong> and you should always be prepared to be called on in an emergency.</p>" +
                                        "<p>People find it difficult to believe that they will ever have to deal with this situation so they do not prepare for it. The result can be <strong>shock</strong>, <strong>hesitation</strong> and <strong>fatal wrong decisions</strong>. </p>" +
                                        "<h4>Training other crew members</h4>" +
                                        "<p>If you are responsible for training others then you should aim to follow the instructions in this section as closely as possible. It will help you to learn how to run effective training sessions with the crew.</p>"
                                    }
                                ]
                            },

                            {
                                type: "group",
                                style: "panel-border",
                                items: [
                                    {
                                        type: "text",
                                        text: "<h1>Preparation</h1>" +
                                        "<p></p>" +
                                        "<h5>Think about the group</h5>" +
                                        "<p>How much do they already know about fire fighting? What questions might they ask you? What do you want this session to achieve?</p>" +
                                        "<h5>Watch the video</h5>" +
                                        "<p>It is important to familiarise yourself with the video in advance, so that you can anticipate possible questions from the group and research your answers.</p>" +
                                        "<h5>Read the Reference</h5>" +
                                        "<p>The Reference reinforces the key points and contains exercises and material that will be of use to expand on the learning, to broaden out the discussion or to answer questions that participants may have.</p>"
                                    }
                                ]
                            }
                        ]
                    },
                    // TYPOGRAPHY -------------------------------------------------------
                    {
                        type: "page",
                        CID: "typography",
                        title: "Typography",
                        nav_title: "Typography", // If present, will appear in 'bread crumbs' and in the menu, if it's a top-level item.
                        nav_item: true, // If true, will appear in the menu.
                        layout: "page-cols", // Page layout. Optional. Default is "page-cols".
                        body: [

                            {
                                type: "group",
                                style: "panel",
                                items: [
                                    {
                                        type: "text",
                                        text: "<h1>H1 title text</h1>" +
                                        "<h2>h2 subtitle text</h2>" +
                                        "<p><h3>H3 page header</h3></p>" +
                                        "<p><h4>H4 page sub-header</h4>" +
                                        "There are many advantages to this particular recommendation.</p>"
                                    },

                                    {
                                        type: "text",
                                        text: "<h5>H5 List title</h5>" +
                                        "<ul>" +
                                        "<li>List item</li>" +
                                        "<li>List item</li>" +
                                        "<li>List item</li>" +
                                        "</ul>"
                                    },

                                    {
                                        type: "media",
                                        kind: "image",
                                        /**
                                         * Background is optional.
                                         * If set to "-1", image will have no background.
                                         * If set to a value "#FFCC00", image will be of that HEX value.
                                         */
                                        background: "#000000",
                                        src: 'res/assets/media/images/home_splash.jpg'
                                    },
                                    {
                                        type: "legend",
                                        kind: "text",
                                        text: "Text legend. Used for short image descriptions. sfjhksh sadkjhadsjkh sadjkhsdjf hsjdhjsdhsd fdssdhafjsdhjsdk sdhsdjhsdajkfh"
                                    }

                                ]
                            },

                            {
                                type: "group",
                                items: [
                                    {
                                        type: "text",
                                        text: "<blockquote>Blockquote text. There are many advantages to this particular recommendation. These are the official, standard, vendor-neutral encodings for these characters according to both Unicode and ISO-10646, so you don't need to worry about them not working in the future." +
                                        "<p>Karl Marx, March 14 1872</p></blockquote>"
                                    },
                                    {
                                        type: "text",
                                        text: "<em>This is an example of Important markup.</em>"
                                    },
                                    {
                                        type: "text",
                                        text: "<p class='warning'>This is an example of a warning. Use sparingly</p>"
                                    },
                                    {
                                        type: "text",
                                        text: "<p></p><p class='justified'>This is justified text. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>"
                                    }
                                ]
                            }

                        ]
                    },

                    {
                        type: "page",
                        title: "Typography in action",
                        nav_title: "Typography in action", // If present, will appear in 'bread crumbs' and in the menu, if it's a top-level item.
                        nav_item: true, // If true, will appear in the menu.
                        layout: "page-cols", // Page layout. Optional. Default is "page-cols".
                        body: [
                            {
                                type: "group",
                                items: [
                                    {
                                        type: "text",
                                        text: "<h4>This is Page Sub-header.</h4>" +
                                        "<p class='justified'>Paragraph text following the sub-header. Convolutional Neural Networks are great: they recognize things, places and people in your personal photos, signs, people and lights in self-driving cars, crops, forests and traffic in aerial imagery, various anomalies in medical images and all kinds of other useful things. But once in a while these powerful visual recognition models can also be warped for distraction, fun and amusement.</p>" +
                                        "<h4>Second paragraph.</h4>" +
                                        "<p class='justified'>Machine learning is the idea that there are generic algorithms that can tell you something interesting about a set of data without you having to write any custom code specific to the problem. Instead of writing code, you feed data to the generic algorithm and it builds its own logic based on the data.</p>"
                                    },
                                    {
                                        type: "text",
                                        text: "<em>This is an example of Important markup.</em>"
                                    },
                                    {
                                        type: "text",
                                        text: "<blockquote>Blockquote text. There are many advantages to this particular recommendation." +
                                        "<p>Karl Marx, March 14 1872</p></blockquote>"
                                    },
                                    {
                                        type: "text",
                                        text: "<p class='warning'>This is an example of a warning. Use sparingly</p>"
                                    }
                                ]
                            },
                            {
                                type: "v-divider"
                            },
                            {
                                type: "group",
                                items: [
                                    {
                                        type: "text",
                                        text: "<h5>This an Ordered List header.</h5>" +
                                        "<ol>" +
                                        "<li>List item 1.</li>" +
                                        "<li>There are many advantages to this particular recommendation.</li>" +
                                        "<li>There are many advantages to this particular recommendation.</li>" +
                                        "</ol>"
                                    },
                                    {
                                        type: "text",
                                        text: "<h5>This a Unordered List header.</h5>" +
                                        "<p>Paragraph with no bottom margin Machine learning is the idea that there are generic algorithms that can tell you something interesting about a set of data without you having to write any custom code specific to the problem.</p>" +
                                        "<ul>" +
                                        "<li>List item 1.</li>" +
                                        "<li>There are many advantages to this particular recommendation.</li>" +
                                        "<li>There are many advantages to this particular recommendation.</li>" +
                                        "</ul>" +
                                        "<p>Machine learning is the idea that there are generic algorithms that can tell you something interesting about a set of data without you having to write any custom code specific to the problem.</p>"
                                    }

                                ]
                            }
                        ]
                    },

                    // BUTTONS -------------------------------------------------------
                    {
                        type: "page",
                        title: "Buttons",
                        nav_title: "Buttons",
                        body: [
                            {
                                type: "group",
                                style: "panel",
                                items: [

                                    {
                                        type: "text",
                                        text: "Primary:"

                                    },

                                    {
                                        type: "button",
                                        cssClass: "btn-primary",
                                        label: "Primary",
                                        event: "UserEvent.doStuff",
                                        payload: "anything"
                                    },

                                    {
                                        type: "button",
                                        cssClass: "btn-primary",
                                        label: "Primary",
                                        icon: "vti_settings"
                                    },

                                    {
                                        type: "button",
                                        cssClass: "btn-primary",
                                        label: "Primary",
                                        icon: "vti_settings",
                                        iconPosition: "right"
                                    },

                                    {
                                        type: "text",
                                        text: "Secondary:"
                                    },

                                    {
                                        type: "button",
                                        cssClass: "btn-secondary",
                                        label: "Secondary"
                                    },

                                    {
                                        type: "button",
                                        cssClass: "btn-secondary",
                                        label: "Secondary",
                                        icon: "vti_settings"
                                    },

                                    {
                                        type: "button",
                                        cssClass: "btn-secondary disabled",
                                        label: "Disabled",
                                        icon: "vti_settings",
                                        iconPosition: "right"
                                    },
                                    {
                                        type: "button",
                                        cssClass: "btn-secondary",
                                        icon: "vti_check"
                                    },
                                    {
                                        type: "text",
                                        text: "Toggle:"
                                    },
                                    {
                                        type: "toggle",
                                        state1: {
                                            type: "button",
                                            label: "Toggle A",
                                            icon: "vti_play",
                                            event: "UserEvent.A"
                                        },
                                        state2: {
                                            type: "button",
                                            label: "Toggle B",
                                            icon: "vti_pause",
                                            event: "UserEvent.B"
                                        }
                                    },
                                    {
                                        type: "toggle",
                                        state1: {
                                            type: "button",
                                            icon: "vti_sound-on",
                                            event: "UserEvent.A"
                                        },
                                        state2: {
                                            type: "button",
                                            icon: "vti_sound-off",
                                            event: "UserEvent.B"
                                        }
                                    }
                                ]
                            },
                            {
                                type: "group",
                                items: [
                                    {
                                        type: "text",
                                        text: "<p>There are 3 styles of buttons/links: Primary, Secondary and Link. Icons can be positioned at either left or right. By default, icons are on the left - if you need them to appear on the right, add <span class='code'>iconPosition: 'right'</span> to their properties.</p>"
                                    },
                                    {
                                        type: "text",
                                        text: "<p>Internal link. It links to <a data-event='UserEvent.gotoCID' data-payload='typography'>Typography page</a> which has declared property CID:'typography'. <br>" +
                                        "CID should be a unique identifier. It can be defined on a <a data-event='UserEvent.gotoCID' data-payload='image-1'>Page</a> element only.</p>"
                                    },
                                    {
                                        type: "media",
                                        kind: "image",
                                        background: "#ffcc00",
                                        src: "res/assets/media/images/internal-links.png"
                                    },
                                    {
                                        type: "text",
                                        text: "<p></p><p>And here's a bunch of <a href='http://www.nanana.co.uk' target='_blank'>external</a> links. <a href='http://www.videotel.com'>Second</a> and <a href='http://www.youtube.com'>third</a> link. Attribute &lt;target=\"_blank\"&gt; is inserted automatically.</p>"
                                    },
                                    {
                                        type: "text",
                                        text: "<p></p><p>This is a <a data-event='UserEvent.showModal' data-payload='study-1'>Modal link</a>. It references a Modal defined in the 'modals' array of this file with an id 'study-1'.</p>"
                                    },
                                    {
                                        type: "media",
                                        kind: "image",
                                        src: "res/assets/media/images/modal-links.png"
                                    }
                                ]
                            }
                        ]
                    },

                    // AUDIO CONTROLLED PAGE ----------------------------------------------
                    {
                        type: "page",
                        title: "Audio",
                        CID: "intro-page",
                        nav_title: "Audio-controlled text",
                        body: [
                            {
                                type: "group",
                                style: "panel",
                                items: [
                                    {
                                        type: "text",
                                        audioControl: "ac-fade", // If audioControl property is present, text of this Element will fade in as audio progresses. Optional.
                                        audioDisplay: "bullets", // If audioDisplay property is present, text <p> elements will appear as per this property value (e.g. "bullets"). Optional.
                                        text: "<p>If audioControl property is present, text of this Element will fade in as audio progresses. Optional.</p>" +
                                        "<p>A page can have only one audio file.</p>" +
                                        "<p>Carousels and Media Explorers, on the other hand, can contain multiple audio tracks.</p>" +
                                        "<p>Main audio will be interrupted if the user activates a modal window, and will continue playing when the modal window is closed.</p>" +
                                        "<a data-event='UserEvent.showModal' data-payload='hint-image'>Modal link</a>"

                                    }
                                ]
                            },
                            {
                                type: "text",
                                audioControl: "ac-fade",
                                text: "<p>Artificial intelligence (AI) is the intelligence exhibited by machines. In computer science, an ideal 'intelligent' machine is a flexible rational agent that perceives its environment and takes actions that maximize its chance of success at an arbitrary goal.</p>"
                            },
                            /*
                             Audio Media element
                             */
                            {
                                type: "media",
                                kind: "audio",
                                src: "res/assets/media/audio/s_audio.mp3",
                                cuePoints: [1, 6, 8, 13, 20.5] // Cue points will determine when <p> elements fade in.
                            }
                        ]
                    },

                    // VIDEO SPLASH ---------------------------------------
                    {
                        type: "page",
                        hideHeader: true,
                        layout: "splash",
                        notCountable: true,
                        nav_item: true,
                        nav_title: "Video Splash",
                        backgroundVideo: "res/assets/media/video/vid_bg_2.mp4",
                        backgroundVideoPoster: "res/assets/media/video/vid_bg_2.png",
                        body: [

                            {
                                type: "group",
                                items: [
                                    {
                                        type: "text", // text element
                                        text: "<h1 class='big-num'>2</h1>" +
                                        "<h1>Full-screen splash</h1>" +
                                        "<h2>SPLASH PAGE SUB-HEADER</h2>"
                                    }
                                ]
                            }
                        ]
                    },

                    // MODALS ----------------------------------------------
                    {
                        type: "page",
                        title: "Modals",
                        nav_title: "Modals",
                        layout: "none",
                        body: [
                            {
                                type: "text",
                                text: "<a data-event='UserEvent.showModal' data-payload='hint-text'>Hint (text)</a><br>" +
                                "<a data-event='UserEvent.showModal' data-payload='prompt-multimedia'>Hint (Mixed Media)</a><br>" +
                                "<a data-event='UserEvent.showModal' data-payload='hint-image'>Hint (image)</a><br>" +
                                "<a data-event='UserEvent.showModal' data-payload='prompt-video'>Hint (video)</a><br>" +
                                "<a data-event='UserEvent.showModal' data-payload='testLeave'>Dialogue</a><br>" +
                                "<a data-event='UserEvent.showModal' data-payload='prompt-character'>Character</a><br>" +
                                "<a data-event='UserEvent.showModal' data-payload='study-1'>Full-screen (PDF)</a><br>" +
                                "<a data-event='UserEvent.showModal' data-payload='flip-prompt'>Flip</a><br>" +
                                "<a data-event='UserEvent.showModal' data-payload='info-modal'>Info</a><br>"
                            }
                        ]
                    }
                ]
            },

            /* MEDIA =================================================================================================*/
            {
                title: "Media",
                nav_title: "Media",
                content: [

                    {
                        title: "Images",
                        nav_title: "Images",
                        nav_item: true,
                        content: [
                            {
                                type: "page",
                                CID: "image-1",
                                title: "Image",
                                nav_title: "Image", // If present, will appear in 'bread crumbs' and in the menu, if it's a top-level item.
                                nav_item: true, // If true, will appear in the menu.
                                layout: "page-cols", // Page layout. Optional. Default is "page-cols".
                                body: [


                                    {
                                        type: "group",
                                        items: [

                                            {
                                                type: "media",
                                                kind: "image",
                                                /**
                                                 * Background is optional.
                                                 * If set to "-1", image will have no background.
                                                 * If set to a value "#FFCC00", image will be of that HEX value.
                                                 */
                                                background: "#ffffff",
                                                border: true, // If set to true, will show a border around an image
                                                src: 'res/assets/media/images/ship_1.png',
                                                hasViewer: true, // If set to true, an icon for Image Viewer will be added.
                                            },
                                            {
                                                type: "legend",
                                                kind: "text",
                                                text: "Text legend. Used for short image descriptions."
                                            }

                                        ]
                                    },

                                    {
                                        type: "text",
                                        text: "<h4>Page Sub-header</h4>" +
                                        "<p class='justified'>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy.</p>"
                                    }

                                ]
                            },
                            {
                                type: "page",
                                title: "Image with Hotspots",
                                nav_title: "Image with Hotspots", // If present, will appear in 'bread crumbs' and in the menu, if it's a top-level item.
                                nav_item: true, // If true, will appear in the menu.
                                layout: "page-cols", // Page layout. Optional. Default is "page-cols".
                                body: [


                                    {
                                        type: "group",
                                        items: [

                                            {
                                                type: "media",
                                                kind: "image",
                                                /**
                                                 * Background is optional.
                                                 * If set to "-1", image will have no background.
                                                 */
                                                background: "#86c3ff", // Optional Modal background
                                                controlsBackground: "#3e5162", // Optional controls background
                                                border: true, // If set to true, will show a border around an image
                                                src: 'res/assets/media/images/ship_1.png',
                                                hasViewer: true, // If set to true, an icon for Image Viewer will be added.
                                                hotspots: [
                                                    {
                                                        type: "label", // circle, rect, label
                                                        id: "1",
                                                        description: "Hotspots can have optional custom backgrounds.", // Max length - 80 chars.
                                                        x: 10,
                                                        y: 40,
                                                        color: "#e18511" // Optional background colour.
                                                    },
                                                    {
                                                        type: "label",
                                                        id: "2",
                                                        description: "This hotspot has the default background.",
                                                        x: 50,
                                                        y: 50
                                                    },
                                                    {
                                                        type: "label",
                                                        id: "3",
                                                        description: "Hotspots with custom backgrounds will not change it when selected.",
                                                        x: 85.5,
                                                        y: 25
                                                    }
                                                ]
                                            },
                                            // Legend has to be present to show hotspot descriptions when not in Viewer mode.
                                            {
                                                type: "legend",
                                                kind: "text",
                                                text: "Select hotspots to see corresponding descriptions. Select <span class='vti_expand inline'></span>icon to enlarge image.",
                                            },

                                        ]
                                    },

                                    {
                                        type: "text",
                                        text: "<h4>Hotspots</h4>" +
                                        "<p class='justified'>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy.</p>"
                                    }

                                ]
                            },


                        ]
                    },

                    {
                        type: "page",
                        title: "Animation",
                        nav_title: "Animation", // If present, will appear in 'bread crumbs' and in the menu, if it's a top-level item.
                        nav_icon: "vti_camera",
                        nav_item: true, // If true, will appear in the menu.
                        layout: "page-cols", // Page layout. Optional. Default is "page-cols".
                        body: [

                            {
                                type: "group",
                                items: [

                                    {
                                        type: "media",
                                        kind: "animation",
                                        src: 'res/assets/animations/14a/14a', // Source. Note the absence of .js extension.
                                        background: "rgba(0, 0, 0, .1)", // Custom background. Optional.
                                        // Button controls for the animation
                                        controls: [
                                            /**
                                             * Toggle button
                                             */
                                            {
                                                type: "toggle",
                                                cssClass: "btn-secondary", // CSS class
                                                state1: {
                                                    type: "button",
                                                    event: "start", // Will fire "start" event on Canvas.
                                                    label: "Start", // Button label
                                                    icon: "vti_play", // Button icon
                                                    startTick: true // This will initiate animation. Mandatory.
                                                },
                                                state2: {
                                                    type: "button",
                                                    event: "stop",
                                                    label: "Stop",
                                                    icon: "vti_stop",
                                                    stopTick: true // This will stop animation. Mandatory.
                                                }
                                            },
                                            {
                                                event: "increase",
                                                label: "RPM",
                                                icon: "vti_increase"
                                            },
                                            {
                                                event: "decrease",
                                                label: "RPM",
                                                icon: "vti_decrease"
                                            }

                                        ]
                                    },

                                    {
                                        type: "legend",
                                        kind: "text",
                                        text: "Text legend. Used for short image descriptions."
                                    }

                                ]
                            },

                            {
                                type: "text",
                                text: "<p class='justified'>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy.</p>"
                            }

                        ]
                    },


                    {
                        type: "page",
                        nav_title: "Video ",
                        nav_icon: "vti_film-reel",
                        title: "Media: Video",
                        layout: "page-cols",
                        body: [
                            {
                                type: "media",
                                kind: "video",
                                src: 'res/assets/media/video/bunny.mp4',
                                poster: 'res/assets/media/video/005.png', // Poster image
                                defaultControls: true
                            },
                            {
                                type: "text",
                                text: "<p>The animation shows the action of a typical eccentric cam.</p>" +
                                "<p class='important'>In this example the eccentric is operating a follower, which in turn will operate a mechanical device on a diesel engine.</p>" +
                                "<a data-event='UserEvent.showModal' data-payload='prompt-character'>Character Prompt</a>"
                            }
                        ]
                    },

                    // Full Page Video
                    {
                        type: "page",
                        nav_title: "Full Page Video",
                        nav_icon: "vti_film",
                        title: "Full Page Video + custom controls",
                        layout: "full-page",
                        body: [
                            {
                                type: "media",
                                kind: "video",
                                src: 'res/assets/media/video/bunny.mp4',
                                poster: 'res/assets/media/video/005.png', // Poster image
                                controlBar: true,
                                autoPlay: true,
                                defaultControls: false,
                                cc: [
                                    {
                                        label: "Off"
                                    },
                                    {
                                        srclang: "en",
                                        label: "English",
                                        src: 'res/assets/media/video/bunny_en.vtt'
                                    },
                                    {
                                        srclang: "ru",
                                        label: "Русский",
                                        src: 'res/assets/media/video/bunny_ru.vtt'
                                    },
                                    {
                                        srclang: "cmn",
                                        label: "中文",
                                        src: 'res/assets/media/video/bunny_cmn.vtt'
                                    }
                                ],
                                chapters: [
                                    {
                                        label: "Chapter 1",
                                        time: 0
                                    },
                                    {
                                        label: "Chapter 2",
                                        time: 5
                                    },
                                    {
                                        label: "Chapter 3",
                                        time: 12.7
                                    },
                                    {
                                        label: "Chapter 4",
                                        time: 21
                                    }
                                ]
                            }

                        ]
                    },

                    // Explorer ---------------------------------------------------
                    {
                        type: "explorer",
                        title: "Explorer",
                        nav_title: "Explorer",
                        layout: "explorer",
                        body: [
                            {
                                description: "This is an example of Explorer type page. Click on the squares to get more info.",
                                image: "res/assets/media/images/explorer/boat.jpg",
                                finalScreen: {
                                    modal: {
                                        type: "hint",
                                        title: "You have seen all there is to see!",
                                        body: [
                                            {
                                                type: "text",
                                                text: "<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis.</p>"
                                            },
                                            {
                                                type: "media",
                                                kind: "audio",
                                                src: "res/assets/media/audio/biggerboat.mp3"
                                            }
                                        ]
                                    }
                                },
                                items: [
                                    {
                                        thumb: "res/assets/media/images/explorer/p1.jpg",
                                        title: "Title for this screen",
                                        zoom: 1.5,
                                        pos: [10, -10],
                                        modal: {
                                            type: "flip",
                                            screens: [
                                                {
                                                    title: "Side 1",
                                                    image: "res/assets/media/images/explorer/p0.jpg",
                                                    body: [
                                                        {
                                                            type: "text",
                                                            text: "<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, " +
                                                            "totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>"
                                                        },
                                                        {
                                                            type: "media",
                                                            kind: "audio",
                                                            src: "res/assets/media/audio/generic-short.mp3"
                                                        }
                                                    ]
                                                },
                                                {
                                                    header: "Shaft Bearings",
                                                    body: [
                                                        {
                                                            type: "text",
                                                            text: "<table>" +
                                                            "<caption>ExxonMobil Product Line</caption>" +
                                                            "<tr><th>Equipment</th><th>Mineral</th><th>Synthetic</th></tr>" +
                                                            "<tr><td></td><td>Mobilgard 300</td><td></td></tr>" +
                                                            "<tr><td></td><td>Mobilgard M Series</td><td></td></tr>" +
                                                            "<tr><td></td><td>Mobilgard 410 NC</td><td></td></tr>" +
                                                            "<tr><td></td><td>Mobilgard ADL Series</td><td></td></tr>" +
                                                            "<tr><td></td><td>Mobilgard 12 Series</td><td></td></tr>" +
                                                            "<tr><td></td><td>Mobil DTE Named Series</td><td></td></tr>" +
                                                            "<tr><td></td><td>Mobil DTE 10 Excel Series</td><td></td></tr>" +
                                                            "<tr><td></td><td>Mobilgear 600 XP Series</td><td></td></tr>" +
                                                            "</table>"
                                                        },
                                                        {
                                                            type: "media",
                                                            kind: "audio",
                                                            src: "res/assets/media/audio/biggerboat.mp3"
                                                        }
                                                    ]
                                                }
                                            ]
                                        }
                                    },
                                    {
                                        thumb: "res/assets/media/images/explorer/p2.jpg",
                                        title: "Screen 2 title",
                                        zoom: 2,
                                        pos: [-5, 20],
                                        modal: {
                                            type: "character",
                                            title: "Jose",
                                            image: "res/assets/media/prompt/character01a.png",
                                            body: [
                                                {
                                                    type: "text",
                                                    text: "<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis.</p>"
                                                },
                                                {
                                                    type: "media",
                                                    kind: "audio",
                                                    src: "res/assets/media/audio/generic-short.mp3"
                                                }
                                            ]
                                        }
                                    },
                                    {
                                        thumb: "res/assets/media/images/explorer/p3.jpg",
                                        title: "Description for screen 3.",
                                        zoom: 2,
                                        pos: [-15, 11],
                                        modal: {
                                            type: "hint",
                                            title: "Totam rem aperiam, eaque ipsa quae ab illo inventore veritatis.",
                                            image: "res/assets/media/images/explorer/p3.jpg",
                                            body: [
                                                {
                                                    type: "text",
                                                    text: "<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis.</p>"
                                                },
                                                {
                                                    type: "media",
                                                    kind: "audio",
                                                    src: "res/assets/media/audio/biggerboat.mp3"
                                                }
                                            ]
                                        }
                                    },
                                    {
                                        thumb: "res/assets/media/images/explorer/p4.jpg",
                                        title: "Screen 4 title/description. This property is optional.",
                                        audio: "res/assets/media/audio/explorer/exp_4.mp3",
                                        zoom: 2,
                                        pos: [-25, -15]
                                    },
                                    {
                                        thumb: "res/assets/media/images/explorer/p5.jpg",
                                        zoom: 2,
                                        pos: [-10, -12]
                                    },
                                    {
                                        thumb: "res/assets/media/images/explorer/p6.jpg",
                                        zoom: 2,
                                        pos: [10, -18]
                                    },
                                    {
                                        thumb: "res/assets/media/images/explorer/p7.jpg",
                                        zoom: 2,
                                        pos: [20, -18]
                                    },
                                    {
                                        thumb: "res/assets/media/images/explorer/p8.jpg",
                                        zoom: 2,
                                        pos: [16, -18]
                                    },
                                    {
                                        thumb: "res/assets/media/images/explorer/p9.jpg",
                                        zoom: 2,
                                        pos: [16, -2]
                                    }
                                ]
                            },
                            {
                                type: "media",
                                kind: "audio",
                                src: "res/assets/media/audio/explorer/explorer.mp3"
                            }
                        ]

                    },

                    // Carousel ---------------------------------------------------
                    {
                        type: "page",
                        nav_item: true,
                        nav_title: "Carousel",
                        title: "Carousel Example",
                        layout: "none",
                        CID: "carousel-1",
                        body: [
                            {
                                type: "media",
                                kind: "carousel",
                                screens: [
                                    {
                                        // Left part of the carousel screen
                                        l: [
                                            {
                                                type: "media",
                                                kind: "image",
                                                src: 'res/assets/media/images/carousel/003.jpg'

                                            },
                                            {
                                                type: "legend",
                                                kind: "text",
                                                text: "An example of text legend."
                                            }
                                        ],
                                        // Right part of the carousel screen
                                        r: [
                                            {
                                                type: "legend",
                                                kind: "alphaNum",
                                                fields: [
                                                    {"1.": "Follower"},
                                                    {"2.": "Eccentric"},
                                                    {"3.": "Shaft"},
                                                    {"4.": "Centre line of eccentric"},
                                                    {"5.": "Centre line of shaft"}
                                                ]
                                            },
                                            {
                                                type: "text",
                                                text: "<p></p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt <img class='math-2-line' src='res/assets/media/equations/001.svg'> ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor <img class='math-1-line' src='res/assets/media/equations/002.svg'> in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>" +
                                                "<a data-event='UserEvent.gotoCID' data-payload='typography'>A link to a page</a>"
                                            },
                                            {
                                                type: "media",
                                                kind: "audio",
                                                src: "res/assets/media/audio/generic-short.mp3"
                                                //onComplete: "complete"
                                            }
                                        ]
                                    },
                                    {
                                        l: [
                                            {
                                                type: "media",
                                                kind: "image",
                                                src: "res/assets/media/images/carousel/003.svg",
                                                background: "#ffcc00"
                                            },
                                            {
                                                type: "legend",
                                                kind: "color",
                                                fields: [
                                                    {"FFF797": "Pump inlet"},
                                                    {"D9D3C3": "Pump shoe"},
                                                    {"289C9C": "Eccentric"},
                                                    {"FCB930": "Drive Shaft"},
                                                    {"CADD9A": "Pump outlet"},
                                                    {"CADD9A": "Delivery"},
                                                    {"FFF797": "Suction"}
                                                ]
                                            }
                                        ],
                                        r: [
                                            {
                                                type: "text",
                                                text: "<ul>" +
                                                "<li>Lorem ipsum dolor sit amet</li>" +
                                                "<li>Consectetur adipiscing elit</li>" +
                                                "<li>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</li>" +
                                                "</ul>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt"
                                            },
                                            {
                                                type: "media",
                                                kind: "audio",
                                                src: "res/assets/media/audio/generic-short.mp3"
                                                //onComplete: "complete"
                                            }
                                        ]
                                    },
                                    {
                                        l: [
                                            {
                                                type: "media",
                                                kind: "image",
                                                src: "res/assets/media/images/carousel/pump1.jpg"
                                            }
                                        ],
                                        r: [
                                            {
                                                type: "text",
                                                audioControl: "ac-fade",
                                                text: "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>" +
                                                "<p>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>" +
                                                "<p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>"
                                            },
                                            {
                                                type: "media",
                                                kind: "audio",
                                                src: "res/assets/media/audio/generic-short.mp3",
                                                cuePoints: [1, 3, 5]
                                            }
                                        ]
                                    },
                                    {
                                        l: [
                                            {
                                                type: "media",
                                                kind: "image",
                                                background: "#eea72a",
                                                src: "res/assets/media/images/carousel/pump2.jpg"
                                            }
                                        ],
                                        r: [
                                            {
                                                type: "text",
                                                text: "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>"
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    },

                    // Cards ------------------------------------------------------
                    {
                        type: "page",
                        nav_item: true,
                        nav_title: "Cards",
                        title: "Cards",
                        layout: "cards",
                        onFocusEvent: "UserEvent.showModal",
                        focusEventPayload: "info-modal",
                        // onEnterEvent: "UserEvent.showModal",
                        // enterEventPayload: "hint-text",
                        body: [
                            {
                                type: "card",
                                cover: {
                                    bgColor: "#3399CC",
                                    text: "Card example 1.",
                                    audio: "res/assets/media/audio/cards.mp3",
                                    icon: "res/assets/media/images/cards/bulb.svg"
                                },
                                content: {
                                    title: "Title for this card.",
                                    text: "The maximum number of characters allowed here is 400. If the limit is exceeded, the application will throw an error. If you are using this component and the application is not loading, open browser's console and check the error message. The reason for this is that the Card components are NOT resizable and will not display scrollbars. You cannot have buttons or links inside Cards. This card is 400."
                                }
                            },

                            {
                                type: "card",
                                cover: {
                                    bgColor: "#339966",
                                    text: "Card example 2.",
                                    audio: "res/assets/media/audio/generic-short.mp3",
                                    icon: "res/assets/media/images/cards/computer.svg"
                                },
                                content: {
                                    title: "Title for this card.",
                                    text: "In image processing and computer vision, anisotropic diffusion, also called Perona–Malik diffusion, is a technique aiming at reducing image noise without removing significant parts of the image content, typically edges, lines or other details that are important for the interpretation of the image."
                                }
                            },
                            {
                                type: "card",
                                cover: {
                                    bgColor: "#CC6666",
                                    text: "Card example 3.",
                                    icon: "res/assets/media/images/cards/notepad.svg"
                                },
                                content: {
                                    title: "Title for this card.",
                                    text: "In image processing and computer vision, anisotropic diffusion, also called Perona–Malik diffusion, is a technique aiming at reducing image noise without removing significant parts of the image content, typically edges, lines or other details that are important for the interpretation of the image."
                                }
                            },

                            {
                                type: "card",
                                cover: {
                                    iconSize: "60%", // Width and hight of icon. Optional
                                    bgColor: "#CCCC00",
                                    text: "Card example 4.",
                                    icon: "res/assets/media/images/cards/rv01_03.svg"
                                },
                                content: {
                                    title: "Title for this card.",
                                    text: "In image processing and computer vision, anisotropic diffusion, also called Perona–Malik diffusion, is a technique aiming at reducing image noise without removing significant parts of the image content, typically edges, lines or other details that are important for the interpretation of the image."
                                }
                            }
                        ]
                    },
                    // Accordion ------------------------------------------------------
                    {
                        type: "page",
                        nav_item: true,
                        nav_title: "Accordion",
                        title: "Accordion",
                        layout: "none",
                        body: [
                            {
                                type: "accordion",
                                autoCollapse: true,
                                items: [
                                    {
                                        title: "Screen 1",
                                        color: "#b9b900",
                                        audio: "res/assets/media/audio/cards.mp3",
                                        body: [
                                            {
                                                type: "html-image",
                                                src: "res/assets/media/images/carousel/pump2.jpg",
                                                hasViewer: true
                                            },
                                            {
                                                type: "text",
                                                text: "<p>In image processing and computer vision, anisotropic diffusion, also called Perona–Malik diffusion, is a technique aiming at reducing image noise without removing significant parts of the image content, typically edges, lines or other details that are important for the interpretation of the image.</p> <a data-event='UserEvent.gotoCID' data-payload='typography'>A link to a page</a>"
                                            }
                                        ]
                                    },

                                    {
                                        title: "Screen 2",
                                        color: "#3399CC",
                                        audio: "res/assets/media/audio/generic-short.mp3",
                                        body: [
                                            {
                                                type: "text",
                                                text: "<p>The WebM format is based on a restricted version of the Matroska container format. It always uses the VP8 or VP9 video codec and the Vorbis or Opus audio codec. WebM is natively supported in desktop and mobile Gecko (Firefox), Chrome and Opera, and support for the format can be added to Internet Explorer and Safari (but not on iOS) by installing an add-on. Native support for VP9 WebM is in progress for the Edge browser.</p>" +
                                                "<p>The WebM format, specifically the VP8 video codec, had been acccused of patent infringment by a group of companies answering a call by the MPEG LA for the formation of a patent pool, but MPEG LA has agreed to license those patents to Google under a 'perpetual, transferable, royalty free license'.  This means, effectively, that all known patents on the WebM format are licensed to everyone for free. </p>"
                                            }
                                        ]
                                    },

                                    {
                                        title: "Screen 3",
                                        color: "#cc6856",
                                        body: [
                                            {
                                                type: "text",
                                                text: "<p>The WebM format is based on a restricted version of the Matroska container format. It always uses the VP8 or VP9 video codec and the Vorbis or Opus audio codec. WebM is natively supported in desktop and mobile Gecko (Firefox), Chrome and Opera, and support for the format can be added to Internet Explorer and Safari (but not on iOS) by installing an add-on. Native support for VP9 WebM is in progress for the Edge browser.</p>"
                                            }
                                        ]
                                    },

                                    {
                                        title: "Screen 4",
                                        color: "#978f77",
                                        body: [
                                            {
                                                type: "text",
                                                text: "<p>The WebM format is based on a restricted version of the Matroska container format. It always uses the VP8 or VP9 video codec and the Vorbis or Opus audio codec. WebM is natively supported in desktop and mobile Gecko (Firefox), Chrome and Opera, and support for the format can be added to Internet Explorer and Safari (but not on iOS) by installing an add-on. Native support for VP9 WebM is in progress for the Edge browser.</p>"
                                            }
                                        ]
                                    },

                                    {
                                        title: "Screen 5",
                                        color: "#3ab85c",
                                        body: [
                                            {
                                                type: "text",
                                                text: "<p>The WebM format is based on a restricted version of the Matroska container format. It always uses the VP8 or VP9 video codec and the Vorbis or Opus audio codec. WebM is natively supported in desktop and mobile Gecko (Firefox), Chrome and Opera, and support for the format can be added to Internet Explorer and Safari (but not on iOS) by installing an add-on. Native support for VP9 WebM is in progress for the Edge browser.</p>"
                                            }
                                        ]
                                    }
                                ]
                            }

                        ]
                    },

                    // Timeline ------------------------------------------------------
                    {
                        type: "page",
                        nav_item: true,
                        nav_title: "Timeline",
                        title: "Timeline",
                        layout: "page-timeline",
                        body: [
                            {
                                type: "timeline",
                                items: [
                                    {
                                        title: "1824",
                                        subtitle: "Carnot cycle published.",
                                        text: "Theory of the thermodynamic cycle of the heat engine published by Sadi Carnot the theoretical basis of the Diesel engine.",
                                        color: "#cccc00",
                                        audio: "res/assets/media/audio/generic-short.mp3",
                                    },
                                    {
                                        title: "1854",
                                        subtitle: "First internal combustion engine built running on gas.",
                                        text: "Patent granted for the first working version of an internal combustion to Eugenio Barsanti and Felice Matteucci.",
                                        color: "#d86d16",
                                        audio: "res/assets/media/audio/generic-short.mp3",
                                    },
                                    {
                                        title: "1872",
                                        subtitle: "The four strokes of an internal combustion engine defined.",
                                        text: "Intake, compression, ignition/expansion, and exhaust are defined in a patent by Nicolaus Otto. This is similar to the patent issued to Barsanti and Matteucci in 1854."
                                    },
                                    {
                                        title: "1878",
                                        subtitle: "First design for a two stroke compression engine.",
                                        text: "The ground work for the 2 stroke compression engine design is developed by Dugald Clerk.",
                                        color: "#cc9019"
                                    },
                                    {
                                        title: "1886",
                                        subtitle: "Hot bulb ignition engine is built.",
                                        text: "A precursor to the Diesel engine compression ignition- a prototype hot bulb ignition gas engine is built by Herbert Akroyd Stuart."
                                    },
                                    {
                                        title: "1890",
                                        subtitle: "Concept of compression ignition described.",
                                        text: "The first compression ignition engine is described by Herbert Akroyd Stuart."
                                    },
                                    {
                                        title: "1891",
                                        subtitle: "Hot bulb engine with pressurized fuel injection patented.",
                                        text: "An internal combustion engine that uses a 'hot bulb' and pressurized fuel injection is patented by Herbert Akroyd Stuart."
                                    },
                                    {
                                        title: "1892",
                                        subtitle: "Rudolf Diesel is granted patent for his engine design.",
                                        text: "A patent titled 'Arbeitsverfahren und Ausführungsart für Verbrennungsmaschinen' (Working Methods and Techniques for Internal Combustion Engines) is granted to Rudolf Diesel.",
                                        color: "#73b515"
                                    }
                                ]
                            }

                        ]
                    },


                    // 3D -----------
                    {
                        nav_title: "3D",
                        title: "3D",

                        content: [

                            // THREE.JS ---------------------------------
                            {
                                nav_title: "THREE.JS",
                                title: "THREE.JS",

                                content: [
                                    {
                                        type: "page",
                                        nav_title: "Three.js in Page",
                                        nav_item: true,
                                        title: "Example 1",
                                        body: [
                                            {
                                                type: "media",
                                                kind: "3d",
                                                library: "THREE.js",
                                                path: "res/assets/3d/1/scene2.json",
                                                settings: "../assets/3d/1/settings2", // Settings file, no ".js" extension or "res" in the filepath!
                                                orbitControls: true
                                            },
                                            {
                                                type: "text",
                                                text: "<h4>3D Media example (THREE.JS)</h4>" +
                                                "This example runs 'inline' as there's no preview property defined for this component."
                                            }
                                        ]
                                    },

                                    {
                                        type: "page",
                                        nav_title: "Three.js in Modal",
                                        nav_item: true,
                                        title: "Example 2",
                                        body: [
                                            {
                                                type: "media",
                                                kind: "3d",
                                                library: "THREE.js",
                                                path: "res/assets/3d/1/scene2.json",
                                                settings: "../assets/3d/1/settings", // Settings file, no ".js" extension or "res" in the filepath!
                                                orbitControls: true,
                                                preview: "res/assets/3d/1/preview.jpg" // if set to true, 3D content will render in a Modal window
                                            },
                                            {
                                                type: "text",
                                                text: "<h4>3D Media example (THREE.JS)</h4>" +
                                                "<p>This example will launch in a Modal window.</p>"
                                            }
                                        ]
                                    }

                                ]
                            },
                            // BABYLON ------------------------------
                            {
                                nav_title: "Babylon",
                                title: "Babylon",
                                content: [
                                    {
                                        type: "page",
                                        nav_title: "Babylon in Page",
                                        nav_item: true,
                                        title: "Example 3",
                                        body: [
                                            {
                                                type: "media",
                                                kind: "3d",
                                                library: "Babylon.js",
                                                settings: "../assets/3d/babylon/1/settings" // Settings file, no ".js" extension or "res" in the filepath!
                                            },
                                            {
                                                type: "text",
                                                text: "<h4>3D Media example (Babylon.js)</h4>" +
                                                "This example runs 'inline' as there's no preview property defined for this component."
                                            }
                                        ]
                                    },

                                    {
                                        type: "page",
                                        nav_title: "Babylon in Modal",
                                        nav_item: true,
                                        title: "Example 4",
                                        body: [
                                            {
                                                type: "media",
                                                kind: "3d",
                                                library: "Babylon.js",
                                                settings: "../assets/3d/babylon/2/settings", // Settings file, no ".js" extension or "res" in the filepath!
                                                preview: "res/assets/3d/babylon/2/preview.jpg"
                                            },
                                            {
                                                type: "text",
                                                text: "<h4>3D Media example (Babylon.js)</h4>" +
                                                "This example will launch in a Modal window."
                                            }
                                        ]
                                    },

                                    {
                                        type: "page",
                                        nav_title: "Babylon (camera inside)",
                                        nav_item: true,
                                        title: "Example 5",
                                        body: [
                                            {
                                                type: "media",
                                                kind: "3d",
                                                library: "Babylon.js",
                                                settings: "../assets/3d/babylon/3/settings", // Settings file, no ".js" extension or "res" in the filepath!
                                                preview: "res/assets/3d/babylon/3/preview.jpg"
                                            },
                                            {
                                                type: "text",
                                                text: "<h4>3D Media example (Babylon.js)</h4>" +
                                                "This example will launch in a Modal window."
                                            }
                                        ]
                                    }
                                ]
                            }

                        ]
                    }
                ]
            },
            // EXTERNALS ===============================================================================================
            {
                nav_title: "External",
                title: "External",
                content: [

                    // External (Maritime English speech) --------------------
                    {
                        type: "page",
                        nav_item: true,
                        nav_title: "Maritime English",
                        title: "Maritime English",
                        layout: "speech", // this will be referenced by speech.css
                        body: [
                            {
                                type: "media",
                                kind: "external",
                                srcHTML: "../assets/external/maritime_english/positives.html",
                                srcJS: "../assets/external//maritime_english/speech",
                                srcCSS: "res/assets/external/maritime_english/speech.css"
                            }

                        ]
                    },

                    {
                        title: "AED",
                        nav_title: "AED",
                        content: [

                            {
                                type: "page",
                                layout: "none",
                                title: "BLS Algorithm",
                                nav_title: "BLS Algorithm",
                                nav_item: true,
                                body: [
                                    {
                                        type: "media",
                                        kind: "external",
                                        srcHTML: "../assets/external/team_examples/aed/bls_algo.html"
                                    }
                                ]
                            },

                            {
                                type: "page",
                                layout: "none",
                                title: "Chain of Survival",
                                nav_title: "Chain of Survival",
                                nav_item: true,
                                body: [
                                    {
                                        type: "media",
                                        kind: "external",
                                        srcHTML: "../assets/external/team_examples/aed/chain.html"
                                    }
                                ]
                            }
                        ]
                    },

                    {
                        nav_title: "Fuel Calculators",
                        title: "Fuel Calculators",
                        content: [
                            // External (Calculator-1) --------------------
                            {
                                type: "page",
                                nav_item: true,
                                nav_title: "Calculator-1",
                                title: "Calculator example-1",
                                layout: "page-cols",
                                body: [
                                    {
                                        type: "media",
                                        kind: "external",
                                        srcHTML: "../assets/external/calculator1/body.html",
                                        srcJS: "../assets/external/calculator1/calc_1"
                                    },
                                    {
                                        type: "text",
                                        text: "<p>External Media is not part of VT-5 Framework, although VT-5 CSS rules will apply.</p>" +
                                        "<p>External Media consists of 2 parts: html and js. Html contains structure and optional CSS, without the usual HEAD and BODY declarations. " +
                                        "JS is pure Javascript wrapped in requirejs's <code>define</code> function." +
                                        "JS must have functions <code>init(div:HTMLDivElement)</code> and <code>clear()</code> defined. The former receives html defined in the HTML part. " +
                                        "This is where you should initialise your JS and set listeners. The latter is called when the page goes out of scope and should clean up, e.g. " +
                                        "remove listeners, dereference elements, etc.</p>" +
                                        "<p class='warning'>This component will only work in server environment.</p>"
                                    }
                                ]
                            },
                            // External (Calculator-2) --------------------
                            {
                                type: "page",
                                nav_item: true,
                                nav_title: "Calculator-2",
                                title: "Calculator example-2",
                                layout: "none",
                                body: [
                                    {
                                        type: "media",
                                        kind: "external",
                                        srcHTML: "../assets/external/calculator2/body.html",
                                        srcJS: "../assets/external/calculator2/calc_2"
                                    }

                                ]
                            }
                        ]
                    }

                ]
            },


            {
                title: "Test Engine",
                nav_title: "Test Engine",
                content: [
                    // FINAL TEST ------------------------
                    {
                        title: "Final Test",
                        nav_title: "Final Test",
                        nav_icon: "vti_test",
                        testId: "final", // "final" for final test, "1", "2", "3" etc for modules.
                        showBreakdown: true, // if set to true, test result page will show a breakdown.
                        type: "test",
                        testMode: 0, //options: normal(0), tutorial(1), review(2)
                        testName: "Final Test",
                        certificateType: "final", // Type of certificate to use. Options: "final", "module".
                        testStart: true, // If set to true, will include the Test Start page.
                        minScore: 10, // Minimum pass score.
                        testEnd: true, // If set to true, will include the Test End page.
                        testResult: true, // If set to true, will include the Test Result page.
                        randomize: false, // If set to true, will randomize questions.
                        tunnel: true, //If set to true, content will only be accessible from page 1 of this module.
                        modalID: "testLeave", // Will show this Modal when user tries to leave the test.

                        pools: [
                            {
                                poolID: "p1", // CID of test pool
                                items: [] // Question ids to include; if left empty, all questions will be included.
                            },
                            {
                                poolID: "p2", // cid of pool
                                items: ["p2-1", "p2-2", "p2-3"] // question ids to include; if left empty, all questions will be included.
                            }

                        ]
                    },
                    // MODULE TEST ------------------------
                    {
                        title: "Module 1 Test",
                        nav_title: "Module Test",
                        nav_icon: "vti_test",
                        testId: "1",
                        type: "test",
                        testMode: 0, //options: normal(0), tutorial(1), review(2)
                        testName: "Module 1 Test",
                        certificateType: "module", // Type of certificate to use. Options: "final", "module".
                        testStart: true, // If set to true, will include the Test Start page.
                        minScore: 10, // Minimum pass score.
                        testEnd: true, // If set to true, will include the Test End page.
                        testResult: true, // If set to true, will include the Test Result page.
                        randomize: false, // If set to true, will randomize questions.
                        tunnel: true, //If set to true, content will only be accessible from page 1 of this module.
                        modalID: "testLeave", // Will show this Modal when user tries to leave the test.

                        pools: [
                            {
                                poolID: "p1", // CID of test pool
                                items: [] // Question ids to include; if left empty, all questions will be included.
                            }

                        ]
                    },

                    // test (tutorial mode)-------------------------------
                    {
                        title: "Test: Tutorial mode",
                        nav_title: "Tutorial",
                        nav_icon: "vti_tutorial",
                        type: "test",
                        testMode: 1, //options: normal(0), tutorial(1), review(2)
                        tunnel: false, //if set to true, content will only be accessible from page 1

                        pools: [
                            {
                                poolID: "p1", // cid of pool
                                items: [] // question ids to include; if left empty, all questions will be included.
                            },
                            {
                                poolID: "p2", // cid of pool
                                items: [] // question ids to include; if left empty, all questions will be included.
                            }
                        ]
                    }
                ]
            }

        ]
    },
    // Test Definition
    test: {
        testStart: {
            type: "page",
            title: "You are about to start the test",
            layout: "none",
            body: [
                {
                    type: "text",
                    text: "<p>You have now reached the end of this module.<br>" +
                    "You may now either run the self-assessment test, or go onto another module.<br>" +
                    "Thank you for using the module, we hope that it has been of value to you.</p>"
                }
            ]
        },
        testEnd: {
            type: "page",
            layout: "splash",
            backgroundImage: "res/assets/media/images/home_splash.jpg", // background image
            backgroundOverlay: "rgba(0,0,0,.1), rgba(0,0,0,.4)", //background overlay
            body: [
                {
                    type: "group",
                    items: [
                        {
                            type: "button",
                            label: "Submit Result",
                            event: "UserEvent.next",
                            cssClass: "btn-primary",
                            icon: "vti_chevron-right",
                            iconPosition: "right"
                        },
                        {
                            type: "text",
                            text: "<h2>YOU HAVE COMPLETED THE ASSESSMENT FOR MODULE M632.'</h2>"
                        }
                    ]
                }

            ]
        },
        testResult: {
            title: "Your Test Results:",
            layout: "page-test-result",
            certificateModuleSrc: "res/assets/media/common/certificate-module.png",
            certificateFinalSrc: "res/assets/media/common/certificate-final.png",
            certificateFinalFields: undefined,
            /*
             If certificateFinalFields or certificateModuleFields arrays are present, the app will use these instead
             of default ones. Should be used for custom or non-standard certificates.
             Not all fields are necessary, but the ones below show all possible fields.
             */
            certificateModuleFields: [
                {
                    field: "courseName",
                    fontSize: 84,
                    x: 95,
                    y: 180,
                    maxWidth: 2000
                },
                {
                    field: "userName",
                    fontSize: 48,
                    x: 480,
                    y: 400,
                    maxWidth: 1800
                },
                {
                    field: "trainingId",
                    fontSize: 48,
                    x: 480,
                    y: 477,
                    maxWidth: 1800
                },
                {
                    field: "testName",
                    fontSize: 48,
                    x: 95,
                    y: 580,
                    maxWidth: 1800
                },
                {
                    field: "scorePercent",
                    addChar: "%", // Use this property if you need to add an extra character(s) to the output.
                    fontSize: 48,
                    x: 480,
                    y: 752,
                    maxWidth: 1800
                },
                {
                    field: "durationString",
                    fontSize: 48,
                    x: 480,
                    y: 823,
                    maxWidth: 1800
                },
                {
                    field: "date",
                    fontSize: 48,
                    x: 480,
                    y: 890,
                    maxWidth: 1800
                },
            ],

            body: [
                {
                    type: "group",
                    items: [
                        {
                            type: "text",
                            text: "<p>You can print your test-completion certificate by clicking the 'Print' button.<br>" +
                            "You can review your answers by clicking the 'Review' button.</p>"
                        },
                        {
                            type: "button",
                            label: "Review",
                            event: "UserEvent.reviewTest",
                            icon: "vti_test",
                            cssClass: "btn-primary"
                        },
                        {
                            type: "button",
                            label: "Save",
                            event: "UserEvent.save",
                            icon: "vti_save",
                            cssClass: "btn-primary"
                        }
                    ]
                }

            ],
            results: {
                name: "User Name",
                trainingId: "Training ID",
                score: "Score",
                scorePercent: "Score Percentage",
                duration: "Duration",
                passResponse: "You have passed the test!",
                failResponse: "You have failed the test.",
                breakdown: "Breakdown"
            }
        },
        response: {
            correct: "CORRECT",
            incorrect: "INCORRECT",
            disqualified: "DISQUALIFIED",
            notAnswered: "NOT ANSWERED",
            showCorrect: "CORRECT ANSWER",
            submitAnswer: "SUBMIT ANSWER"
        }
    },

    // Pools
    pools: [

        // pool for module 1 -------------------------
        {
            poolID: "p1", // Questions pool for Module 1
            poolName: "Section 1",
            content: [
                // Questions
                {
                    id: "p1-0", // Unique ID
                    kind: "select", // Question type
                    optionType: "radio", // Selection type ("radio" or "checkbox")
                    scorePoints: 1, // Score value
                    title: "True/False type",
                    layout: "none",
                    body: [
                        {
                            type: "question",
                            question: [
                                {
                                    description: "Is this statement true or false?", // optional. If present, will layout elements in a table-cell, description first.
                                    options: [
                                        {
                                            label: "True", // Label
                                            a: 0 // Answer (0=incorrect, 1=correct)
                                        },
                                        {
                                            label: "False",
                                            a: 1
                                        }
                                    ]
                                }
                            ]
                        }
                    ],
                    hint: {
                        title: "Hint",
                        type: "hint",
                        body: [
                            {
                                type: "text",
                                text: "<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>"
                            },
                            {
                                type: "media",
                                kind: "image",
                                src: "res/assets/media/images/carousel/pump1.jpg"
                            }
                        ]
                    }
                },
                // Select type + audio
                {
                    id: "p1-0-sound", // Unique ID
                    kind: "select", // Question type
                    optionType: "radio", // Selection type ("radio" or "checkbox")
                    scorePoints: 1, // Score value
                    title: "True/False type with audio",
                    layout: "none",
                    body: [
                        {
                            type: "question",
                            question: [
                                {
                                    description: "This question has an audio track. True or false?", // optional. If present, will layout elements in a table-cell, description first.
                                    audio: "res/assets/media/audio/horn.mp3",
                                    options: [
                                        {
                                            label: "True", // Label
                                            a: 0 // Answer (0=incorrect, 1=correct)
                                        },
                                        {
                                            label: "False",
                                            a: 1
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                },
                {
                    id: "p1-1",
                    kind: "select",
                    optionType: "radio",
                    scorePoints: 1,
                    title: "Comparing DP to more traditional position-keeping options, such as Anchor barges and Jack-up rigs, the various methods offer different advantages and disadvantages. Which of the following advantages apply for DP Vessels?",
                    layout: "none",
                    body: [
                        {
                            type: "question",
                            question: [
                                {
                                    description: "This is option's body 1", // optional. If present, will layout elements in a table-cell, description first.
                                    options: [
                                        {
                                            label: "A",
                                            a: 0
                                        },
                                        {
                                            label: "B",
                                            a: 1
                                        },
                                        {
                                            label: "C",
                                            a: 0
                                        }
                                    ]
                                },
                                {
                                    description: "This is option's body 2",
                                    options: [
                                        {
                                            label: "A",
                                            a: 1
                                        },
                                        {
                                            label: "B",
                                            a: 0
                                        },
                                        {
                                            label: "C",
                                            a: 0
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                },
                {
                    id: "p1-1a",
                    kind: "select",
                    optionType: "checkbox",
                    scorePoints: 1,
                    title: "What is a common use for cam operated piston pumps?",
                    subTitle: "Additional description of the question or task. This is 'radio' type of select.",
                    layout: "none",
                    body: [
                        {
                            type: "question",
                            question: [
                                {
                                    description: "This is option's body 1.", // optional. If present, will layout elements in a table-cell, description first.
                                    options: [
                                        {
                                            label: "A",
                                            a: 1
                                        },
                                        {
                                            label: "B",
                                            a: 1
                                        },
                                        {
                                            label: "C",
                                            a: 0
                                        }
                                    ]
                                },
                                {
                                    description: "This is option's body 2.",
                                    options: [
                                        {
                                            label: "A",
                                            a: 1
                                        },
                                        {
                                            label: "B",
                                            a: 0
                                        },
                                        {
                                            label: "C",
                                            a: 0
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                },
                {
                    id: "p1-2",
                    kind: "select",
                    optionType: "checkbox",
                    title: "What is a common use for cam operated piston pumps?",
                    layout: "none",
                    body: [
                        {
                            type: "question",
                            question: [
                                {
                                    options: [
                                        {
                                            label: "Option 1",
                                            a: 0
                                        }
                                    ]
                                },
                                {
                                    options: [
                                        {
                                            label: "Option 2",
                                            a: 1
                                        }
                                    ]
                                },
                                {
                                    options: [
                                        {
                                            label: "Option 3",
                                            a: 1
                                        }
                                    ]
                                }


                            ]
                        }

                    ],
                    hint: {
                        title: "Hint",
                        type: "hint",
                        body: [
                            {
                                type: "media",
                                kind: "video",
                                src: 'res/assets/media/video/005.mp4',
                                poster: 'res/assets/media/video/005.png',
                                defaultControls: true
                            }
                        ]
                    }
                },
                {
                    id: "p1-3",
                    kind: "select",
                    optionType: "radio",
                    title: "What is a common use for cam operated piston pumps?",
                    body: [
                        {
                            type: "media",
                            kind: "image",
                            scale: "vh-60",
                            background: true,
                            src: 'res/assets/media/images/carousel/pump1.jpg'
                        },
                        {
                            type: "question",
                            question: [
                                {
                                    options: [
                                        {
                                            label: "Option 1",
                                            a: 0
                                        }
                                    ]
                                },
                                {
                                    options: [
                                        {
                                            label: "Option 2",
                                            a: 0
                                        }
                                    ]
                                },
                                {
                                    options: [
                                        {
                                            label: "Option 3",
                                            a: 1
                                        }
                                    ]
                                },
                                {
                                    options: [
                                        {
                                            label: "Option 4",
                                            a: 0
                                        }
                                    ]
                                }


                            ]
                        }

                    ],
                    // Hint will be shown in Interactive (Tutorial) mode if the question is answered incorrectly.
                    hint: {
                        title: "Hint",
                        type: "hint",
                        body: [
                            {
                                type: "text",
                                text: "<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p><p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p><p>Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>"
                            }
                        ]
                    }
                },
                {
                    id: "p1-4",
                    kind: "dropdown",
                    title: "An example of dropdown question",
                    //layout: "none",
                    body: [
                        {
                            type: "media",
                            kind: "image",
                            src: "res/assets/media/images/carousel/pump1.jpg"
                        },
                        {
                            type: "question",
                            text: "<p>This is a {{d1,1}} type of question. You may choose another {{d1,0}} type here. " +
                            "You can use the same list for all drop-downs, or you can mix and match! " +
                            "Here is a different options list: " +
                            "Magna Carta was created in {{d2,2}}.</p>"
                        }

                    ],

                    options: {
                        d1: [
                            "radio",
                            "dropdown",
                            "checkbox",
                            "timed video"
                        ],
                        d2: [
                            "834",
                            "1011",
                            "1215",
                            "1326",
                            "1443"
                        ]
                    },

                    hint: {
                        title: "Hint",
                        type: "hint",
                        body: [
                            {
                                type: "text",
                                text: "<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>"
                            },
                            {
                                type: "media",
                                kind: "image",
                                src: "res/assets/media/images/carousel/pump1.jpg"
                            }
                        ]
                    }

                }
            ]
        },
        {
            poolID: "p2", // Questions pool for Module 2
            content: [
                // Questions
                {
                    id: "p2-1",
                    kind: "orderList",
                    title: "Put these items in order",
                    layout: "none",
                    scorePoints: 4,
                    body: [
                        {
                            type: "question",
                            question: [
                                "Red",
                                "Orange with a much longer line here",
                                "Yellow",
                                "Green",
                                "Blue. This is an example of Order List Question.",
                                "Violet"
                            ]
                        }
                    ],
                    hint: {
                        title: "Hint",
                        type: "hint",
                        body: [
                            {
                                type: "text",
                                text: "<p>000 Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>"
                            }
                        ]
                    }
                },
                // Hotspots (multi) -------------------------
                {
                    id: "p2-2",
                    kind: "hotspots",
                    title: "Select 3 heat exchanger gauges",
                    body: [
                        {
                            type: "media",
                            kind: "image",
                            hasViewer: true,
                            src: "res/assets/media/images/heat_exchanger_guage.jpg",
                            hotspots: [
                                {id: 1, type: 'circle', x: 38.3, y: 26.5, width: 17.6, height: 25.3},
                                {id: 2, type: 'circle', x: 28.6, y: 58.0, width: 19.4, height: 28.3},
                                {id: 3, type: 'circle', x: 46.4, y: 55.3, width: 14.7, height: 22.8}
                            ]
                        },
                        {
                            type: "text",
                            text: "<p>Select 3 heat exchanger gauges by clicking on the image. Click again on your pinned area to remove the pin. You can zoom the image by clicking the <span class='vti_expand inline'></span> icon, but you won't be able to add/remove pins in zoomed mode.</p>"
                        },
                        {
                            type: "question",
                            question: {
                                answerType: "multi", // "multi" or "single"
                            }
                        }
                    ],
                    hint: {
                        title: "Hint",
                        type: "hint",
                        body: [
                            {
                                type: "text",
                                text: "<p>Select 3 heat exchanger gauges by clicking on the image. Click again on your pinned area to remove the pin. You can zoom the image by clicking the <span class='vti_expand inline'></span> icon, but you won't be able to add/remove pins in zoomed mode.</p>"
                            }
                        ]
                    }
                },
                // Hotspots (single) -------------------------
                {
                    id: "p2-3",
                    kind: "hotspots",
                    title: "Select the fire extinguisher",
                    body: [
                        {
                            type: "media",
                            kind: "image",
                            hasViewer: true,
                            src: "res/assets/media/images/fire_extinguisher.jpg",
                            hotspots: [{id:1, type:'rect', x:25.8, y:40.9, width:12.5, height:51.3}]
                        },
                        {
                            type: "text",
                            text: "<p>Select the fire extinguisher by clicking on the image. Click on another area to change your answer. You can zoom the image by clicking the <span class='vti_expand inline'></span> icon, but you won't be able to add/remove pins in zoomed mode.</p>"
                        },
                        {
                            type: "question",
                            question: {
                                answerType: "single", // "multi" or "single"
                            }
                        }
                    ],
                    hint: {
                        title: "Hint",
                        type: "hint",
                        body: [
                            {
                                type: "text",
                                text: "<p>Select 3 heat exchanger gauges by clicking on the image. Click again on your pinned area to remove the pin. You can zoom the image by clicking the <span class='vti_expand inline'></span> icon, but you won't be able to add/remove pins in zoomed mode.</p><p>If you click too many times, your answer will be disqualified.</p>"
                            }
                        ]
                    }
                }
            ]
        },


        /// MODALS =================================================================
        {
            poolID: "modals",

            content: [

                {
                    type: "about",
                    title: "About",
                    id: "system-about",
                    body: [
                        {
                            type: "logos",
                            content: [
                                "res/assets/media/logos/videotel_grey.svg"
                            ]
                        },
                        {
                            type: "production",
                            title: "A VIDEOTEL PRODUCTION"
                        },
                        {
                            type: "credits",
                            title: "Credits",
                            content: [
                                {
                                    title: "In association with:",
                                    list: [
                                        "The Steamship Mutual Underwriting Association (Bermuda) Ltd"
                                    ]
                                },
                                {
                                    title: "The producers would like to acknowledge the assistance of:",
                                    list: [
                                        "A P Moller-Maersk A/S (Maersk Tankers)",
                                        "BIMCO",
                                        "CMA GSM",
                                        "The Great Eastern Shipping Co. Ltd.",
                                        "Grimaldi Group Naples",
                                        "Inernational Maritime Organization (IMO)",
                                        "SMC Marine Services"
                                    ]
                                },
                                {
                                    title: "Consultants:",
                                    list: [
                                        "Prof. Jon Wonham",
                                        "Stuart McCuloch"
                                    ]
                                },
                                {
                                    title: "Course Writer:",
                                    list: [
                                        "Richard Hackett"
                                    ]
                                },
                                {
                                    title: "Producer:",
                                    list: [
                                        "Raal Harris"
                                    ]
                                },
                                {
                                    title: "Multimedia Team:",
                                    list: [
                                        "Jon Saunders",
                                        "Kate Crabtree",
                                        "Max Kostin",
                                        "Terry Eagling-Joyce"
                                    ]
                                }
                            ]
                        },
                        {
                            type: "legal",
                            title: "Legal",
                            content: [
                                {
                                    title: "Warning",
                                    list: [
                                        "<p>Any unauthorised copying, hiring, lending, exhibition, diffusion, sale, public performance or other exploitation of this program is strictly prohibited and may result in prosecution.</p>" +
                                        "<p>This program is intended to reflect the best available techniques and practices at the time of production, it is intended purely as comment. No responsibility is accepted by Videotel, or by any firm, corporation " +
                                        "or organisation who or which has been in any way concerned, with the production or authorised translation, supply or sale of this program for accuracy of any information given here on or for any omission here from.</p>"
                                    ]
                                },
                                {
                                    title: "Version vt5-1",
                                    list: []
                                },
                                {
                                    title: "Copyright© 2016 Videotel",
                                    list: []
                                }
                            ]
                        }
                    ]
                },

                {
                    type: "help",
                    title: "Help",
                    id: "system-help",
                    body: [
                        {
                            type: "support",
                            title: "Contact support",
                            content: [
                                {
                                    name: "Email:",
                                    value: "support@videotel.com"
                                },
                                {
                                    name: "Phone:",
                                    value: "+(44) 207 299 1800"
                                }
                            ]
                        },
                        {
                            type: "icons",
                            title: "Navigation",
                            content: [
                                {
                                    icons: ["vti_empty"],
                                    location: "Location",
                                    description: "Description"
                                },
                                {
                                    icons: ["vti_hamburger"],
                                    location: "Upper left corner",
                                    description: "Main Menu. You can access any screen or section from the Main Menu."
                                },
                                {
                                    icons: ["vti_play", "vti_pause", "vti_play-step-back"],
                                    location: "Upper right corner",
                                    description: "Audio Controls (Play, Pause, Rewind). These buttons will be disabled or enabled, depending on whether the page you are currently viewing has audio."
                                },
                                {
                                    icons: ["vti_arrow-left", "vti_arrow-right"],
                                    location: "Bottom",
                                    description: "Next screen / Previous screen. If you are using touch-screen device, you can swipe left or right to navigate. You can also use your keyboard's Left and Right keys."
                                },
                                {
                                    icons: ["vti_play", "vti_pause", "vti_stop"],
                                    location: "Video",
                                    description: "Play/Pause/Stop video"
                                },
                                {
                                    icons: ["vti_sound-on", "vti_sound-off"],
                                    location: "Video",
                                    description: "Sound on/off"
                                },
                                {
                                    icons: ["vti_list"],
                                    location: "Video",
                                    description: "Select video chapter"
                                },
                                {
                                    icons: ["vti_cc"],
                                    location: "Video",
                                    description: "Select subtitle language"
                                },
                                {
                                    icons: ["vti_fullscreen"],
                                    location: "Video",
                                    description: "Enter or exit fullscreen mode"
                                },
                                {
                                    icons: ["vti_close"],
                                    location: "Modal windows",
                                    description: "Close modal window. You can also press Esc key on your keyboard."
                                },
                                {
                                    icons: ["vti_flip"],
                                    location: "Modal windows",
                                    description: "Flip modal window. Click to see what's on the other side!"
                                },
                                {
                                    icons: ["vti_link-external"],
                                    location: "Text",
                                    description: "External link. Opens new browser window."
                                },
                                {
                                    icons: ["vti_link-internal"],
                                    location: "Text",
                                    description: "Internal link. Navigates to a section or screen within current course."
                                },
                                {
                                    icons: ["vti_link-popup"],
                                    location: "Text",
                                    description: "Modal link. Presents information in a modal (popup) window."
                                },
                                {
                                    icons: ["vti_expand"],
                                    location: "Images",
                                    description: "Press this icon to open Image Viewer."
                                },
                                {
                                    icons: ["vti_zoom-out", "vti_zoom-in"],
                                    location: "Image Viewer",
                                    description: "Zoom-in / Zoom-out controls."
                                },
                                {
                                    icons: ["vti_3d"],
                                    location: "3D content",
                                    description: "Opens 3D viewer."
                                },
                                {
                                    icons: ["vti_vr"],
                                    location: "VR content",
                                    description: "Opens VR viewer."
                                }
                            ]
                        }
                    ]
                },

                {
                    id: "testLeave",
                    title: "Do you want to leave the Test?",
                    type: "dialog",
                    kind: "warning",
                    body: [
                        {
                            type: "text",
                            text: "Your session will be lost. Press 'Yes' to continue or 'No' to stay in the current Test."
                        }
                    ],
                    buttons: [
                        {
                            type: "button",
                            label: "Yes",
                            event: "UserEvent.testLeave",
                            icon: "vti_correct",
                            focus: true,
                            cssClass: "btn-primary"
                        },
                        {
                            type: "button",
                            label: "No",
                            event: "UserEvent.cancel",
                            icon: "vti_incorrect",
                            cssClass: "btn-primary"
                        }
                    ]
                },

                {
                    id: "system-welcome-back",
                    title: "Welcome back!",
                    type: "welcomeBack",
                    body: [
                        {
                            type: "text",
                            text: "Your last visit was on {{date}}. Do you want to continue training from where you left off?"
                        }
                    ],
                    buttons: [
                        {
                            type: "button",
                            label: "Yes",
                            event: "UserEvent.resumeLastVisit",
                            icon: "vti_correct",
                            focus: true,
                            cssClass: "btn-primary"
                        },
                        {
                            type: "button",
                            label: "No",
                            event: "UserEvent.cancel",
                            icon: "vti_incorrect",
                            cssClass: "btn-primary"
                        }
                    ]
                },

                {
                    id: "info-modal",
                    title: "Info Modal",
                    type: "info", // slides from top.
                    body: [
                        {
                            type: "text",
                            text: "<p>Navigation plays a crucial role in the success or failure of any website. An easy to navigate website is more likely to be explored in more detail as compared to a website with more complex navigation.</p>"
                        },
                        {
                            type: "button",
                            label: "OK",
                            icon: "vti_correct",
                            event: "UserEvent.cancel"
                        },
                        {
                            type: "media",
                            kind: "audio",
                            src: "res/assets/media/audio/generic-short.mp3"
                        }
                    ]
                },

                {
                    id: "study-1",
                    title: "Additional material I",
                    type: "PDF",
                    src: "res/assets/docs/2.pdf", // Path to PDF file
                    page: 4 // Page to display. Optional. Default is page 1.
                },
                {
                    id: "hint-text",
                    title: "Modal example",
                    type: "hint",
                    body: [
                        {
                            type: "text",
                            text: "<p>1. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>" +
                            "<p>2. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>" +
                            "<p>3. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>" +
                            "<p>4. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>" +
                            "<p>5. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>"
                        },
                        {
                            type: "media",
                            kind: "audio",
                            src: "res/assets/media/audio/generic-short.mp3"
                        }
                    ]
                },
                {
                    id: "prompt-multimedia",
                    title: "Multimedia Modal",
                    type: "hint",
                    body: [
                        {
                            type: "text",
                            text: "<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>"
                        },
                        {
                            type: "media",
                            kind: "image",
                            src: "res/assets/media/images/bilge_controls.jpg"
                        },
                        {
                            type: "media",
                            kind: "audio",
                            src: "res/assets/media/audio/generic-short.mp3"
                        }
                    ]
                },
                {
                    id: "prompt-video",
                    type: "hint",
                    title: "Video Prompt",
                    body: [
                        {
                            type: "media",
                            kind: "video",
                            src: 'res/assets/media/video/005.mp4',
                            poster: 'res/assets/media/video/005.png',
                            defaultControls: true
                        }
                    ]
                },
                {
                    id: "prompt-character",
                    type: "character",
                    title: "Jose",
                    image: "res/assets/media/prompt/character01a.png",
                    body: [
                        {
                            type: "text",
                            text: "<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>" +
                            "<p> The runtime has added lightweight, best-effort detection of concurrent misuse of maps. As always, if one goroutine is writing to a map, no other goroutine should be reading or writing the map concurrently. If the runtime detects this condition, it prints a diagnosis and crashes the program. The best way to find out more about the problem is to run it under the race detector, which will more reliably identify the race and give more detail. The runtime has also changed how it prints program-ending panics. It now prints only the stack of the panicking goroutine, rather than all existing goroutines. This behavior can be configured using the GOTRACEBACK environment variable or by calling the debug.SetTraceback function. </p>"
                        },
                        {
                            type: "media",
                            kind: "audio",
                            src: "res/assets/media/audio/generic-short.mp3"
                        }
                    ]
                },
                {
                    id: "hint-image",
                    type: "hint",
                    title: "Hint Prompt + image",
                    image: "res/assets/media/images/carousel/003.jpg",
                    body: [
                        {
                            type: "text",
                            text: "<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>" +
                            "<p> The runtime has added lightweight, best-effort detection of concurrent misuse of maps. As always, if one goroutine is writing to a map, no other goroutine should be reading or writing the map concurrently. If the runtime detects this condition, it prints a diagnosis and crashes the program. The best way to find out more about the problem is to run it under the race detector, which will more reliably identify the race and give more detail.</p>"
                        },
                        {
                            type: "media",
                            kind: "audio",
                            src: "res/assets/media/audio/generic-short.mp3"
                        }
                    ]
                },
                {
                    id: "flip-prompt",
                    type: "flip",
                    screens: [
                        {
                            title: "Side 1",
                            flipBtn: "Flip me",
                            image: "res/assets/media/images/carousel/pump2.jpg",
                            body: [
                                {
                                    type: "text",
                                    text: "<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, " +
                                    "totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>"
                                },
                                {
                                    type: "media",
                                    kind: "audio",
                                    src: "res/assets/media/audio/generic-short.mp3"
                                }
                            ]
                        },
                        {
                            title: "Side 2",
                            flipBtn: "Flip me back",
                            body: [
                                {
                                    type: "text",
                                    text: "<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, " +
                                    "totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>"
                                },
                                {
                                    type: "media",
                                    kind: "audio",
                                    src: "res/assets/media/audio/biggerboat.mp3"
                                }
                            ]
                        }
                    ]

                },

                // GLOSSARY =====================================================
                {
                    type: "glossary",
                    title: "Glossary",
                    id: "glossary", // Static content ID.
                    sortItems: true, // If set to true, will auto-sort items in ASC order. Optional.
                    body: [

                        {
                            a: "GlossaryPage item 1",
                            b: "Description of the item."
                        },
                        {
                            a: "Another item",
                            b: "Another description of the item."
                        },
                        {
                            a: "Megator",
                            b: "Megator is a leading manufacturer of innovative and unique positive displacement pumps, pumping systems and pollution control solutions."
                        },
                        {
                            a: "Sliding Shoe Pump",
                            b: "Originally developed for mining and marine applications, it is a unique self-priming, positive displacement pump that runs safely under dry suction, has low shear and emulsification characteristics, high suction lift, and is self-compensating for wear."
                        },
                        {
                            a: "Rotary Lobe",
                            b: "Uniquely designed for high-pressure, high-viscosity, and abrasives- and solids-laden stationary pumping applications."
                        },
                        {
                            a: "Weir Skimmers",
                            b: "Designed to remove pollutants and nuisance media from the surface of calm or sheltered waters and shoreline areas."
                        },
                        {
                            a: "Truxor",
                            b: "This Multi-Purpose Amphibious Vehicle is equipped with the Salarollpump and brush skimmer for oil spill cleanup. Its superior amphibious properties make it ideal for working in and around the borders between land and water."
                        },
                        {
                            a: "Recovery Unit",
                            b: "A unique packaged pump and skimmer system designed to recover oil from water in all industrial applications."
                        }
                    ]
                }


            ]
        }

    ]
});










