define(
        {
           languages: [
               {
                   title: "English",
                   file: "data_en-gb"
               },
               {
                   title: "Русский",
                   file: "data_ru"
               },
               {
                   title: "中文",
                   file: "data_zh-cn"
               }
           ]
        }
);