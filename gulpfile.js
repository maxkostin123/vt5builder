/**
 * Created by max.kostin on 21/05/2015
 */

const gulp = require("gulp");
const less = require("gulp-less");
const rename = require("gulp-rename");
const autoprefixer = require("gulp-autoprefixer");
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const fs = require("fs-extra");



/* =====================================================================================
MAKE RELEASE
 ======================================================================================*/
gulp.task("release", () => {

    let zip = require("gulp-zip");

    let settings = JSON.parse(fs.readFileSync("package.json").toString());
    let versionArr = settings.version.split(".");
    versionArr[2] = parseInt(versionArr[2]) + 1;
    settings.version = versionArr.join(".");

    let zipName = versionArr.join("_") + ".zip";
    settings.updates.files[0].src = zipName;

    fs.emptyDirSync("../RELEASE");

    fs.writeFileSync('../RELEASE/package.json', JSON.stringify(settings));

        fs.copy("./app", "../RELEASE/app", ()=> {
        fs.copy("./b", "../RELEASE/b", ()=> {
            fs.copy("./doc", "../RELEASE/doc", ()=> {
                fs.copy("./sample_app", "../RELEASE/sample_app", ()=> {

                    return gulp.src("../RELEASE/**/*")
                        .pipe(zip(zipName))
                        .pipe(gulp.dest('../RELEASE'));
                })
            })
        })
    })


});


/* =====================================================================================
 CSS
 ======================================================================================*/
gulp.task("less", function () {
    "use strict";
    clear();
    return gulp.src("less/main.less")
        .pipe(less())
        .on("error", function (err) {
            console.log(err);
            this.emit("end");
        })
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(rename("styles.css"))
        .pipe(gulp.dest('b/styles/'));
});




/* =====================================================================================
 CLEARS CONSOLE WINDOW
 ======================================================================================*/
function clear() {
    process.stdout.write('\033c');
}



/* =====================================================================================
 MAIN
 ======================================================================================*/



gulp.task('watch', function () {
    gulp.watch('less/**/*.less', ['less']);
});

gulp.task('default', ['watch']);













