/**
 * Created by Max Kostin on 17/02/2016
 *
 * This is a wrapper class for communicating with servers/services. You can add any variables or methods,
 * but the ones defined below are required: if any of these are missing, the application will throw an Error.
 *
 * If any of these methods are not needed, just leave their body empty.
 *
 * In the most simple scenario, you will need to do the following:
 *
 * (1) add a variable with the server address and use this address later to
 * send or receive data. This should probably be done inside the init() method.
 *
 * (2) Obtain user name from the server and change value of the variable userName (also inside init method).
 *
 * (3) Obtain sessionId if needed.
 *
 * (4) Invoke method this.serviceReadyCallback() (defined by VT5 app) when 1, 2 and 3 are done.
 *
 * (5) Test results is a an Object with many properties (example is at the end of this file). You can grab any value needed,
 * construct your own object/json/xml and send it on to your endpoint.
 */
"use strict";

define ({

    minPassScore:0,
    userName: "VT5",
    server: "",
    tid: "12345",
    courseId: "",

    /**
     * This method should be invoked when user has successfully logged in. It is defined by VT5 App.
     * @param message:String optional
     */
    loginSuccessCallback: undefined,

    /**
     * This method should be invoked when user login has failed. It is defined by VT5 App.
     * @param message:String optional
     */
    loginFailCallback: undefined,


    /** This method should be invoked when
     * a) user name/surname is available.
     * b) training ID is available.
     * c) user is logged in (if required).
     * It can be called from within any method of this file where it makes sense and when all required fields are available.
     * This method is defined by VT5 app.
     * @param message String optional
     */
    serviceReadyCallback: undefined,


    /**
     * Invoke this method when user's session has expired and he needs to login outside VT5 app.
     * If a session can expire, you can, for example, set an interval of 5/10/60 minutes or whatever is required and
     * check server for session's validity.
     * This method should not be used together with requestLoginCallback() below.
     * This method is defined by VT5 app.
     * @param message:String optional
     */
    sessionExpiredCallback: undefined,

    /**
     * Invoke this method when user's session has expired and he can login from VT5 app.
     * If a session can expire, you can, for example, set an interval of 5/10/60 minutes or whatever is required and
     * check server for session's validity.
     * his method should not be used together with sessionExpiredCallback() above.
     * This method is defined by VT5 app.
     * @param message:String optional
     */
    requestLoginCallback: undefined,


    /**
     * Called when App is initialized
     */
    init:function(){

        if (this.serviceReadyCallback) {
            this.serviceReadyCallback();
        }

        setTimeout(function() {
            //this.sessionExpiredCallback("Your session has expired.");
            //this.requestLoginCallback("Your session has expired.")
        }, 1000);

    },


    /**
     * Sets minimum pass score
     * @param value
     */
    setMinPassScore: function (value) {
        this.minPassScore = value;
    },

    /**
     * Sends test result to the service.
     * @param result
     */
    sendTestResults: function (result) {
        console.log(result);
    },
    
    sendVar:function(){
        
    },

    /**
     * Called when Window is closed
     */
    onExit: function () {

    },



    /**
     * Tells VT5 app if the user needs to log in.
     * @returns {boolean}
     */
    requiresAuth:function() {
        return false;
    },

    /**
     * Returns an Array of label/type Objects to be used as fields in the Login Modal.
     * This method is required if requireAuth() is set to return boolean 'true'.
     * @returns {{title: string, fields: *[]}}
     */
    getAuthFields:function() {
        return {
            type: "login",
            title: "Login",
            fields: [
                {
                    label: "First Name",
                    type: "text",
                    id: "firstname"
                },
                {
                    label: "Last Name",
                    type: "text",
                    id: "lastname"
                },
                {
                    label: "Training ID",
                    type: "password",
                    id: "tid"
                }
            ]
        };
    },


    login:function (argsArray, callback) {
        this.loginSuccessCallback()
    },


    receiveVar: function(name, callback) {

    },

    /**
     * Returns user name
     * @returns {string}
     */
    getUserName: function() {
        return this.userName;
    },

    /**
     * Returns training ID
     * @returns {string}
     */
    getTrainingId: function() {
        return this.tid;
    }


});

/*
 TestResultData {
 score: 3,
 maxScore: 11,
 scorePercent: 27,
 duration: 10071,
 userName: "VT5"}
 breakdown: Array[2]0: Object
 maxScore: 7
 poolId: "p1"
 testId: "final"
 poolName: "Section 1"
 userScore: 3
 Object
 maxScore: 4
 poolId: "p2"
 poolName: undefined
 userScore: 0
 certificateType: "final"
 courseName: "VT5"
 date: "12/07/2016"
 duration: 10071
 durationString: "00:00:10"
 maxScore: 11
 passed: true
 score: 3
 scorePercent: 27
 testName: "Final Test"
 trainingId: "12345"
 userName: "VT5"
 }
 */


