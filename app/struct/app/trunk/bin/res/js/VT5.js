var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var vt;
(function (vt) {
    var TestMode;
    (function (TestMode) {
        TestMode[TestMode["Test"] = 0] = "Test";
        TestMode[TestMode["Tutorial"] = 1] = "Tutorial";
        TestMode[TestMode["Review"] = 2] = "Review";
    })(TestMode = vt.TestMode || (vt.TestMode = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var model;
    (function (model) {
        var TestResultData = (function () {
            function TestResultData() {
                this.score = 0;
                this.maxScore = 0;
                this.scorePercent = 0;
                this.duration = 0;
            }
            return TestResultData;
        }());
        model.TestResultData = TestResultData;
    })(model = vt.model || (vt.model = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var common;
    (function (common) {
        var errors;
        (function (errors) {
            var Error = (function () {
                function Error() {
                }
                return Error;
            }());
            Error.ERROR_SINGLETON = "This is a Singleton. Use ClassName.getInstance()";
            errors.Error = Error;
        })(errors = common.errors || (common.errors = {}));
    })(common = vt.common || (vt.common = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var model;
    (function (model) {
        var AppModel = (function () {
            function AppModel(enforcer) {
                this._contentMap = {};
                this._poolsMap = {};
                this._CIDMap = {};
                this._pagesList = [];
                this._devMode = false;
                this._userName = "";
                this._trainingId = "";
                this._theme = "light";
                this._titleDividerChar = "&nbsp;&nbsp;&#8226;&nbsp;&nbsp;";
                if (!enforcer) {
                    throw "[vt.model.AppModel] is a Singleton. Use AppModel.getInstance()";
                }
            }
            AppModel.prototype.getTheme = function () {
                return this._theme;
            };
            AppModel.prototype.setTheme = function (value) {
                this._theme = value;
            };
            AppModel.prototype.getUserName = function () {
                return this._userName;
            };
            AppModel.prototype.setUserName = function (value) {
                this._userName = value;
            };
            AppModel.prototype.getTrainingId = function () {
                return this._trainingId;
            };
            AppModel.prototype.setTrainingId = function (value) {
                this._trainingId = value;
            };
            AppModel.getInstance = function () {
                if (this._instance)
                    return this._instance;
                this._instance = new AppModel(new Enforcer());
                return this._instance;
            };
            AppModel.prototype.setData = function (data) {
                this._product = data.product;
                this._introLogo = data.introLogo;
                this.parsePools(data);
                this.parseData(data.data, [], "");
                this.data = data.data;
                this._ui = data.ui;
            };
            AppModel.prototype.isDevMode = function () {
                return this._devMode;
            };
            AppModel.prototype.setDevMode = function (value) {
                this._devMode = value;
            };
            AppModel.prototype.getProductData = function () {
                return this._product;
            };
            AppModel.prototype.getData = function () {
                return this.data.content;
            };
            AppModel.prototype.getUIData = function () {
                return this._ui;
            };
            AppModel.prototype.getContentItemById = function (id) {
                return this._contentMap[id];
            };
            AppModel.prototype.getContentItemByIndex = function (index) {
                return this._pagesList[index];
            };
            AppModel.prototype.getContentItemByCID = function (cid) {
                return this._CIDMap[cid];
            };
            AppModel.prototype.getPageIndex = function (id) {
                return this._contentMap[id].position;
            };
            AppModel.prototype.getTotalPages = function () {
                return this._pagesList.length;
            };
            AppModel.prototype.setTestResult = function (value) {
                this._testResult = value;
            };
            AppModel.prototype.getTestResult = function () {
                return this._testResult;
            };
            AppModel.prototype.getTestResponse = function () {
                return this._poolsMap._testResponse;
            };
            AppModel.prototype.getPoolItem = function (id) {
                return this._poolsMap[id];
            };
            AppModel.prototype.getIntroLogo = function () {
                return this._introLogo;
            };
            AppModel.prototype.parsePools = function (data) {
                if (data.pools) {
                    for (var i = 0; i < data.pools.length; i++) {
                        var pool = data.pools[i];
                        this._poolsMap[pool.poolID] = pool;
                        for (var j = 0; j < pool.content.length; j++) {
                            var poolItem = pool.content[j];
                            poolItem.poolId = pool.poolID;
                            poolItem.poolName = pool.poolName;
                            if (this._poolsMap[poolItem.id]) {
                                throw "[vt.model.AppModel] pool item " + poolItem.id + " already exists.";
                            }
                            this._poolsMap[poolItem.id] = poolItem;
                        }
                    }
                }
                if (data.test.testStart) {
                    this._poolsMap._testStart = data.test.testStart;
                }
                if (data.test.testEnd) {
                    this._poolsMap._testEnd = data.test.testEnd;
                }
                if (data.test.testResult) {
                    this._poolsMap._testResult = data.test.testResult;
                }
                if (data.test.response) {
                    this._poolsMap._testResponse = data.test.response;
                }
            };
            AppModel.prototype.parseData = function (obj, tmpArray, id) {
                if (tmpArray === void 0) { tmpArray = []; }
                if (id === void 0) { id = ""; }
                obj.id = id.substring(0, id.length - 1);
                if (obj.CID) {
                    if (this._CIDMap[obj.CID]) {
                        throw "Content Item with CID " + obj.CID + " already exists!";
                    }
                    else {
                        this._CIDMap[obj.CID] = obj;
                    }
                }
                if (obj.content) {
                    for (var i = 0; i < obj.content.length; i++) {
                        var o = obj.content[i];
                        o.id = id + (i + 1);
                        o.parentId = obj.id;
                        this._contentMap[o.id] = o;
                        if (o.ref) {
                            var refObject = this._poolsMap[o.ref];
                            o.body = AppModel.clone(refObject.body);
                            if (!o.title) {
                                o.title = refObject.title ? refObject.title : "";
                            }
                            AppModel.copyMissingProperties(refObject, o);
                            if (o.type === "question") {
                                o.answers = [];
                            }
                        }
                        if (o.body) {
                            obj.hasPages = true;
                            o.pagesInSection = AppModel.getNumPages(obj, o);
                            if (o.type !== "splash") {
                                o.header = this.getTitles(o.id, "");
                                o.header = o.header.substr(this._titleDividerChar.length, o.header.length);
                            }
                            this._pagesList.push(o);
                            o.position = this._pagesList.length - 1;
                        }
                        this.parseData(o, [], id + (i + 1) + "_");
                        if (o.type === "test") {
                            o.content = this.getTest(o);
                        }
                    }
                }
                return tmpArray;
            };
            AppModel.prototype.getTitles = function (id, list) {
                var item = this.getContentItemById(id);
                if (item.nav_title) {
                    list = this._titleDividerChar + item.nav_title + list;
                }
                if (item.parentId) {
                    return this.getTitles(item.parentId, list);
                }
                return list;
            };
            AppModel.getNumPages = function (parent, obj) {
                var num = 0;
                if (parent.content) {
                    for (var i = 0; i < parent.content.length; i++) {
                        if (parent.content[i].body && !parent.content[i].notCountable) {
                            num++;
                            if (parent.content[i] === obj) {
                                obj.pageIndex = num;
                            }
                        }
                    }
                }
                return num;
            };
            AppModel.prototype.getTest = function (obj) {
                obj.hasPages = true;
                var arr = [];
                var questions = [];
                if (obj.testStart) {
                    var tStart = {
                        id: obj.id + "_" + "testStart",
                        parentId: obj.id,
                        position: this._pagesList.length,
                        type: "page",
                        layout: this._poolsMap._testStart.layout,
                        title: this._poolsMap._testStart.title,
                        pageIndex: 0,
                        pagesInSection: null,
                        body: this._poolsMap._testStart.body,
                        hasPages: false
                    };
                    this._contentMap[tStart.id] = tStart;
                    this._pagesList.push(tStart);
                    arr.push(tStart);
                }
                for (var i = 0; i < obj.pools.length; i++) {
                    if (obj.pools[i].items) {
                        if (obj.pools[i].items.length === 0) {
                            questions = questions.concat(this._poolsMap[obj.pools[i].poolID].content);
                        }
                        else {
                            for (var j = 0; j < obj.pools[i].items.length; j++) {
                                var id = obj.pools[i].items[j].toString();
                                var q = this._poolsMap[id];
                                questions.push(q);
                            }
                        }
                    }
                    else if (obj.pools[i].numItems) {
                        var poolArr = this._poolsMap[obj.pools[i].poolID].content;
                        if (obj.randomize) {
                            AppModel.randomize(poolArr);
                        }
                        questions = questions.concat(poolArr.slice(0, obj.pools[i].numItems));
                    }
                }
                if (obj.randomize) {
                    AppModel.randomize(questions);
                }
                for (var i = 0; i < questions.length; i++) {
                    var question = AppModel.clone(questions[i]);
                    question.id = obj.id + "_" + question.id;
                    question.position = this._pagesList.length;
                    question.nav_title = "";
                    question.type = "question";
                    question.scorePoints = question.scorePoints || 1;
                    question.parentId = obj.id;
                    question.pageIndex = i + 1;
                    question.userAnswers = [];
                    question.pagesInSection = questions.length;
                    question.position = this._pagesList.length;
                    question.testMode = obj.testMode;
                    this._contentMap[question.id] = question;
                    this._pagesList.push(question);
                    arr.push(question);
                }
                if (obj.testEnd) {
                    var tEnd = {
                        id: obj.id + "_" + "testEnd",
                        parentId: obj.id,
                        position: this._pagesList.length,
                        type: "page",
                        pageIndex: 0,
                        pagesInSection: null,
                        body: this._poolsMap._testEnd.body,
                        hasPages: false
                    };
                    AppModel.copyMissingProperties(this._poolsMap._testEnd, tEnd);
                    this._contentMap[tEnd.id] = tEnd;
                    this._pagesList.push(tEnd);
                    arr.push(tEnd);
                }
                if (obj.testResult) {
                    var tResult = AppModel.clone(this._poolsMap._testResult);
                    tResult.id = obj.id + "_" + "testResult";
                    tResult.type = "testResult";
                    tResult.parentId = obj.id;
                    tResult.pageIndex = 0;
                    tResult.pagesInSection = null;
                    tResult.position = this._pagesList.length;
                    this._contentMap[tResult.id] = tResult;
                    this._pagesList.push(tResult);
                }
                return arr;
            };
            AppModel.randomize = function (array) {
                for (var i = 0; i < array.length; i++) {
                    var rand = Math.floor(Math.random() * array.length);
                    var tmp = array[i];
                    array[i] = array[rand];
                    array[rand] = tmp;
                }
            };
            AppModel.clone = function (src) {
                return JSON.parse(JSON.stringify(src));
            };
            AppModel.copyMissingProperties = function (src, target) {
                for (var p in src) {
                    if (!target.hasOwnProperty(p)) {
                        target[p] = src[p];
                    }
                }
            };
            return AppModel;
        }());
        model.AppModel = AppModel;
        var Enforcer = (function () {
            function Enforcer() {
            }
            return Enforcer;
        }());
    })(model = vt.model || (vt.model = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var events;
    (function (events) {
        var VTEvent = (function () {
            function VTEvent(type, data, target) {
                if (data === void 0) { data = null; }
                if (target === void 0) { target = null; }
                this.type = type;
                this.data = data;
                this.target = target;
            }
            VTEvent.prototype.clone = function () {
                return new VTEvent(this.type, this.data, this.target);
            };
            return VTEvent;
        }());
        events.VTEvent = VTEvent;
    })(events = vt.events || (vt.events = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var events;
    (function (events) {
        var NavEvent = (function (_super) {
            __extends(NavEvent, _super);
            function NavEvent(type, data, target) {
                if (data === void 0) { data = null; }
                if (target === void 0) { target = null; }
                var _this = _super.call(this, type, data, target) || this;
                _this.type = type;
                _this.data = data;
                _this.target = target;
                return _this;
            }
            return NavEvent;
        }(vt.events.VTEvent));
        NavEvent.NAME = "NavEvent";
        NavEvent.CONTENT_ITEM_SELECT = NavEvent.NAME + ".contentItemSelected";
        NavEvent.CLOSE_MENU = NavEvent.NAME + ".closeMenu";
        NavEvent.SWIPE_LEFT = NavEvent.NAME + ".swipeLeft";
        NavEvent.SWIPE_RIGHT = NavEvent.NAME + ".swipeRight";
        NavEvent.TOUCH_SCROLL_START = NavEvent.NAME + ".touchScrollStart";
        NavEvent.NEXT = NavEvent.NAME + ".next";
        NavEvent.PREV = NavEvent.NAME + ".prev";
        NavEvent.PAGE_CHANGE = NavEvent.NAME + ".pageChange";
        NavEvent.GOTO_CID = NavEvent.NAME + ".gotoCID";
        events.NavEvent = NavEvent;
    })(events = vt.events || (vt.events = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var events;
    (function (events) {
        var SoundEvent = (function (_super) {
            __extends(SoundEvent, _super);
            function SoundEvent(actionType, data, target) {
                if (data === void 0) { data = null; }
                if (target === void 0) { target = null; }
                var _this = _super.call(this, SoundEvent.NAME, data, target) || this;
                _this.actionType = actionType;
                _this.data = data;
                _this.target = target;
                return _this;
            }
            return SoundEvent;
        }(vt.events.VTEvent));
        SoundEvent.NAME = "SoundEvent";
        SoundEvent.SOUND_EVENT = SoundEvent.NAME;
        SoundEvent.ACTION_PLAY = SoundEvent.NAME + ".play";
        SoundEvent.ACTION_PLAY_COMPLETE = SoundEvent.NAME + ".complete";
        SoundEvent.ACTION_REPLAY = SoundEvent.NAME + ".replay";
        SoundEvent.ACTION_PAUSE = SoundEvent.NAME + ".pause";
        SoundEvent.ACTION_SOUND_ON = SoundEvent.NAME + ".soundOn";
        SoundEvent.ACTION_SOUND_OFF = SoundEvent.NAME + ".soundOff";
        SoundEvent.ACTION_CUE_POINT = SoundEvent.NAME + ".cuePoint";
        events.SoundEvent = SoundEvent;
    })(events = vt.events || (vt.events = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var events;
    (function (events) {
        var UserEvent = (function (_super) {
            __extends(UserEvent, _super);
            function UserEvent(actionType, data, target) {
                if (data === void 0) { data = null; }
                if (target === void 0) { target = null; }
                var _this = _super.call(this, UserEvent.USER_EVENT, data, target) || this;
                _this.actionType = actionType;
                _this.data = data;
                _this.target = target;
                return _this;
            }
            return UserEvent;
        }(vt.events.VTEvent));
        UserEvent.NAME = "UserEvent";
        UserEvent.USER_EVENT = UserEvent.NAME + ".userEvent";
        UserEvent.ACTION_NEXT = UserEvent.NAME + ".next";
        UserEvent.ACTION_SUBMIT_TEST = UserEvent.NAME + ".submitTest";
        UserEvent.ACTION_REVIEW_TEST = UserEvent.NAME + ".reviewTest";
        UserEvent.ACTION_TEST_LEAVE = UserEvent.NAME + ".testLeave";
        UserEvent.ACTION_START_TRAINING = UserEvent.NAME + ".startTraining";
        UserEvent.ACTION_CANCEL_MODAL = UserEvent.NAME + ".cancel";
        UserEvent.ACTION_SHOW_MODAL = UserEvent.NAME + ".showModal";
        UserEvent.ACTION_SHOW_VIEWER = UserEvent.NAME + ".showViewer";
        UserEvent.ACTION_GOTO_CID = UserEvent.NAME + ".gotoCID";
        UserEvent.ACTION_LOGIN = UserEvent.NAME + ".login";
        UserEvent.ACTION_LOGIN_SUCCESS = UserEvent.NAME + ".loginSuccess";
        UserEvent.ACTION_LOGIN_FAIL = UserEvent.NAME + ".loginFail";
        UserEvent.ACTION_SERVICE_READY = UserEvent.NAME + ".serviceReady";
        UserEvent.ACTION_SET_THEME_DARK = UserEvent.NAME + ".setThemeDark";
        UserEvent.ACTION_SET_THEME_LIGHT = UserEvent.NAME + ".setThemeLight";
        UserEvent.ACTION_AUTH = UserEvent.NAME + ".auth";
        UserEvent.ACTION_LOGOUT = UserEvent.NAME + ".logout";
        UserEvent.ACTION_SHOW_WELCOME = UserEvent.NAME + ".showWelcome";
        UserEvent.ACTION_PLAY_AUDIO = UserEvent.NAME + ".playAudio";
        UserEvent.ACTION_STOP_AUDIO = UserEvent.NAME + ".stopAudio";
        UserEvent.ACTION_PAUSE_MAIN_AUDIO = UserEvent.NAME + ".stopMainAudio";
        UserEvent.ACTION_SET_LANGUAGE = UserEvent.NAME + ".setLanguage";
        UserEvent.ACTION_RESUME_LAST_VISIT = UserEvent.NAME + ".resumeLastVisit";
        events.UserEvent = UserEvent;
    })(events = vt.events || (vt.events = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var VTPromise = (function () {
        function VTPromise(resolve, reject) {
            if (reject === void 0) { reject = null; }
            this.resolved = false;
            resolve();
        }
        VTPromise.prototype.then = function (resolver) {
            if (resolver === void 0) { resolver = null; }
            if (!this.resolved) {
                this.resolveFunc = resolver;
            }
            else {
                if (this.resolveFunc)
                    this.resolveFunc();
            }
        };
        VTPromise.prototype.resolve = function () {
            var _this = this;
            window.setTimeout(function () {
                _this.resolved = true;
                _this.then();
            }, 2);
        };
        return VTPromise;
    }());
    vt.VTPromise = VTPromise;
})(vt || (vt = {}));
var vt;
(function (vt) {
    var events;
    (function (events) {
        var Dispatcher = (function () {
            function Dispatcher() {
                this._listeners = {};
            }
            Dispatcher.prototype.addListener = function (type, listener) {
                if (this._listeners[type] === undefined) {
                    this._listeners[type] = [];
                }
                this._listeners[type].push(listener);
            };
            Dispatcher.prototype.removeListener = function (type, listener) {
                if (!this._listeners[type])
                    return;
                var ind = this._listeners[type].indexOf(listener);
                if (ind != -1)
                    this._listeners[type].splice(ind, 1);
            };
            Dispatcher.prototype.removeAllListeners = function (type) {
                if (type) {
                    delete this._listeners[type];
                }
                else {
                    this._listeners = {};
                }
            };
            Dispatcher.prototype.hasListener = function (type, listener) {
                return (this._listeners[type] !== undefined && this._listeners[type].indexOf(listener) != -1);
            };
            Dispatcher.prototype.dispatch = function (evt) {
                var _this = this;
                if (this._listeners[evt.type]) {
                    evt.target = this;
                    this._listeners[evt.type].forEach(function (el) {
                        el.call(_this, evt);
                    });
                }
            };
            return Dispatcher;
        }());
        events.Dispatcher = Dispatcher;
    })(events = vt.events || (vt.events = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var view;
    (function (view) {
        var renderers;
        (function (renderers) {
            var ButtonRenderer = (function () {
                function ButtonRenderer() {
                }
                ButtonRenderer.render = function (item) {
                    var cssClass = item.cssClass ? item.cssClass : "btn-secondary";
                    switch (item.type) {
                        case "button":
                        case undefined:
                        case "":
                            if (item.label) {
                                return ButtonRenderer.renderBtn(item);
                            }
                            else {
                                var payload = ButtonRenderer.getPayloadStr(item);
                                var evt = ButtonRenderer.getEvtStr(item);
                                return "<button class=\"" + cssClass + "\" " + evt + " " + payload + "><span class=\"" + item.icon + "\"></span></button>";
                            }
                        case "toggle":
                            var toggleVO = item;
                            var p1 = "";
                            var p2 = "";
                            if (toggleVO.state1.label) {
                                p1 = ButtonRenderer.renderTogglePart(toggleVO.state1, "toggle-off " + cssClass);
                                p2 = ButtonRenderer.renderTogglePart(toggleVO.state2, "toggle-on " + cssClass);
                                return "<label class=\"btn-toggle " + cssClass + "\"><input type=\"checkbox\">" + p1 + p2 + "</label>";
                            }
                            else {
                                p1 = ButtonRenderer.renderTogglePart(toggleVO.state1, "toggle-off " + cssClass);
                                p2 = ButtonRenderer.renderTogglePart(toggleVO.state2, "toggle-on " + cssClass);
                                return "<label class=\"btn-toggle " + cssClass + "\"><input type=\"checkbox\">" + p1 + p2 + "</label>";
                            }
                        case "toolButton":
                            return ButtonRenderer.renderToolBtn(item);
                        case "switchButton":
                            var toggleVO = item;
                            return toggleVO.state1.label + " <label class=\"btn-switch\">\n                        <input type=\"checkbox\" \n                        data-off=\"" + toggleVO.state1.event + "\" \n                        data-on=\"" + toggleVO.state2.event + "\"><span class=\"knob\"></span>\n                        </label> " + toggleVO.state2.label;
                        default:
                            return "*** UNKNOWN BUTTON TYPE ***";
                    }
                };
                ButtonRenderer.renderBtn = function (item) {
                    var cssClass = item.cssClass ? "class=\"" + item.cssClass + "\"" : "class='btn-secondary'";
                    var icon = item.icon ? "<span class=\"" + item.icon + "\"></span>" : "";
                    var payload = ButtonRenderer.getPayloadStr(item);
                    var evt = ButtonRenderer.getEvtStr(item);
                    var focus = item.focus ? " data-focus='true'" : "";
                    if (item.iconPosition === "right") {
                        return "<button " + cssClass + " " + evt + " " + payload + focus + "><span>" + item.label + "</span>" + icon + "</button>";
                    }
                    else {
                        return "<button " + cssClass + " " + evt + " " + payload + focus + ">" + icon + "<span>" + item.label + "</span></button>";
                    }
                };
                ButtonRenderer.renderTogglePart = function (item, state) {
                    var label = item.label ? "<span>" + item.label + "</span>" : "";
                    var icon = item.icon ? "<span class=\"" + item.icon + "\"></span>" : "";
                    var payload = ButtonRenderer.getPayloadStr(item);
                    var minWidth = ButtonRenderer.getMinWidthStyle(item);
                    var evt = ButtonRenderer.getEvtStr(item);
                    return "<span class=\"" + state + "\" " + evt + " " + payload + " " + minWidth + ">" + icon + label + "</span>";
                };
                ButtonRenderer.renderToolBtn = function (item) {
                    var payload = ButtonRenderer.getPayloadStr(item);
                    var evt = ButtonRenderer.getEvtStr(item);
                    return "<span><button " + evt + " " + payload + "><span class=\"" + item.icon + "\"></span></button><span>" + item.label + "</span></span>";
                };
                ButtonRenderer.getPayloadStr = function (item) {
                    return item.payload ? "data-payload=\"" + item.payload + "\"" : "";
                };
                ButtonRenderer.getEvtStr = function (item) {
                    return item.event ? "data-event=\"" + item.event + "\"" : "";
                };
                ButtonRenderer.getMinWidthStyle = function (item) {
                    return item.minWidth ? "style=\"min-width: " + item.minWidth + ";\"" : "";
                };
                return ButtonRenderer;
            }());
            renderers.ButtonRenderer = ButtonRenderer;
        })(renderers = view.renderers || (view.renderers = {}));
    })(view = vt.view || (vt.view = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var view;
    (function (view) {
        var nav;
        (function (nav) {
            var NavEvent = vt.events.NavEvent;
            var ButtonRenderer = vt.view.renderers.ButtonRenderer;
            var UserEvent = vt.events.UserEvent;
            var AppModel = vt.model.AppModel;
            var MainNav = (function (_super) {
                __extends(MainNav, _super);
                function MainNav(contentData, uiData) {
                    var _this = _super.call(this) || this;
                    _this.contentData = contentData;
                    _this.uiData = uiData;
                    _this.className = "MainNav";
                    _this.navMap = {};
                    _this.init();
                    return _this;
                }
                MainNav.prototype.getHTML = function () {
                    return this.html;
                };
                MainNav.prototype.setSelected = function (id) {
                    this.selectedItem = this.navMap[id];
                    if (!this.selectedItem)
                        return;
                    var buttons = this.html.querySelectorAll(".selected");
                    for (var i = 0; i < buttons.length; i++) {
                        buttons[i]['classList'].remove("selected");
                    }
                    var trail = this.selectedItem.id.split("_");
                    var str = "";
                    while (trail.length > 0) {
                        str += trail.shift();
                        var btn = this.html.querySelector("[data-id='" + str + "']");
                        if (btn !== null && btn.getAttribute("data-id") !== "back") {
                            btn.classList.add("selected");
                        }
                        str += "_";
                    }
                };
                MainNav.prototype.init = function () {
                    var _this = this;
                    this.useCaps = this.uiData.mainNav.useCaps;
                    this.useOrderedLists = this.uiData.mainNav.useOrderedLists;
                    this.showAllPages = this.uiData.mainNav.showAllPagesInNav;
                    this.itemHeight = this.uiData.mainNav.lineHeight;
                    this.html = document.createElement("div");
                    this.html.classList.add("nav-modules");
                    this.createMap();
                    this.currentUL = this.buildNav();
                    this.createPageMediaControls();
                    this.html.addEventListener("click", function (e) { return _this.moduleNavHandler(e); }, false);
                    this.html.addEventListener("change", function (e) { return _this.changeLanguage(e); }, false);
                    this.removeSubmenu = this.removeSubmenu.bind(this);
                };
                MainNav.prototype.createMap = function (root) {
                    if (root === void 0) { root = this.contentData; }
                    for (var i = 0; i < root.length; i++) {
                        var obj = root[i];
                        this.navMap[obj.id] = obj;
                        if (obj.content) {
                            this.createMap(obj.content);
                        }
                    }
                };
                MainNav.prototype.buildNav = function (pos) {
                    if (pos === void 0) { pos = 1; }
                    var div = document.createElement("div");
                    div.setAttribute("data-nav", "main");
                    var sheet = document.createElement('style');
                    sheet.innerHTML = ".nav-modules__sub-holder {line-height: " + this.itemHeight + ";}";
                    document.head.appendChild(sheet);
                    div.classList.add("nav-modules__sub-holder");
                    var str = "";
                    for (var i = 0; i < this.contentData.length; i++) {
                        var obj = this.contentData[i];
                        if (obj.nav_item || obj.content) {
                            str += this.createNavItem(obj, (i + 1) + "", MainNav.TYPE_STANDARD);
                        }
                    }
                    var strTitle = this.createNavItem({ nav_title: this.uiData.mainNav.menu.main }, "", MainNav.TYPE_TITLE);
                    var strClose = this.createNavItem(this.uiData.mainNav.menu.close, "", MainNav.TYPE_CLOSE);
                    str = strTitle + strClose + str;
                    var buttons = this.uiData.mainNav.toolButtons;
                    for (var i = 0; i < buttons.length; i++) {
                        if (buttons[i]["show"]) {
                            str += this.createNavItem(buttons[i], "", buttons[i].type);
                        }
                    }
                    div.innerHTML = str;
                    var x = pos === 1 ? this.navWidth : -this.navWidth;
                    div.style.transform = div.style.webkitTransform = ("translateX(" + x + "px)");
                    this.html.appendChild(div);
                    if (this.selectedItem) {
                        this.setSelected(this.selectedItem.id);
                    }
                    return div;
                };
                MainNav.prototype.changeLanguage = function (e) {
                    if (e.target.selectedIndex !== undefined) {
                        var sel = this.html.querySelector("select[data-lang-id='language-select']");
                        var lang = sel.options[e.target.selectedIndex]["value"];
                        vt.VT5.getInstance().dispatchUserEvent(new UserEvent(UserEvent.ACTION_SET_LANGUAGE, lang));
                    }
                };
                MainNav.prototype.moduleNavHandler = function (e) {
                    var btn = e.target;
                    var id = btn.getAttribute("data-id");
                    this.navWidth = this.html.getBoundingClientRect().width;
                    if (!id) {
                        return;
                    }
                    switch (id) {
                        case "close":
                            this.dispatch(new NavEvent(NavEvent.CLOSE_MENU));
                            return;
                        case "change-theme":
                            if (e.target.checked === true) {
                                vt.VT5.getInstance().dispatchUserEvent(new UserEvent(e.target.getAttribute("data-on")));
                            }
                            else {
                                vt.VT5.getInstance().dispatchUserEvent(new UserEvent(e.target.getAttribute("data-off")));
                            }
                            return;
                    }
                    if (id !== null && id !== "back") {
                        if (btn.getAttribute("data-has-children") === "true") {
                            var ul = this.createSubMenu(id, 1);
                            T._(this.currentUL, .3, [{ prop: "translateX", to: -this.navWidth }, { prop: "translateZ", to: 0 }]);
                            T._(ul, .3, [{ prop: "translateX", to: 0 }], { call: this.removeSubmenu });
                            this.current = id;
                            this.currentUL = ul;
                        }
                    }
                    else if (id === "back") {
                        var parent = this.navMap[this.current].parentId;
                        var ul;
                        if (parent !== "") {
                            ul = this.createSubMenu(parent, -1);
                        }
                        else {
                            ul = this.buildNav(-1);
                        }
                        T._(this.currentUL, .3, [{ prop: "translateX", to: this.navWidth }, { prop: "translateZ", to: 0 }]);
                        T._(ul, .3, [{ prop: "translateX", to: 0 }], { call: this.removeSubmenu });
                        this.current = parent;
                        this.currentUL = ul;
                    }
                    if (btn.getAttribute("data-has-children") !== "true" && id !== "back" && id !== "undefined") {
                        this.dispatch(new NavEvent(NavEvent.CONTENT_ITEM_SELECT, id));
                        this.setSelected(id);
                    }
                };
                MainNav.prototype.createSubMenu = function (id, pos) {
                    var obj = this.navMap[id];
                    var data = obj.content;
                    var ul = document.createElement("div");
                    ul.classList.add("nav-modules__sub-holder");
                    var str = "";
                    str += this.createNavItem(obj, "", MainNav.TYPE_TITLE);
                    str += this.createNavItem(this.uiData.mainNav.menu.back, "", MainNav.TYPE_BACK);
                    for (var j = 0; j < data.length; j++) {
                        var o = data[j];
                        str += this.createNavItem(o, (j + 1) + "", MainNav.TYPE_STANDARD);
                    }
                    ul.innerHTML = str;
                    var x = pos === 1 ? this.navWidth : -this.navWidth;
                    ul.style.transform = ul.style.webkitTransform = ("translateX(" + x + "px)");
                    this.html.appendChild(ul);
                    if (this.selectedItem) {
                        this.setSelected(this.selectedItem.id);
                    }
                    return ul;
                };
                MainNav.prototype.removeSubmenu = function () {
                    var nodes = this.html.childNodes;
                    for (var i = nodes.length - 1; i >= 0; i--) {
                        if (this.currentUL !== nodes[i]) {
                            this.html.removeChild(nodes[i]);
                        }
                    }
                };
                MainNav.prototype.createNavItem = function (vo, num, type) {
                    var title = "";
                    if (vo.nav_title) {
                        title = this.useCaps ? vo.nav_title.toUpperCase() : vo.nav_title;
                    }
                    var icon = vo.nav_icon ? "<span class=\"" + vo.nav_icon + " nav-icon--content\"></span>" : "<span class=\"vti_page nav-icon--content\"></span>";
                    var hasContent = vo.content !== undefined && (!vo.hasPages || this.showAllPages);
                    if (vo.content && vo.content[0] && vo.content[0].nav_item) {
                        hasContent = true;
                    }
                    switch (type) {
                        case MainNav.TYPE_STANDARD:
                            if (hasContent) {
                                icon = '<span class="vti_chevron-right nav-icon--content"></span>';
                            }
                            return "<div class=\"nav-item\" data-id='" + vo.id + "' data-has-children='" + hasContent + "'>" + icon + "<span>" + (title || "") + "</span></div>";
                        case MainNav.TYPE_TITLE:
                            return "<div class=\"nav-title nav-item\"><span>" + title + "</span></div>";
                        case MainNav.TYPE_CLOSE:
                            var label = this.useCaps ? this.uiData.mainNav.menu.close.toLocaleUpperCase() : this.uiData.mainNav.menu.close;
                            return "<div data-id=\"close\" class=\"nav-item\"><span class=\"vti_close nav-icon--direction\"></span><span>" + label + "</span></div>";
                        case MainNav.TYPE_BACK:
                            var label = this.useCaps ? this.uiData.mainNav.menu.back.toLocaleUpperCase() : this.uiData.mainNav.menu.back;
                            return "<div data-id=\"back\" class=\"btn-back nav-item\"><span class=\"vti_chevron-left nav-icon--direction\"></span><span>" + label + "</span></div>";
                        case "tool":
                        case "toolButton":
                            var evt = vo.event ? "data-event=\"" + vo.event + "\" data-payload=\"" + vo.payload + "\"" : "";
                            return "<div class=\"nav-item\" " + evt + "'><span class=\"" + vo.icon + " nav-icon--tool\"></span><span>" + vo.label + "</span></div>";
                        case "settings":
                            var checked = AppModel.getInstance().getTheme() === "dark" ? "checked" : "";
                            var langs = "<select data-lang-id='language-select'>";
                            var list = vt.services.AppService.getInstance().getLanguageList();
                            for (var i = 0; i < list.length; i++) {
                                var sel = list[i].file === vt.services.AppService.getInstance().getPreferredLanguage() ? "selected" : "";
                                langs += "<option " + sel + " value=\"" + list[i].file + "\">" + list[i].title + "</option>";
                            }
                            langs += "</select>";
                            return "<label class=\"nav-item nav-settings\">\n                                <span class=\"" + vo.icon + " nav-icon--tool\"></span><span>" + vo.label + "</span>\n                                <input type=\"checkbox\">\n                                <div class=\"settings\">\n                                    <div><span class=\"title\">" + vo.theme.label + ":</span>" + vo.theme.theme1 + " <label class=\"btn-switch\"><input data-id=\"change-theme\" type=\"checkbox\" " + checked + " data-off=\"UserEvent.setThemeLight\" data-on=\"UserEvent.setThemeDark\"><span class=\"knob\"></span></label> " + vo.theme.theme2 + "</div>\n                                    <div><span class=\"title\">" + vo.language + ":</span><span>" + langs + "</span></div>\n                                </div>\n                          </label>";
                    }
                };
                MainNav.prototype.createPageMediaControls = function () {
                    var vos = this.uiData.mainNav.soundControls;
                    var str = "";
                    str += "<input type='range' min='0' max='100' class='slider'>";
                    for (var i = 0; i < vos.length; i++) {
                        str += ButtonRenderer.render(vos[i]);
                    }
                    this.mediaControls = document.querySelector(".page-media-controls");
                    this.mediaControls.innerHTML = str;
                };
                return MainNav;
            }(vt.events.Dispatcher));
            MainNav.TYPE_CLOSE = "close";
            MainNav.TYPE_BACK = "back";
            MainNav.TYPE_TITLE = "title";
            MainNav.TYPE_STANDARD = "standard";
            nav.MainNav = MainNav;
        })(nav = view.nav || (view.nav = {}));
    })(view = vt.view || (vt.view = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var view;
    (function (view) {
        var components;
        (function (components) {
            var Preloader = (function () {
                function Preloader() {
                }
                Preloader.getHtmlString = function () {
                    return '<div class="preloader"><span></span><span></span><span></span><span></span><span></span></div>';
                };
                return Preloader;
            }());
            components.Preloader = Preloader;
        })(components = view.components || (view.components = {}));
    })(view = vt.view || (vt.view = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var view;
    (function (view) {
        var components;
        (function (components) {
            var media;
            (function (media) {
                var PreloadableMedia = (function (_super) {
                    __extends(PreloadableMedia, _super);
                    function PreloadableMedia(mediaItem) {
                        var _this = _super.call(this) || this;
                        _this.mediaItem = mediaItem;
                        _this.initialize();
                        return _this;
                    }
                    PreloadableMedia.prototype.getHtmlString = function () {
                        var preloader = vt.view.components.Preloader.getHtmlString();
                        var bg = this.mediaItem.background ? "style=\"background: " + this.mediaItem.background + ";\"" : "";
                        var border = this.mediaItem.border ? " border" : "";
                        return "<div class=\"media" + border + "\" " + bg + "><div class=\"media-placeholder\">" + preloader + "</div></div>";
                    };
                    PreloadableMedia.prototype.load = function (container) { return null; };
                    PreloadableMedia.prototype.unload = function () { };
                    ;
                    PreloadableMedia.prototype.stop = function () { };
                    ;
                    PreloadableMedia.prototype.hide = function () { };
                    ;
                    PreloadableMedia.prototype.then = function () { return this.loadComplete; };
                    ;
                    PreloadableMedia.prototype.pause = function () { };
                    PreloadableMedia.prototype.resume = function () { };
                    PreloadableMedia.prototype.loadComplete = function (e) { };
                    PreloadableMedia.prototype.addControls = function () {
                        if (this.mediaItem.controls) {
                            var str = "";
                            for (var i = 0; i < this.mediaItem.controls.length; i++) {
                                var btn = this.mediaItem.controls[i];
                                str += vt.view.renderers.ButtonRenderer.render(btn);
                            }
                            var controlsDiv = document.createElement("div");
                            controlsDiv.classList.add("media-controls");
                            controlsDiv.innerHTML = str;
                            this.div.appendChild(controlsDiv);
                            this.controlButtonHandler = this.controlButtonHandler.bind(this);
                            this.div.addEventListener("click", this.controlButtonHandler);
                        }
                    };
                    PreloadableMedia.prototype.removePreloader = function () {
                        this.div.removeChild(this.div.childNodes[0]);
                    };
                    PreloadableMedia.prototype.initialize = function () {
                        this.type = this.mediaItem.type;
                        this.kind = this.mediaItem.kind;
                        this.loadComplete = this.loadComplete.bind(this);
                    };
                    PreloadableMedia.prototype.controlButtonHandler = function (e) { };
                    return PreloadableMedia;
                }(vt.events.Dispatcher));
                media.PreloadableMedia = PreloadableMedia;
            })(media = components.media || (components.media = {}));
        })(components = view.components || (view.components = {}));
    })(view = vt.view || (vt.view = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    function debounce(callback, delay, context) {
        var any = [];
        for (var _i = 3; _i < arguments.length; _i++) {
            any[_i - 3] = arguments[_i];
        }
        var timeout;
        return function () {
            clearTimeout(timeout);
            timeout = setTimeout(function () {
                timeout = null;
                callback.call(context, any);
            }, delay);
        };
    }
    vt.debounce = debounce;
    function throttle(callback, delay, context) {
        var any = [];
        for (var _i = 3; _i < arguments.length; _i++) {
            any[_i - 3] = arguments[_i];
        }
        var timeout = null;
        return function () {
            if (!timeout) {
                callback.call(context, any);
                timeout = setTimeout(function () {
                    callback.call(context, any);
                    timeout = null;
                }, delay);
            }
        };
    }
    vt.throttle = throttle;
    function getImageRatioStyle(ratio) {
        var style = "";
        if (ratio >= 2) {
            style = "vh-50";
        }
        else if (ratio < 2 && ratio >= 1.3) {
            style = "vh-60";
        }
        else if (ratio < 1.3) {
            style = "vh-70";
        }
        return style;
    }
    vt.getImageRatioStyle = getImageRatioStyle;
    function shuffle(array) {
        var newArr = array.concat();
        for (var i = 0; i < array.length; i++) {
            var rand = Math.floor(Math.random() * newArr.length);
            var tmp = newArr[i];
            newArr[i] = newArr[rand];
            newArr[rand] = tmp;
        }
        return newArr;
    }
    vt.shuffle = shuffle;
})(vt || (vt = {}));
var vt;
(function (vt) {
    var view;
    (function (view) {
        var components;
        (function (components) {
            var media;
            (function (media) {
                var MediaAnimation = (function (_super) {
                    __extends(MediaAnimation, _super);
                    function MediaAnimation(mediaItem) {
                        var _this = _super.call(this, mediaItem) || this;
                        _this.mediaItem = mediaItem;
                        _this.stageTickIsOn = false;
                        return _this;
                    }
                    MediaAnimation.prototype.load = function (container) {
                        var _this = this;
                        this.promise = new vt.VTPromise(function () {
                            _this.src = "../../" + _this.mediaItem.src;
                            _this.div = container;
                            var self = _this;
                            requirejs(["createjs"], function () {
                                requirejs([self.src], function (lib) {
                                    self.lib = lib;
                                    self.canvas = lib.canvas;
                                    if (!self.mediaItem.scale) {
                                        var style = vt.getImageRatioStyle(self.canvas.width / self.canvas.height);
                                        self.canvas.classList.add(style);
                                    }
                                    else {
                                        self.canvas.classList.add(self.mediaItem.scale);
                                    }
                                    self.removePreloader();
                                    var mediaInner = document.createElement("div");
                                    mediaInner.classList.add("media-inner");
                                    mediaInner.appendChild(self.canvas);
                                    container.appendChild(mediaInner);
                                    container.classList.add("reveal");
                                    self.addControls();
                                    self.promise.resolve();
                                });
                            });
                        });
                        return this.promise;
                    };
                    MediaAnimation.prototype.unload = function () {
                        if (this.div) {
                            this.div.removeEventListener("click", this.controlButtonHandler);
                        }
                        this.canvas = null;
                        _super.prototype.unload.call(this);
                        if (this.lib) {
                            this.lib.dispose();
                        }
                        requirejs.undef(this.src);
                        requirejs.undef("createjs");
                        this.lib = null;
                    };
                    MediaAnimation.prototype.stop = function () {
                        if (this.canvas) {
                            var evt = document.createEvent("Event");
                            evt.initEvent("stop", true, false);
                            this.canvas.dispatchEvent(evt);
                        }
                    };
                    MediaAnimation.prototype.makeMediaControlButton = function (btn) {
                        var icon = btn.icon ? "<span class=\"" + btn.icon + "\"></span>" : "";
                        return "<button\n                    class=\"btn-secondary\"\n                    data-type=\"" + btn.event + "\"\n                    data-startTick = \"" + btn.startTick + "\"\n                    data-stopTick = \"" + btn.stopTick + "\"\n                    >" + btn.label + "<span>&nbsp;</span>" + icon + "</button>";
                    };
                    MediaAnimation.prototype.controlButtonHandler = function (e) {
                        var btn = e.target;
                        var type = btn.getAttribute("data-event");
                        var startTick = btn.getAttribute("data-startTick");
                        var stopTick = btn.getAttribute("data-stopTick");
                        if (type && this.canvas) {
                            if (startTick !== "undefined" && !this.stageTickIsOn) {
                                this.stageTickIsOn = true;
                                createjs.Ticker.addEventListener("tick", this.lib.stage);
                            }
                            var evt = document.createEvent("Event");
                            evt.initEvent(type, true, false);
                            this.canvas.dispatchEvent(evt);
                            if (stopTick !== "undefined" && this.stageTickIsOn) {
                                this.stageTickIsOn = false;
                            }
                        }
                    };
                    return MediaAnimation;
                }(media.PreloadableMedia));
                media.MediaAnimation = MediaAnimation;
            })(media = components.media || (components.media = {}));
        })(components = view.components || (view.components = {}));
    })(view = vt.view || (vt.view = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var controllers;
    (function (controllers) {
        var SoundEvent = vt.events.SoundEvent;
        var SoundController = (function (_super) {
            __extends(SoundController, _super);
            function SoundController() {
                var _this = _super.call(this) || this;
                _this._isUserPaused = false;
                _this._hasFinished = false;
                _this.init();
                return _this;
            }
            SoundController.getInstance = function () {
                if (!SoundController._instance) {
                    SoundController._instance = new SoundController();
                }
                return SoundController._instance;
            };
            SoundController.prototype.playAudio = function (delay, src) {
                if (delay === void 0) { delay = 0; }
                if (src === void 0) { src = null; }
                if (src) {
                    this.audio.src = src;
                }
                if (!this._isUserPaused) {
                    if (delay === 0) {
                        this.audio.play();
                    }
                    else {
                        setTimeout(this.playAudio, delay * 1000);
                    }
                }
            };
            SoundController.prototype.pauseAudio = function () {
                this.audio.pause();
                this.mediaPlayInput.checked = true;
            };
            SoundController.prototype.enableMediaControls = function (value) {
                this.slider.value = "0";
                if (value) {
                    this.mediaControls.classList.remove("disabled");
                    this.intId = setInterval(this.progressHandler, 500);
                }
                else {
                    this.mediaControls.classList.add("disabled");
                    clearInterval(this.intId);
                    this.audio.pause();
                }
            };
            SoundController.prototype.isUserPaused = function () {
                return this._isUserPaused;
            };
            SoundController.prototype.isMuted = function () {
                return this.audio.muted;
            };
            SoundController.prototype.hasFinished = function () {
                return this._hasFinished;
            };
            SoundController.prototype.init = function () {
                var _this = this;
                this.audio = document.getElementById("audio-target");
                this.mediaControls = document.querySelector(".page-media-controls");
                var btnPlay = this.mediaControls.querySelector(".vti_pause").parentNode.parentNode;
                this.mediaPlayInput = btnPlay.querySelector("input");
                document.querySelector(".page-media-controls").addEventListener("click", function (e) { return _this.soundHandler(e); });
                this.slider = document.querySelector(".page-media-controls > input[type='range']");
                this.progressHandler = this.progressHandler.bind(this);
                this.onMeta = this.onMeta.bind(this);
                this.scrub = this.scrub.bind(this);
                this.audio.addEventListener("loadedmetadata", this.onMeta);
                this.slider.addEventListener("input", this.scrub);
                this.audio.addEventListener("play", function () { return _this.playStartHandler(); });
                this.audio.addEventListener("ended", function () { return _this.playEndHandler(); });
                this.slider.value = "0";
                this.playAudio = this.playAudio.bind(this);
            };
            SoundController.prototype.soundHandler = function (e) {
                var evt = e.target.getAttribute("data-event");
                if (evt) {
                    this.dispatch(new SoundEvent(evt));
                }
                switch (evt) {
                    case SoundEvent.ACTION_SOUND_OFF:
                        this.audio.muted = true;
                        break;
                    case SoundEvent.ACTION_SOUND_ON:
                        this.audio.muted = false;
                        break;
                    case SoundEvent.ACTION_PLAY:
                        this.audio.play();
                        this._isUserPaused = false;
                        break;
                    case SoundEvent.ACTION_PAUSE:
                        this.audio.pause();
                        this._isUserPaused = true;
                        break;
                    case SoundEvent.ACTION_REPLAY:
                        this.audio.currentTime = 0;
                        this.audio.play();
                        this.mediaPlayInput.checked = false;
                        this._isUserPaused = false;
                        break;
                }
            };
            SoundController.prototype.onMeta = function () {
                this.slider.max = this.audio.duration.toString();
            };
            SoundController.prototype.progressHandler = function () {
                if (!this.audio.paused) {
                    this.slider.value = this.audio.currentTime.toString();
                }
            };
            SoundController.prototype.playStartHandler = function () {
                this.mediaPlayInput.checked = false;
                this._hasFinished = false;
            };
            SoundController.prototype.playEndHandler = function () {
                this.mediaPlayInput.checked = true;
                this._hasFinished = true;
            };
            SoundController.prototype.scrub = function () {
                this.audio.currentTime = parseInt(this.slider.value, 10);
            };
            return SoundController;
        }(vt.events.Dispatcher));
        controllers.SoundController = SoundController;
    })(controllers = vt.controllers || (vt.controllers = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var view;
    (function (view) {
        var components;
        (function (components) {
            var media;
            (function (media) {
                var AudioMedia = (function (_super) {
                    __extends(AudioMedia, _super);
                    function AudioMedia(mediaItem) {
                        var _this = _super.call(this, mediaItem) || this;
                        _this.mediaItem = mediaItem;
                        _this.index = 0;
                        _this.oldIndex = -1;
                        _this.holder = document.getElementById("audio-holder");
                        _this.init();
                        return _this;
                    }
                    AudioMedia.prototype.getCue = function () {
                        return this.cues;
                    };
                    AudioMedia.prototype.goToQueIndex = function (value) {
                        if (this.audio && this.cues) {
                            this.audio.currentTime = this.cues[value][0];
                        }
                    };
                    AudioMedia.prototype.init = function () {
                        this.playHandler = this.playHandler.bind(this);
                        this.onCompleteHandler = this.onCompleteHandler.bind(this);
                        if (this.mediaItem.cuePoints) {
                            this.cues = [];
                            for (var i = 0; i < this.mediaItem.cuePoints.length - 1; i++) {
                                var q = this.mediaItem.cuePoints[i];
                                this.cues.push([q, this.mediaItem.cuePoints[i + 1]]);
                            }
                            this.cues.push([q, q + 1000]);
                        }
                    };
                    AudioMedia.prototype.getHtmlString = function () {
                        return "";
                    };
                    AudioMedia.prototype.load = function (container) {
                        var _this = this;
                        this.promise = new vt.VTPromise(function () {
                            _this.audio = document.getElementById("audio-target");
                            _this.audio.src = _this.mediaItem.src;
                            _this.intervalId = setInterval(_this.playHandler, 300);
                            if (!vt.controllers.SoundController.getInstance().isUserPaused()) {
                                _this.audio.play();
                            }
                            if (_this.mediaItem.onComplete) {
                                _this.audio.addEventListener("ended", _this.onCompleteHandler);
                            }
                        });
                        this.promise.resolve();
                        return this.promise;
                    };
                    AudioMedia.prototype.stop = function () {
                        if (this.audio) {
                            this.audio.pause();
                            clearInterval(this.intervalId);
                            this.audio.removeEventListener("ended", this.onCompleteHandler);
                        }
                    };
                    AudioMedia.prototype.unload = function () {
                        if (this.audio) {
                            this.audio.src = "";
                            this.audio = undefined;
                        }
                    };
                    AudioMedia.prototype.playHandler = function () {
                        var ct;
                        if (!this.audio.paused && this.cues) {
                            ct = this.audio.currentTime;
                            var range = this.cues[this.index];
                            if (ct >= range[0] && ct < range[1]) {
                                if (this.oldIndex !== this.index) {
                                    this.dispatch(new vt.events.SoundEvent(vt.events.SoundEvent.ACTION_CUE_POINT, this.index));
                                    this.oldIndex = this.index;
                                }
                            }
                            else {
                                if (ct > range[1] && this.index < this.cues.length - 1) {
                                    this.index++;
                                }
                                else if (ct < range[0] && this.index > 0) {
                                    this.index--;
                                }
                            }
                        }
                    };
                    AudioMedia.prototype.onCompleteHandler = function () {
                    };
                    return AudioMedia;
                }(media.PreloadableMedia));
                media.AudioMedia = AudioMedia;
            })(media = components.media || (components.media = {}));
        })(components = view.components || (view.components = {}));
    })(view = vt.view || (vt.view = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var view;
    (function (view) {
        var renderers;
        (function (renderers) {
            var LegendRenderer = (function () {
                function LegendRenderer() {
                }
                LegendRenderer.render = function (legend) {
                    var str = "";
                    switch (legend.kind) {
                        case "text":
                            str = "<div class=\"legend\" data-id=\"legend\">" + legend.text + "</div>";
                            break;
                        case "alphaNum":
                            var fields = legend.fields;
                            str += "<legend>";
                            for (var i = 0, k = fields.length; i < k; i++) {
                                var key = Object.keys(fields[i])[0];
                                var value = fields[i][key];
                                str += "<span>" + key + " " + value + "</span>";
                            }
                            str += "</legend>";
                            break;
                        case "color":
                            var fields = legend.fields;
                            str += "<legend>";
                            for (var i = 0, k = fields.length; i < k; i++) {
                                var key = Object.keys(fields[i])[0];
                                var color = "#" + key;
                                var value = fields[i][key];
                                str += " <span class=\"legend--color\" style=\"border-left: .8em solid " + color + ";\">" + value + "</span> ";
                            }
                            str += "</legend>";
                            break;
                    }
                    return str;
                };
                return LegendRenderer;
            }());
            renderers.LegendRenderer = LegendRenderer;
        })(renderers = view.renderers || (view.renderers = {}));
    })(view = vt.view || (vt.view = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var view;
    (function (view) {
        var components;
        (function (components) {
            var media;
            (function (media_1) {
                var CarouselMedia = (function (_super) {
                    __extends(CarouselMedia, _super);
                    function CarouselMedia(data) {
                        var _this = _super.call(this, data) || this;
                        _this.data = data;
                        _this.index = 0;
                        _this.init();
                        return _this;
                    }
                    CarouselMedia.prototype.init = function () {
                        this.clickHandler = this.clickHandler.bind(this);
                        this.screens = [];
                        for (var i = 0; i < this.data.screens.length; i++) {
                            var screen = {
                                l: "",
                                r: "",
                                media: []
                            };
                            screen.l = this.parseBody(this.data.screens[i].l, screen);
                            screen.r = this.parseBody(this.data.screens[i].r, screen);
                            this.screens.push(screen);
                        }
                        this.audio = document.getElementById("audio-target");
                        this.audioCompeteHandler = this.audioCompeteHandler.bind(this);
                        this.audio.addEventListener("ended", this.audioCompeteHandler);
                        this.tweenComplete = this.tweenComplete.bind(this);
                    };
                    CarouselMedia.prototype.load = function (container) {
                        var _this = this;
                        this.promise = new vt.VTPromise(function () {
                            _this.container = container;
                            _this.index = -1;
                            container.classList.add("carousel");
                            var controlsStr = "<div class=\"carousel_controls\">\n                <div class=\"carousel_btn\" data-id=\"prev\"><span class=\"vti_chevron-left\" data-id=\"prev\"></span></div>\n                <div class=\"carousel_btn\" data-id=\"next\"><span class=\"vti_chevron-right\" data-id=\"next\"></span></div>";
                            for (var i = 0; i < _this.data.screens.length; i++) {
                                controlsStr += '<div class="carousel_circle"><span class="vti_circle-filled"></span></div>';
                            }
                            controlsStr += "</div>";
                            container.innerHTML = "\n            " + controlsStr + "\n                <div class=\"carousel_inner\">\n                    <div class=\"carousel_img\"></div>\n                    <div class=\"carousel_txt\"></div>\n                </div>";
                            _this.divImg = container.querySelector(".carousel_img");
                            _this.divTxt = container.querySelector(".carousel_txt");
                            container.querySelector(".carousel_controls").addEventListener("click", _this.clickHandler);
                            _this.next();
                            container.classList.add("reveal");
                            container.classList.remove("transparent");
                        });
                        this.promise.resolve();
                        return this.promise;
                    };
                    CarouselMedia.prototype.unload = function () {
                        if (this.container) {
                            this.container.querySelector(".carousel_controls").removeEventListener("click", this.clickHandler);
                            this.container = null;
                        }
                        this.audio.removeEventListener("ended", this.audioCompeteHandler);
                        this.audio = undefined;
                    };
                    CarouselMedia.prototype.stop = function () {
                        this.audio.pause();
                    };
                    CarouselMedia.prototype.clickHandler = function (e) {
                        var btn = e.target;
                        var type = btn.getAttribute("data-id");
                        if (type === "next") {
                            this.next();
                        }
                        else if (type === "prev") {
                            this.prev();
                        }
                    };
                    CarouselMedia.prototype.next = function () {
                        if (this.index < this.data.screens.length - 1) {
                            if (this.data.hasAudio) {
                                this.audio.pause();
                            }
                            this.index++;
                            this.divTxt.innerHTML = this.screens[this.index].r;
                            this.setSelectedDot(this.index);
                            T._(this.divImg, .2, [{ prop: "opacity", to: 0 }])._(this.tweenComplete);
                        }
                    };
                    CarouselMedia.prototype.tweenComplete = function () {
                        this.divImg.innerHTML = this.screens[this.index].l;
                        this.initMedia();
                        T._(this.divImg, .2, [{ prop: "opacity", to: 1 }]);
                    };
                    CarouselMedia.prototype.prev = function () {
                        if (this.index > 0) {
                            if (this.data.hasAudio) {
                                this.audio.pause();
                            }
                            this.index--;
                            this.divTxt.innerHTML = this.screens[this.index].r;
                            this.setSelectedDot(this.index);
                            T._(this.divImg, .2, [{ prop: "opacity", to: 0 }])._(this.tweenComplete);
                        }
                    };
                    CarouselMedia.prototype.parseBody = function (body, screen) {
                        var str = "";
                        for (var i = 0; i < body.length; i++) {
                            var b = body[i];
                            switch (b.type) {
                                case "text":
                                    str += b.text;
                                    break;
                                case "media":
                                    var media = media_1.MediaFactory.getMedia(b);
                                    if (b.kind === "audio") {
                                        this.data.hasAudio = true;
                                    }
                                    str += media.getHtmlString();
                                    if (media) {
                                        screen.media.push(media);
                                    }
                                    break;
                                case "legend":
                                    str += vt.view.renderers.LegendRenderer.render(b);
                                    break;
                            }
                        }
                        return str;
                    };
                    CarouselMedia.prototype.initMedia = function () {
                        if (!this.container) {
                            return;
                        }
                        var scrMedia = this.screens[this.index].media;
                        var placeHolders = this.container.querySelectorAll(".media");
                        for (var i = 0; i < scrMedia.length; i++) {
                            scrMedia[i].load(placeHolders[i]);
                        }
                    };
                    CarouselMedia.prototype.setSelectedDot = function (num) {
                        if (!this.container) {
                            return;
                        }
                        var items = this.container.querySelectorAll(".carousel_circle");
                        for (var i = 0; i < items.length; i++) {
                            items[i]["classList"].remove("carousel_circle--selected");
                        }
                        items[num]["classList"].add("carousel_circle--selected");
                    };
                    CarouselMedia.prototype.audioCompeteHandler = function () {
                        this.next();
                    };
                    return CarouselMedia;
                }(media.PreloadableMedia));
                media_1.CarouselMedia = CarouselMedia;
            })(media = components.media || (components.media = {}));
        })(components = view.components || (view.components = {}));
    })(view = vt.view || (vt.view = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var view;
    (function (view) {
        var components;
        (function (components) {
            var media;
            (function (media) {
                var ExternalMedia = (function (_super) {
                    __extends(ExternalMedia, _super);
                    function ExternalMedia(mediaItem) {
                        var _this = _super.call(this, mediaItem) || this;
                        _this.mediaItem = mediaItem;
                        return _this;
                    }
                    ExternalMedia.prototype.load = function (container) {
                        var _this = this;
                        this.promise = new vt.VTPromise(function () {
                            _this.div = container;
                            _this.div.innerHTML = "";
                            var deps = ["libs/text!" + _this.mediaItem.srcHTML];
                            var that = _this;
                            if (_this.mediaItem.srcJS) {
                                deps.push(_this.mediaItem.srcJS);
                            }
                            if (_this.mediaItem.srcCSS) {
                                var link = document.createElement("link");
                                link.type = "text/css";
                                link.rel = "stylesheet";
                                link.href = _this.mediaItem.srcCSS;
                                link.id = _this.mediaItem.srcCSS;
                                document.getElementsByTagName("head")[0].appendChild(link);
                            }
                            requirejs(deps, function (html, js) {
                                _this.div.innerHTML = html;
                                if (js) {
                                    _this.js = js;
                                    _this.js.init(_this.div);
                                }
                                _this.div.classList.add("reveal");
                                that.promise.resolve();
                            });
                        });
                        return this.promise;
                    };
                    ExternalMedia.prototype.unload = function () {
                        if (this.js) {
                            this.js.clear();
                        }
                        requirejs.undef("libs/text");
                        if (this.mediaItem.srcJS) {
                            requirejs.undef(this.mediaItem.srcJS);
                        }
                        if (this.mediaItem.srcCSS) {
                            var css = document.getElementById(this.mediaItem.srcCSS);
                            if (css) {
                                document.getElementsByTagName("head")[0].removeChild(css);
                            }
                        }
                    };
                    ExternalMedia.prototype.stop = function () {
                    };
                    return ExternalMedia;
                }(media.PreloadableMedia));
                media.ExternalMedia = ExternalMedia;
            })(media = components.media || (components.media = {}));
        })(components = view.components || (view.components = {}));
    })(view = vt.view || (vt.view = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var view;
    (function (view) {
        var renderers;
        (function (renderers) {
            var HotspotsRenderer = (function () {
                function HotspotsRenderer() {
                }
                HotspotsRenderer.render = function (hotspots) {
                    var str = "<div class='hotspots touchable'>";
                    for (var i = 0; i < hotspots.length; i++) {
                        var h = hotspots[i];
                        var type = h.type;
                        if (h.type === "label") {
                            type = "circle label";
                        }
                        else if (h.type === "pin") {
                            type = "circle label pin";
                        }
                        var id = (h.type === "label") ? "<span class='el-id'>" + h.id + "</span>" : "";
                        var pin = (h.type === "pin") ? "<span class='el-id vti_pin'></span>" : "";
                        var size = h.width ? "width: " + h.width + "%; height: " + h.height + "%;" : "";
                        var description = h.description ? "data-s-description=\"" + h.description + "\"" : "";
                        var color = h.color ? "background: " + h.color + ";" : "";
                        str += "<div data-s-id=\"" + h.id + "\" " + description + " data-s-x=\"" + h.x + "\" data-s-y=\"" + h.y + "\" class=\"hspot " + type + "\" style=\"left: " + h.x + "%; top: " + h.y + "%; " + size + "; " + color + "\">" + id + pin + "</div>";
                    }
                    return str + "</div>";
                };
                return HotspotsRenderer;
            }());
            renderers.HotspotsRenderer = HotspotsRenderer;
        })(renderers = view.renderers || (view.renderers = {}));
    })(view = vt.view || (vt.view = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var view;
    (function (view) {
        var components;
        (function (components) {
            var media;
            (function (media) {
                var UserEvent = vt.events.UserEvent;
                var HotspotsRenderer = vt.view.renderers.HotspotsRenderer;
                var ImageMedia = (function (_super) {
                    __extends(ImageMedia, _super);
                    function ImageMedia(mediaItem) {
                        var _this = _super.call(this, mediaItem) || this;
                        _this.mediaItem = mediaItem;
                        return _this;
                    }
                    ImageMedia.prototype.load = function (container) {
                        var _this = this;
                        this.promise = new vt.VTPromise(function () {
                            _this.loadComplete = _this.loadComplete.bind(_this);
                            _this.launchViewer = _this.launchViewer.bind(_this);
                            _this.div = container;
                            _this.img = document.createElement("img");
                            _this.img.addEventListener("load", _this.loadComplete);
                            _this.img.src = _this.mediaItem.src;
                        });
                        return this.promise;
                    };
                    ImageMedia.prototype.unload = function () {
                        if (this.viewBtn) {
                            this.viewBtn.removeEventListener("click", this.launchViewer);
                        }
                        this.div = null;
                        this.imgMap = null;
                        this.viewBtn = null;
                    };
                    ImageMedia.prototype.loadComplete = function (e) {
                        this.img.removeEventListener("load", this.loadComplete);
                        this.removePreloader();
                        var btnViewer = (this.mediaItem.hasViewer || this.mediaItem.has3dViewer) ? '<button class="vti_expand btn-open-explorer" id="btn-viewer"></button>' : "";
                        var classMap = "";
                        var divHpots = "";
                        if (this.mediaItem.hotspots) {
                            classMap = "class='imgMap'";
                            divHpots = HotspotsRenderer.render(this.mediaItem.hotspots);
                        }
                        this.div.innerHTML = "<div " + classMap + ">\n                " + btnViewer + "\n                <img src=\"" + this.mediaItem.src + "\">\n                " + divHpots + "\n            </div>";
                        if (this.mediaItem.background === "-1") {
                            this.div.style.background = "none";
                        }
                        this.div.classList.add("reveal");
                        if (this.mediaItem.hasViewer || this.mediaItem.has3dViewer) {
                            document.getElementById("btn-viewer").addEventListener("click", this.launchViewer);
                        }
                        if (this.mediaItem.hotspots) {
                            this.legendEl = document.querySelector(".legend[data-id='legend']");
                            if (this.legendEl) {
                                this.hotspotClickHandler = this.hotspotClickHandler.bind(this);
                                this.hotspotsEl = this.div.querySelector(".hotspots");
                                this.hotspotsEl.addEventListener("click", this.hotspotClickHandler);
                            }
                        }
                        this.promise.resolve();
                    };
                    ImageMedia.prototype.launchViewer = function (e) {
                        if (this.mediaItem.hasViewer) {
                            this.launchImageViewer();
                        }
                        else {
                            this.launch3DViewer();
                        }
                    };
                    ImageMedia.prototype.launch3DViewer = function () {
                        var vo = {
                            vo3d: this.mediaItem.properties,
                            type: "3d"
                        };
                        vt.VT5.getInstance().dispatchUserEvent(new UserEvent(UserEvent.ACTION_SHOW_MODAL, vo));
                    };
                    ImageMedia.prototype.launchImageViewer = function () {
                        var vo = {
                            type: "imageExplorer",
                            media: this.mediaItem
                        };
                        vt.VT5.getInstance().dispatchUserEvent(new UserEvent(UserEvent.ACTION_SHOW_MODAL, vo));
                    };
                    ImageMedia.prototype.hotspotClickHandler = function (e) {
                        var descr = e.target.getAttribute("data-s-description");
                        if (!descr)
                            return;
                        var sel = this.hotspotsEl.querySelector(".selected");
                        if (sel)
                            sel.classList.remove("selected");
                        e.target.classList.add("selected");
                        this.legendEl.textContent = descr;
                    };
                    return ImageMedia;
                }(media.PreloadableMedia));
                media.ImageMedia = ImageMedia;
            })(media = components.media || (components.media = {}));
        })(components = view.components || (view.components = {}));
    })(view = vt.view || (vt.view = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var view;
    (function (view) {
        var components;
        (function (components) {
            var media;
            (function (media) {
                var THREEjs = (function (_super) {
                    __extends(THREEjs, _super);
                    function THREEjs(mediaItem) {
                        var _this = _super.call(this, mediaItem) || this;
                        _this.mediaItem = mediaItem;
                        _this.allowRun = true;
                        _this.autoRotateY = 0;
                        return _this;
                    }
                    THREEjs.prototype.load = function (container) {
                        var _this = this;
                        this.promise = new vt.VTPromise(function () {
                            _this.div = container;
                            _this.div.classList.add("div-3d");
                            var orbControls = _this.mediaItem.orbitControls ? "OrbitControls" : null;
                            var setts = _this.mediaItem.settings ? _this.mediaItem.settings : null;
                            requirejs(["three", orbControls, setts], function (lib, orbControls, settings) {
                                _this.THREE = lib;
                                _this.settings = settings;
                                _this.initScene();
                                _this.removePreloader();
                                _this.promise.resolve();
                            });
                        });
                        return this.promise;
                    };
                    THREEjs.prototype.unload = function () {
                        this.controls.dispose();
                        this.controls = undefined;
                        this.allowRun = false;
                        window.removeEventListener("resize", this.onWindowResize);
                        requirejs.undef("three");
                        requirejs.undef("OrbitControls");
                        requirejs.undef(this.mediaItem.settings);
                        this.div = null;
                    };
                    THREEjs.prototype.initScene = function () {
                        this.objectLoaded = this.objectLoaded.bind(this);
                        this.render = this.render.bind(this);
                        this.onWindowResize = this.onWindowResize.bind(this);
                        var rect = this.get3dCanvasRect();
                        this.scene = new this.THREE.Scene();
                        this.camera = new this.THREE.PerspectiveCamera(35, rect.width / rect.height, .1, 1000);
                        this.camera.position.x = this.settings.camera.x;
                        this.camera.position.y = this.settings.camera.y;
                        this.camera.position.z = this.settings.camera.z;
                        this.camera.lookAt(new this.THREE.Vector3(0, 0, 0));
                        this.renderer = new this.THREE.WebGLRenderer();
                        this.renderer.setSize(rect.width, rect.height);
                        this.renderer.setClearColor(this.settings.background | 0x666666, 1);
                        if (this.mediaItem.orbitControls) {
                            this.setOrbitControls();
                        }
                        window.addEventListener('resize', this.onWindowResize, false);
                        this.div.appendChild(this.renderer.domElement);
                        var loader = new this.THREE.ObjectLoader();
                        loader.load(this.mediaItem.path, this.objectLoaded);
                    };
                    THREEjs.prototype.setOrbitControls = function () {
                        this.controls = new this.THREE.OrbitControls(this.camera);
                        this.controls.rotateSpeed = .05;
                        this.controls.zoomSpeed = .5;
                        this.controls.panSpeed = 0.5;
                        this.controls.enableZoom = true;
                        this.controls.enablePan = true;
                        this.controls.enableDamping = true;
                        this.controls.dampingFactor = .1;
                        this.controls.keys = [65, 83, 68];
                    };
                    THREEjs.prototype.get3dCanvasRect = function () {
                        return this.div.getBoundingClientRect();
                    };
                    THREEjs.prototype.objectLoaded = function (obj) {
                        this.world = obj;
                        var ambientLight = new this.THREE.AmbientLight(0xcccccc);
                        this.scene.add(ambientLight);
                        var sceneSets = this.settings.scene;
                        if (sceneSets) {
                            for (var p in sceneSets.rotation) {
                                obj.rotation[p] = sceneSets.rotation[p] + Math.PI;
                            }
                            obj.scale.x = obj.scale.y = obj.scale.z = sceneSets.scale | 1;
                            if (sceneSets.autoRotateY) {
                                this.autoRotateY = sceneSets.autoRotateY;
                            }
                        }
                        for (var i = 0; i < obj.children.length; i++) {
                            var child = obj.children[i];
                        }
                        this.scene.add(obj);
                        this.render();
                    };
                    THREEjs.prototype.onWindowResize = function () {
                        var rect = this.get3dCanvasRect();
                        this.camera.aspect = rect.width / rect.height;
                        this.camera.updateProjectionMatrix();
                        this.renderer.setSize(rect.width, rect.height);
                    };
                    THREEjs.prototype.render = function () {
                        if (this.allowRun) {
                            requestAnimationFrame(this.render);
                        }
                        if (this.controls) {
                            this.controls.update();
                        }
                        if (this.autoRotateY !== 0) {
                            this.world.rotation.y += this.autoRotateY;
                        }
                        this.renderer.render(this.scene, this.camera);
                    };
                    return THREEjs;
                }(media.PreloadableMedia));
                media.THREEjs = THREEjs;
            })(media = components.media || (components.media = {}));
        })(components = view.components || (view.components = {}));
    })(view = vt.view || (vt.view = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var view;
    (function (view) {
        var components;
        (function (components) {
            var media;
            (function (media) {
                var Babylon = (function (_super) {
                    __extends(Babylon, _super);
                    function Babylon(mediaItem) {
                        var _this = _super.call(this, mediaItem) || this;
                        _this.mediaItem = mediaItem;
                        _this.allowRun = true;
                        return _this;
                    }
                    Babylon.prototype.load = function (container) {
                        var _this = this;
                        this.div = container;
                        this.div.classList.add("div-3d");
                        this.canvas = document.createElement("canvas");
                        this.canvas.setAttribute("touch-action", "none");
                        this.onWindowResize = this.onWindowResize.bind(this);
                        this.div.appendChild(this.canvas);
                        requirejs(["babylon", "pep", this.mediaItem.settings], function (lib, pep, settings) {
                            _this.settings = settings;
                            settings.babylon = _this;
                            _this.removePreloader();
                            if (BABYLON.Engine.isSupported()) {
                                var canvas = _this.canvas;
                                var resize = _this.onWindowResize;
                                var antialias = settings.scene.antialias === undefined ? true : settings.scene.antialias;
                                var engine = new BABYLON.Engine(_this.canvas, true, { stencil: true }, false);
                                _this.engine = engine;
                                BABYLON.ref = _this;
                                var to_rad = Math.PI / 180;
                                var scene = new BABYLON.Scene(engine);
                                var camera = new BABYLON.ArcRotateCamera("CameraArc", 0, 0, 0, BABYLON.Vector3.Zero(), scene);
                                var camPos = settings.camera.position;
                                camera.setPosition(new BABYLON.Vector3(camPos[0], camPos[1], camPos[2]));
                                if (settings.camera.copyPositionFromSceneCamera) {
                                    camera.setPosition(scene.activeCamera.position);
                                }
                                if (settings.camera.target) {
                                    camera.target = new BABYLON.Vector3(settings.camera.target[0], settings.camera.target[1], settings.camera.target[2]);
                                }
                                camera.alpha = settings.camera.alpha * to_rad || 0;
                                camera.beta = settings.camera.beta * to_rad || 60 * to_rad;
                                camera.fov = settings.camera.fov * to_rad || .6;
                                camera.angularSensibilityX = settings.camera.angularSensibilityX || 1500;
                                camera.panningSensibility = settings.camera.panningSensibility || 200;
                                camera.wheelPrecision = settings.camera.wheelPrecision || 80;
                                camera.pinchPrecision = settings.camera.pinchPrecision || 70;
                                camera.upperRadiusLimit = settings.camera.upperRadiusLimit || 150;
                                camera.lowerRadiusLimit = settings.camera.lowerRadiusLimit || 3;
                                camera.lowerBetaLimit = settings.camera.lowerBetaLimit * to_rad || -Math.PI;
                                camera.upperBetaLimit = settings.camera.upperBetaLimit * to_rad || Math.PI;
                                _this.camera = camera;
                                if (_this.settings.src) {
                                    BABYLON.SceneLoader.Append(_this.settings.src.root, _this.settings.src.file, scene, function () {
                                        _this.sceneReady(scene, settings);
                                    });
                                }
                                else {
                                    _this.sceneReady(scene, settings);
                                }
                                window.addEventListener('resize', _this.onWindowResize, false);
                            }
                        });
                        return null;
                    };
                    Babylon.prototype.sceneReady = function (scene, settings) {
                        var _this = this;
                        var clearColor = settings.scene.clearColor;
                        var ambientColor = settings.scene.ambientColor;
                        scene.clearColor = new BABYLON.Color3(clearColor[0], clearColor[1], clearColor[2]);
                        scene.ambientColor = new BABYLON.Color3(ambientColor[0], ambientColor[1], ambientColor[2]);
                        if (settings.scene.skybox) {
                            Babylon.createSkyBox(settings.scene.skybox, scene);
                        }
                        scene.executeWhenReady(function () {
                            var root = new BABYLON.Mesh("$$root", scene);
                            for (var i = 0; i < scene.meshes.length; i++) {
                                var current = scene.meshes[i];
                                if (!current.parent && current != root) {
                                    current.parent = root;
                                }
                            }
                            if (settings.scene.scale) {
                                root.scaling.x = root.scaling.y = root.scaling.z = settings.scene.scale;
                            }
                            if (settings.scene.useOptimizer) {
                                BABYLON.SceneOptimizer.OptimizeAsync(scene);
                            }
                            if (!settings.camera.useSceneCamera) {
                                scene.activeCamera = _this.camera;
                            }
                            scene.activeCamera.attachControl(_this.canvas, false, false);
                            if (settings.init) {
                                settings.init(scene);
                            }
                            var upd = settings.update !== undefined;
                            _this.engine.runRenderLoop(function () {
                                scene.render();
                            });
                            scene.registerBeforeRender(function () {
                                if (upd) {
                                    settings.update();
                                }
                            });
                        });
                    };
                    Babylon.prototype.unload = function () {
                        document.body.setAttribute("oncontextmenu", "return true");
                        if (this.canvas) {
                            var gl = this.canvas.getContext("webgl");
                            gl["getExtension"]('WEBGL_lose_context').loseContext();
                        }
                        if (this.engine) {
                            this.engine.stopRenderLoop();
                        }
                        this.allowRun = false;
                        if (this.scene) {
                            this.scene.dispose();
                            this.scene = null;
                        }
                        var div = document.getElementById("babylonjsLoadingDiv");
                        if (div)
                            div.parentNode.removeChild(div);
                        window.removeEventListener("resize", this.onWindowResize);
                        requirejs.undef("babylon");
                        if (this.settings) {
                            this.settings.destroy();
                            this.settings.babylon = null;
                        }
                        requirejs.undef(this.mediaItem.settings);
                        this.div = null;
                    };
                    Babylon.prototype.onWindowResize = function () {
                        this.engine.resize();
                    };
                    Babylon.createSkyBox = function (skyBoxSettings, scene) {
                        var skybox = BABYLON.Mesh.CreateBox("skyBox", skyBoxSettings.size, scene);
                        var skyboxMaterial = new BABYLON.StandardMaterial("skyBox", scene);
                        skyboxMaterial.backFaceCulling = false;
                        skyboxMaterial.disableLighting = true;
                        skybox.material = skyboxMaterial;
                        skybox.infiniteDistance = skyBoxSettings.infiniteDistance;
                        skyboxMaterial.diffuseColor = new BABYLON.Color3(0, 0, 0);
                        skyboxMaterial.specularColor = new BABYLON.Color3(0, 0, 0);
                        skyboxMaterial.reflectionTexture = new BABYLON.CubeTexture(skyBoxSettings.src, scene);
                        skyboxMaterial.reflectionTexture.coordinatesMode = BABYLON.Texture.SKYBOX_MODE;
                    };
                    return Babylon;
                }(media.PreloadableMedia));
                media.Babylon = Babylon;
            })(media = components.media || (components.media = {}));
        })(components = view.components || (view.components = {}));
    })(view = vt.view || (vt.view = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var view;
    (function (view) {
        var components;
        (function (components) {
            var media;
            (function (media) {
                var MediaVideo = (function (_super) {
                    __extends(MediaVideo, _super);
                    function MediaVideo(mediaItem) {
                        var _this = _super.call(this, mediaItem) || this;
                        _this.mediaItem = mediaItem;
                        _this._isInterrupted = false;
                        _this.hasSubtitles = false;
                        _this.init();
                        return _this;
                    }
                    MediaVideo.prototype.init = function () {
                        this.checkState = this.checkState.bind(this);
                    };
                    MediaVideo.prototype.isPlaying = function () {
                        return !!(this.video.currentTime > 0 && !this.video.paused && !this.video.ended && this.video.readyState > 2);
                    };
                    MediaVideo.prototype.pause = function () {
                        if (!!(this.video.currentTime > 0 && !this.video.paused && !this.video.ended && this.video.readyState > 2)) {
                            this._isInterrupted = true;
                            this.video.pause();
                        }
                    };
                    MediaVideo.prototype.resume = function () {
                        if (this._isInterrupted) {
                            this._isInterrupted = false;
                            this.video.play();
                        }
                    };
                    MediaVideo.prototype.load = function (container) {
                        var _this = this;
                        this.promise = new vt.VTPromise(function () {
                            _this.div = container;
                            _this.video = document.createElement("video");
                            _this.video.setAttribute("crossorigin", "anonymous");
                            _this.video.setAttribute("preload", "metadata");
                            _this.video.addEventListener("contextmenu", MediaVideo.removeContext);
                            if (_this.mediaItem.defaultControls) {
                                _this.video.controls = true;
                            }
                            if (_this.mediaItem.poster) {
                                _this.video.poster = _this.mediaItem.poster;
                            }
                            if (_this.mediaItem.autoPlay) {
                                _this.video.setAttribute("autoplay", "true");
                            }
                            if (_this.mediaItem.controlBar) {
                                _this.makeControls();
                            }
                            _this.intervalId = setInterval(_this.checkState, 300);
                            _this.video.src = _this.mediaItem.src;
                        });
                        this.promise.resolve();
                        return this.promise;
                    };
                    MediaVideo.prototype.checkState = function () {
                        if (this.video.readyState >= this.video.HAVE_METADATA) {
                            this.loadComplete();
                            clearInterval(this.intervalId);
                        }
                    };
                    MediaVideo.removeContext = function (e) {
                        e.preventDefault();
                        return false;
                    };
                    MediaVideo.prototype.stop = function () {
                        if (this.video) {
                            this.video.pause();
                        }
                    };
                    MediaVideo.prototype.unload = function () {
                        if (this.div) {
                            this.div.removeEventListener("click", this.controlButtonHandler);
                        }
                        if (this.video) {
                            this.video.src = "";
                            this.video.removeEventListener("loadedmetadata", this.videoEvtHandler);
                            this.video.removeEventListener("ended", this.videoEvtHandler);
                            this.video.removeEventListener("contextmenu", MediaVideo.removeContext);
                        }
                        document.removeEventListener('webkitfullscreenchange', this.fullscreenChangeHandler);
                        document.removeEventListener('mozfullscreenchange', this.fullscreenChangeHandler);
                        document.removeEventListener('fullscreenchange', this.fullscreenChangeHandler);
                        document.removeEventListener('MSFullscreenChange', this.fullscreenChangeHandler);
                        document.removeEventListener('keydown', this.keyHandler);
                        if (this.controlBar) {
                            this.scrub.removeEventListener("change", this.scrubHandler);
                            this.controlBar.removeEventListener("click", this.controlBarHandler);
                        }
                        if (this.videoContainer) {
                            this.videoContainer.removeEventListener("click", this.mouseHandler);
                            this.videoContainer.removeEventListener("mousemove", this.mouseHandler);
                        }
                        this.div = null;
                        clearInterval(this.progressInterval);
                    };
                    MediaVideo.prototype.controlButtonHandler = function (e) {
                        var btn = e.target;
                        var type = btn.getAttribute("data-event");
                        switch (type) {
                            case "start":
                            case "play":
                                this.video.play();
                                break;
                            case "pause":
                                this.video.pause();
                                break;
                        }
                    };
                    MediaVideo.prototype.loadComplete = function () {
                        this.div.classList.add("reveal");
                        this.removePreloader();
                        if (!this.mediaItem.defaultControls && !this.mediaItem.controlBar) {
                            this.video.loop = true;
                        }
                        this.videoContainer = document.createElement("div");
                        this.videoContainer.classList.add("video-holder");
                        this.videoContainer.appendChild(this.video);
                        this.div.appendChild(this.videoContainer);
                        this.addControls();
                        if (this.controlBar) {
                            this.video.parentNode.appendChild(this.controlBar);
                            this.videoContainer.addEventListener("click", this.mouseHandler);
                            this.videoContainer.addEventListener("mousemove", this.mouseHandler);
                        }
                    };
                    MediaVideo.prototype.makeControls = function () {
                        this.controlBar = document.createElement("div");
                        this.controlBar.classList.add("video-controls-bar");
                        var ccStr = "";
                        var tracks = "";
                        if (this.mediaItem.cc) {
                            ccStr = "<span class=\"vti_cc vc\" data-type=\"cc\"><div class=\"select hidden\" data-type=\"select-lang\">";
                            this.hasSubtitles = true;
                            for (var i = 0; i < this.mediaItem.cc.length; i++) {
                                var cc = this.mediaItem.cc[i];
                                var checked = i === 0 ? "checked" : "";
                                ccStr += "<label class=\"select_option\"><input type=\"radio\" " + checked + " name=\"lang\" data-type=\"lang\" data-lang=\"" + i + "\"><span>" + cc.label + "</span></label>";
                                if (i !== 0) {
                                    tracks += "<track src=\"" + cc.src + "\" kind=\"subtitles\" srclang=\"" + cc.srclang + "\" label=\"" + cc.label + "\">";
                                }
                            }
                            ccStr += "</div></span>";
                            this.video.innerHTML = tracks;
                        }
                        var chaptersStr = "";
                        if (this.mediaItem.chapters) {
                            chaptersStr = "<span class=\"vti_list vc\" data-type=\"chapters\"><div class=\"select hidden\" data-type=\"select-chapter\">";
                            for (var i = 0; i < this.mediaItem.chapters.length; i++) {
                                var chapter = this.mediaItem.chapters[i];
                                chaptersStr += "<div class=\"select_option\" data-type=\"chapter\" data-chapter=\"" + i + "\">" + chapter.label + "</div>";
                            }
                            chaptersStr += "</div></span>";
                        }
                        var fsBtnStr = MediaVideo.fullScreenEnabled() ? '<span class="vti_fullscreen vc" data-type="full-screen"></span>' : "";
                        this.controlBar.innerHTML = "<span class=\"vti_play vc\" data-type=\"play\"></span><span class=\"vti_pause vc hidden\" data-type=\"pause\"></span>\n            <span class=\"vti_stop vc\" data-type=\"stop\"></span>\n            <span class=\"vti_sound-on vc\" data-type=\"sound-on\"></span><span class=\"vti_sound-off vc hidden\" data-type=\"sound-off\"></span>\n            " + chaptersStr + "\n            " + ccStr + "\n            " + fsBtnStr + "\n            <span class=\"divider--time\"></span>\n            <span class=\"vc-time\" data-type=\"time-current\">00:00:00</span>\n            <input type=\"range\" step=\"any\" data-type=\"scrub\">\n            <span class=\"vc-time\" data-type=\"time-total\">00:00:00</span>\n            ";
                        this.controlBarHandler = this.controlBarHandler.bind(this);
                        this.fullscreenChangeHandler = this.fullscreenChangeHandler.bind(this);
                        this.progressHandler = this.progressHandler.bind(this);
                        this.videoEvtHandler = this.videoEvtHandler.bind(this);
                        this.scrubHandler = this.scrubHandler.bind(this);
                        this.mouseHandler = this.mouseHandler.bind(this);
                        this.keyHandler = this.keyHandler.bind(this);
                        this.controlBar.addEventListener("click", this.controlBarHandler);
                        this.btnPlay = this.controlBar.querySelector("span[data-type='play']");
                        this.btnPause = this.controlBar.querySelector("span[data-type='pause']");
                        this.btnSoundOn = this.controlBar.querySelector("span[data-type='sound-on']");
                        this.btnSoundOff = this.controlBar.querySelector("span[data-type='sound-off']");
                        this.timeCurrent = this.controlBar.querySelector("span[data-type='time-current']");
                        this.timeTotal = this.controlBar.querySelector("span[data-type='time-total']");
                        this.scrub = this.controlBar.querySelector("input[data-type='scrub']");
                        this.cc = this.controlBar.querySelector("div[data-type='select-lang']");
                        this.chapters = this.controlBar.querySelector("div[data-type='select-chapter']");
                        this.video.addEventListener("loadedmetadata", this.videoEvtHandler);
                        this.video.addEventListener("ended", this.videoEvtHandler);
                        document.addEventListener('webkitfullscreenchange', this.fullscreenChangeHandler, false);
                        document.addEventListener('mozfullscreenchange', this.fullscreenChangeHandler, false);
                        document.addEventListener('fullscreenchange', this.fullscreenChangeHandler, false);
                        document.addEventListener('MSFullscreenChange', this.fullscreenChangeHandler, false);
                        document.addEventListener('keydown', this.keyHandler, false);
                        this.scrub.addEventListener("change", this.scrubHandler);
                        this.scrub.value = "0";
                        if (this.mediaItem.autoPlay) {
                            this.btnPause.classList.remove("hidden");
                            this.btnPlay.classList.add("hidden");
                            this.progressInterval = setInterval(this.progressHandler, 300);
                        }
                    };
                    MediaVideo.prototype.mouseHandler = function () {
                        var _this = this;
                        T.killTweaksOf(this.controlBar);
                        clearInterval(this.timeoutInterval);
                        T._(this.controlBar, .01, [{ prop: "opacity", to: 1 }]);
                        this.timeoutInterval = setInterval(function () {
                            T._(_this.controlBar, .3, [{ prop: "opacity", to: 0 }]);
                            if (_this.cc) {
                                _this.cc.classList.add("hidden");
                            }
                            if (_this.chapters) {
                                _this.chapters.classList.add("hidden");
                            }
                        }, 3000);
                    };
                    MediaVideo.prototype.keyHandler = function (e) {
                        if (e.keyCode === 32) {
                            this.playPause();
                        }
                    };
                    MediaVideo.prototype.controlBarHandler = function (e) {
                        var action = e.target.getAttribute("data-type");
                        switch (action) {
                            case "play":
                            case "pause":
                                this.playPause();
                                break;
                            case "stop":
                                this.video.pause();
                                this.video.currentTime = 0;
                                this.btnPause.classList.add("hidden");
                                this.btnPlay.classList.remove("hidden");
                                clearInterval(this.progressInterval);
                                this.scrub.value = "0";
                                break;
                            case "full-screen":
                                this.setFullScreen();
                                break;
                            case "cc":
                                this.cc.classList.toggle("hidden");
                                break;
                            case "lang":
                                var lang = parseInt(e.target.getAttribute("data-lang"));
                                for (var i = 0; i < this.video.textTracks.length; i++) {
                                    this.video.textTracks[i].mode = 'disabled';
                                }
                                if (lang !== 0) {
                                    this.video.textTracks[lang - 1].mode = 'showing';
                                }
                                break;
                            case "chapters":
                                this.chapters.classList.toggle("hidden");
                                break;
                            case "chapter":
                                var chapter = parseInt(e.target.getAttribute("data-chapter"));
                                var time = this.mediaItem.chapters[chapter].time;
                                this.video.currentTime = time;
                                this.scrub.value = time.toString();
                                break;
                            case "sound-on":
                                this.btnSoundOff.classList.remove("hidden");
                                this.btnSoundOn.classList.add("hidden");
                                this.video.volume = 0;
                                break;
                            case "sound-off":
                                this.btnSoundOff.classList.add("hidden");
                                this.btnSoundOn.classList.remove("hidden");
                                this.video.volume = 1;
                                break;
                        }
                        if (this.cc && action !== "cc" && action !== "lang" && action !== null) {
                            this.hideCCMenu();
                        }
                        if (this.chapters && action !== "chapters" && action !== "chapter" && action !== null) {
                            this.hideChapters();
                        }
                    };
                    MediaVideo.prototype.hideCCMenu = function () {
                        this.cc.classList.add("hidden");
                    };
                    MediaVideo.prototype.hideChapters = function () {
                        this.chapters.classList.add("hidden");
                    };
                    MediaVideo.prototype.scrubHandler = function () {
                        this.video.currentTime = parseFloat(this.scrub.value);
                        this.timeCurrent.textContent = MediaVideo.getFormattedTime(this.video.currentTime);
                    };
                    MediaVideo.prototype.playPause = function () {
                        if (this.video.paused) {
                            this.video.play();
                            this.btnPause.classList.remove("hidden");
                            this.btnPlay.classList.add("hidden");
                            this.progressInterval = setInterval(this.progressHandler, 300);
                        }
                        else {
                            this.video.pause();
                            this.btnPause.classList.add("hidden");
                            this.btnPlay.classList.remove("hidden");
                            clearInterval(this.progressInterval);
                        }
                    };
                    MediaVideo.prototype.videoEvtHandler = function (e) {
                        var type = e.type;
                        var dur;
                        switch (type) {
                            case "loadedmetadata":
                                dur = this.video.duration;
                                this.scrub.max = dur.toString();
                                this.timeTotal.textContent = MediaVideo.getFormattedTime(dur);
                                break;
                            case "ended":
                                this.btnPause.classList.add("hidden");
                                this.btnPlay.classList.remove("hidden");
                                clearInterval(this.progressInterval);
                                break;
                        }
                    };
                    MediaVideo.prototype.progressHandler = function () {
                        var ct = this.video.currentTime;
                        this.scrub.value = ct.toString();
                        this.timeCurrent.textContent = MediaVideo.getFormattedTime(ct);
                    };
                    MediaVideo.prototype.setFullScreen = function () {
                        if (!!(document["fullScreen"] || document.webkitIsFullScreen || document["mozFullScreen"] || document["msFullscreenElement"] || document.fullscreenElement)) {
                            if (document.exitFullscreen)
                                document.exitFullscreen();
                            else if (document["mozCancelFullScreen"])
                                document["mozCancelFullScreen"]();
                            else if (document.webkitCancelFullScreen)
                                document.webkitCancelFullScreen();
                            else if (document["msExitFullscreen"])
                                document["msExitFullscreen"]();
                        }
                        else {
                            if (this.videoContainer.requestFullscreen)
                                this.videoContainer.requestFullscreen();
                            else if (this.videoContainer["mozRequestFullScreen"])
                                this.videoContainer["mozRequestFullScreen"]();
                            else if (this.videoContainer.webkitRequestFullScreen)
                                this.videoContainer.webkitRequestFullScreen();
                            else if (this.videoContainer["msRequestFullscreen"])
                                this.videoContainer["msRequestFullscreen"]();
                        }
                    };
                    MediaVideo.prototype.fullscreenChangeHandler = function (e) {
                        if (MediaVideo.isFullScreen()) {
                            this.videoContainer.classList.add("video-holder--fs");
                            this.videoContainer.classList.remove("video-holder");
                        }
                        else {
                            this.videoContainer.classList.remove("video-holder--fs");
                            this.videoContainer.classList.add("video-holder");
                        }
                    };
                    MediaVideo.isFullScreen = function () {
                        return !!(document["fullScreen"] || document.webkitIsFullScreen || document["mozFullScreen"] || document["msFullscreenElement"] || document.fullscreenElement);
                    };
                    MediaVideo.fullScreenEnabled = function () {
                        return (document.fullscreenEnabled ||
                            document.webkitFullscreenEnabled ||
                            document["mozFullScreenEnabled"] ||
                            document["msFullscreenEnabled"]);
                    };
                    MediaVideo.getFormattedTime = function (val) {
                        MediaVideo.date.setTime(val * 1000);
                        return MediaVideo.date.toTimeString().split(" ")[0];
                    };
                    return MediaVideo;
                }(media.PreloadableMedia));
                MediaVideo.date = new Date();
                media.MediaVideo = MediaVideo;
            })(media = components.media || (components.media = {}));
        })(components = view.components || (view.components = {}));
    })(view = vt.view || (vt.view = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var view;
    (function (view) {
        var components;
        (function (components) {
            var media;
            (function (media) {
                var MediaFactory = (function () {
                    function MediaFactory() {
                    }
                    MediaFactory.getMedia = function (data) {
                        switch (data.kind) {
                            case "3d":
                                if (data["preview"]) {
                                    var imgData = {
                                        type: "media",
                                        kind: "image",
                                        src: data["preview"],
                                        label: "",
                                        has3dViewer: true,
                                        properties: data
                                    };
                                    return new media.ImageMedia(imgData);
                                }
                                else {
                                    if (data["library"] === "THREE.js") {
                                        return new media.THREEjs(data);
                                    }
                                    else {
                                        return new media.Babylon(data);
                                    }
                                }
                            case "animation":
                                return new media.MediaAnimation(data);
                            case "audio":
                                return new media.AudioMedia(data);
                            case "carousel":
                                return new media.CarouselMedia(data);
                            case "external":
                                return new media.ExternalMedia(data);
                            case "image":
                                return new media.ImageMedia(data);
                            case "video":
                                return new media.MediaVideo(data);
                            default:
                                return null;
                        }
                    };
                    return MediaFactory;
                }());
                media.MediaFactory = MediaFactory;
            })(media = components.media || (components.media = {}));
        })(components = view.components || (view.components = {}));
    })(view = vt.view || (vt.view = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var view;
    (function (view) {
        var renderers;
        (function (renderers) {
            var CardRenderer = (function () {
                function CardRenderer() {
                }
                CardRenderer.render = function (vo) {
                    var len = vo.content.text.length;
                    if (len > 500) {
                        throw "Card component with title \"" + vo.cover.text + "\" is above 500 character limit (" + len + " chars.)";
                    }
                    var size = vo.cover.iconSize ? "style = \"height: " + vo.cover.iconSize + "; width: " + vo.cover.iconSize + ";\"" : "";
                    var icon = vo.cover.icon ? "<img class=\"card-icon\" src=\"" + vo.cover.icon + "\" " + size + ">" : "";
                    var evtOn = "", evtOff = "";
                    if (vo.cover.audio) {
                        evtOn = "data-event=\"UserEvent.playAudio\" data-payload=" + vo.cover.audio;
                        evtOff = "data-event=\"UserEvent.stopAudio\"";
                    }
                    return "<label class=\"card\" style=\"border: 3px solid " + vo.cover.bgColor + ";\">\n\t\t<input type=\"checkbox\">\n\t\t<span class=\"cover\" style=\"background: " + vo.cover.bgColor + ";\" " + evtOn + ">\n\t\t<span class=\"pointer\"><span class=\"vti_finger-up\" style=\"color: " + vo.cover.bgColor + "; background-color: " + vo.textColor + ";\"></span></span>\n\t\t\t" + icon + "\n\t\t\t<span class=\"description\">" + vo.cover.text + "</span>\n\t\t</span>\n\t\t<span class=\"content\" " + evtOff + ">\n\t\t    <h5 style=\"color: " + vo.cover.bgColor + ";\">" + vo.content.title + "</h5>\n\t\t    <p>" + vo.content.text + "</p>\n\t\t</span>\n\t</label>";
                };
                return CardRenderer;
            }());
            renderers.CardRenderer = CardRenderer;
        })(renderers = view.renderers || (view.renderers = {}));
    })(view = vt.view || (vt.view = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var view;
    (function (view) {
        var renderers;
        (function (renderers) {
            var AccordionRenderer = (function () {
                function AccordionRenderer() {
                }
                AccordionRenderer.render = function (vo) {
                    var str = "<div class='accordion'>";
                    var items = vo.items;
                    var inputType = vo.autoCollapse ? "radio" : "checkbox";
                    for (var i = 0; i < items.length; i++) {
                        var obj = items[i];
                        var id = "acc___" + i;
                        var evtOn = obj.audio ? "data-event=\"UserEvent.playAudio\" data-payload=" + obj.audio : "data-event=\"UserEvent.playAudio\" data-payload=\"res/assets/default/placeholder.mp3\"";
                        var bodyStr = AccordionRenderer.parseBody(obj.body);
                        str += "<div class=\"accordion-section\">\n                            <input type=\"" + inputType + "\" name=\"acc_group__0\" id=\"" + id + "\">\n                            <label for=\"" + id + "\" style=\"background-color: " + obj.color + ";\" " + evtOn + "><span>" + obj.title + "</span><span class=\"vti_chevron-down\"></span></label>\n                            <article><div>" + bodyStr + "</div></article>\n                        </div>";
                    }
                    return str + "</div>";
                };
                AccordionRenderer.parseBody = function (body) {
                    var str = "";
                    for (var i = 0; i < body.length; i++) {
                        var obj = body[i];
                        str += renderers.RenderFactory.render(body[i]);
                    }
                    return str;
                };
                return AccordionRenderer;
            }());
            renderers.AccordionRenderer = AccordionRenderer;
        })(renderers = view.renderers || (view.renderers = {}));
    })(view = vt.view || (vt.view = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var view;
    (function (view) {
        var renderers;
        (function (renderers) {
            var HTMLimageRenderer = (function () {
                function HTMLimageRenderer() {
                }
                HTMLimageRenderer.render = function (item) {
                    var imgViewer = item.hasViewer ? "<button class=\"vti_expand btn-open-explorer\" data-event=\"UserEvent.showViewer\" data-payload=\"" + item.src + "\"></button>" : "";
                    return "<div class=\"img-holder media\">" + imgViewer + "<img src=\"" + item.src + "\"></div>";
                };
                return HTMLimageRenderer;
            }());
            renderers.HTMLimageRenderer = HTMLimageRenderer;
        })(renderers = view.renderers || (view.renderers = {}));
    })(view = vt.view || (vt.view = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var view;
    (function (view) {
        var renderers;
        (function (renderers) {
            var TextRenderer = (function () {
                function TextRenderer() {
                }
                TextRenderer.render = function (body) {
                    var audioControl = body.audioControl ? " data-ac=\"" + body.audioControl + "\"" : "";
                    var style = body.style || "";
                    var text = body.text;
                    var extPat = /<a href=(.*?)<\/a>/gi;
                    var extMatches = text.match(extPat);
                    if (extMatches) {
                        for (var i = 0; i < extMatches.length; i++) {
                            var m = extMatches[i];
                            if (m.indexOf("http://") > -1 || m.indexOf("www.") > -1 && m.indexOf("_blank") < 0) {
                                var m1 = m.replace("<a ", "<a target='_blank'");
                                text = text.replace(m, m1);
                            }
                        }
                    }
                    var bulletsClass = body.audioDisplay ? body.audioDisplay + " " : "";
                    return "<div class='text" + style + " " + bulletsClass + "' " + audioControl + ">" + text + "</div>";
                };
                return TextRenderer;
            }());
            renderers.TextRenderer = TextRenderer;
        })(renderers = view.renderers || (view.renderers = {}));
    })(view = vt.view || (vt.view = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var view;
    (function (view) {
        var renderers;
        (function (renderers) {
            var TimelineRenderer = (function () {
                function TimelineRenderer() {
                }
                TimelineRenderer.render = function (vo) {
                    var str = "<ol class='timeline'>";
                    var items = vo.items;
                    for (var i = 0; i < items.length; i++) {
                        var obj = items[i];
                        var subtitle = obj.subtitle ? "<div class=\"timeline-subtitle\">" + obj.subtitle + "</div>" : "";
                        var btn = obj.audio ? "<button data-event=\"UserEvent.playAudio\" data-payload=\"" + obj.audio + "\" class=\"vti_sound-on\"></button>" : "";
                        var color = "";
                        var arrowColor = "";
                        if (obj.color) {
                            color = "style=\"background-color:" + obj.color + "\"";
                            if (i % 2 === 0) {
                                arrowColor = "style=\"border-left-color:" + obj.color + "\"";
                            }
                            else {
                                arrowColor = "style=\"border-right-color:" + obj.color + "\"";
                            }
                        }
                        str += "<li><span class=\"arrow\" " + arrowColor + "></span><section " + color + ">\n                            <h5>" + btn + obj.title + "</h5>" + subtitle + "<div>" + obj.text + "</div>\n                        </section></li>";
                    }
                    return str + "</ol>";
                };
                return TimelineRenderer;
            }());
            renderers.TimelineRenderer = TimelineRenderer;
        })(renderers = view.renderers || (view.renderers = {}));
    })(view = vt.view || (vt.view = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var view;
    (function (view) {
        var renderers;
        (function (renderers) {
            var RenderFactory = (function () {
                function RenderFactory() {
                }
                RenderFactory.render = function (body) {
                    switch (body.type) {
                        case "text":
                            return renderers.TextRenderer.render(body);
                        case "legend":
                            return renderers.LegendRenderer.render(body);
                        case "button":
                        case "toggle":
                        case "switchButton":
                            return renderers.ButtonRenderer.render(body);
                        case "card":
                            return renderers.CardRenderer.render(body);
                        case "accordion":
                            return renderers.AccordionRenderer.render(body);
                        case "v-divider":
                            return "<span class='v-divider'></span>";
                        case "html-image":
                            return renderers.HTMLimageRenderer.render(body);
                        case "timeline":
                            return renderers.TimelineRenderer.render(body);
                        default:
                            return "";
                    }
                };
                return RenderFactory;
            }());
            renderers.RenderFactory = RenderFactory;
        })(renderers = view.renderers || (view.renderers = {}));
    })(view = vt.view || (vt.view = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var view;
    (function (view) {
        var MediaFactory = vt.view.components.media.MediaFactory;
        var RenderFactory = vt.view.renderers.RenderFactory;
        var SoundEvent = vt.events.SoundEvent;
        var UserEvent = vt.events.UserEvent;
        var Page = (function () {
            function Page(data) {
                this.data = data;
                this._mediaList = [];
                this.audioControlledEls = [];
                this.div = document.createElement("div");
                this._hasAudio = false;
                this.numMediaLoaded = 0;
                this.parseData();
            }
            Object.defineProperty(Page.prototype, "html", {
                get: function () {
                    return this.div;
                },
                enumerable: true,
                configurable: true
            });
            Page.prototype.loadMedia = function () {
                var _this = this;
                var items = this.div.querySelectorAll(".media");
                for (var i = 0, k = this._mediaList.length; i < k; i++) {
                    this._mediaList[i].load(items[i]).then(function () {
                        _this.numMediaLoaded++;
                        if (_this.numMediaLoaded === items.length) {
                            _this.onMediaLoaded();
                        }
                    });
                }
            };
            Page.prototype.hideMedia = function () {
                for (var i = 0; i < this._mediaList.length; i++) {
                    this._mediaList[i]["hide"]();
                }
            };
            Page.prototype.destroy = function () {
                this.div = null;
                this.data = null;
                for (var i = 0; i < this._mediaList.length; i++) {
                    this._mediaList[i].unload();
                }
                if (this.audioMedia) {
                    this.audioMedia.removeListener(SoundEvent.SOUND_EVENT, this.onSoundEvent);
                }
            };
            Page.prototype.stop = function () {
                if (this.div) {
                    this.div.style.overflow = "hidden";
                }
                for (var i = 0; i < this._mediaList.length; i++) {
                    this._mediaList[i].stop();
                }
            };
            Page.prototype.hasAudio = function () {
                return this._hasAudio;
            };
            Page.prototype.pauseMedia = function () {
                for (var i = 0; i < this._mediaList.length; i++) {
                    this._mediaList[i].pause();
                }
            };
            Page.prototype.resumeMedia = function () {
                for (var i = 0; i < this._mediaList.length; i++) {
                    this._mediaList[i].resume();
                }
            };
            Page.prototype.dispatchEnter = function () {
                if (this.data.onEnterEvent) {
                    vt.VT5.getInstance().dispatchUserEvent(new UserEvent(this.data.onEnterEvent, this.data.enterEventPayload));
                }
            };
            Page.prototype.dispatchInFocus = function () {
                if (this.data.onFocusEvent) {
                    vt.VT5.getInstance().dispatchUserEvent(new UserEvent(this.data.onFocusEvent, this.data.focusEventPayload));
                }
            };
            Page.prototype.dispatchLeave = function () {
                if (this.data.onLeaveEvent) {
                    vt.VT5.getInstance().dispatchUserEvent(new UserEvent(this.data.onLeaveEvent, this.data.leaveEventPayload));
                }
            };
            Page.prototype.parseData = function () {
                this.onSoundEvent = this.onSoundEvent.bind(this);
                if (!this.data)
                    return;
                this.id = this.data.id;
                this.index = this.data.position;
                this.div.classList.add("page");
                this.type = this.data.type;
                var overlay = this.data.backgroundOverlay ? "linear-gradient(" + this.data.backgroundOverlay + ")," : "";
                var img = this.data.backgroundImage ? "url(" + this.data.backgroundImage : "";
                if (img) {
                    this.div.setAttribute("style", "\n                background: " + overlay + " url(" + this.data.backgroundImage + ");\n                background-size: cover;");
                }
                else if (overlay && !img) {
                    this.div.setAttribute("style", "background: " + overlay);
                }
                var vidPoster = this.data.backgroundVideoPoster ? "poster=\"" + this.data.backgroundVideoPoster + "\"" : "";
                var bgVid = this.data.backgroundVideo ? "<div class=\"video-bg\"><video src=\"" + this.data.backgroundVideo + "\" " + vidPoster + " autoplay loop></video></div>" : "";
                var str = "" + bgVid;
                if (!this.data.hideHeader && this.data.title) {
                    str = "<div class='hgroup'>";
                    str += this.data.header ? "<small>" + this.data.header + "</small>" : "";
                    str += this.data.title ? "<h3>" + this.data.title + "</h3>" : "";
                    str += "</div>";
                }
                var layout = this.data.layout ? this.data.layout : "";
                str += "<div class=\"" + (layout || 'page-cols') + " page-in\">";
                if (layout === "splash") {
                    this.div.classList.add("no-scroll");
                }
                this.parseBody(str);
                this.onBodyParsed();
            };
            Page.prototype.parseBody = function (str) {
                for (var i = 0; i < this.data.body.length; i++) {
                    var obj = this.data.body[i];
                    str += RenderFactory.render(obj);
                    switch (obj.type) {
                        case "media":
                            if (obj.kind === "audio") {
                                str += this.getMediaNode(obj);
                            }
                            else {
                                str += "<div>" + this.getMediaNode(obj) + "</div>";
                            }
                            break;
                        case "group":
                            str += this.getGroupNode(obj);
                            break;
                        default:
                            str += this.parseDefault(obj);
                    }
                }
                str += "</div>";
                this.div.innerHTML = str;
            };
            Page.prototype.parseDefault = function (obj) {
                return "";
            };
            Page.prototype.onBodyParsed = function () {
                var acs = this.div.querySelectorAll("div[data-ac='ac-fade'] > p");
                for (var i = 0; i < acs.length; i++) {
                    this.audioControlledEls.push(acs[i]);
                    acs[i]["classList"].add("cued");
                }
            };
            Page.prototype.onMediaLoaded = function () {
            };
            Page.prototype.getGroupNode = function (obj) {
                var str = obj.style ? "<div class=\"" + obj.style + " group\">" : "<div class='group'>";
                for (var i = 0; i < obj.items.length; i++) {
                    var obj1 = obj.items[i];
                    str += RenderFactory.render(obj1);
                    if (obj1.type === "media") {
                        str += this.getMediaNode(obj1);
                    }
                }
                return str + "</div>";
            };
            Page.prototype.getMediaNode = function (obj) {
                var str = "";
                var media = MediaFactory.getMedia(obj);
                if (obj.kind === "audio" || obj.hasAudio) {
                    if (!this.audioMedia) {
                        this.audioMedia = media;
                    }
                    else {
                        throw "[Page] can have only one audio element.";
                    }
                    this._hasAudio = true;
                    this.data.hasAudio = true;
                    if (obj.quePoints || obj.cuePoints) {
                        media.addListener(SoundEvent.SOUND_EVENT, this.onSoundEvent);
                    }
                }
                if (media) {
                    this._mediaList.push(media);
                    str += media.getHtmlString();
                }
                return str;
            };
            Page.prototype.onSoundEvent = function (e) {
                if (this.audioMedia && this.audioControlledEls.length > 0 && this.audioMedia["getCue"]().length !== this.audioControlledEls.length) {
                    throw "[Page] The number of audio-controlled html elements do not match the number of cue points.";
                }
                if (e.actionType === SoundEvent.ACTION_CUE_POINT) {
                    for (var i = 0; i < this.audioControlledEls.length; i++) {
                        if (i <= e.data) {
                            this.audioControlledEls[i].classList.remove("cued");
                        }
                        else {
                            this.audioControlledEls[i].classList.add("cued");
                        }
                    }
                }
            };
            return Page;
        }());
        view.Page = Page;
    })(view = vt.view || (vt.view = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var view;
    (function (view) {
        var LinksPage = (function (_super) {
            __extends(LinksPage, _super);
            function LinksPage() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            LinksPage.prototype.parseDefault = function (obj) {
                var str = "<div class='links'>";
                for (var i = 0; i < obj.items.length; i++) {
                    var item = obj.items[i];
                    if (item.link && item.cidRef) {
                        throw ("[LinksPage] an item cannot have both a link and a CID reference: link=" + item.link + ", cidRef: " + item.cidRef);
                    }
                    var icon = item.link ? "vti_link-external" : "vti_link-internal";
                    if (item.link) {
                        str += "<div><span class=\"vti_link-external\"></span><span><a href=\"" + item.link + "\" target=\"_blank\">" + item.title + "</a></span></div>";
                    }
                    else if (item.cidRef) {
                        var pageId = vt.model.AppModel.getInstance().getContentItemByCID(item.cidRef).id;
                        str += "<div><span class=\"vti_link-internal\"></span><span><a href=\"#" + pageId + "\">" + item.title + "</a></span></div>";
                    }
                    else if (item.modalRef) {
                        str += "<div><span class=\"vti_link-popup\"></span><span><a href=\"#" + this.id + "\" data-event=\"UserEvent.showModal\" data-payload=\"" + item.modalRef + "\">" + item.title + "</a></span></div>";
                    }
                }
                str += "</div>";
                return str;
            };
            return LinksPage;
        }(view.Page));
        view.LinksPage = LinksPage;
    })(view = vt.view || (vt.view = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var view;
    (function (view) {
        var UserEvent = vt.events.UserEvent;
        var ExplorerPage = (function (_super) {
            __extends(ExplorerPage, _super);
            function ExplorerPage() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.selected = 0;
                _this.clicked = [];
                return _this;
            }
            ExplorerPage.prototype.parseDefault = function (obj) {
                this.audioEl = document.getElementById("audio-target");
                var strThumbs = "";
                this.items = [];
                var span;
                for (var i = 0; i < obj.items.length; i++) {
                    var item = obj.items[i];
                    this.items.push(item);
                    strThumbs += "<button class=\"thumb\" data-id=\"" + i + "\"><img src=\"" + item.thumb + "\"></button>";
                }
                this.finalScr = obj.finalScreen;
                return "<div class='description'>" + obj.description + "</div>\n                    <div class=\"body\">\n                        <div class=\"img-holder\"><button class=\"vti_zoom-out btn-tertiary overlay\" data-type=\"zoom-out\" style=\"opacity: 0\"></button><img class=\"explorer-img\" src=\"" + obj.image + "\"></div>\n                        <div class=\"table\">" + strThumbs + "</div>\n                    </div>\n            ";
            };
            ExplorerPage.prototype.onBodyParsed = function () {
                _super.prototype.onBodyParsed.call(this);
                this.thumbClickHandler = this.thumbClickHandler.bind(this);
                this.zoomOut = this.zoomOut.bind(this);
                this.showModal = this.showModal.bind(this);
                this.img = this.div.querySelector(".explorer-img");
                this.thumbs = this.div.querySelector(".table");
                this.thumbs.addEventListener("click", this.thumbClickHandler);
                this.title = this.div.querySelector(".description");
                this.btnZoomOut = this.div.querySelector("button[data-type='zoom-out']");
                this.btnZoomOut.addEventListener("click", this.zoomOut);
            };
            ExplorerPage.prototype.thumbClickHandler = function (e) {
                var _this = this;
                var num = parseInt(e.target.getAttribute("data-id"));
                if (isNaN(num)) {
                    return;
                }
                e.target.classList.add("explorer_thumb--clicked");
                T._(this.btnZoomOut, .5, [{ prop: "opacity", to: .9 }]);
                if (this.clicked.indexOf(num) === -1) {
                    this.clicked.push(num);
                }
                this.selected = num;
                var item = this.items[num];
                var zoom = item.zoom;
                var x = item.pos[0];
                var y = item.pos[1];
                if (item.title) {
                    T._(this.title, .2, [{ prop: "opacity", to: 0 }], { call: function () { _this.title.innerHTML = item.title; } })
                        ._(.2, [{ prop: "opacity", to: 1 }]);
                }
                T.killTweaksOf(this.img);
                T._(this.img, 0.5, [{ prop: "scaleX", to: zoom }, { prop: "scaleY", to: zoom },
                    { prop: "translateX", to: x + "%" }, { prop: "translateY", to: y + "%" }, { prop: "scaleZ", to: 1 }])
                    ._(this.showModal);
                if (item.audio) {
                    this.audioEl.src = item.audio;
                    this.audioEl.play();
                }
                else {
                    this.audioEl.src = "";
                    this.audioEl.pause();
                }
            };
            ExplorerPage.prototype.showModal = function () {
                var modalObj = this.items[this.selected].modal;
                if (modalObj) {
                    vt.VT5.getInstance().dispatchUserEvent(new UserEvent(UserEvent.ACTION_SHOW_MODAL, modalObj));
                }
            };
            ExplorerPage.prototype.zoomOut = function () {
                T._(this.img, .5, [{ prop: "scaleX", to: 1 }, { prop: "scaleY", to: 1 }, { prop: "translateX", to: "0%" }, { prop: "translateY", to: "0%" }]);
                T._(this.btnZoomOut, .5, [{ prop: "opacity", to: 0 }]);
            };
            return ExplorerPage;
        }(view.Page));
        view.ExplorerPage = ExplorerPage;
    })(view = vt.view || (vt.view = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var events;
    (function (events) {
        var AppEvent = (function (_super) {
            __extends(AppEvent, _super);
            function AppEvent(type, data, target) {
                if (data === void 0) { data = null; }
                if (target === void 0) { target = null; }
                var _this = _super.call(this, type, data, target) || this;
                _this.type = type;
                _this.data = data;
                _this.target = target;
                return _this;
            }
            return AppEvent;
        }(vt.events.VTEvent));
        AppEvent.NAME = "AppEvent";
        AppEvent.APP_DATA_LOADED = AppEvent.NAME + ".appDataLoaded";
        events.AppEvent = AppEvent;
    })(events = vt.events || (vt.events = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var view;
    (function (view) {
        var test;
        (function (test) {
            var UserEvent = vt.events.UserEvent;
            var Question = (function (_super) {
                __extends(Question, _super);
                function Question(testItem) {
                    var _this = _super.call(this, testItem) || this;
                    _this.testItem = testItem;
                    _this._isShowingUserAnswer = true;
                    if (!_this.testItem.userAnswers) {
                        _this.testItem.userAnswers = [];
                    }
                    _this.getQuestionBody();
                    return _this;
                }
                Question.prototype.destroy = function () {
                    if (this._showCorrectBtn) {
                        this._showCorrectBtn.removeEventListener("click", this.showCorrectBtnHandler);
                    }
                    if (this._submitAnswerBtn) {
                        this._submitAnswerBtn.removeEventListener("click", this.submitAnswerHandler);
                    }
                };
                Question.prototype.parseData = function () {
                    var resp = vt.model.AppModel.getInstance().getTestResponse();
                    Question.CORRECT_STR = "<span class='correct q-answer'><span class='vti_correct'></span> " + resp.correct + "</span>";
                    _super.prototype.parseData.call(this);
                    this.div.classList.add("question");
                };
                Question.prototype.getQuestionBody = function () {
                    for (var i = 0; i < this.testItem.body.length; i++) {
                        if (this.testItem.body[i].type === "question") {
                            this._questionBody = this.testItem.body[i];
                            break;
                        }
                    }
                };
                Question.prototype.setShowCorrectBtn = function () {
                    this.showCorrectBtnHandler = this.showCorrectBtnHandler.bind(this);
                    var hgroup = this.div.querySelector("small");
                    if (hgroup) {
                        this._showCorrectBtn = document.createElement("button");
                        this._showCorrectBtn.classList.add("btn-correct-answer");
                        this._showCorrectBtn.classList.add("btn-secondary");
                        this._showCorrectBtn.innerHTML = "<span class='vti_check'></span> <span>" + this.testItem.showCorrect + "</span>";
                        hgroup.appendChild(this._showCorrectBtn);
                        this._showCorrectBtn.addEventListener("click", this.showCorrectBtnHandler);
                    }
                };
                Question.prototype.showSubmitAnswerBtn = function () {
                    this.submitAnswerHandler = this.submitAnswerHandler.bind(this);
                    var p = this.div.querySelector(".page-in > .question_body");
                    if (p) {
                        var str = vt.model.AppModel.getInstance().getTestResponse().submitAnswer;
                        this._submitAnswerBtn = document.createElement("button");
                        this._submitAnswerBtn.classList.add("btn-secondary");
                        this._submitAnswerBtn.textContent = str;
                        p.appendChild(this._submitAnswerBtn);
                        this._submitAnswerBtn.addEventListener("click", this.submitAnswerHandler);
                    }
                };
                Question.showIfCorrect = function (value) {
                    var icon = document.querySelector(".hgroup_icon");
                    if (!icon) {
                        return;
                    }
                    if (value > 0) {
                        icon.classList.remove("vti_tutorial");
                        icon.classList.add("vti_correct");
                        icon.classList.add("pulsing");
                    }
                    else {
                        icon.classList.add("vti_tutorial");
                        icon.classList.remove("vti_correct");
                        icon.classList.remove("pulsing");
                    }
                };
                Question.prototype.showCorrectAnswers = function () { };
                Question.prototype.showUserAnswers = function () { };
                Question.prototype.clearMarks = function () { };
                Question.prototype.markUserAnswer = function () { };
                Question.prototype.showCorrectBtnHandler = function () {
                    if (this._isShowingUserAnswer) {
                        this.showCorrectAnswers();
                        this._showCorrectBtn.classList.add("btn-correct-answer--on");
                    }
                    else {
                        this.showUserAnswers();
                        this._showCorrectBtn.classList.remove("btn-correct-answer--on");
                    }
                    this._isShowingUserAnswer = !this._isShowingUserAnswer;
                };
                Question.prototype.submitAnswerHandler = function () {
                    this.clearMarks();
                    this.markUserAnswer();
                    if (this.testItem.testMode === vt.TestMode.Tutorial && this.testItem.isCorrect !== 1) {
                        this.showHint();
                    }
                };
                Question.prototype.showHint = function () {
                    if (this.testItem.hint) {
                        vt.VT5.getInstance().dispatchUserEvent(new UserEvent(UserEvent.ACTION_SHOW_MODAL, this.testItem.hint));
                    }
                };
                return Question;
            }(vt.view.Page));
            test.Question = Question;
        })(test = view.test || (view.test = {}));
    })(view = vt.view || (vt.view = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var view;
    (function (view) {
        var test;
        (function (test) {
            var UserEvent = vt.events.UserEvent;
            var SelectOptionQuestion = (function (_super) {
                __extends(SelectOptionQuestion, _super);
                function SelectOptionQuestion(testItem) {
                    var _this = _super.call(this, testItem) || this;
                    _this.hasOptionAudio = false;
                    return _this;
                }
                SelectOptionQuestion.prototype.destroy = function () {
                    for (var i = 0; i < this.inputs.length; i++) {
                        this.inputs[i].removeEventListener("click", this.inputHandler);
                    }
                    this.body.removeEventListener("click", this.playSound);
                    this.body = undefined;
                    this._audioEl = undefined;
                    _super.prototype.destroy.call(this);
                };
                SelectOptionQuestion.prototype.parseDefault = function (obj) {
                    this.testItem = this.data;
                    var disabled = this.testItem.testMode === vt.TestMode.Review ? " disabled--opaque" : "";
                    var str = "<div class='question-options" + disabled + "'>";
                    this.hasOptionAudio = false;
                    for (var i = 0; i < obj.question.length; i++) {
                        var q = obj.question[i];
                        var options = this.renderOption(q.options, "n" + i);
                        var description = q.description ? q.description : "";
                        if (description && description.length > 0) {
                            if (q.audio) {
                                this.hasOptionAudio = true;
                            }
                            var sound = this.hasOptionAudio ? "<span class=\"vti_sound-on btn-inline-icon\" data-audio=\"" + q.audio + "\"></span>" : "";
                            str += "<div class=\"question_row question_row--multi\">\n                    <div class=\"question_description\">" + sound + description + "</div>\n                    <div class=\"question_options\">" + options + "</div>\n                    </div>";
                        }
                        else {
                            str += "<div class=\"question_row question_row--single\">" + options + "</div>";
                        }
                    }
                    str += "</div>";
                    if (this.hasOptionAudio) {
                        str += "<audio class='hidden' data-id='question-audio-el'></audio>";
                    }
                    return str;
                };
                SelectOptionQuestion.prototype.renderOption = function (options, name) {
                    var str = "";
                    var type;
                    var inpType = this.data["optionType"];
                    var option;
                    for (var i = 0; i < options.length; i++) {
                        var obj = options[i];
                        if (options.length === 1) {
                            name = "a";
                        }
                        if (inpType == "radio") {
                            option = "<label class=\"input_select\">\n                    <input type=\"radio\" name=\"" + name + "\">\n                    <span class=\"evt\">\n                    <span class=\"vti_radio-off selector--off\"></span>\n                    <span class=\"vti_radio-on selector--on\"></span>\n                    " + obj.label + "\n                    </span>\n                    </label>";
                        }
                        else {
                            option = "<label class=\"input_select\">\n                    <input type=\"checkbox\">\n                    <span class=\"evt\">\n                    <span class=\"vti_checkbox-off selector--off\"></span>\n                    <span class=\"vti_checkbox-on selector--on\"></span>\n                    " + obj.label + "\n                    </span>\n                    </label>";
                        }
                        str += "" + option;
                    }
                    return str;
                };
                SelectOptionQuestion.prototype.onBodyParsed = function () {
                    this.inputHandler = this.inputHandler.bind(this);
                    this.playSound = this.playSound.bind(this);
                    if (this.testItem.testMode === vt.TestMode.Test || this.testItem.testMode === vt.TestMode.Review) {
                        this.setUserAnswers();
                    }
                    if (!this._questionBody) {
                        this.getQuestionBody();
                    }
                    if (this.testItem.testMode === vt.TestMode.Review) {
                        this.markUserAnswer();
                        if (this.testItem.isCorrect !== 1) {
                            this.setShowCorrectBtn();
                        }
                    }
                    var nodes = this.div.querySelectorAll("input");
                    this.inputs = [];
                    for (var i = 0; i < nodes.length; i++) {
                        nodes[i].addEventListener("click", this.inputHandler);
                        this.inputs.push(nodes[i]);
                    }
                    this.body = this.div.querySelector(".page-in");
                    this.body.addEventListener("click", this.playSound);
                    this._audioEl = this.div.querySelector("audio");
                };
                SelectOptionQuestion.prototype.setUserAnswers = function () {
                    var checked = false;
                    var options = this.div.querySelectorAll(".question_row");
                    for (var i = 0; i < options.length; i++) {
                        var optionsRow = options[i]["querySelectorAll"]("input");
                        for (var j = 0; j < optionsRow.length; j++) {
                            if (this.testItem.userAnswers && this.testItem.userAnswers.length > 0) {
                                checked = this.testItem.userAnswers[i][j] === 1;
                            }
                            optionsRow[j]["checked"] = checked;
                        }
                    }
                };
                SelectOptionQuestion.prototype.playSound = function (e) {
                    var sound = e.target.getAttribute("data-audio");
                    if (sound) {
                        vt.VT5.getInstance().dispatchUserEvent(new UserEvent(UserEvent.ACTION_PAUSE_MAIN_AUDIO));
                        this._audioEl.src = sound;
                        this._audioEl.play();
                    }
                };
                SelectOptionQuestion.prototype.inputHandler = function () {
                    var isCorrect = 1;
                    var options = this.div.querySelectorAll(".question_row");
                    for (var i = 0; i < options.length; i++) {
                        this.testItem.userAnswers[i] = [];
                        var answersRow = this._questionBody.question[i].options;
                        var optionsRow = options[i]["querySelectorAll"]("input");
                        for (var j = 0; j < optionsRow.length; j++) {
                            this.testItem.userAnswers[i][j] = optionsRow[j]["checked"] ? 1 : 0;
                            if (optionsRow[j]["checked"] && answersRow[j].a === 0) {
                                isCorrect = -1;
                            }
                            else if (!optionsRow[j]["checked"] && answersRow[j].a === 1) {
                                isCorrect = -1;
                            }
                        }
                    }
                    if (this.div.querySelectorAll("input:checked").length === 0) {
                        isCorrect = 0;
                    }
                    this.testItem.isCorrect = isCorrect;
                    if (this.testItem.testMode === vt.TestMode.Tutorial) {
                        this.clearMarks();
                        this.markUserAnswer();
                        test.Question.showIfCorrect(isCorrect);
                    }
                };
                SelectOptionQuestion.prototype.markUserAnswer = function () {
                    var options = this.div.querySelectorAll(".question_row");
                    for (var i = 0; i < options.length; i++) {
                        this.testItem.userAnswers[i] = [];
                        var answersRow = this._questionBody.question[i].options;
                        var optionsRow = options[i]["querySelectorAll"]("input");
                        for (var j = 0; j < optionsRow.length; j++) {
                            this.testItem.userAnswers[i][j] = optionsRow[j]["checked"] ? 1 : 0;
                            if (optionsRow[j]["checked"] && answersRow[j].a === 0) {
                                options[i]["classList"].add("incorrect");
                                if (this.testItem.testMode === vt.TestMode.Tutorial) {
                                    this.showHint();
                                }
                            }
                            else if (optionsRow[j]["checked"] && answersRow[j].a === 1) {
                                options[i]["classList"].add("correct");
                            }
                        }
                    }
                };
                SelectOptionQuestion.prototype.clearMarks = function () {
                    var options = this.div.querySelectorAll(".question_row");
                    for (var i = 0; i < options.length; i++) {
                        options[i]["classList"].remove("correct");
                        options[i]["classList"].remove("incorrect");
                    }
                };
                SelectOptionQuestion.prototype.showCorrectAnswers = function () {
                    var options = this.div.querySelectorAll(".question_row");
                    for (var i = 0; i < options.length; i++) {
                        var answersRow = this._questionBody.question[i].options;
                        var optionsRow = options[i]["querySelectorAll"]("input");
                        options[i]["classList"].remove("correct");
                        options[i]["classList"].remove("incorrect");
                        for (var j = 0; j < optionsRow.length; j++) {
                            optionsRow[j]["checked"] = answersRow[j].a === 1;
                        }
                    }
                };
                SelectOptionQuestion.prototype.showUserAnswers = function () {
                    this.setUserAnswers();
                    this.markUserAnswer();
                };
                return SelectOptionQuestion;
            }(test.Question));
            test.SelectOptionQuestion = SelectOptionQuestion;
        })(test = view.test || (view.test = {}));
    })(view = vt.view || (vt.view = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var view;
    (function (view) {
        var test;
        (function (test) {
            var DropdownQuestion = (function (_super) {
                __extends(DropdownQuestion, _super);
                function DropdownQuestion(testItem) {
                    return _super.call(this, testItem) || this;
                }
                DropdownQuestion.prototype.destroy = function () {
                    for (var i = 0; i < this.inputs.length; i++) {
                        this.inputs[i].removeEventListener("change", this.inputHandler);
                    }
                    this.inputs = null;
                    this.testItem = null;
                    _super.prototype.destroy.call(this);
                };
                DropdownQuestion.prototype.parseDefault = function (obj) {
                    if (!this.options) {
                        this.options = this.data["options"];
                        for (var key in this.options) {
                            if (this.options.hasOwnProperty(key) && this.options[key][0] !== "") {
                                this.options[key].unshift("");
                            }
                        }
                    }
                    return "<div class='question_body'>" + this.parseTemplate(obj.text) + "</div>";
                };
                DropdownQuestion.prototype.parseTemplate = function (str) {
                    this.correctAnswers = [];
                    var matches = str.match(/{{([^}]+)?}}/g);
                    for (var i = 0; i < matches.length; i++) {
                        var arr = matches[i].substring(2, matches[i].length - 2).split(",");
                        var key = arr[0];
                        var correctAnswer = Number(arr[1]) + 1;
                        this.correctAnswers.push(correctAnswer);
                        var sel = this.options[key];
                        var strSelect = "<select>";
                        for (var j = 0; j < sel.length; j++) {
                            strSelect += "<option>" + sel[j] + "</option>";
                        }
                        strSelect += "</select>";
                        str = str.replace(matches[i], strSelect);
                    }
                    return str;
                };
                DropdownQuestion.prototype.onBodyParsed = function () {
                    this.testItem = this.data;
                    this.inputs = this.div.querySelectorAll("select");
                    this.inputHandler = this.inputHandler.bind(this);
                    for (var i = 0; i < this.inputs.length; i++) {
                        this.inputs[i].addEventListener("change", this.inputHandler);
                        if (this.testItem.testMode === vt.TestMode.Review) {
                            this.inputs[i]["classList"].add("disabled--opaque");
                        }
                    }
                    if (this.testItem.testMode === vt.TestMode.Test || this.testItem.testMode === vt.TestMode.Review
                        && this.testItem.userAnswers && this.testItem.userAnswers.length > 0) {
                        for (var i = 0; i < this.testItem.userAnswers.length; i++) {
                            this.inputs[i]["selectedIndex"] = this.testItem.userAnswers[i];
                        }
                    }
                    if (this.testItem.testMode === vt.TestMode.Review) {
                        this.markUserAnswer();
                        if (this.testItem.isCorrect !== 1) {
                            this.setShowCorrectBtn();
                        }
                    }
                    if (this.testItem.testMode === vt.TestMode.Tutorial) {
                        this.showSubmitAnswerBtn();
                    }
                };
                DropdownQuestion.prototype.inputHandler = function (e) {
                    var correct = 0;
                    var unanswered = 0;
                    var len = this.inputs.length;
                    for (var i = 0; i < len; i++) {
                        var selIndex = this.inputs[i]["selectedIndex"];
                        this.testItem.userAnswers[i] = selIndex;
                        if (selIndex === this.correctAnswers[i]) {
                            correct++;
                        }
                        else if (selIndex !== 0) {
                            if (this.testItem.testMode === vt.TestMode.Tutorial) {
                            }
                        }
                        if (selIndex === 0) {
                            unanswered++;
                            this.testItem.userAnswers[i] = 0;
                        }
                    }
                    if (correct === len) {
                        this.testItem.isCorrect = 1;
                    }
                    else if (unanswered === len) {
                        this.testItem.isCorrect = 0;
                    }
                    else {
                        this.testItem.isCorrect = -1;
                    }
                    if (this.testItem.testMode === vt.TestMode.Tutorial) {
                        this.clearMarks();
                    }
                    e.target.blur();
                };
                DropdownQuestion.prototype.markUserAnswer = function () {
                    var dropdowns = this.div.querySelectorAll("select");
                    var isCorrect = 0;
                    for (var i = 0; i < dropdowns.length; i++) {
                        var sel = dropdowns[i];
                        if (sel.selectedIndex === this.correctAnswers[i]) {
                            sel.classList.add("correct");
                            isCorrect += 1;
                        }
                        else {
                            sel.classList.add("incorrect");
                            isCorrect = -900;
                        }
                        if (sel.item(sel.selectedIndex).value === "") {
                            sel.classList.remove("incorrect");
                            sel.classList.remove("correct");
                        }
                    }
                    test.Question.showIfCorrect(isCorrect);
                };
                DropdownQuestion.prototype.clearMarks = function () {
                    var dropdowns = this.div.querySelectorAll("select");
                    for (var i = 0; i < dropdowns.length; i++) {
                        var sel = dropdowns[i];
                        sel.classList.remove("correct");
                        sel.classList.remove("incorrect");
                    }
                };
                DropdownQuestion.prototype.showCorrectAnswers = function () {
                    var dropdowns = this.div.querySelectorAll("select");
                    for (var i = 0; i < dropdowns.length; i++) {
                        var sel = dropdowns[i];
                        sel.classList.remove("correct");
                        sel.classList.remove("incorrect");
                        sel.selectedIndex = this.correctAnswers[i];
                    }
                };
                DropdownQuestion.prototype.showUserAnswers = function () {
                    var dropdowns = this.div.querySelectorAll("select");
                    for (var i = 0; i < dropdowns.length; i++) {
                        var sel = dropdowns[i];
                        if (this.testItem.userAnswers.length === 0) {
                            sel.selectedIndex = 0;
                        }
                        else {
                            sel.selectedIndex = this.testItem.userAnswers[i];
                        }
                    }
                    this.markUserAnswer();
                };
                return DropdownQuestion;
            }(test.Question));
            test.DropdownQuestion = DropdownQuestion;
        })(test = view.test || (view.test = {}));
    })(view = vt.view || (vt.view = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var view;
    (function (view) {
        var test;
        (function (test) {
            var OrderListQuestion = (function (_super) {
                __extends(OrderListQuestion, _super);
                function OrderListQuestion(testItem) {
                    return _super.call(this, testItem) || this;
                }
                OrderListQuestion.prototype.destroy = function () {
                    this.testItem = null;
                    _super.prototype.destroy.call(this);
                };
                OrderListQuestion.prototype.parseDefault = function (obj) {
                    this.inputHandler = this.inputHandler.bind(this);
                    this.vo = this.data;
                    this.testItem = this.data;
                    var str = "<div class='order-list question_body'>";
                    this.getQuestionBody();
                    this._correctAnswers = this._questionBody.question.concat();
                    if (!this.vo.shuffled) {
                        this.vo.shuffled = vt.shuffle(this._correctAnswers);
                    }
                    return str + OrderListQuestion.renderList(this.vo.shuffled) + "<p></p></div>";
                };
                OrderListQuestion.renderList = function (list) {
                    var str = "";
                    for (var i = 0; i < list.length; i++) {
                        str += "<div class=\"order-list_item\" draggable=\"true\">\n                <span class=\"order-list_text\">" + list[i] + "</span>\n                <span>\n                <button class=\"btn-secondary\" data-type=\"up\"><span class=\"vti_increase\"></span></button>\n                <button class=\"btn-secondary\" data-type=\"down\"><span class=\"vti_decrease\"></span></button>\n                </span>\n                </div>";
                    }
                    return str;
                };
                OrderListQuestion.prototype.onBodyParsed = function () {
                    this.holder = this.div.querySelector(".order-list");
                    this.holder.addEventListener("click", this.inputHandler);
                    if (this.vo.testMode === vt.TestMode.Review) {
                        this.markUserAnswer();
                        this.holder.classList.add("disabled--opaque");
                        if (this.vo.isCorrect !== 1) {
                            this.setShowCorrectBtn();
                        }
                    }
                    else if (this.vo.testMode === vt.TestMode.Tutorial) {
                        this.showSubmitAnswerBtn();
                    }
                    this.setAnswer();
                };
                OrderListQuestion.prototype.inputHandler = function (e) {
                    if (e.target.getAttribute("data-type") === "up") {
                        if (this.holder.firstChild !== e.target.parentNode.parentNode) {
                            this.holder.insertBefore(e.target.parentNode.parentNode, e.target.parentNode.parentNode.previousSibling);
                        }
                    }
                    else if (e.target.getAttribute("data-type") === "down") {
                        if (this.holder.lastChild !== e.target.parentNode.parentNode) {
                            this.holder.insertBefore(e.target.parentNode.parentNode.nextSibling, e.target.parentNode.parentNode);
                        }
                    }
                    this.setAnswer();
                };
                OrderListQuestion.prototype.setAnswer = function () {
                    var items = this.holder.querySelectorAll(".order-list_text");
                    var correct = 1;
                    for (var i = 0; i < items.length; i++) {
                        this.vo.shuffled[i] = items[i].textContent;
                        if (this.vo.shuffled[i] !== this._questionBody.question[i]) {
                            correct = -1;
                        }
                    }
                    this.vo.isCorrect = correct;
                };
                OrderListQuestion.prototype.markUserAnswer = function () {
                    var answers = this.holder.querySelectorAll(".order-list_item");
                    var isCorrect = 1;
                    for (var i = 0; i < this.vo.shuffled.length; i++) {
                        if (this.vo.shuffled[i] === this._questionBody.question[i]) {
                            answers[i]["classList"].add("correct");
                        }
                        else {
                            answers[i]["classList"].add("incorrect");
                            isCorrect = -1;
                        }
                    }
                    test.Question.showIfCorrect(isCorrect);
                };
                OrderListQuestion.prototype.clearMarks = function () {
                    var answers = this.holder.querySelectorAll(".order-list_item");
                    for (var i = 0; i < answers.length; i++) {
                        answers[i]["classList"].remove("correct");
                        answers[i]["classList"].remove("incorrect");
                    }
                };
                OrderListQuestion.prototype.showCorrectAnswers = function () {
                    this.holder.innerHTML = OrderListQuestion.renderList(this._questionBody.question);
                };
                OrderListQuestion.prototype.showUserAnswers = function () {
                    this.holder.innerHTML = OrderListQuestion.renderList(this.vo.shuffled);
                    this.markUserAnswer();
                };
                return OrderListQuestion;
            }(test.Question));
            test.OrderListQuestion = OrderListQuestion;
        })(test = view.test || (view.test = {}));
    })(view = vt.view || (vt.view = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var view;
    (function (view) {
        var test;
        (function (test) {
            var HotspotsQuestion = (function (_super) {
                __extends(HotspotsQuestion, _super);
                function HotspotsQuestion(testItem) {
                    return _super.call(this, testItem) || this;
                }
                HotspotsQuestion.prototype.destroy = function () {
                    this.testItem = null;
                    _super.prototype.destroy.call(this);
                };
                HotspotsQuestion.prototype.parseDefault = function (obj) {
                    if (obj.type === "question") {
                        this.isMulti = obj["question"].answerType === "multi";
                    }
                    return "";
                };
                HotspotsQuestion.prototype.onBodyParsed = function () {
                    this.elMedia = this.div.querySelector(".media");
                    this.testItem = this.data;
                    this.hotspotsArr = this.data.body[0]["hotspots"];
                    if (this.testItem.testMode !== vt.TestMode.Review) {
                        this.onMediaClick = this.onMediaClick.bind(this);
                        this.elMedia.addEventListener("click", this.onMediaClick);
                    }
                };
                HotspotsQuestion.prototype.onMediaLoaded = function () {
                    if (this.testItem.testMode === vt.TestMode.Review) {
                        this.markUserAnswer();
                        if (this.testItem.isCorrect !== 1) {
                            this.setShowCorrectBtn();
                        }
                    }
                    else if (this.testItem.testMode === vt.TestMode.Tutorial) {
                        this.removeAllHotspots();
                        if (this.isMulti) {
                            this.submitAnswerHandler = this.submitAnswerHandler.bind(this);
                            var p = this.div.querySelector(".text");
                            if (p) {
                                var str = vt.model.AppModel.getInstance().getTestResponse().submitAnswer;
                                this._submitAnswerBtn = document.createElement("button");
                                this._submitAnswerBtn.classList.add("btn-secondary");
                                this._submitAnswerBtn.textContent = str;
                                p.appendChild(this._submitAnswerBtn);
                                this._submitAnswerBtn.addEventListener("click", this.submitAnswerHandler);
                            }
                        }
                    }
                };
                HotspotsQuestion.prototype.dispatchEnter = function () {
                    if (this.testItem.userAnswers.length === 0) {
                        var arr = this.data.body[0]["hotspots"];
                        for (var i = arr.length - 1; i >= 0; i--) {
                            if (arr[i].type === "pin") {
                                arr.splice(i, 1);
                            }
                        }
                    }
                    _super.prototype.dispatchEnter.call(this);
                };
                HotspotsQuestion.prototype.onMediaClick = function (e) {
                    var el = e.target;
                    if (el.id === "btn-viewer")
                        return;
                    if (el.classList.contains("pin")) {
                        this.removeHotspot(el);
                        return;
                    }
                    if (!this.isMulti) {
                        this.removeAllHotspots();
                    }
                    var rectHolder = this.div.querySelector(".imgMap").getBoundingClientRect();
                    var x = (e.offsetX * 100 / rectHolder.width).toFixed(1);
                    var y = (e.offsetY * 100 / rectHolder.height).toFixed(1);
                    var spotsNum = Date.now();
                    var pin = document.createElement("div");
                    pin.classList.add("hspot");
                    pin.classList.add("circle");
                    pin.classList.add("label");
                    pin.classList.add("pin");
                    pin.setAttribute("data-s-id", spotsNum + "");
                    pin.setAttribute("data-s-x", x);
                    pin.setAttribute("data-s-y", y);
                    pin.innerHTML = "<span class='el-id vti_pin'></span>";
                    pin.style.left = x + "%";
                    pin.style.top = y + "%";
                    this.div.querySelector(".hotspots").appendChild(pin);
                    var spot = {
                        id: spotsNum + "",
                        type: "pin",
                        x: parseFloat(x),
                        y: parseFloat(y),
                    };
                    this.hotspotsArr.push(spot);
                    this.testItem.userAnswers.push(spot);
                    this.setAnswer();
                    if (!this.isMulti && this.testItem.testMode === vt.TestMode.Tutorial) {
                        this.submitAnswerHandler();
                    }
                };
                HotspotsQuestion.prototype.removeHotspot = function (node) {
                    var id = node.getAttribute("data-s-id");
                    node.parentNode.removeChild(node);
                    for (var i = this.hotspotsArr.length - 1; i >= 0; i--) {
                        if (this.hotspotsArr[i].id === id) {
                            this.hotspotsArr.splice(i, 1);
                            break;
                        }
                    }
                    for (var i = this.testItem.userAnswers.length - 1; i >= 0; i--) {
                        if (this.testItem.userAnswers[i].id === id) {
                            this.testItem.userAnswers.splice(i, 1);
                            break;
                        }
                    }
                    this.setAnswer();
                };
                HotspotsQuestion.prototype.removeAllHotspots = function () {
                    var pins = this.div.querySelectorAll(".hspot.pin");
                    for (var i = pins.length - 1; i >= 0; i--) {
                        this.removeHotspot(pins[i]);
                    }
                };
                HotspotsQuestion.prototype.setAnswer = function () {
                    var correct = 1;
                    var hotspots = [];
                    var pins = this.testItem.userAnswers;
                    for (var i = 0; i < this.hotspotsArr.length; i++) {
                        if (this.hotspotsArr[i].type !== "pin") {
                            hotspots.push(this.hotspotsArr[i]);
                            this.hotspotsArr[i].ok = false;
                        }
                    }
                    for (var i = 0; i < hotspots.length; i++) {
                        var h = hotspots[i];
                        for (var k = 0; k < pins.length; k++) {
                            var p = pins[k];
                            if (HotspotsQuestion.insideShape(h, p)) {
                                h.ok = true;
                                p.ok = true;
                            }
                        }
                        if (!h.ok)
                            correct = -1;
                    }
                    for (var k = 0; k < pins.length; k++) {
                        if (!pins[k].ok) {
                            correct = -1;
                            break;
                        }
                    }
                    if (pins.length === 0)
                        correct = 0;
                    this.testItem.isCorrect = correct;
                };
                HotspotsQuestion.prototype.markUserAnswer = function () {
                    this.setAnswer();
                    var pins = this.testItem.userAnswers;
                    for (var i = 0; i < pins.length; i++) {
                        var p = pins[i];
                        var hspot = this.div.querySelector(".hotspots .pin[data-s-id='" + p.id + "']");
                        hspot.setAttribute("data-s-user", "1");
                        if (p.ok) {
                            hspot.style.backgroundColor = "#2da027";
                        }
                        else {
                            hspot.style.backgroundColor = "#ff0000";
                        }
                    }
                };
                HotspotsQuestion.prototype.submitAnswerHandler = function () {
                    this.markUserAnswer();
                    if (this.testItem.testMode === vt.TestMode.Tutorial && this.testItem.isCorrect !== 1) {
                        this.showHint();
                    }
                    test.Question.showIfCorrect(this.testItem.isCorrect);
                };
                HotspotsQuestion.prototype.showCorrectAnswers = function () {
                    var div = this.div.querySelector(".hotspots");
                    var userSpots = this.div.querySelectorAll(".hspot.pin[data-s-user='1']");
                    for (var i = 0; i < userSpots.length; i++) {
                        userSpots[i]["style"].display = "none";
                    }
                    var hotSpots = this.div.querySelectorAll(".hspot:not(.pin)");
                    for (var i = 0; i < hotSpots.length; i++) {
                        var h = hotSpots[i];
                        var s = HotspotsQuestion.getPin();
                        s.style.left = h.style.left;
                        s.style.top = h.style.top;
                        div.appendChild(s);
                    }
                };
                HotspotsQuestion.prototype.showUserAnswers = function () {
                    var div = this.div.querySelector(".hotspots");
                    var userSpots = this.div.querySelectorAll(".hspot.pin[data-s-user='1']");
                    for (var i = 0; i < userSpots.length; i++) {
                        userSpots[i]["style"].display = "";
                    }
                    var hotSpots = this.div.querySelectorAll(".hspot.pin[data-s-user='2']");
                    for (var i = 0; i < hotSpots.length; i++) {
                        var s = hotSpots[i];
                        s.parentNode.removeChild(s);
                    }
                    this.markUserAnswer();
                };
                HotspotsQuestion.$insideEllipse = function (h, k, rx, ry, x, y) {
                    return ((x - h) * (x - h)) / (rx * rx) + ((y - k) * (y - k)) / (ry * ry) <= 1;
                };
                HotspotsQuestion.insideShape = function (h1, h2) {
                    if (h1.type === "circle")
                        return ((h2.x - h1.x) * (h2.x - h1.x)) / ((h1.width / 2) * (h1.width / 2)) + ((h2.y - h1.y) * (h2.y - h1.y)) / ((h1.height / 2) * (h1.height / 2)) <= 1;
                    return h2.x <= h1.x + h1.width && h2.x >= h1.x && h2.y <= h1.y + h1.height && h2.y >= h1.y;
                };
                HotspotsQuestion.getPin = function () {
                    var s = document.createElement("div");
                    s.classList.add("hspot");
                    s.classList.add("circle");
                    s.classList.add("label");
                    s.classList.add("pin");
                    s.setAttribute("data-s-user", "2");
                    s.innerHTML = "<span class='el-id vti_pin'></span>";
                    return s;
                };
                return HotspotsQuestion;
            }(test.Question));
            test.HotspotsQuestion = HotspotsQuestion;
        })(test = view.test || (view.test = {}));
    })(view = vt.view || (vt.view = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var view;
    (function (view) {
        var test;
        (function (test) {
            var UserEvent = vt.events.UserEvent;
            var TestResultPage = (function (_super) {
                __extends(TestResultPage, _super);
                function TestResultPage(data) {
                    var _this = _super.call(this, data) || this;
                    _this.data = data;
                    return _this;
                }
                TestResultPage.prototype.dispatchEnter = function () {
                };
                TestResultPage.prototype.parseData = function () {
                    this.saveResult = this.saveResult.bind(this);
                    this.fileLoadHandler = this.fileLoadHandler.bind(this);
                    vt.VT5.getInstance().dispatchUserEvent(new UserEvent(UserEvent.ACTION_SUBMIT_TEST));
                    _super.prototype.parseData.call(this);
                    this.renderResults();
                    this.btnSave = this.div.querySelector("button[data-event='UserEvent.save']");
                    this.btnSave.addEventListener("click", this.saveResult);
                    this.btnSave.classList.add("disabled");
                };
                TestResultPage.prototype.renderResults = function () {
                    var res = vt.model.AppModel.getInstance().getTestResult();
                    if (!res) {
                        return;
                    }
                    var div = document.createElement("div");
                    div.classList.add("test-result");
                    var resNames = this.data["results"];
                    var str = "";
                    if (res.passed !== undefined) {
                        if (res.passed) {
                            str += "<h4 class=\"correct\">" + resNames.passResponse + "</h4>";
                        }
                        else {
                            str += "<h4 class=\"incorrect\">" + resNames.failResponse + "</h4>";
                        }
                    }
                    str += "\n            <div><span>" + resNames.name + ":</span><span>" + res.userName + "</span></div>\n            <div><span>" + resNames.trainingId + ":</span><span>" + res.trainingId + "</span></div>\n            <div><span>" + resNames.score + ":</span><span>" + res.score + " / " + res.maxScore + "</span></div>\n            <div><span>" + resNames.scorePercent + ":</span><span>" + res.scorePercent + "%</span></div>\n            <div><span>" + resNames.duration + ":</span><span>" + res.durationString + "</span></div>\n            ";
                    if (res.showBreakdown === true) {
                        str += "<p></p><h4>" + resNames.breakdown + ":</h4>";
                        for (var i = 0; i < res.breakdown.length; i++) {
                            var pool = res.breakdown[i];
                            var poolName = pool.poolName || pool.poolId;
                            str += "<div><span>" + poolName + ":</span> " + pool.userScore + " / " + pool.maxScore + "</div>";
                        }
                    }
                    div.innerHTML = str;
                    this.div.querySelector(".page-in > div").insertBefore(div, this.div.querySelector(".text"));
                    this.certificateTemplate = document.createElement("img");
                    this.certificateTemplate.addEventListener("load", this.fileLoadHandler);
                    this.certificateTemplate.setAttribute('crossOrigin', 'anonymous');
                    var vo = this.data;
                    this.certificateTemplate.src = res.certificateType === "final" ? vo.certificateFinalSrc : vo.certificateModuleSrc;
                };
                TestResultPage.prototype.saveResult = function () {
                    var res = vt.model.AppModel.getInstance().getTestResult();
                    if (!res) {
                    }
                    var canvas = document.createElement("canvas");
                    canvas.width = 2330;
                    canvas.height = 1650;
                    var ctx = canvas.getContext("2d");
                    ctx.fillStyle = "#ffffff";
                    ctx.fillRect(0, 0, canvas.width, canvas.height);
                    ctx.drawImage(this.certificateTemplate, 0, 0);
                    ctx.fillStyle = "#000000";
                    var fields;
                    if (res.certificateType === "module") {
                        if (!this.data["certificateModuleFields"]) {
                            ctx.font = "84px VTFont";
                            ctx.fillText(res.courseName, 95, 180, 2000);
                            ctx.font = "48px VTFont";
                            ctx.fillText(res.userName, 480, 400, 1800);
                            ctx.fillText(res.trainingId, 480, 477, 1800);
                            ctx.fillText(res.testName, 95, 580, 1800);
                            ctx.fillText(res.scorePercent + "%", 480, 752, 1800);
                            ctx.fillText(res.durationString, 480, 823, 1800);
                            ctx.fillText(res.date, 480, 890, 1800);
                        }
                        else {
                            fields = this.data["certificateModuleFields"];
                            TestResultPage.setFields(ctx, fields, res);
                        }
                    }
                    else if (res.certificateType === "final") {
                        if (!this.data["certificateFinalFields"]) {
                            ctx.font = "60px VTFont";
                            ctx.fillText(res.courseName, 240, 290, 1846);
                            ctx.font = "38px VTFont";
                            ctx.fillText(res.userName, 680, 470, 450);
                            ctx.fillText(res.trainingId, 680, 556, 450);
                            ctx.fillText(res.scorePercent + "%", 680, 640, 450);
                            ctx.fillText(res.durationString, 1670, 470, 450);
                            ctx.fillText(res.date, 1670, 640, 450);
                        }
                        else {
                            fields = this.data["certificateFinalFields"];
                            TestResultPage.setFields(ctx, fields, res);
                        }
                    }
                    requirejs(["libs/FileSaver"], function (saveAs) {
                        var c = canvas;
                        c["toBlob"](function (blob) {
                            saveAs(blob, "certificate.png");
                        });
                    });
                };
                TestResultPage.setFields = function (ctx, fields, res) {
                    for (var i = 0; i < fields.length; i++) {
                        var field = fields[i];
                        ctx.font = field.fontSize + "px VTFont";
                        var char = field.addChar ? field.addChar : "";
                        ctx.fillText(res[field.field] + char, field.x, field.y, field.maxWidth);
                    }
                };
                TestResultPage.prototype.fileLoadHandler = function () {
                    this.btnSave.classList.remove("disabled");
                };
                return TestResultPage;
            }(vt.view.Page));
            test.TestResultPage = TestResultPage;
        })(test = view.test || (view.test = {}));
    })(view = vt.view || (vt.view = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var view;
    (function (view) {
        var PageFactory = (function () {
            function PageFactory() {
            }
            PageFactory.getPage = function (obj) {
                if (!obj)
                    return null;
                switch (obj.type) {
                    case "page":
                        return new vt.view.Page(obj);
                    case "linksPage":
                        return new vt.view.LinksPage(obj);
                    case "question":
                        return PageFactory.getQuestion(obj);
                    case "testResult":
                        return new vt.view.test.TestResultPage(obj);
                    case "explorer":
                        return new vt.view.ExplorerPage(obj);
                    default:
                        throw "[PageFactory] Page of type '" + obj.type + "' cannot be created.";
                }
            };
            PageFactory.getQuestion = function (obj) {
                switch (obj.kind) {
                    case "select":
                        return new vt.view.test.SelectOptionQuestion(obj);
                    case "dropdown":
                        return new vt.view.test.DropdownQuestion(obj);
                    case "orderList":
                        return new vt.view.test.OrderListQuestion(obj);
                    case "hotspots":
                        return new vt.view.test.HotspotsQuestion(obj);
                }
            };
            return PageFactory;
        }());
        view.PageFactory = PageFactory;
    })(view = vt.view || (vt.view = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var view;
    (function (view) {
        var PageFactory = vt.view.PageFactory;
        var PagesView = (function (_super) {
            __extends(PagesView, _super);
            function PagesView() {
                var _this = _super.call(this) || this;
                _this.className = "PagesView";
                _this.currentIndex = 0;
                _this.pages = [];
                _this.init();
                return _this;
            }
            PagesView.prototype.showFirstPage = function (first) {
                var fp = this.addPage(first);
                fp.loadMedia();
                this.currentIndex = first.position;
                this.currentPage = fp;
                this.pages.push(fp);
                this.currentPage.dispatchInFocus();
            };
            PagesView.prototype.hasAudio = function () {
                return this.currentPage.hasAudio();
            };
            PagesView.prototype.next = function (contentItem) {
                this.stopAllMedia();
                var p = this.addPage(contentItem);
                this.currentIndex = contentItem.position;
                this.currentPage = p;
                T.killTweaksOf(this.container);
                T._(this.container, .5, [{ prop: "translateX", to: -PagesView.getLeft(p.html) }, { prop: "translateZ", to: 0 }], { call: this.pageInFocusHandler });
            };
            PagesView.prototype.prev = function (contentItem) {
                this.stopAllMedia();
                var p = this.addPage(contentItem);
                this.currentIndex = contentItem.position;
                this.currentPage = p;
                var pos = -PagesView.getLeft(this.pages[0].html);
                T.killTweaksOf(this.container);
                T._(this.container, .5, [{ prop: "translateX", to: pos }, { prop: "translateZ", to: 0 }], { call: this.pageInFocusHandler });
            };
            PagesView.prototype.pauseMedia = function () {
                if (this.currentPage)
                    this.currentPage.pauseMedia();
            };
            PagesView.prototype.resumeMedia = function () {
                this.currentPage.resumeMedia();
            };
            PagesView.prototype.init = function () {
                this.container = document.getElementById("content");
                this.viewportWidth = window.innerWidth;
                this.pageInFocusHandler = this.pageInFocusHandler.bind(this);
                this.pageButtonsHandler = this.pageButtonsHandler.bind(this);
            };
            PagesView.prototype.addPage = function (contentItem) {
                var p = PageFactory.getPage(contentItem);
                if (!p)
                    return;
                var x = 0;
                if (this.pages.length > 0) {
                    if (contentItem.position > this.currentIndex) {
                        x = PagesView.getLeft(this.pages[(this.pages.length - 1)].html) + this.viewportWidth;
                        this.pages.push(p);
                    }
                    else {
                        x = PagesView.getLeft(this.pages[0].html) - this.viewportWidth;
                        this.pages.unshift(p);
                    }
                }
                PagesView.setX(p.html, x);
                this.container.appendChild(p.html);
                p.dispatchEnter();
                return p;
            };
            PagesView.prototype.pageInFocusHandler = function () {
                this.currentPage.dispatchInFocus();
                this.removeAll();
                this.currentPage.loadMedia();
            };
            PagesView.prototype.pageButtonsHandler = function (e) {
                var action = e.target.getAttribute("data-event");
                var payload = e.target.getAttribute("data-payload");
                if (action) {
                    this.dispatch(new vt.events.UserEvent(action, payload));
                }
            };
            PagesView.prototype.removePage = function (p) {
                if (p) {
                    this.container.removeChild(p.html);
                    p.destroy();
                    this.pages.splice(this.pages.indexOf(p), 1);
                }
            };
            PagesView.prototype.removeAll = function () {
                for (var i = this.pages.length - 1; i >= 0; i--) {
                    var p = this.pages[i];
                    if (p.index !== this.currentIndex) {
                        this.removePage(p);
                    }
                }
            };
            PagesView.prototype.stopAllMedia = function () {
                for (var i = 0; i < this.pages.length; i++) {
                    var obj = this.pages[i];
                    obj.stop();
                }
                this.currentPage.dispatchLeave();
            };
            PagesView.setX = function (el, value) {
                el.style.left = value + "px";
            };
            PagesView.getLeft = function (el) {
                var s = el.style.left;
                return Number(s.substr(0, s.length - 2));
            };
            PagesView.prototype.getHTML = function () {
                return undefined;
            };
            PagesView.prototype.resize = function () {
                this.viewportWidth = window.innerWidth;
                this.removeAll();
            };
            return PagesView;
        }(vt.events.Dispatcher));
        view.PagesView = PagesView;
    })(view = vt.view || (vt.view = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var controllers;
    (function (controllers) {
        var NavEvent = vt.events.NavEvent;
        var TouchController = (function (_super) {
            __extends(TouchController, _super);
            function TouchController() {
                var _this = _super.call(this) || this;
                _this.x0 = 0;
                _this.x1 = 0;
                _this.y0 = 0;
                _this.y1 = 0;
                _this.time = 0;
                _this.distX = 0;
                _this.distY = 0;
                _this.containerInitX = 0;
                _this.keepWatch = true;
                _this.isHorizontal = false;
                _this.threshold = 20;
                _this.swipeDist = 60;
                _this.evt = {
                    start: "",
                    end: "",
                    move: ""
                };
                _this.init();
                return _this;
            }
            TouchController.prototype.add = function (element) {
                this.element = element;
                this.element.addEventListener(this.evt.start, this.pStart.bind(this));
                this.element.addEventListener(this.evt.end, this.pEnd.bind(this));
            };
            TouchController.prototype.init = function () {
                var isTouch = window["ontouchstart"] !== undefined;
                if (isTouch) {
                    this.evt = {
                        start: "touchstart",
                        end: "touchend",
                        move: "touchmove"
                    };
                }
                else {
                    this.evt = {
                        start: "mousedown",
                        end: "mouseup",
                        move: "mousemove"
                    };
                }
                this.boundPMove = this.pMove.bind(this);
            };
            TouchController.prototype.pStart = function (e) {
                this.reset();
                if (this.evt.start === "touchstart") {
                    this.x0 = e.touches[0].clientX;
                    this.y0 = e.touches[0].clientY;
                    if (e.touches.length > 1)
                        e.preventDefault();
                }
                else {
                    this.x0 = e.clientX;
                    this.y0 = e.clientY;
                }
                this.containerInitX = this.getX() - this.x0;
                this.element.addEventListener(this.evt.move, this.boundPMove);
            };
            TouchController.prototype.pEnd = function (e) {
                this.element.removeEventListener(this.evt.move, this.boundPMove);
                this.keepWatch = false;
                var speedX = Math.abs(this.distX) / this.time;
                var dist = Math.abs(this.distX);
                if (dist > this.swipeDist && this.isHorizontal && speedX > 10) {
                    if (this.distX > 0) {
                        this.dispatch(new NavEvent(NavEvent.SWIPE_RIGHT));
                    }
                    else {
                        this.dispatch(new NavEvent(NavEvent.SWIPE_LEFT));
                    }
                }
            };
            TouchController.prototype.pMove = function (e) {
                if (this.evt.start === "touchstart") {
                    this.x1 = e.touches[0].clientX;
                    this.y1 = e.touches[0].clientY;
                }
                else {
                    this.x1 = e.clientX;
                    this.y1 = e.clientY;
                }
                this.distX = this.x1 - this.x0;
                this.distY = this.y1 - this.y0;
                if ((Math.abs(this.distX) > this.threshold || Math.abs(this.distY) > this.threshold) && this.keepWatch) {
                    if (Math.abs(this.distY) >= Math.abs(this.distX)) {
                        this.element.removeEventListener(this.evt.move, this.boundPMove);
                        this.isHorizontal = false;
                    }
                    else {
                        var dir = this.distX > 0 ? 1 : -1;
                        this.dispatch(new NavEvent(NavEvent.TOUCH_SCROLL_START, dir));
                        this.isHorizontal = true;
                    }
                    this.keepWatch = false;
                }
                if (this.isHorizontal) {
                    var p = this.containerInitX + this.x1;
                    this.time++;
                }
            };
            TouchController.prototype.getX = function () {
                return (this.element.getBoundingClientRect().left);
            };
            TouchController.prototype.reset = function () {
                this.keepWatch = true;
                this.isHorizontal = false;
                this.x0 = 0;
                this.x1 = 0;
                this.y0 = 0;
                this.y1 = 0;
                this.time = 0;
                this.distX = 0;
                this.distY = 0;
            };
            return TouchController;
        }(vt.events.Dispatcher));
        controllers.TouchController = TouchController;
    })(controllers = vt.controllers || (vt.controllers = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var controllers;
    (function (controllers) {
        var NavEvent = vt.events.NavEvent;
        var UserEvent = vt.events.UserEvent;
        var NavController = (function (_super) {
            __extends(NavController, _super);
            function NavController() {
                var _this = _super.call(this) || this;
                _this.transSpeed = .3;
                _this.isMenuOpen = false;
                _this.init();
                return _this;
            }
            NavController.prototype.setHash = function (value) {
                this.currentHash = "#" + value;
            };
            NavController.getHash = function () {
                var hash = window.location.hash;
                var url;
                if (hash) {
                    var exp = /(#[a-zA-z-_0-9]+)/i;
                    var match = hash.match(exp);
                    if (match) {
                        url = match[0].substring(1, match[0].length);
                    }
                }
                return url;
            };
            NavController.getParam = function (name) {
                return NavController._params[name];
            };
            NavController.prototype.setMainNavSelection = function (id) {
                this.mainNav.setSelected(id);
            };
            NavController.prototype.showMainNav = function () {
                T._(this.mainNavContainer, this.transSpeed, [{ prop: "translateX", to: 0 }, { prop: "translateZ", to: 0 }]);
                this.isMenuOpen = true;
            };
            NavController.prototype.hideMainNav = function (delay) {
                if (delay === void 0) { delay = 0; }
                T._(this.mainNavContainer, this.transSpeed, [{
                        prop: "translateX",
                        to: -this.mainNavContainer.getBoundingClientRect().width - 100
                    },
                    { prop: "translateZ", to: 0 }], { delay: delay });
                this.isMenuOpen = false;
            };
            NavController.prototype.init = function () {
                var _this = this;
                this.pagesView = vt.VT5.getInstance().getView("PagesView");
                this.mainNavContainer = document.getElementById("vt-main-nav");
                this.mainNav = vt.VT5.getInstance().getView("MainNav");
                this.mainNav.addListener(NavEvent.CONTENT_ITEM_SELECT, function (e) { return _this.mainNavHandler(e); });
                this.mainNav.addListener(NavEvent.GOTO_CID, function (e) { return _this.gotoCIDHandler(e); });
                this.mainNav.addListener(NavEvent.CLOSE_MENU, function () { return _this.hideMainNav(); });
                document.getElementById("btn-menu-main").addEventListener("click", function () { return _this.showMainNav(); });
                document.getElementById("vt-footer").addEventListener("click", function (e) { return _this.footerHandler(e); });
                document.addEventListener("click", function (e) { return NavController.buttonClickHandler(e); });
                window.addEventListener("keyup", function (e) { return _this.keyboardNavHandler(e); });
                window.addEventListener("keydown", NavController.removeFocus);
                window.addEventListener("resize", function () { return _this.doResize(); });
                window.addEventListener("hashchange", function () { return _this.locationHashChangeHandler(); });
                this.touch = new controllers.TouchController();
                this.touch.add(document.body);
                this.touch.addListener(NavEvent.SWIPE_LEFT, function (e) { return _this.touchHandler(e); });
                this.touch.addListener(NavEvent.SWIPE_RIGHT, function (e) { return _this.touchHandler(e); });
                this.touch.addListener(NavEvent.TOUCH_SCROLL_START, function (e) { return _this.touchHandler(e); });
                this.hideMainNav(.1);
                document.getElementById("container").addEventListener("click", function () {
                    if (_this.isMenuOpen)
                        _this.hideMainNav();
                });
                var params = window.location.search;
                if (params) {
                    params = params.substr(1);
                    var arr = params.split("&");
                    for (var i = 0; i < arr.length; i++) {
                        var p = arr[i].split("=");
                        NavController._params[p[0]] = p[1];
                        if (p[0] === "dm" && p[1] === "101" && window.location.hostname.match(/localhost.?|\d.?/i) != null) {
                            NavController._params["devMode"] = "1";
                        }
                    }
                }
            };
            NavController.prototype.mainNavHandler = function (e) {
                this.dispatch(new NavEvent(NavEvent.PAGE_CHANGE, e.data));
            };
            NavController.prototype.gotoCIDHandler = function (e) {
                this.dispatch(e);
            };
            NavController.prototype.keyboardNavHandler = function (e) {
                if (e.keyCode === 39) {
                    this.next();
                    e.preventDefault();
                }
                else if (e.keyCode === 37) {
                    this.prev();
                    e.preventDefault();
                }
            };
            NavController.removeFocus = function (e) {
                if (e.keyCode === 39 || e.keyCode === 37) {
                    e.preventDefault();
                    document.activeElement['blur']();
                }
            };
            NavController.prototype.touchHandler = function (e) {
                switch (e.type) {
                    case NavEvent.SWIPE_LEFT:
                        this.next();
                        break;
                    case NavEvent.SWIPE_RIGHT:
                        this.prev();
                        break;
                    case NavEvent.TOUCH_SCROLL_START:
                        if (e.data === -1) {
                        }
                        break;
                }
            };
            NavController.prototype.footerHandler = function (e) {
                switch (e.target["id"]) {
                    case "vt-btn-next":
                        this.next();
                        break;
                    case "vt-btn-prev":
                        this.prev();
                        break;
                }
            };
            NavController.prototype.next = function () {
                this.dispatch(new NavEvent(NavEvent.NEXT));
            };
            NavController.prototype.prev = function () {
                this.dispatch(new NavEvent(NavEvent.PREV));
            };
            NavController.buttonClickHandler = function (e) {
                var evt = e.target.getAttribute("data-event");
                var payload = e.target.getAttribute("data-payload");
                if (evt) {
                    vt.VT5.getInstance().dispatchUserEvent(new UserEvent(evt, payload));
                }
            };
            NavController.prototype.sendHashEvent = function () {
                var hash = window.location.hash;
                if (hash) {
                    this.currentHash = hash;
                    var url = hash.substring(1, hash.length);
                    var evt = new NavEvent(NavEvent.CONTENT_ITEM_SELECT, url);
                    this.mainNavHandler(evt);
                }
            };
            NavController.prototype.locationHashChangeHandler = function () {
                if (this.currentHash !== window.location.hash) {
                    this.sendHashEvent();
                }
            };
            NavController.prototype.doResize = function () {
                if (window.innerWidth >= window.innerHeight) {
                    this.transSpeed = .3;
                }
                else {
                    this.transSpeed = .25;
                }
                var w = -this.mainNavContainer.getBoundingClientRect().width - 100;
                this.mainNavContainer.style.transform = "translateX(" + w + "px)";
                this.mainNavContainer.style["webkitTransform"] = "translateX(" + w + "px)";
                this.pagesView.resize();
            };
            return NavController;
        }(vt.events.Dispatcher));
        NavController._params = {};
        controllers.NavController = NavController;
    })(controllers = vt.controllers || (vt.controllers = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var controllers;
    (function (controllers) {
        var TestResult = vt.model.TestResultData;
        var TestController = (function () {
            function TestController(enforcer) {
                if (enforcer === void 0) { enforcer = null; }
                this.questions = [];
                this.isResultCalculated = false;
                if (!enforcer) {
                    throw new Error("TestController is a singleton. Use .getInstance()");
                }
                this.init();
            }
            TestController.getInstance = function () {
                if (TestController.__instance) {
                    return TestController.__instance;
                }
                return new TestController(new Enforcer());
            };
            TestController.prototype.startTest = function (test) {
                var headerIcon = "<span class=\"vti_badge hgroup_icon\">";
                var headerTxt = "</span>" + test.title;
                if (!this.test && test.testMode === vt.TestMode.Test) {
                    this._originalTestMode = test.testMode;
                    this.result = new TestResult();
                    this.result.testId = test.testId === undefined ? "final" : test.testId;
                    this.result.showBreakdown = test.showBreakdown;
                    this.testStart = Date.now();
                    this.test = test;
                    this.pools = {};
                    for (var i = 0; i < test.content.length; i++) {
                        var q = test.content[i];
                        q.header = "" + headerIcon + headerTxt;
                        if (q.type === "question") {
                            q.testMode = this._originalTestMode;
                            this.result.maxScore += q.scorePoints;
                            this.questions.push(q);
                            if (!this.pools.hasOwnProperty(q.poolId)) {
                                this.pools[q.poolId] = {
                                    maxScore: 0,
                                    userScore: 0,
                                    poolId: q.poolId,
                                    poolName: q.poolName,
                                    questions: []
                                };
                            }
                            this.pools[q.poolId].questions.push(q);
                            this.pools[q.poolId].maxScore += q.scorePoints;
                        }
                    }
                }
                else if (test.testMode === vt.TestMode.Tutorial) {
                    headerIcon = "<span class=\"vti_tutorial hgroup_icon\">";
                    for (var i = 0; i < test.content.length; i++) {
                        var q = test.content[i];
                        q.header = "" + headerIcon + headerTxt;
                    }
                }
            };
            TestController.prototype.setMode = function (value) {
                for (var i = 0; i < this.questions.length; i++) {
                    this.questions[i].testMode = value;
                }
            };
            TestController.prototype.clearTest = function () {
                for (var i = 0; i < this.questions.length; i++) {
                    this.questions[i].userAnswers = [];
                    this.questions[i].testMode = this._originalTestMode;
                    this.questions[i].header = "";
                    this.questions[i].isCorrect = 0;
                }
                this.test = null;
                this.questions = [];
                this.testStart = 0;
                this.testEnd = 0;
                this.isResultCalculated = false;
                this.result = null;
            };
            TestController.prototype.getTestResult = function () {
                if (this.isResultCalculated) {
                    return this.result;
                }
                if (!this.result) {
                    return null;
                }
                var userScore = 0;
                for (var i = 0; i < this.questions.length; i++) {
                    var isCorrect = this.questions[i].isCorrect;
                    userScore += isCorrect === 1 ? this.questions[i].scorePoints : 0;
                    switch (isCorrect) {
                        case 1:
                            this.questions[i].header = "<span class='correct q-answer'><span class='vti_correct'></span> " + this.appModel.getTestResponse().correct + "</span>";
                            break;
                        case 0:
                            this.questions[i].header = "<span class='incorrect q-answer'><span class='vti_incorrect'></span> " + this.appModel.getTestResponse().notAnswered + "</span>";
                            break;
                        case -1:
                            this.questions[i].header = "<span class='incorrect q-answer'><span class='vti_incorrect'></span> " + this.appModel.getTestResponse().incorrect + "</span>";
                            break;
                        case -2:
                            this.questions[i].header = "<span class='incorrect q-answer'><span class='vti_incorrect'></span> " + this.appModel.getTestResponse().disqualified + "</span>";
                            break;
                    }
                    this.questions[i].showCorrect = this.appModel.getTestResponse().showCorrect;
                }
                this.isResultCalculated = true;
                this.result.userName = this.appModel.getUserName();
                this.result.trainingId = this.appModel.getTrainingId();
                this.result.score = userScore;
                this.result.scorePercent = Math.round(userScore * 100 / this.result.maxScore);
                this.result.duration = Date.now() - this.testStart;
                this.result.durationString = TestController.getFormattedDuration(this.result.duration);
                this.result.breakdown = this.getTestBreakdown();
                if (this.test.minScore) {
                    this.result.passed = this.result.scorePercent >= this.test.minScore;
                }
                this.result.courseName = this.appModel.getProductData().title;
                var d = new Date();
                var day = d.getDate() < 10 ? "0" + d.getDate() : d.getDate() + "";
                var mmm = d.getMonth() + 1;
                var m = mmm < 10 ? "0" + mmm : mmm + "";
                this.result.date = day + "/" + m + "/" + d.getFullYear();
                this.result.certificateType = this.test.certificateType;
                this.result.testName = this.test.testName;
                return this.result;
            };
            TestController.prototype.getTestBreakdown = function () {
                var arr = [];
                for (var k in this.pools) {
                    if (this.pools.hasOwnProperty(k)) {
                        var pool = this.pools[k];
                        var questions = pool.questions;
                        for (var i = 0; i < questions.length; i++) {
                            var q = questions[i];
                            if (q.isCorrect > 0) {
                                pool.userScore += q.scorePoints;
                            }
                        }
                        arr.push({
                            poolId: pool.poolId,
                            poolName: pool.poolName,
                            maxScore: pool.maxScore,
                            userScore: pool.userScore,
                        });
                    }
                }
                return arr;
            };
            TestController.prototype.init = function () {
                this.appModel = vt.model.AppModel.getInstance();
            };
            TestController.getFormattedDuration = function (value) {
                var d = new Date(value);
                var hh = d.getHours() > 9 ? "" + d.getHours() : "0" + d.getHours();
                var mm = d.getMinutes() > 9 ? "" + d.getMinutes() : "0" + d.getMinutes();
                var ss = d.getSeconds() > 9 ? "" + d.getSeconds() : "0" + d.getSeconds();
                return hh + ":" + mm + ":" + ss;
            };
            return TestController;
        }());
        controllers.TestController = TestController;
        var Enforcer = (function () {
            function Enforcer() {
            }
            return Enforcer;
        }());
    })(controllers = vt.controllers || (vt.controllers = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var view;
    (function (view) {
        var nav;
        (function (nav) {
            var Footer = (function (_super) {
                __extends(Footer, _super);
                function Footer(data) {
                    var _this = _super.call(this) || this;
                    _this.data = data;
                    _this.className = "Footer";
                    _this.init();
                    return _this;
                }
                Footer.prototype.getHTML = function () {
                    return this.html;
                };
                Footer.prototype.setPageCount = function (value) {
                    this.pageCountDisplay.innerHTML = value;
                };
                Footer.prototype.setEnabledNext = function (value) {
                    if (!value) {
                        this.btnNext.classList.add("disabled");
                    }
                    else {
                        this.btnNext.classList.remove("disabled");
                    }
                };
                Footer.prototype.setEnabledPrev = function (value) {
                    if (!value) {
                        this.btnPrev.classList.add("disabled");
                    }
                    else {
                        this.btnPrev.classList.remove("disabled");
                    }
                };
                Footer.setUserName = function (value) {
                    var userName = document.querySelector("footer > .user-name");
                    userName.textContent = value;
                };
                Footer.prototype.init = function () {
                    this.html = document.getElementById("vt-footer");
                    this.btnNext = document.getElementById("vt-btn-next");
                    this.btnPrev = document.getElementById("vt-btn-prev");
                    this.pageCountDisplay = document.getElementById("vt-page-count-display");
                    if (this.data) {
                        var userName = document.querySelector("footer > .user-name");
                        userName.textContent = this.data.userName;
                    }
                };
                return Footer;
            }(vt.events.Dispatcher));
            nav.Footer = Footer;
        })(nav = view.nav || (view.nav = {}));
    })(view = vt.view || (vt.view = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var view;
    (function (view) {
        var components;
        (function (components) {
            var modal;
            (function (modal) {
                var MediaFactory = vt.view.components.media.MediaFactory;
                var RenderFactory = vt.view.renderers.RenderFactory;
                var AbstractModal = (function () {
                    function AbstractModal(vo) {
                        this._mediaList = [];
                        this._audioList = [];
                        this._isCancelable = true;
                        this.vo = vo;
                        this.init();
                    }
                    AbstractModal.prototype.getHtml = function () {
                        return this.div;
                    };
                    Object.defineProperty(AbstractModal.prototype, "type", {
                        get: function () {
                            return this.vo.type;
                        },
                        enumerable: true,
                        configurable: true
                    });
                    AbstractModal.prototype.isCancelable = function () {
                        return this._isCancelable;
                    };
                    AbstractModal.prototype.loadMedia = function () {
                        var items = this.div.querySelectorAll(".media");
                        for (var i = 0, k = this._mediaList.length; i < k; i++) {
                            this._mediaList[i].load(items[i]);
                        }
                    };
                    AbstractModal.prototype.destroy = function (callback) {
                        if (callback === void 0) { callback = null; }
                        this.vo = null;
                        for (var i = 0; i < this._mediaList.length; i++) {
                            this._mediaList[i].stop();
                            this._mediaList[i].unload();
                        }
                        if (this.audioDiv.src !== "") {
                            this.audioDiv.pause();
                        }
                        if (callback) {
                            callback();
                        }
                    };
                    AbstractModal.prototype.playSound = function () {
                        this.audioDiv.play();
                    };
                    AbstractModal.prototype.init = function (controls) {
                        if (controls === void 0) { controls = null; }
                        this.div = document.createElement("div");
                        this.div.classList.add("modal");
                        this.audioDiv = document.createElement("audio");
                        this.audioDiv.style.visibility = "none";
                    };
                    AbstractModal.prototype.getButtonsStr = function () {
                        return "";
                    };
                    AbstractModal.prototype.parseBody = function (body) {
                        var str = "";
                        for (var i = 0; i < body.length; i++) {
                            var obj = body[i];
                            str += RenderFactory.render(obj);
                            switch (obj.type) {
                                case "media":
                                    if (obj.kind !== "audio") {
                                        str += this.getMediaNode(obj);
                                    }
                                    else {
                                        this.audioDiv.src = obj.src;
                                        this._audioList.push(obj.src);
                                    }
                                    break;
                                case "group":
                                    {
                                        return "<div class='group'>" + this.parseBody(obj["items"]) + "</div>";
                                    }
                            }
                        }
                        return str;
                    };
                    AbstractModal.prototype.getMediaNode = function (obj) {
                        var str = "";
                        var media = MediaFactory.getMedia(obj);
                        if (media) {
                            this._mediaList.push(media);
                            str += media.getHtmlString();
                        }
                        return str;
                    };
                    return AbstractModal;
                }());
                modal.AbstractModal = AbstractModal;
            })(modal = components.modal || (components.modal = {}));
        })(components = view.components || (view.components = {}));
    })(view = vt.view || (vt.view = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var view;
    (function (view) {
        var components;
        (function (components) {
            var modal;
            (function (modal) {
                var FullScreenModal = (function (_super) {
                    __extends(FullScreenModal, _super);
                    function FullScreenModal() {
                        return _super !== null && _super.apply(this, arguments) || this;
                    }
                    FullScreenModal.prototype.init = function () {
                        _super.prototype.init.call(this);
                        var body = this.parseBody(this.vo.body);
                        this.div.classList.add("modal_full-screen");
                        this.div.innerHTML = "\n            <div class=\"modal_controls\"><span class=\"modal_full-screen--title\">" + this.vo.title + "</span><button class=\"vti_close\" data-event=\"UserEvent.cancel\"></button></div>\n            <div class=\"modal_body\">\n            " + body + "\n            </div>";
                        this.loadMedia();
                    };
                    return FullScreenModal;
                }(vt.view.components.modal.AbstractModal));
                modal.FullScreenModal = FullScreenModal;
            })(modal = components.modal || (components.modal = {}));
        })(components = view.components || (view.components = {}));
    })(view = vt.view || (vt.view = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var view;
    (function (view) {
        var components;
        (function (components) {
            var modal;
            (function (modal) {
                var AboutModal = (function (_super) {
                    __extends(AboutModal, _super);
                    function AboutModal() {
                        return _super !== null && _super.apply(this, arguments) || this;
                    }
                    AboutModal.prototype.init = function () {
                        _super.prototype.init.call(this);
                        var body = AboutModal.parseAbout(this.vo.body);
                        var img = this.vo.image ? "<img src=\"" + this.vo.image + "\">" : "";
                        this.div.innerHTML = "<div class=\"modal_controls\"><div class=\"modal_full-screen--title\">" + this.vo.title + "</div><button class=\"vti_close\" data-event=\"UserEvent.cancel\"></button></div>\n            <div class=\"modal_full-screen--body\">\n            " + img + "\n            " + body + "\n            </div>";
                        this.loadMedia();
                    };
                    AboutModal.parseAbout = function (body) {
                        var str = "<div class='about'>";
                        for (var i = 0; i < body.length; i++) {
                            var item = body[i];
                            if (item.title) {
                                str += "<h4>" + item.title + "</h4>";
                            }
                            switch (item.type) {
                                case "logos":
                                    str += AboutModal.parseLogos(item.content);
                                    break;
                                case "credits":
                                    str += AboutModal.parseCredits(item.content, "credits");
                                    break;
                                case "legal":
                                    str += AboutModal.parseCredits(item.content, "legal");
                                    break;
                            }
                        }
                        return str + "</div>";
                    };
                    AboutModal.parseLogos = function (items) {
                        var str = "<div class='logos'>";
                        for (var i = 0; i < items.length; i++) {
                            var item = items[i];
                            str += "<img src=\"" + item + "\" class=\"logo\">";
                        }
                        return (str + "</div>");
                    };
                    AboutModal.parseCredits = function (items, style) {
                        var str = "<div>";
                        for (var i = 0; i < items.length; i++) {
                            var item = items[i];
                            var list = item.list.join(", ");
                            str += "<h5>" + item.title + "</h5>" + list + "<p></p>";
                        }
                        return str + "</div>";
                    };
                    return AboutModal;
                }(vt.view.components.modal.FullScreenModal));
                modal.AboutModal = AboutModal;
            })(modal = components.modal || (components.modal = {}));
        })(components = view.components || (view.components = {}));
    })(view = vt.view || (vt.view = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var view;
    (function (view) {
        var components;
        (function (components) {
            var modal;
            (function (modal) {
                var CharacterModal = (function (_super) {
                    __extends(CharacterModal, _super);
                    function CharacterModal() {
                        return _super !== null && _super.apply(this, arguments) || this;
                    }
                    CharacterModal.prototype.init = function () {
                        _super.prototype.init.call(this);
                        var body = this.parseBody(this.vo.body);
                        this.div.classList.add("modal_character");
                        this.div.innerHTML = "\n            <div class=\"modal_header\">" + this.vo.title + "<button class=\"vti_close\" data-event=\"UserEvent.cancel\"></button></div>\n            <div class=\"character-in\">\n            <img class=\"character\" src=\"" + this.vo.image + "\">\n            <div class=\"modal_body\">" + body + "</div>\n            </div>";
                        this.loadMedia();
                    };
                    return CharacterModal;
                }(vt.view.components.modal.AbstractModal));
                modal.CharacterModal = CharacterModal;
            })(modal = components.modal || (components.modal = {}));
        })(components = view.components || (view.components = {}));
    })(view = vt.view || (vt.view = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var view;
    (function (view) {
        var components;
        (function (components) {
            var modal;
            (function (modal) {
                var DialogModal = (function (_super) {
                    __extends(DialogModal, _super);
                    function DialogModal() {
                        return _super !== null && _super.apply(this, arguments) || this;
                    }
                    DialogModal.prototype.init = function () {
                        _super.prototype.init.call(this);
                        var body = this.parseBody(this.vo.body);
                        var buttons = this.getButtonsStr();
                        this.div.innerHTML = "<div class=\"modal_header\"><span></span><button class=\"vti_close\" data-event=\"UserEvent.cancel\"></button></div>\n            <div class=\"modal_body\">\n            <h3>" + this.vo.title + "</h3>\n            " + body + "\n            <div class=\"modal_buttons-row\">" + buttons + "</div>\n            </div>";
                    };
                    DialogModal.prototype.getButtonsStr = function () {
                        var buttons = "";
                        for (var i = 0; i < this.vo.buttons.length; i++) {
                            var item = this.vo.buttons[i];
                            buttons += vt.view.renderers.ButtonRenderer.render(item);
                        }
                        return buttons;
                    };
                    return DialogModal;
                }(vt.view.components.modal.AbstractModal));
                modal.DialogModal = DialogModal;
            })(modal = components.modal || (components.modal = {}));
        })(components = view.components || (view.components = {}));
    })(view = vt.view || (vt.view = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var view;
    (function (view) {
        var components;
        (function (components) {
            var modal;
            (function (modal) {
                var FlipModal = (function (_super) {
                    __extends(FlipModal, _super);
                    function FlipModal(vo) {
                        var _this = _super.call(this, vo) || this;
                        _this.rotation = 0;
                        return _this;
                    }
                    FlipModal.prototype.playSound = function () {
                        if (this._audioList.length > 0) {
                            this.audioDiv.src = this._audioList[0];
                            this.audioDiv.play();
                        }
                    };
                    FlipModal.prototype.init = function () {
                        _super.prototype.init.call(this);
                        this.div.classList.add("modal_flip");
                        this.flipVO = this.vo;
                        var cssClass = "modal_flip-front";
                        this.div.appendChild(this.getScreen(this.flipVO.screens[0], "modal_flip-front"));
                        this.div.appendChild(this.getScreen(this.flipVO.screens[1], "modal_flip-back"));
                        this.btnHandler = this.btnHandler.bind(this);
                        this.div.addEventListener("click", this.btnHandler);
                        this.loadMedia();
                        this.flip = this.flip.bind(this);
                    };
                    FlipModal.prototype.getScreen = function (screen, cssClass) {
                        var div = document.createElement("div");
                        div.classList.add("modal_flip-side");
                        div.classList.add(cssClass);
                        var character = screen.image ? "<img src=\"" + screen.image + "\">" : "";
                        var header = screen.header ? " <span>" + screen.header + "</span> " : "";
                        var title = screen.title ? "<h3>" + screen.title + "</h3>" : "";
                        div.innerHTML = "\n             <div class=\"modal_header\"><button data-id=\"flip\" class=\"vti_flip\"></button> " + header + " <button class=\"vti_close\" data-event=\"UserEvent.cancel\"></button></div>\n             <div class=\"modal_body\">" + character + "\n             " + title + "\n             " + this.parseBody(screen.body) + "\n             </div>";
                        if (cssClass === "modal_flip-front") {
                            this.side1 = div;
                        }
                        else {
                            this.side2 = div;
                        }
                        return div;
                    };
                    FlipModal.prototype.btnHandler = function (e) {
                        if (e.target.getAttribute("data-id") === "flip") {
                            if (this.rotation < 180) {
                                this.rotation = 180;
                            }
                            else {
                                this.rotation = 0;
                            }
                            T._(this.div, .4, [{ prop: "rotateY", to: this.rotation / 2 || 90 }], { ease: T.easeIn, call: this.flip })
                                ._(.4, [{ prop: "rotateY", to: this.rotation }], { ease: T.easeOut });
                        }
                    };
                    FlipModal.prototype.flip = function () {
                        this.audioDiv.src = "";
                        this.audioDiv.pause();
                        if (this.rotation > 0) {
                            this.side1.style.display = "none";
                            this.side2.style.display = "block";
                            if (this._audioList.length === 2) {
                                this.audioDiv.src = this._audioList[1];
                                this.audioDiv.play();
                            }
                        }
                        else {
                            this.side1.style.display = "block";
                            this.side2.style.display = "none";
                            if (this._audioList.length > 0) {
                                this.audioDiv.src = this._audioList[0];
                                this.audioDiv.play();
                            }
                        }
                    };
                    return FlipModal;
                }(vt.view.components.modal.AbstractModal));
                modal.FlipModal = FlipModal;
            })(modal = components.modal || (components.modal = {}));
        })(components = view.components || (view.components = {}));
    })(view = vt.view || (vt.view = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var view;
    (function (view) {
        var components;
        (function (components) {
            var modal;
            (function (modal) {
                var HelpModal = (function (_super) {
                    __extends(HelpModal, _super);
                    function HelpModal() {
                        return _super !== null && _super.apply(this, arguments) || this;
                    }
                    HelpModal.prototype.init = function () {
                        _super.prototype.init.call(this);
                        var body = this.parseBody(this.vo.body);
                        var img = this.vo.image ? "<img src=\"" + this.vo.image + "\">" : "";
                        this.div.innerHTML = "<div class=\"modal_controls\"><div class=\"modal_full-screen--title\">" + this.vo.title + "</div><button class=\"vti_close\" data-event=\"UserEvent.cancel\"></button></div>\n            <div class=\"modal_full-screen--body\">\n            " + img + "\n            " + body + "\n            </div>";
                        this.loadMedia();
                    };
                    HelpModal.prototype.parseBody = function (body) {
                        var str = "<div class='help'>";
                        for (var i = 0; i < body.length; i++) {
                            var item = body[i];
                            str += "<h4>" + item.title + "</h4>";
                            switch (item.type) {
                                case "icons":
                                    str += HelpModal.parseIcons(item.content);
                                    break;
                                case "support":
                                    str += HelpModal.parseSupport(item.content);
                                    break;
                            }
                        }
                        return str + "</div>";
                    };
                    HelpModal.parseSupport = function (items) {
                        var str = "<div>";
                        for (var i = 0; i < items.length; i++) {
                            var item = items[i];
                            str += "<div class=\"row-support\">\n                            <span>" + item.name + "</span>\n                            <span>" + item.value + "</span>\n                        </div>";
                        }
                        return str + "</div>";
                    };
                    HelpModal.parseIcons = function (items) {
                        var str = "<div>";
                        for (var i = 0; i < items.length; i++) {
                            var item = items[i];
                            var iconsStr = "";
                            for (var j = 0; j < item.icons.length; j++) {
                                iconsStr += "<span class=\"" + item.icons[j] + "\"></span> ";
                            }
                            str += "<div class=\"row\">\n                            <span>" + iconsStr + "</span>\n                            <span>" + item.location + "</span>\n                            <span>" + item.description + "</span>\n                        </div>";
                        }
                        return str + "</div>";
                    };
                    return HelpModal;
                }(vt.view.components.modal.FullScreenModal));
                modal.HelpModal = HelpModal;
            })(modal = components.modal || (components.modal = {}));
        })(components = view.components || (view.components = {}));
    })(view = vt.view || (vt.view = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var view;
    (function (view) {
        var components;
        (function (components) {
            var modal;
            (function (modal) {
                var THREEjsMedia = vt.view.components.media.THREEjs;
                var Babylon = vt.view.components.media.Babylon;
                var GL3dModal = (function (_super) {
                    __extends(GL3dModal, _super);
                    function GL3dModal(vo) {
                        return _super.call(this, vo) || this;
                    }
                    GL3dModal.prototype.init = function () {
                        _super.prototype.init.call(this);
                        var body = "";
                        this.div.classList.add("modal_full-screen");
                        this.div.innerHTML = "\n            <div class=\"modal_header\"><span></span><button class=\"vti_close\" data-event=\"UserEvent.cancel\"></button></div>\n            <div class=\"modal_body\">\n            " + body + "\n            </div>";
                        this.loadMedia();
                    };
                    GL3dModal.prototype.loadMedia = function () {
                        if (this.vo["vo3d"].library === "THREE.js") {
                            this.glMedia = new THREEjsMedia(this.vo["vo3d"]);
                        }
                        else {
                            this.glMedia = new Babylon(this.vo["vo3d"]);
                        }
                        this.glMedia.load(this.div);
                    };
                    GL3dModal.prototype.destroy = function (callback) {
                        _super.prototype.destroy.call(this, callback);
                        if (this.glMedia) {
                            this.glMedia.unload();
                        }
                    };
                    GL3dModal.prototype.parseBody = function () {
                        return "";
                    };
                    return GL3dModal;
                }(vt.view.components.modal.AbstractModal));
                modal.GL3dModal = GL3dModal;
            })(modal = components.modal || (components.modal = {}));
        })(components = view.components || (view.components = {}));
    })(view = vt.view || (vt.view = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var view;
    (function (view) {
        var components;
        (function (components) {
            var modal;
            (function (modal) {
                var GlossaryModal = (function (_super) {
                    __extends(GlossaryModal, _super);
                    function GlossaryModal() {
                        return _super !== null && _super.apply(this, arguments) || this;
                    }
                    GlossaryModal.prototype.init = function () {
                        _super.prototype.init.call(this);
                        var body = this.parseGlossary(this.vo.body);
                        var img = this.vo.image ? "<img src=\"" + this.vo.image + "\">" : "";
                        this.div.innerHTML = "<div class=\"modal_controls\"><div class=\"modal_full-screen--title\">" + this.vo.title + "</div><button class=\"vti_close\" data-event=\"UserEvent.cancel\"></button></div>\n            <div class=\"modal_full-screen--body\">\n            " + img + "\n            " + body + "\n            </div>";
                        this.loadMedia();
                    };
                    GlossaryModal.prototype.parseGlossary = function (obj) {
                        if (this.vo["sortItems"]) {
                            obj.sort(function (a, b) {
                                if (a.a > b.a) {
                                    return 1;
                                }
                                else if (a.a < b.a) {
                                    return -1;
                                }
                                return 0;
                            });
                        }
                        var str = "<div class='glossary'>";
                        for (var i = 0; i < obj.length; i++) {
                            var item = obj[i];
                            str += "<div class=\"row\"><span>" + item.a + "</span><span>" + item.b + "</span></div>";
                        }
                        str += "</div>";
                        return str;
                    };
                    return GlossaryModal;
                }(vt.view.components.modal.FullScreenModal));
                modal.GlossaryModal = GlossaryModal;
            })(modal = components.modal || (components.modal = {}));
        })(components = view.components || (view.components = {}));
    })(view = vt.view || (vt.view = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var view;
    (function (view) {
        var components;
        (function (components) {
            var modal;
            (function (modal) {
                var HintModal = (function (_super) {
                    __extends(HintModal, _super);
                    function HintModal() {
                        return _super !== null && _super.apply(this, arguments) || this;
                    }
                    HintModal.prototype.init = function () {
                        _super.prototype.init.call(this);
                        var body = "<h3>" + (this.vo.title || "") + "</h3>" + this.parseBody(this.vo.body);
                        var img = this.vo.image ? "<img src=\"" + this.vo.image + "\">" : "";
                        this.div.innerHTML = "<div class=\"modal_header\"><span></span><button class=\"vti_close\" data-event=\"UserEvent.cancel\"></button></div>\n            <div class=\"modal_body\">\n            " + img + "\n            " + body + "\n            </div>";
                        this.loadMedia();
                    };
                    return HintModal;
                }(vt.view.components.modal.AbstractModal));
                modal.HintModal = HintModal;
            })(modal = components.modal || (components.modal = {}));
        })(components = view.components || (view.components = {}));
    })(view = vt.view || (vt.view = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var view;
    (function (view) {
        var components;
        (function (components) {
            var modal;
            (function (modal) {
                var HotspotsRenderer = vt.view.renderers.HotspotsRenderer;
                var ImageExplorerModal = (function (_super) {
                    __extends(ImageExplorerModal, _super);
                    function ImageExplorerModal() {
                        var _this = _super !== null && _super.apply(this, arguments) || this;
                        _this.scale = 100.0;
                        _this.diffX = 0;
                        _this.diffY = 0;
                        _this.dist0 = 0;
                        _this.isScaling = false;
                        _this.progressCount = 0;
                        return _this;
                    }
                    ImageExplorerModal.prototype.init = function () {
                        var _this = this;
                        var mediaItem = this.vo.media;
                        this.div = document.createElement("div");
                        var hotspots = "";
                        var imgMap = "";
                        this.div.classList.add("media-explorer");
                        this.div.classList.add("touchable");
                        if (mediaItem.hotspots) {
                            this.hasHotspots = true;
                            hotspots = HotspotsRenderer.render(mediaItem.hotspots);
                            imgMap = " imgMap";
                        }
                        if (mediaItem.background)
                            this.div.style.backgroundColor = mediaItem.background;
                        var controlsBg = mediaItem.controlsBackground ? "style=\"background-color: " + mediaItem.controlsBackground + ";\"" : "";
                        this.div.innerHTML = "\n                <div class=\"description\"></div>\n                <div class=\"modal_controls\" " + controlsBg + ">\n                    <button class=\"vti_zoom-in\" data-id=\"zoom-in\"></button>\n                    <button class=\"vti_zoom-out\" data-id=\"zoom-out\"></button>\n                    <button class=\"vti_close\" data-event=\"UserEvent.cancel\"></button>\n                </div>\n                <div class=\"draggable touchable\">\n                <div class=\"media-holder touchable" + imgMap + "\">\n                    <img src=\"" + mediaItem.src + "\" class=\"touchable\">\n                    " + hotspots + "\n                </div>\n                </div>\n            ";
                        this.draggable = this.div.querySelector(".draggable");
                        this.audioDiv = document.createElement("audio");
                        this.audioDiv.style.visibility = "none";
                        this.elControls = this.div.querySelector(".modal_controls");
                        this.elMediaHolder = this.div.querySelector(".media-holder");
                        this.elDescription = this.div.querySelector(".media-explorer > .description");
                        this.controlsHandler = this.controlsHandler.bind(this);
                        this.dragStart = this.dragStart.bind(this);
                        this.dragProgress = this.dragProgress.bind(this);
                        this.dragStop = this.dragStop.bind(this);
                        this.touchStart = this.touchStart.bind(this);
                        this.touchProgress = this.touchProgress.bind(this);
                        this.elControls.addEventListener("click", this.controlsHandler);
                        this.draggable.addEventListener("mousedown", this.dragStart);
                        document.addEventListener("mouseup", this.dragStop);
                        this.draggable.addEventListener("touchstart", this.touchStart);
                        this.draggable.addEventListener("touchmove", this.touchProgress);
                        if (mediaItem.hotspots) {
                            this.hotspotClickHandler = this.hotspotClickHandler.bind(this);
                            this.elHotspots = this.div.querySelector(".hotspots");
                            this.elHotspots.addEventListener("click", this.hotspotClickHandler);
                        }
                        var pageMedia = document.querySelector(".media");
                        if (pageMedia) {
                            var rect = pageMedia.getBoundingClientRect();
                            var windW = window.screen.width;
                            var windH = window.screen.height;
                            var w = rect.width * 100 / windW;
                            var h = rect.height * 100 / windH;
                            this.div.style.position = "fixed";
                            this.div.style.width = w + "%";
                            this.div.style.height = h + "%";
                            this.div.style.left = rect.left + "px";
                            this.div.style.top = rect.top + "px";
                            T._(this.div, .3, [{ prop: "left", to: 0 },
                                { prop: "top", to: 0 },
                                { prop: "width", to: "100%" },
                                { prop: "height", to: "100%" },
                                { prop: "scaleZ", to: 1 }], { call: function () {
                                    _this.div.style.height = "";
                                    _this.div.style.width = "";
                                    _this.div.style.left = "";
                                    _this.div.style.top = "";
                                    _this.div.style.position = "";
                                    _this.div.style.transform = "";
                                } });
                        }
                    };
                    ImageExplorerModal.prototype.controlsHandler = function (e) {
                        var type = e.target.getAttribute("data-id");
                        if (!type)
                            return;
                        switch (type) {
                            case "zoom-in":
                                this.scale += 20;
                                this.setZoom();
                                break;
                            case "zoom-out":
                                this.scale -= 20;
                                this.setZoom();
                                break;
                        }
                    };
                    ImageExplorerModal.prototype.setZoom = function () {
                        if (this.scale > 400) {
                            this.scale = 400;
                        }
                        if (this.scale < 40) {
                            this.scale = 40;
                        }
                        this.elMediaHolder.style.minWidth = this.scale + "%";
                        this.elMediaHolder.style.maxWidth = this.scale + "%";
                    };
                    ImageExplorerModal.prototype.dragStart = function (e) {
                        this.rect = this.elMediaHolder.getBoundingClientRect();
                        this.divRect = this.div.getBoundingClientRect();
                        this.diffX = e.clientX - this.elMediaHolder.offsetLeft + this.divRect.width - this.rect.width;
                        this.diffY = e.clientY - this.elMediaHolder.offsetTop + this.divRect.height - this.rect.height;
                        this.draggable.addEventListener("mousemove", this.dragProgress);
                        e.preventDefault();
                    };
                    ImageExplorerModal.prototype.dragProgress = function (e) {
                        var x = e.clientX - this.diffX + this.rect.left;
                        var y = e.clientY - this.diffY + this.rect.top;
                        this.elMediaHolder.style.marginLeft = x + "px";
                        this.elMediaHolder.style.marginTop = y + "px";
                        e.preventDefault();
                    };
                    ImageExplorerModal.prototype.dragStop = function (e) {
                        this.draggable.removeEventListener("mousemove", this.dragProgress);
                        e.preventDefault();
                    };
                    ImageExplorerModal.prototype.touchStart = function (e) {
                        var el = e.target;
                        if (!el.classList.contains("hspot") && this.hasHotspots) {
                            this.hideDescription();
                        }
                        this.progressCount = 0;
                        if (!el.classList.contains("touchable"))
                            return;
                        this.isScaling = false;
                        this.rect = this.elMediaHolder.getBoundingClientRect();
                        this.divRect = this.div.getBoundingClientRect();
                        this.diffX = e.touches[0].clientX - this.elMediaHolder.offsetLeft + this.divRect.width - this.rect.width;
                        this.diffY = e.touches[0].clientY - this.elMediaHolder.offsetTop + this.divRect.height - this.rect.height;
                        e.preventDefault();
                    };
                    ImageExplorerModal.prototype.touchProgress = function (e) {
                        if (e.touches.length === 1 && !this.isScaling) {
                            var x = e.touches[0].clientX - this.diffX + this.rect.left;
                            var y = e.touches[0].clientY - this.diffY + this.rect.top;
                            this.elMediaHolder.style.marginLeft = x + "px";
                            this.elMediaHolder.style.marginTop = y + "px";
                        }
                        else if (e.touches.length === 2) {
                            this.isScaling = true;
                            var dist = ImageExplorerModal.getDist(e.touches[0], e.touches[1]);
                            if (dist - this.dist0 > 0) {
                                this.scale += 5;
                            }
                            else {
                                this.scale -= 5;
                            }
                            if (this.progressCount > 3)
                                this.setZoom();
                            this.dist0 = ImageExplorerModal.getDist(e.touches[0], e.touches[1]);
                            this.progressCount++;
                        }
                        e.preventDefault();
                    };
                    ImageExplorerModal.getDist = function (touch1, touch2) {
                        return Math.sqrt((touch1.clientX - touch2.clientX) * (touch1.clientX - touch2.clientX) + (touch1.clientY - touch2.clientY) * (touch1.clientY - touch2.clientY));
                    };
                    ImageExplorerModal.prototype.hotspotClickHandler = function (e) {
                        var descr = e.target.getAttribute("data-s-description");
                        if (!descr) {
                            this.hideDescription();
                            return;
                        }
                        this.hideDescription();
                        e.target.classList.add("selected");
                        this.elDescription.textContent = descr;
                        this.elDescription.classList.add("selected");
                        e.preventDefault();
                    };
                    ImageExplorerModal.prototype.hideDescription = function () {
                        var sel = this.elHotspots.querySelector(".selected");
                        if (sel) {
                            sel.classList.remove("selected");
                            this.elDescription.classList.remove("selected");
                        }
                    };
                    return ImageExplorerModal;
                }(vt.view.components.modal.FullScreenModal));
                modal.ImageExplorerModal = ImageExplorerModal;
            })(modal = components.modal || (components.modal = {}));
        })(components = view.components || (view.components = {}));
    })(view = vt.view || (vt.view = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var view;
    (function (view) {
        var components;
        (function (components) {
            var modal;
            (function (modal) {
                var InfoModal = (function (_super) {
                    __extends(InfoModal, _super);
                    function InfoModal() {
                        return _super !== null && _super.apply(this, arguments) || this;
                    }
                    InfoModal.prototype.init = function () {
                        this.div = document.createElement("div");
                        this.div.classList.add("info-modal");
                        this.audioDiv = document.createElement("audio");
                        this.audioDiv.style.visibility = "none";
                        var body = "<span class=\"vti_bulb\"></span>\n            <div><h5>" + this.vo.title + "</h5>" + this.parseBody(this.vo.body);
                        var img = this.vo.image ? "<img src=\"" + this.vo.image + "\">" : "";
                        this.div.innerHTML = "\n            <div class=\"info-modal_body\">\n            " + body + "\n            </div></div>";
                        this.loadMedia();
                    };
                    InfoModal.prototype.destroy = function (callback) {
                        if (callback === void 0) { callback = null; }
                        this.vo = null;
                        for (var i = 0; i < this._mediaList.length; i++) {
                            this._mediaList[i].stop();
                            this._mediaList[i].unload();
                        }
                        if (this.audioDiv.src !== "") {
                            this.audioDiv.pause();
                        }
                        T._(this.div, .3, [{ prop: "translateY", to: "-100%" }])
                            ._(callback);
                    };
                    return InfoModal;
                }(vt.view.components.modal.AbstractModal));
                modal.InfoModal = InfoModal;
            })(modal = components.modal || (components.modal = {}));
        })(components = view.components || (view.components = {}));
    })(view = vt.view || (vt.view = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var view;
    (function (view) {
        var components;
        (function (components) {
            var modal;
            (function (modal) {
                var UserEvent = vt.events.UserEvent;
                var LoginModal = (function (_super) {
                    __extends(LoginModal, _super);
                    function LoginModal() {
                        return _super !== null && _super.apply(this, arguments) || this;
                    }
                    LoginModal.prototype.destroy = function (callback) {
                        this.btn.removeEventListener("click", this.login);
                        window.removeEventListener("keydown", this.keyDown);
                        _super.prototype.destroy.call(this, callback);
                    };
                    LoginModal.prototype.init = function () {
                        _super.prototype.init.call(this);
                        var data = this.vo;
                        var body = data.message ? "<p>" + data.message + "</p>" : "";
                        var type;
                        for (var i = 0; i < data.fields.length; i++) {
                            var field = data.fields[i];
                            body += "<div class=\"login-field\"><span>" + field.label + "</span><input type=\"" + field.type + "\" name=\"" + field.id + "\"></div>";
                        }
                        this.div.innerHTML = "<div class=\"modal_header\"><span>" + this.vo.title + "</span></div>\n            <div class=\"modal_body\">\n            " + body + "\n            <p></p>\n            <button class=\"btn-secondary\" data-id=\"login-btn\">" + this.vo.title + "</button>\n            </div>";
                        this.login = this.login.bind(this);
                        this.keyDown = this.keyDown
                            .bind(this);
                        this.btn = this.div.querySelector("button[data-id='login-btn']");
                        this.btn.addEventListener("click", this.login);
                        window.addEventListener("keydown", this.keyDown);
                    };
                    LoginModal.prototype.keyDown = function (e) {
                        if (e.key === "Enter") {
                            this.login();
                        }
                    };
                    LoginModal.prototype.login = function () {
                        var fields = this.div.querySelectorAll("input");
                        var arr = [];
                        for (var i = 0; i < fields.length; i++) {
                            var f = fields[i];
                            arr.push({ name: f.name, value: f.value });
                        }
                        vt.VT5.getInstance().dispatchUserEvent(new UserEvent(UserEvent.ACTION_LOGIN, arr));
                    };
                    return LoginModal;
                }(vt.view.components.modal.AbstractModal));
                modal.LoginModal = LoginModal;
            })(modal = components.modal || (components.modal = {}));
        })(components = view.components || (view.components = {}));
    })(view = vt.view || (vt.view = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var view;
    (function (view) {
        var components;
        (function (components) {
            var modal;
            (function (modal) {
                var PDFModal = (function (_super) {
                    __extends(PDFModal, _super);
                    function PDFModal() {
                        var _this = _super !== null && _super.apply(this, arguments) || this;
                        _this.totalPages = 0;
                        _this.scale = 1;
                        return _this;
                    }
                    PDFModal.prototype.init = function () {
                        _super.prototype.init.call(this);
                        this.div.classList.add("modal_full-screen");
                        this.div.classList.add("pdf");
                        this.div.innerHTML = "\n            <div class=\"modal_controls\"><span class=\"modal_full-screen--title\">" + this.vo.title + "</span>\n                <span class=\"modal_control page-nums\"><span class=\"pageCounter\"></span><input type=\"number\" data-id=\"currentPage\" step=\"1\"> / <span data-id=\"totalPages\">2</span></span>\n                <button class=\"vti_arrow-left\" data-id=\"prev\"></button>\n                <button class=\"vti_arrow-right\" data-id=\"next\"></button>\n                <button class=\"vti_zoom-out\" data-id=\"zoomOut\"></button>\n                <button class=\"vti_zoom-in\" data-id=\"zoomIn\"></button>\n                <a href=\"" + this.vo["src"] + "\" target=\"_blank\" class=\"vti_save\" data-id=\"save\"></a>\n                <button class=\"vti_close\" data-event=\"UserEvent.cancel\"></button>\n            </div>\n            <div class=\"modal_full-screen--body\">\n                <canvas></canvas>\n            </div>";
                        this.controls = this.div.querySelector(".modal_controls");
                        this.canvas = this.div.querySelector("canvas");
                        this.context = this.canvas.getContext('2d');
                        this.pageNum = this.vo["page"] ? this.vo["page"] : 1;
                        this.load();
                    };
                    PDFModal.prototype.load = function () {
                        var _this = this;
                        this.renderPage = this.renderPage.bind(this);
                        this.controlsHandler = this.controlsHandler.bind(this);
                        this.pageChangeHandler = this.pageChangeHandler.bind(this);
                        this.src = "" + this.vo["src"];
                        this.div.classList.add("pdf-holder");
                        this.div.classList.remove("media");
                        var str = "";
                        this.elTotalPages = this.controls.querySelector("span[data-id='totalPages']");
                        this.elCurrentPage = this.controls.querySelector("input[data-id='currentPage']");
                        this.elPageCounter = this.controls.querySelector(".pageCounter");
                        this.elCurrentPage.min = "1";
                        this.controls.addEventListener("click", this.controlsHandler);
                        this.elCurrentPage.addEventListener("change", this.pageChangeHandler);
                        var self = this;
                        requirejs(["pdfjs-dist/build/pdf"], function (lib) {
                            _this.PDFJS = lib.PDFJS;
                            _this.PDFJS.getDocument(_this.src).then(function (pdf) {
                                self.pdf = pdf;
                                self.totalPages = pdf.numPages;
                                self.elTotalPages.textContent = self.totalPages + "";
                                self.elCurrentPage.max = self.totalPages + "";
                                self.renderPage();
                            });
                        });
                    };
                    PDFModal.prototype.renderPage = function () {
                        var self = this;
                        this.elCurrentPage.value = this.pageNum + "";
                        this.elPageCounter.textContent = this.pageNum + "";
                        self.pdf.getPage(self.pageNum).then(function (page) {
                            var viewport = page.getViewport(self.scale);
                            self.canvas.width = viewport.width;
                            self.canvas.height = viewport.height;
                            self.canvas.style.width = viewport.width + "px";
                            self.canvas.style.height = viewport.height + "px";
                            var renderContext = {
                                canvasContext: self.context,
                                viewport: viewport
                            };
                            page.render(renderContext);
                            var promise = page.getAnnotations().then(function (annotationsData) {
                                for (var i = 0; i < annotationsData.length; i++) {
                                    var obj = annotationsData[i];
                                }
                            });
                        });
                    };
                    PDFModal.prototype.controlsHandler = function (e) {
                        var id = e.target.getAttribute("data-id");
                        if (!this.pdf) {
                            return;
                        }
                        switch (id) {
                            case "next":
                                if (this.pageNum < this.totalPages) {
                                    this.pageNum++;
                                    this.div.scrollTop = 0;
                                    this.renderPage();
                                }
                                break;
                            case "prev":
                                if (this.pageNum > 1) {
                                    this.pageNum--;
                                    this.div.scrollTop = 0;
                                    this.renderPage();
                                }
                                break;
                            case "currentPage":
                                break;
                            case "zoomIn":
                                var p = this.pdf.getPage(this.pageNum);
                                if (this.scale < 3) {
                                    this.scale += .25;
                                }
                                this.renderPage();
                                break;
                            case "zoomOut":
                                var p = this.pdf.getPage(this.pageNum);
                                if (this.scale > .5) {
                                    this.scale -= .25;
                                }
                                this.renderPage();
                                break;
                        }
                    };
                    PDFModal.prototype.pageChangeHandler = function () {
                        var val = parseInt(this.elCurrentPage.value);
                        if (val > this.totalPages) {
                            val = this.totalPages;
                        }
                        else if (val < 1) {
                            val = 1;
                        }
                        this.pageNum = val;
                        this.renderPage();
                    };
                    PDFModal.prototype.unload = function () {
                        requirejs.undef("libs/pdf/pdfjs_min");
                        if (this.controls) {
                            this.controls.removeEventListener("click", this.controlsHandler);
                            this.elCurrentPage.removeEventListener("change", this.pageChangeHandler);
                        }
                        if (this.elCurrentPage) {
                            this.elCurrentPage.removeEventListener("change", this.controlsHandler);
                        }
                        this.div = null;
                    };
                    return PDFModal;
                }(vt.view.components.modal.AbstractModal));
                modal.PDFModal = PDFModal;
            })(modal = components.modal || (components.modal = {}));
        })(components = view.components || (view.components = {}));
    })(view = vt.view || (vt.view = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var view;
    (function (view) {
        var components;
        (function (components) {
            var modal;
            (function (modal) {
                var SystemErrorModal = (function (_super) {
                    __extends(SystemErrorModal, _super);
                    function SystemErrorModal() {
                        return _super !== null && _super.apply(this, arguments) || this;
                    }
                    SystemErrorModal.prototype.init = function () {
                        this._isCancelable = false;
                        _super.prototype.init.call(this);
                        var body = "<h3>" + (this.vo.title || "") + "</h3>" + this.parseBody(this.vo.body);
                        var img = this.vo.image ? "<img src=\"" + this.vo.image + "\">" : "";
                        this.div.innerHTML = "<div class=\"modal_header error\" style=\" min-width: 10rem;\"><span class=\"vti_warning\" style=\"color: #ffffff; font-size: 2rem;\"></span></div>\n            <div class=\"modal_body\">\n            " + img + "\n            " + body + "\n            </div>";
                        this.loadMedia();
                    };
                    return SystemErrorModal;
                }(modal.HintModal));
                modal.SystemErrorModal = SystemErrorModal;
            })(modal = components.modal || (components.modal = {}));
        })(components = view.components || (view.components = {}));
    })(view = vt.view || (vt.view = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var services;
    (function (services) {
        var AppEvent = vt.events.AppEvent;
        var UserEvent = vt.events.UserEvent;
        var AppService = (function (_super) {
            __extends(AppService, _super);
            function AppService(env, lang) {
                var _this = _super.call(this) || this;
                _this.env = env;
                _this.lang = lang;
                _this.api = "/api";
                _this.preferredLanguageStr = "_VT5_pref_lang";
                AppService._instance = _this;
                _this.servicePath = "../js/services/" + env;
                _this.themeLoadedHandler = _this.themeLoadedHandler.bind(_this);
                _this.requestLoginHandler = _this.requestLoginHandler.bind(_this);
                AppService.sessionExpiredHandler = AppService.sessionExpiredHandler.bind(_this);
                _this.localStorageAvailable = AppService.checkStorageAvailable();
                return _this;
            }
            AppService.getInstance = function () {
                if (!AppService._instance) {
                    throw ("AppService has not been initialized!");
                }
                return AppService._instance;
            };
            AppService.prototype.getLanguageList = function () {
                return this.languageList;
            };
            AppService.prototype.setPreferredLanguage = function (lang) {
                if (this.localStorageAvailable) {
                    window.localStorage.setItem(this.preferredLanguageStr, lang);
                }
            };
            AppService.prototype.getPreferredLanguage = function () {
                var lang = "data_en-gb";
                if (this.localStorageAvailable) {
                    var tmpLang = window.localStorage.getItem(this.preferredLanguageStr);
                    if (tmpLang) {
                        lang = tmpLang;
                    }
                }
                return lang;
            };
            AppService.prototype.loadJSData = function () {
                var _this = this;
                requirejs(["../data/language_list"], function (data) {
                    _this.languageList = data.languages;
                    _this.loadData(false);
                }, function (err) {
                    console.log("1. Error:", err);
                    _this.languageList = [{
                            title: "English",
                            file: "data_en-gb"
                        }];
                    _this.loadData(true);
                });
            };
            AppService.prototype.loadData = function (isError) {
                var _this = this;
                if (isError === void 0) { isError = false; }
                var preferredLang = "data_en-gb";
                if (!isError && this.localStorageAvailable) {
                    preferredLang = window.localStorage.getItem(this.preferredLanguageStr) || this.languageList[0].file;
                }
                requirejs(["../data/" + preferredLang, this.servicePath], function (data, service) {
                    _this.service = service;
                    _this.service.courseId = data.product.courseCode;
                    _this.service.loginSuccessCallback = AppService.loginSuccessHandler;
                    _this.service.loginFailCallback = AppService.loginFailHandler;
                    _this.service.serviceReadyCallback = AppService.guestServiceReadyHandler;
                    _this.service.sessionExpiredCallback = AppService.sessionExpiredHandler;
                    _this.service.requestLoginCallback = _this.requestLoginHandler;
                    _this.service.init();
                    window.addEventListener("unload", function () { return _this.exit(); });
                    if (data.addon) {
                        if (data.addon.css) {
                            var css = document.createElement("link");
                            css.rel = "stylesheet";
                            css.href = data.addon.css;
                            document.head.appendChild(css);
                        }
                    }
                    _this.dispatch(new AppEvent(AppEvent.APP_DATA_LOADED, data));
                    requirejs.undef("../data/language_list");
                    requirejs.undef("../data/" + preferredLang);
                    _this.checkServiceValid();
                }, function (err) {
                    _this.loadData(true);
                });
            };
            AppService.prototype.getUserName = function () {
                return this.service.getUserName().toUpperCase();
            };
            AppService.prototype.getTrainingId = function () {
                return this.service.getTrainingId();
            };
            AppService.prototype.loadTheme = function (type) {
                var current = document.querySelector("[id*=\"theme-\"]").id;
                if (current.indexOf(type) > -1) {
                    return;
                }
                this.newTheme = document.createElement("link");
                this.newTheme.setAttribute("rel", "stylesheet");
                this.newTheme.setAttribute("type", "text/css");
                this.newTheme.addEventListener("load", this.themeLoadedHandler);
                if (type === "dark") {
                    this.currentTheme = document.getElementById("theme-light");
                    this.newTheme.setAttribute("id", "theme-dark");
                    this.newTheme.setAttribute("href", "res/css/theme-dark.css");
                }
                else {
                    this.currentTheme = document.getElementById("theme-dark");
                    this.newTheme.setAttribute("id", "theme-light");
                    this.newTheme.setAttribute("href", "res/css/theme-light.css");
                }
                document.head.appendChild(this.newTheme);
            };
            AppService.prototype.themeLoadedHandler = function () {
                document.head.removeChild(this.currentTheme);
                this.newTheme.removeEventListener("load", this.themeLoadedHandler);
            };
            AppService.prototype.sendScore = function (score) {
                this.service.sendTestResults(score);
            };
            AppService.prototype.exit = function () {
                this.service.onExit();
            };
            AppService.prototype.checkServiceValid = function () {
                var props = { init: 1, setMinPassScore: 1, sendTestResults: 1, sendVar: 1, onExit: 1, requiresAuth: 1, receiveVar: 1, getUserName: 1, getTrainingId: 1 };
                for (var p in props) {
                    if (!this.service.hasOwnProperty(p)) {
                        vt.VT5.getInstance().dispatchUserEvent(new UserEvent(UserEvent.ACTION_SHOW_MODAL, {
                            type: "system-error",
                            title: "ERROR",
                            body: [
                                {
                                    type: "text",
                                    text: "Service " + this.servicePath + " does not implement method <span class=\"bold\">" + p + "()</span>."
                                }
                            ]
                        }));
                    }
                }
            };
            AppService.prototype.setLocalData = function (courseId, value) {
                if (this.localStorageAvailable) {
                    window.localStorage.setItem(courseId, JSON.stringify(value));
                }
            };
            AppService.prototype.getLocalData = function (courseId) {
                if (this.localStorageAvailable) {
                    var ls = window.localStorage.getItem(courseId);
                    if (ls) {
                        return JSON.parse(ls);
                    }
                }
                return null;
            };
            AppService.checkStorageAvailable = function () {
                try {
                    var storage = window.localStorage, x = '__storage_test__';
                    storage.setItem(x, x);
                    storage.removeItem(x);
                    return true;
                }
                catch (e) {
                    console.log("localStorage is unavailable.");
                    return false;
                }
            };
            AppService.prototype.requiresAuth = function () {
                return this.service.requiresAuth();
            };
            AppService.prototype.getAuthVO = function () {
                return this.service.getAuthFields();
            };
            AppService.prototype.sendLoginRequest = function (data) {
                this.service.login(data, null);
            };
            AppService.guestServiceReadyHandler = function () {
                setTimeout(function () {
                    vt.VT5.getInstance().dispatchUserEvent(new UserEvent(UserEvent.ACTION_SERVICE_READY));
                }, 200);
            };
            AppService.loginSuccessHandler = function (message) {
                vt.VT5.getInstance().dispatchUserEvent(new UserEvent(UserEvent.ACTION_LOGIN_SUCCESS));
            };
            AppService.loginFailHandler = function (message) {
                vt.VT5.getInstance().dispatchUserEvent(new UserEvent(UserEvent.ACTION_LOGIN_FAIL));
            };
            AppService.sessionExpiredHandler = function (message) {
                vt.VT5.getInstance().dispatchUserEvent(new UserEvent(UserEvent.ACTION_SHOW_MODAL, {
                    type: "system-error",
                    title: "ERROR",
                    body: [{ type: "text", text: message || "Your session has expired." }]
                }));
            };
            AppService.prototype.requestLoginHandler = function (message) {
                var vo = this.getAuthVO();
                vo.message = message;
                vt.VT5.getInstance().dispatchUserEvent(new UserEvent(UserEvent.ACTION_SHOW_MODAL, vo));
            };
            return AppService;
        }(vt.events.Dispatcher));
        services.AppService = AppService;
        var Enforcer = (function () {
            function Enforcer() {
            }
            return Enforcer;
        }());
    })(services = vt.services || (vt.services = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var view;
    (function (view) {
        var components;
        (function (components) {
            var modal;
            (function (modal) {
                var AppService = vt.services.AppService;
                var AppModel = vt.model.AppModel;
                var WelcomeBackModal = (function (_super) {
                    __extends(WelcomeBackModal, _super);
                    function WelcomeBackModal() {
                        return _super !== null && _super.apply(this, arguments) || this;
                    }
                    WelcomeBackModal.prototype.init = function () {
                        _super.prototype.init.call(this);
                        var courseId = AppModel.getInstance().getProductData().courseCode;
                        var unixDate = AppService.getInstance().getLocalData(courseId).date;
                        var date = new Date(unixDate);
                        var year = date.getFullYear();
                        var month = date.getMonth() + 1;
                        var monthStr = month > 9 ? month : "0" + month;
                        var day = date.getDate();
                        var dayStr = day > 9 ? day : "0" + day;
                        var dateStr = dayStr + "/" + monthStr + "/" + year;
                        var body = this.parseBody(this.vo.body);
                        body = body.replace("{{date}}", dateStr);
                        var buttons = this.getButtonsStr();
                        this.div.innerHTML = "<div class=\"modal_header\"><span></span><button class=\"vti_close\" data-event=\"UserEvent.cancel\"></button></div>\n            <div class=\"modal_body\">\n            <h3>" + this.vo.title + "</h3>\n            " + body + "\n            <div class=\"modal_buttons-row\">" + buttons + "</div>\n            </div>";
                    };
                    WelcomeBackModal.prototype.getButtonsStr = function () {
                        var buttons = "";
                        for (var i = 0; i < this.vo.buttons.length; i++) {
                            var item = this.vo.buttons[i];
                            buttons += vt.view.renderers.ButtonRenderer.render(item);
                        }
                        return buttons;
                    };
                    return WelcomeBackModal;
                }(vt.view.components.modal.AbstractModal));
                modal.WelcomeBackModal = WelcomeBackModal;
            })(modal = components.modal || (components.modal = {}));
        })(components = view.components || (view.components = {}));
    })(view = vt.view || (vt.view = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var view;
    (function (view) {
        var components;
        (function (components) {
            var modal;
            (function (modal) {
                var UserEvent = vt.events.UserEvent;
                var ModalManager = (function (_super) {
                    __extends(ModalManager, _super);
                    function ModalManager() {
                        var _this = _super.call(this) || this;
                        _this.bg = document.createElement("div");
                        _this.doDestroy = _this.doDestroy.bind(_this);
                        _this.keyHandler = _this.keyHandler.bind(_this);
                        return _this;
                    }
                    ModalManager.prototype.showModal = function (vo, state) {
                        if (state === void 0) { state = undefined; }
                        this.hideModal();
                        this.bg.classList.remove("info-modal_bg");
                        this.bg.classList.remove("modal_bg");
                        if (vo.type === "info") {
                            this.bg.classList.add("info-modal_bg");
                        }
                        else {
                            this.bg.classList.add("modal_bg");
                        }
                        this.blocksUI = true;
                        switch (vo.type) {
                            case "about":
                                this.modal = new modal.AboutModal(vo);
                                break;
                            case "character":
                                this.modal = new modal.CharacterModal(vo);
                                break;
                            case "dialog":
                                this.modal = new modal.DialogModal(vo);
                                break;
                            case "flip":
                                this.modal = new modal.FlipModal(vo);
                                break;
                            case "fullScreen":
                                this.modal = new modal.FullScreenModal(vo);
                                break;
                            case "PDF":
                                this.modal = new modal.PDFModal(vo);
                                break;
                            case "glossary":
                                this.modal = new modal.GlossaryModal(vo);
                                break;
                            case "help":
                                this.modal = new modal.HelpModal(vo);
                                break;
                            case "hint":
                                this.modal = new modal.HintModal(vo);
                                break;
                            case "system-error":
                                this.modal = new modal.SystemErrorModal(vo);
                                break;
                            case "info":
                                this.modal = new modal.InfoModal(vo);
                                this.blocksUI = false;
                                break;
                            case "imageExplorer":
                                this.modal = new modal.ImageExplorerModal(vo);
                                break;
                            case "login":
                                this.modal = new modal.LoginModal(vo);
                                break;
                            case "3d":
                                this.modal = new modal.GL3dModal(vo);
                                break;
                            case "welcomeBack":
                                this.modal = new modal.WelcomeBackModal(vo);
                                break;
                            default:
                                var mvo = {
                                    id: "MODAL____ERROR",
                                    kind: "info",
                                    title: "ERROR!",
                                    type: "info",
                                    body: [
                                        {
                                            type: "text",
                                            text: "<p>The type of Modal you specified (" + vo.type + ") does not exist.</p>"
                                        }
                                    ]
                                };
                                this.modal = new modal.HintModal(mvo);
                        }
                        if (vo.background) {
                        }
                        this.bg.appendChild(this.modal.getHtml());
                        document.body.appendChild(this.bg);
                        window.addEventListener("keydown", this.keyHandler);
                        var buttons = this.modal.getHtml().querySelectorAll("button");
                        for (var i = 0; i < buttons.length; i++) {
                            if (buttons[i]["getAttribute"]("data-focus")) {
                                buttons[i]["focus"]();
                            }
                        }
                    };
                    ModalManager.prototype.playSound = function () {
                        this.modal.playSound();
                    };
                    ModalManager.prototype.hideModal = function () {
                        if (this.modal) {
                            window.removeEventListener("keydown", this.keyHandler);
                            this.modal.destroy(this.doDestroy);
                        }
                    };
                    ModalManager.prototype.isBlockingUI = function () {
                        return this.blocksUI;
                    };
                    ModalManager.prototype.isCancelable = function () {
                        if (this.modal) {
                            return this.modal.isCancelable();
                        }
                    };
                    ModalManager.prototype.showIncorrect = function () {
                        if (this.modal) {
                            var m = this.modal.getHtml();
                            T._(m, .1, [{ prop: "translateX", to: 20 }]);
                            T._(m, .1, [{ prop: "translateX", to: -20 }], { delay: .1 });
                            T._(m, .1, [{ prop: "translateX", to: 10 }], { delay: .2 });
                            T._(m, .1, [{ prop: "translateX", to: 0 }], { delay: .3 });
                        }
                    };
                    ModalManager.prototype.keyHandler = function (e) {
                        if (e.keyCode === 27) {
                            if (this.modal && this.modal.type !== "login") {
                                ModalManager.cancel();
                            }
                        }
                    };
                    ModalManager.cancel = function () {
                        vt.VT5.getInstance().dispatch(new UserEvent(UserEvent.ACTION_CANCEL_MODAL));
                    };
                    ModalManager.prototype.doDestroy = function () {
                        if (this.modal) {
                            document.body.removeChild(this.bg);
                            this.bg.removeChild(this.modal.getHtml());
                            this.modal = null;
                        }
                    };
                    return ModalManager;
                }(vt.events.Dispatcher));
                modal.ModalManager = ModalManager;
            })(modal = components.modal || (components.modal = {}));
        })(components = view.components || (view.components = {}));
    })(view = vt.view || (vt.view = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var controllers;
    (function (controllers) {
        var NavController = vt.controllers.NavController;
        var NavEvent = vt.events.NavEvent;
        var UserEvent = vt.events.UserEvent;
        var Footer = vt.view.nav.Footer;
        var AppController = (function () {
            function AppController() {
                this.currentIndex = 0;
                this.isModalOn = false;
                this.isNavBlocked = false;
                this.init();
            }
            AppController.prototype.init = function () {
                var _this = this;
                this.appModel = vt.model.AppModel.getInstance();
                this.totalPages = this.appModel.getTotalPages();
                this.pagesView = vt.VT5.getInstance().getView("PagesView");
                this.pagesView.addListener(UserEvent.USER_EVENT, function (e) { return _this.userEventHandler(e); });
                this.footerView = vt.VT5.getInstance().getView("Footer");
                this.modalManager = new vt.view.components.modal.ModalManager();
                this.pageChangeHandler = this.pageChangeHandler.bind(this);
                this.navController = new NavController();
                this.navController.addListener(NavEvent.NEXT, this.pageChangeHandler);
                this.navController.addListener(NavEvent.PREV, this.pageChangeHandler);
                this.navController.addListener(NavEvent.PAGE_CHANGE, this.pageChangeHandler);
                this.navController.addListener(NavEvent.GOTO_CID, this.pageChangeHandler);
                this.soundController = vt.controllers.SoundController.getInstance();
                this.appService = vt.services.AppService.getInstance();
                vt.VT5.getInstance().addListener(vt.events.UserEvent.USER_EVENT, function (e) { return _this.userEventHandler(e); });
                this.testController = vt.controllers.TestController.getInstance();
                if (NavController.getParam("devMode") === "1") {
                    this.appModel.setDevMode(true);
                    document.getElementById("devMode").classList.remove("hidden");
                    document.body.classList.add("dev-mode");
                }
                this.courseId = this.appModel.getProductData().courseCode;
                this.localData = this.appService.getLocalData(this.courseId);
                window.onbeforeunload = function () {
                    _this.localData.date = Date.now();
                    _this.appService.setLocalData(_this.courseId, _this.localData);
                };
                if (this.localData) {
                    this.setTheme(this.localData.theme);
                }
                else {
                    this.localData = {
                        theme: "light",
                        currentPage: "1"
                    };
                }
            };
            AppController.prototype.runStartup = function () {
                this.currentIndex = this.getFirstPageIndex();
                if (this.appService.requiresAuth()) {
                    this.modalManager.showModal(this.appService.getAuthVO());
                    this.isModalOn = true;
                    AppController.hidePreloader();
                }
                else {
                    if (this.shouldShowWelcomeModal()) {
                        this.currentIndex = 0;
                        this.loadFirstPage();
                        this.showWelcomeModal();
                    }
                    else {
                        this.loadFirstPage();
                    }
                    AppController.hidePreloader();
                }
            };
            AppController.hidePreloader = function () {
                var preloaderEl = document.querySelector(".preloader_screen");
                T._(preloaderEl, .3, [{ prop: "scaleY", to: 0 }], { delay: 1, call: function () { preloaderEl.parentNode.removeChild(preloaderEl); } });
            };
            AppController.prototype.getFirstPageIndex = function () {
                var contItem;
                var index = 0;
                if (this.localData) {
                    contItem = this.appModel.getContentItemById(this.localData.currentPage);
                    if (contItem) {
                        index = contItem.position;
                    }
                }
                var hash = NavController.getHash();
                if (hash && hash !== "1") {
                    contItem = this.appModel.getContentItemById(hash);
                    if (contItem) {
                        index = contItem.position;
                    }
                    else {
                        index = 0;
                    }
                }
                else {
                    index = 0;
                }
                if (index === undefined) {
                    index = 0;
                }
                return index;
            };
            AppController.prototype.shouldShowWelcomeModal = function () {
                if (this.localData && Date.now() - this.localData.date > 2 * 86400000) {
                    var contItem = this.appModel.getContentItemById(this.localData.currentPage);
                    if (contItem && contItem.position > 0) {
                        return true;
                    }
                }
                return false;
            };
            AppController.prototype.showWelcomeModal = function () {
                setTimeout(function () {
                    vt.VT5.getInstance().dispatchUserEvent(new UserEvent(UserEvent.ACTION_SHOW_MODAL, "system-welcome-back"));
                }, 1000);
            };
            AppController.prototype.loadFirstPage = function () {
                var contItem = this.appModel.getContentItemByIndex(this.currentIndex);
                if (contItem) {
                    this.currentParent = this.appModel.getContentItemById(contItem.parentId);
                    if (this.currentParent && this.currentParent.tunnel && !this.appModel.isDevMode()) {
                        this.currentIndex = this.currentParent.content[0].position;
                    }
                }
                if (this.currentParent && this.currentParent.type === "test") {
                    this.testController.startTest(this.currentParent);
                }
                var item = this.appModel.getContentItemByIndex(this.currentIndex);
                this.pagesView.showFirstPage(item);
                this.updatePageCountDisplay();
                this.navController.setMainNavSelection(item.id);
                this.soundController.enableMediaControls(item.hasAudio);
                if (this.isModalOn) {
                    this.soundController.pauseAudio();
                    this.pagesView.pauseMedia();
                }
            };
            AppController.prototype.pageChangeHandler = function (e, allowPass) {
                if (allowPass === void 0) { allowPass = false; }
                if (this.isNavBlocked || (this.isModalOn && this.modalManager.isBlockingUI())) {
                    return;
                }
                else if (this.isModalOn && !this.modalManager.isBlockingUI()) {
                    this.modalManager.hideModal();
                }
                this.navController.hideMainNav();
                var newItem;
                switch (e.type) {
                    case NavEvent.NEXT:
                        if (this.currentIndex < this.totalPages - 1) {
                            newItem = this.appModel.getContentItemByIndex(this.currentIndex + 1);
                        }
                        break;
                    case NavEvent.PREV:
                        if (this.currentIndex > 0) {
                            newItem = this.appModel.getContentItemByIndex(this.currentIndex - 1);
                            if (newItem.type === "testResult") {
                                var prnt = this.appModel.getContentItemById(newItem.parentId);
                                newItem = prnt.content[0];
                            }
                        }
                        break;
                    case NavEvent.PAGE_CHANGE:
                        newItem = this.appModel.getContentItemById(e.data);
                        if (!newItem) {
                            newItem = this.appModel.getContentItemByIndex(0);
                        }
                        else if (!newItem.body) {
                            newItem = this.appModel.getContentItemById(newItem.content[0].id);
                        }
                        break;
                    case NavEvent.GOTO_CID:
                        var target = this.appModel.getContentItemByCID(e.data).id;
                        if (target) {
                            this.pageChangeHandler(new NavEvent(NavEvent.PAGE_CHANGE, target));
                        }
                        break;
                }
                if (!newItem) {
                    return;
                }
                this.navController.setMainNavSelection(newItem.id);
                var pId = this.appModel.getContentItemByIndex(this.currentIndex).parentId;
                this.currentParent = this.appModel.getContentItemById(pId);
                var newParent = this.appModel.getContentItemById(newItem.parentId);
                if (this.currentParent && this.currentParent.type === "test" && this.currentParent === newParent) {
                    this.testController.startTest(this.currentParent);
                }
                else if (newItem["testMode"] === 1) {
                    this.testController.startTest(newParent);
                }
                if (!allowPass && this.currentParent && this.currentParent.tunnel) {
                    if (newParent === undefined || newParent.id !== this.currentParent.id) {
                        var modalVO = this.appModel.getPoolItem(this.currentParent["modalID"]);
                        this.modalManager.showModal(modalVO);
                        this.isModalOn = true;
                        this.pendingEvent = e;
                        return;
                    }
                }
                this.gotoNewPage(newItem);
                this.soundController.enableMediaControls(newItem.hasAudio);
                this.localData.currentPage = newItem.id;
            };
            AppController.prototype.gotoNewPage = function (item) {
                this.pageChange(item);
                this.updatePageCountDisplay();
            };
            AppController.prototype.next = function (ci) {
                this.pagesView.next(ci);
            };
            AppController.prototype.prev = function (ci) {
                this.pagesView.prev(ci);
            };
            AppController.prototype.pageChange = function (ci) {
                var tmpIndex = this.currentIndex;
                this.currentIndex = ci.position;
                if (tmpIndex < this.currentIndex) {
                    this.next(ci);
                }
                else if (tmpIndex > this.currentIndex) {
                    this.prev(ci);
                }
            };
            AppController.prototype.userEventHandler = function (e) {
                var _this = this;
                switch (e.actionType) {
                    case UserEvent.ACTION_START_TRAINING:
                        this.soundController.playAudio();
                        AppController.removePreloader();
                        this.modalManager.hideModal();
                        this.isModalOn = false;
                        break;
                    case UserEvent.ACTION_SUBMIT_TEST:
                        var res = this.testController.getTestResult();
                        if (res !== this.testResult) {
                            this.appModel.setTestResult(res);
                            this.appService.sendScore(res);
                            this.testController.setMode(vt.TestMode.Review);
                            this.testResult = res;
                        }
                        break;
                    case UserEvent.ACTION_REVIEW_TEST:
                        this.testController.setMode(vt.TestMode.Review);
                        this.pageChangeHandler(new NavEvent(NavEvent.PAGE_CHANGE, this.currentParent.content[1].id));
                        break;
                    case UserEvent.ACTION_CANCEL_MODAL:
                        if (this.modalManager.isCancelable()) {
                            this.modalManager.hideModal();
                            this.isModalOn = false;
                            if (this.pagesView.hasAudio()) {
                                if (!this.soundController.hasFinished()) {
                                    this.soundController.playAudio(.5);
                                }
                            }
                            setTimeout(function () { return _this.pagesView.resumeMedia(); }, 500);
                        }
                        break;
                    case UserEvent.ACTION_TEST_LEAVE:
                        this.modalManager.hideModal();
                        this.testController.clearTest();
                        this.isModalOn = false;
                        if (this.pendingEvent) {
                            this.pageChangeHandler(this.pendingEvent, true);
                            this.pendingEvent = null;
                        }
                        break;
                    case UserEvent.ACTION_SHOW_MODAL:
                        var modalVo;
                        if (e.data && e.data.hasOwnProperty("type")) {
                            modalVo = e.data;
                        }
                        else {
                            modalVo = this.appModel.getPoolItem(e.data);
                        }
                        this.soundController.pauseAudio();
                        this.modalManager.showModal(modalVo);
                        this.pagesView.pauseMedia();
                        this.isModalOn = true;
                        this.modalManager.playSound();
                        break;
                    case UserEvent.ACTION_NEXT:
                        var evt = new NavEvent(NavEvent.NEXT);
                        this.pageChangeHandler(evt);
                        break;
                    case UserEvent.ACTION_GOTO_CID:
                        var id = this.appModel.getContentItemByCID(e.data).id;
                        var evt = new NavEvent(NavEvent.PAGE_CHANGE, id);
                        this.pageChangeHandler(evt);
                        break;
                    case UserEvent.ACTION_SET_THEME_DARK:
                        this.setTheme("dark");
                        break;
                    case UserEvent.ACTION_SET_THEME_LIGHT:
                        this.setTheme("light");
                        break;
                    case UserEvent.ACTION_PLAY_AUDIO:
                        this.soundController.enableMediaControls(true);
                        this.soundController.playAudio(0, e.data);
                        break;
                    case UserEvent.ACTION_STOP_AUDIO:
                        this.soundController.enableMediaControls(false);
                        break;
                    case UserEvent.ACTION_PAUSE_MAIN_AUDIO:
                        this.soundController.pauseAudio();
                        break;
                    case UserEvent.ACTION_SET_LANGUAGE:
                        this.appService.setPreferredLanguage(e.data);
                        window.location.reload();
                        break;
                    case UserEvent.ACTION_RESUME_LAST_VISIT:
                        this.modalManager.hideModal();
                        this.isModalOn = false;
                        this.pageChangeHandler(new NavEvent(NavEvent.PAGE_CHANGE, this.localData.currentPage));
                        break;
                    case UserEvent.ACTION_LOGIN:
                        this.appService.sendLoginRequest(e.data);
                        break;
                    case UserEvent.ACTION_LOGIN_SUCCESS:
                        if (this.shouldShowWelcomeModal()) {
                            this.currentIndex = 0;
                            this.loadFirstPage();
                            vt.VT5.getInstance().dispatchUserEvent(new UserEvent(UserEvent.ACTION_CANCEL_MODAL));
                            this.showWelcomeModal();
                        }
                        else {
                            this.loadFirstPage();
                            vt.VT5.getInstance().dispatchUserEvent(new UserEvent(UserEvent.ACTION_CANCEL_MODAL));
                        }
                        break;
                    case UserEvent.ACTION_LOGIN_FAIL:
                        this.modalManager.showIncorrect();
                        break;
                    case UserEvent.ACTION_SERVICE_READY:
                        this.updateUser();
                        this.runStartup();
                        break;
                    case UserEvent.ACTION_SHOW_VIEWER:
                        var vo = {
                            type: "imageExplorer",
                            media: {
                                type: "media",
                                src: e.data
                            }
                        };
                        vt.VT5.getInstance().dispatchUserEvent(new UserEvent(UserEvent.ACTION_SHOW_MODAL, vo));
                        break;
                }
            };
            AppController.prototype.updateUser = function () {
                this.appModel.setUserName(this.appService.getUserName());
                this.appModel.setTrainingId(this.appService.getTrainingId());
                Footer.setUserName(this.appService.getUserName());
            };
            AppController.prototype.setTheme = function (value) {
                this.appService.loadTheme(value);
                this.appModel.setTheme(value);
                this.localData.theme = value;
                this.appService.setLocalData(this.courseId, this.localData);
            };
            AppController.prototype.updatePageCountDisplay = function () {
                var current = this.appModel.getContentItemByIndex(this.currentIndex);
                this.navController.setHash(current.id);
                window.location.hash = current.id;
                if (current.pagesInSection < 1 || current.type === "splash" || current.notCountable) {
                    this.footerView.setPageCount("&#8226; &#8226; &#8226;");
                }
                else {
                    this.footerView.setPageCount(current.pageIndex + " / " + current.pagesInSection);
                }
            };
            AppController.removePreloader = function () {
                var preloader = document.querySelector(".preloader_screen");
                if (preloader) {
                    T._(preloader, .5, [{ prop: "rotateX", to: 90 }], {
                        delay: .1,
                        call: function () { return document.body.removeChild(preloader); }
                    });
                }
            };
            return AppController;
        }());
        controllers.AppController = AppController;
    })(controllers = vt.controllers || (vt.controllers = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var AppEvent = vt.events.AppEvent;
    var VTServiceAdaptor = vt.services.AppService;
    var $ = function (id) {
        return document.getElementById(id);
    };
    var VT5 = (function (_super) {
        __extends(VT5, _super);
        function VT5(env, lang) {
            if (env === void 0) { env = "vt5"; }
            if (lang === void 0) { lang = "en_gb"; }
            var _this = _super.call(this) || this;
            _this.env = env;
            _this.lang = lang;
            _this.views = {};
            _this.init();
            VT5.instance = _this;
            return _this;
        }
        VT5.getInstance = function () {
            return VT5.instance;
        };
        VT5.prototype.getView = function (view) {
            return this.views[view];
        };
        VT5.prototype.dispatchUserEvent = function (e) {
            this.dispatch(e);
        };
        VT5.prototype.init = function () {
            var _this = this;
            this.appService = new VTServiceAdaptor(this.env, this.lang);
            this.appModel = vt.model.AppModel.getInstance();
            this.appService.addListener(AppEvent.APP_DATA_LOADED, function (e) {
                _this.appModel.setData(e.data);
                var ld = _this.appService.getLocalData(_this.appModel.getProductData().courseCode);
                if (ld) {
                    _this.appModel.setTheme(ld.theme);
                }
                _this.initializeComponents();
            });
            this.appService.loadJSData();
        };
        VT5.prototype.initializeComponents = function () {
            this.appModel.setUserName(this.appService.getUserName());
            this.appModel.setTrainingId(this.appService.getTrainingId());
            this.registerView(new vt.view.nav.MainNav(this.appModel.getData(), this.appModel.getUIData()));
            $("vt-main-nav").appendChild(this.getView("MainNav").getHTML());
            this.registerView(new vt.view.PagesView());
            this.registerView(new vt.view.nav.Footer({ userName: this.appModel.getUserName() }));
            this.appController = new vt.controllers.AppController();
        };
        VT5.prototype.registerView = function (view, name) {
            if (name === void 0) { name = null; }
            if (this.views[view.className] || this.views[name]) {
                throw new Error("view " + view.className + " is already registered.");
            }
            if (name) {
                this.views[name] = view;
            }
            else {
                this.views[view.className] = view;
            }
        };
        return VT5;
    }(vt.events.Dispatcher));
    vt.VT5 = VT5;
})(vt || (vt = {}));
