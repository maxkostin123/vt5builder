define({
    // product info
    product: {
        type: "cbt", // Course type. Other types: cobalt, interactive
        courseCode: "632",
        title: "VT5",
        appPath: "632/",
        dm: 102
    },
    //scorm info
    scorm: {
        id: "VT5_SCORM_TEST_3", // no illegal characters or spaces!
        passScore: 0, // minimum pass scrore
        title: "VT5 SCORM TEST 3"
    },
    //introLogo: "res/assets/media/images/sample_logo.png", // If present, custom logo will be displayed at loading screen instead of VT logo.
    // UI
    ui: {
        mainNav: {
            useCaps: false, //Nav titles will be converted to UPPER CASE.
            showAllPagesInNav: false, // If set to true, all pages will be accessible from the Menu. Leave as 'false'.
            soundControls: [

                {
                    type: "toggle",
                    cssClass: "btn-icon",
                    state1: {
                        type: "button",
                        event: "SoundEvent.pause",
                        icon: "vti_pause"
                    },
                    state2: {
                        type: "button",
                        event: "SoundEvent.play",
                        icon: "vti_play"
                    }
                },
                {
                    cssClass: "btn-icon",
                    type: "button",
                    event: "SoundEvent.replay",
                    icon: "vti_play-step-back"
                }
            ],

            menu: {
                main: "Main Menu",
                back: "Back",
                close: "Close Menu",
                search: "Search..."
            },

            toolButtons: [
                {
                    type: "tool",
                    label: "Help",
                    icon: "vti_help",
                    event: "UserEvent.showModal",
                    payload: "system-help",
                    show: true
                },
                {
                    type: "tool",
                    label: "About",
                    icon: "vti_info",
                    event: "UserEvent.showModal",
                    payload: "system-about",
                    show: true
                },
                {
                    type: "tool",
                    label: "Glossary",
                    icon: "vti_glossary",
                    event: "UserEvent.showModal",
                    payload: "glossary",
                    show: true
                },
                {
                    type: "tool",
                    label: "Feedback",
                    icon: "vti_feedback",
                    event: "UserEvent.feedback",
                    show: true
                },
                {
                    type: "settings",
                    label: "Settings",
                    icon: "vti_settings",
                    event: "UserEvent.showModal",
                    show: true,
                    payload: "system-settings",
                    theme: {
                        label: "Theme",
                        theme1: "Light",
                        theme2: "Dark"
                    },
                    language: "Language"

                }

            ]
        }
    },
    /// Data =========================================================================================================
    data: {
        content: [
            {
                type: "page",
                CID: "m0_0", // Content ID. This is used as a unique identifier to be used with hyper links. Optional.
                nav_item: true, // Optional. If set to true, the item will appear on main Menu if it's the 1st level.
                nav_icon: "vti_home", // Menu item can have an icon. Optional.
                nav_title: "Home", // Optional. If present, title will appear as menu item. Only relevant for top-level items.
                hideHeader: true, // Optional. If true, title 'breadcrumb' on this page will not appear.
                /*
                 Layout of the Page. The following are currently supported:
                 'splash' - for splash pages
                 'page-cols' (columns)
                 'none'
                 */
                layout: "splash",
                notCountable: true, // the page will not be counted in the Paginator and will not display page numbers.
                backgroundImage: "res/assets/media/images/home_splash.jpg", // background image
                backgroundOverlay: "rgba(0,0,0,.1), rgba(0,0,0,.4)", //background gradient overlay
                body: [ // Any element with property 'body' is treated as a Page. This is what will be rendered in the browser.

                    {
                        type: "group",
                        items: [
                            {
                                type: "text", // text element
                                text: "<h1 class='big-num'>1</h1>" +
                                "<h1>COURSE TITLE</h1>" +
                                "<h2>SPLASH PAGE SUB-HEADER</h2>"
                            }
                        ]
                    }
                ]
            },

            /* CONTENT ======================================================================== */
            {
                title: "Content",
                nav_title: "Content",
                content: [
                    {
                        type: "page",
                        title: "Page",
                        nav_title: "Page", // If present, will appear in 'bread crumbs' and in the menu, if it's a top-level item.
                        nav_item: true, // If true, will appear in the menu.
                        layout: "page-cols", // Page layout. Optional. Default is "page-cols".
                        body: [

                            {
                                type: "group",
                                items: [

                                    {
                                        type: "media",
                                        kind: "image",
                                        /**
                                         * Background is optional.
                                         * If set to "-1", image will have no background.
                                         * If set to a value "#FFCC00", image will be of that HEX value.
                                         */
                                        background: "#000000",
                                        src: 'res/assets/media/images/home_splash.jpg',
                                        hasViewer: true // If set to true, an icon for Image Viewer will be added.
                                    },
                                    {
                                        type: "legend",
                                        kind: "text",
                                        text: "Text legend. Used for short image descriptions."
                                    }

                                ]
                            },

                            {
                                type: "text",
                                text: "<p class='justified'>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy.</p>"
                            }

                        ]
                    }
                ]
            },



            {
                title: "Final Test",
                nav_title: "Final Test",
                nav_icon: "vti_test",
                type: "test",
                testMode: 0, //options: normal(0), tutorial(1), review(2)
                testName: "Final Test",
                certificateType: "final", // Type of certificate to use. Options: "final", "module".
                testId: "final", // "final" for final test, "1", "2", "3" etc for modules.
                showBreakdown: true, // if set to true, test result page will show a breakdown.
                testStart: true, // If set to true, will include the Test Start page.
                minScore: 10, // Minimum pass score.
                testEnd: true, // If set to true, will include the Test End page.
                testResult: true, // If set to true, will include the Test Result page.
                randomize: false, // If set to true, will randomize questions.
                tunnel: true, //If set to true, content will only be accessible from page 1 of this module.
                modalID: "testLeave", // Will show this Modal when user tries to leave the test.

                pools: [
                    {
                        poolID: "p1", // CID of test pool
                        items: [] // Question ids to include; if left empty, all questions will be included.
                    },
                    {
                        poolID: "p2", // CID of test pool
                        items: [] // Question ids to include; if left empty, all questions will be included.
                    }

                ]
            }


        ]
    },
    // Test Definition
    test: {
        testStart: {
            type: "page",
            title: "You are about to start the test",
            layout: "none",
            body: [
                {
                    type: "text",
                    text: "<p>You have now reached the end of this module.<br>" +
                    "You may now either run the self-assessment test, or go onto another module.<br>" +
                    "Thank you for using the module, we hope that it has been of value to you.</p>"
                }
            ]
        },
        testEnd: {
            type: "page",
            layout: "splash",
            backgroundImage: "res/assets/media/images/home_splash.jpg", // background image
            backgroundOverlay: "rgba(0,0,0,.1), rgba(0,0,0,.4)", //background overlay
            body: [
                {
                    type: "group",
                    items: [
                        {
                            type: "button",
                            label: "Submit Result",
                            event: "UserEvent.next",
                            cssClass: "btn-primary",
                            icon: "vti_chevron-right",
                            iconPosition: "right"
                        },
                        {
                            type: "text",
                            text: "<h2>YOU HAVE COMPLETED THE ASSESSMENT FOR MODULE M632.'</h2>"
                        }
                    ]
                }

            ]
        },
        testResult: {
            title: "Your Test Results:",
            onEnterEvent: "UserEvent.submitTest",
            layout: "page-test-result",
            certificateModuleSrc: "res/assets/media/common/certificate-module.png",
            certificateFinalSrc: "res/assets/media/common/certificate-final.png",
            body: [
                {
                    type: "group",
                    items: [
                        {
                            type: "text",
                            text: "<p>You can print your test-completion certificate by clicking the 'Print' button.<br>" +
                            "You can review your answers by clicking the 'Review' button.</p>"
                        },
                        {
                            type: "button",
                            label: "Review",
                            event: "UserEvent.reviewTest",
                            icon: "vti_test",
                            cssClass: "btn-primary"
                        },
                        {
                            type: "button",
                            label: "Save",
                            event: "UserEvent.save",
                            icon: "vti_save",
                            cssClass: "btn-primary"
                        }
                    ]
                }

            ],
            results: {
                name: "User Name",
                trainingId: "Training ID",
                score: "Score",
                scorePercent: "Score Percentage",
                duration: "Duration",
                passResponse: "You have passed the test!",
                failResponse: "You have failed the test.",
                breakdown: "Breakdown"
            }
        },
        response: {
            correct: "CORRECT",
            incorrect: "INCORRECT",
            notAnswered: "NOT ANSWERED",
            showCorrect: "CORRECT ANSWER",
            submitAnswer: "SUBMIT ANSWER"
        }
    },

    // Pools
    pools: [

        // pool for module 1 -------------------------
        {
            poolID: "p1", // Questions pool for Module 1
            poolName: "Section 1",
            content: [
                // Questions
                {
                    id: "p1-0", // Unique ID
                    kind: "select", // Question type
                    optionType: "radio", // Selection type ("radio" or "checkbox")
                    scorePoints: 1, // Score value
                    title: "True/False type",
                    layout: "none",
                    body: [
                        {
                            type: "question",
                            question: [
                                {
                                    description: "Is this statement true or false?", // optional. If present, will layout elements in a table-cell, description first.
                                    options: [
                                        {
                                            label: "True", // Label
                                            a: 0 // Answer (0=incorrect, 1=correct)
                                        },
                                        {
                                            label: "False",
                                            a: 1
                                        }
                                    ]
                                }
                            ]
                        }
                    ],
                    hint: {
                        title: "Hint",
                        type: "hint",
                        body: [
                            {
                                type: "text",
                                text: "<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>"
                            },
                            {
                                type: "media",
                                kind: "image",
                                src: "res/assets/media/images/carousel/pump1.jpg"
                            }
                        ]
                    }
                },
                {
                    id: "p1-1",
                    kind: "select",
                    optionType: "radio",
                    scorePoints: 1,
                    title: "Comparing DP to more traditional position-keeping options, such as Anchor barges and Jack-up rigs, the various methods offer different advantages and disadvantages. Which of the following advantages apply for DP Vessels?",
                    layout: "none",
                    body: [
                        {
                            type: "question",
                            question: [
                                {
                                    description: "This is option's body 1", // optional. If present, will layout elements in a table-cell, description first.
                                    options: [
                                        {
                                            label: "A",
                                            a: 0
                                        },
                                        {
                                            label: "B",
                                            a: 1
                                        },
                                        {
                                            label: "C",
                                            a: 0
                                        }
                                    ]
                                },
                                {
                                    description: "This is option's body 2",
                                    options: [
                                        {
                                            label: "A",
                                            a: 1
                                        },
                                        {
                                            label: "B",
                                            a: 0
                                        },
                                        {
                                            label: "C",
                                            a: 0
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                },
                {
                    id: "p1-2",
                    kind: "select",
                    optionType: "checkbox",
                    title: "What is a common use for cam operated piston pumps?",
                    layout: "none",
                    body: [
                        {
                            type: "question",
                            question: [
                                {
                                    options: [
                                        {
                                            label: "Option 1",
                                            a: 0
                                        }
                                    ]
                                },
                                {
                                    options: [
                                        {
                                            label: "Option 2",
                                            a: 1
                                        }
                                    ]
                                },
                                {
                                    options: [
                                        {
                                            label: "Option 3",
                                            a: 1
                                        }
                                    ]
                                }


                            ]
                        }

                    ],
                    hint: {
                        title: "Hint",
                        type: "hint",
                        body: [
                            {
                                type: "media",
                                kind: "video",
                                src: 'res/assets/media/video/005.mp4',
                                poster: 'res/assets/media/video/005.png',
                                controls: [
                                    {
                                        type: "toggle",
                                        cssClass: "btn-secondary",
                                        state1: {
                                            type: "button",
                                            event: "play",
                                            label: "Play",
                                            icon: "vti_play"
                                        },
                                        state2: {
                                            type: "button",
                                            event: "pause",
                                            label: "Pause",
                                            icon: "vti_pause"
                                        }
                                    }
                                ]
                            }
                        ]
                    }
                }
            ]
        },
        {
            poolID: "p2", // Questions pool for Module 2
            content: [
                // Questions
                {
                    id: "p2-1",
                    kind: "orderList",
                    title: "Put these items in order",
                    layout: "none",
                    body: [
                        {
                            type: "question",
                            question: [
                                "Red",
                                "Orange with a much longer line here",
                                "Yellow",
                                "Green",
                                "Blue. This is an example of Order List Question.",
                                "Violet"
                            ]
                        }
                    ],
                    hint: {
                        title: "Hint",
                        type: "hint",
                        body: [
                            {
                                type: "text",
                                text: "<p>000 Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>"
                            }
                        ]
                    }
                }
            ]
        },


        /// MODALS =================================================================
        {
            poolID: "modals",

            content: [

                {
                    id: "system-welcome-back",
                    title: "Welcome back!",
                    type: "welcomeBack",
                    body: [
                        {
                            type: "text",
                            text: "Your last visit was on {{date}}. Do you want to continue training from where you left off?"
                        }
                    ],
                    buttons: [
                        {
                            type: "button",
                            label: "Yes",
                            event: "UserEvent.resumeLastVisit",
                            icon: "vti_correct",
                            focus: true,
                            cssClass: "btn-primary"
                        },
                        {
                            type: "button",
                            label: "No",
                            event: "UserEvent.cancel",
                            icon: "vti_incorrect",
                            cssClass: "btn-primary"
                        }
                    ]
                },

                {
                    type: "about",
                    title: "About",
                    id: "system-about",
                    body: [
                        {
                            type: "logos",
                            content: [
                                "res/assets/media/logos/videotel_grey.svg"
                            ]
                        },
                        {
                            type: "production",
                            title: "A VIDEOTEL PRODUCTION"
                        },
                        {
                            type: "credits",
                            title: "Credits",
                            content: [
                                {
                                    title: "In association with:",
                                    list: [
                                        "The Steamship Mutual Underwriting Association (Bermuda) Ltd"
                                    ]
                                },
                                {
                                    title: "The producers would like to acknowledge the assistance of:",
                                    list: [
                                        "A P Moller-Maersk A/S (Maersk Tankers)",
                                        "BIMCO",
                                        "CMA GSM",
                                        "The Great Eastern Shipping Co. Ltd.",
                                        "Grimaldi Group Naples",
                                        "Inernational Maritime Organization (IMO)",
                                        "SMC Marine Services"
                                    ]
                                },
                                {
                                    title: "Consultants:",
                                    list: [
                                        "Prof. Jon Wonham",
                                        "Stuart McCuloch"
                                    ]
                                },
                                {
                                    title: "Course Writer:",
                                    list: [
                                        "Richard Hackett"
                                    ]
                                },
                                {
                                    title: "Producer:",
                                    list: [
                                        "Raal Harris"
                                    ]
                                },
                                {
                                    title: "Multimedia Team:",
                                    list: [
                                        "Jon Saunders",
                                        "Kate Crabtree",
                                        "Max Kostin",
                                        "Terry Eagling-Joyce"
                                    ]
                                }
                            ]
                        },
                        {
                            type: "legal",
                            title: "Legal",
                            content: [
                                {
                                    title: "Warning",
                                    list: [
                                        "<p>Any unauthorised copying, hiring, lending, exhibition, diffusion, sale, public performance or other exploitation of this program is strictly prohibited and may result in prosecution.</p>" +
                                        "<p>This program is intended to reflect the best available techniques and practices at the time of production, it is intended purely as comment. No responsibility is accepted by Videotel, or by any firm, corporation " +
                                        "or organisation who or which has been in any way concerned, with the production or authorised translation, supply or sale of this program for accuracy of any information given here on or for any omission here from.</p>"
                                    ]
                                },
                                {
                                    title: "Version vt5-1",
                                    list: []
                                },
                                {
                                    title: "Copyright© 2016 Videotel",
                                    list: []
                                }
                            ]
                        }
                    ]
                },

                {
                    type: "help",
                    title: "Help",
                    id: "system-help",
                    body: [
                        {
                            type: "support",
                            title: "Contact support",
                            content: [
                                {
                                    name: "Email:",
                                    value: "support@videotel.com"
                                },
                                {
                                    name: "Phone:",
                                    value: "+(44) 207 299 1800"
                                }
                            ]
                        },
                        {
                            type: "icons",
                            title: "Navigation",
                            content: [
                                {
                                    icons: ["vti_empty"],
                                    location: "Location",
                                    description: "Description"
                                },
                                {
                                    icons: ["vti_hamburger"],
                                    location: "Upper left corner",
                                    description: "Main Menu. You can access any screen or section from the Main Menu."
                                },
                                {
                                    icons: ["vti_play", "vti_pause", "vti_refresh"],
                                    location: "Upper right corner",
                                    description: "Audio Controls (Play, Pause, Replay). These buttons will be disabled or enabled, depending on whether the page you are currently viewing has audio."
                                },
                                {
                                    icons: ["vti_arrow-left", "vti_arrow-right"],
                                    location: "Bottom",
                                    description: "Next screen / Previous screen. If you are using touch-screen device, you can swipe left or right to navigate. You can also use your keyboard's Left and Right keys."
                                },
                                {
                                    icons: ["vti_play", "vti_pause", "vti_stop"],
                                    location: "Video",
                                    description: "Play/Pause/Stop video"
                                },
                                {
                                    icons: ["vti_sound-on", "vti_sound-off"],
                                    location: "Video",
                                    description: "Sound on/off"
                                },
                                {
                                    icons: ["vti_list"],
                                    location: "Video",
                                    description: "Select video chapter"
                                },
                                {
                                    icons: ["vti_cc"],
                                    location: "Video",
                                    description: "Select subtitle language"
                                },
                                {
                                    icons: ["vti_fullscreen"],
                                    location: "Video",
                                    description: "Enter or exit fullscreen mode"
                                },
                                {
                                    icons: ["vti_close"],
                                    location: "Modal windows",
                                    description: "Close modal window. You can also press Esc key on your keyboard."
                                },
                                {
                                    icons: ["vti_flip"],
                                    location: "Modal windows",
                                    description: "Flip modal window. Click to see what's on the other side!"
                                },
                                {
                                    icons: ["vti_link-external"],
                                    location: "Text",
                                    description: "External link. Opens new browser window."
                                },
                                {
                                    icons: ["vti_link-internal"],
                                    location: "Text",
                                    description: "Internal link. Navigates to a section or screen within current course."
                                },
                                {
                                    icons: ["vti_link-popup"],
                                    location: "Text",
                                    description: "Modal link. Presents information in a modal (popup) window."
                                },
                                {
                                    icons: ["vti_expand"],
                                    location: "Images",
                                    description: "Press this icon to open Image Viewer."
                                },
                                {
                                    icons: ["vti_zoom-out", "vti_zoom-in"],
                                    location: "Image Viewer",
                                    description: "Zoom-in / Zoom-out controls."
                                },
                                {
                                    icons: ["vti_3d"],
                                    location: "3D animations",
                                    description: "Opens 3D viewer."
                                }
                            ]
                        }
                    ]
                },

                {
                    id: "testLeave",
                    title: "Do you want to leave the Test?",
                    type: "dialog",
                    kind: "warning",
                    body: [
                        {
                            type: "text",
                            text: "Your session will be lost. Press 'Yes' to continue or 'No' to stay in the current Test."
                        }
                    ],
                    buttons: [
                        {
                            type: "button",
                            label: "Yes",
                            event: "UserEvent.testLeave",
                            icon: "vti_correct",
                            focus: true,
                            cssClass: "btn-primary"
                        },
                        {
                            type: "button",
                            label: "No",
                            event: "UserEvent.cancel",
                            icon: "vti_incorrect",
                            cssClass: "btn-primary"
                        }
                    ]
                },

                {
                    id: "hint-text",
                    title: "Modal example",
                    type: "hint",
                    body: [
                        {
                            type: "text",
                            text: "<p>1. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>" +
                            "<p>2. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>" +
                            "<p>3. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>" +
                            "<p>4. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>" +
                            "<p>5. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>"
                        },
                        {
                            type: "media",
                            kind: "audio",
                            src: "res/assets/media/audio/generic-short.mp3"
                        }
                    ]
                },

                // GLOSSARY =====================================================
                {
                    type: "glossary",
                    title: "Glossary",
                    id: "glossary", // Static content ID.
                    sortItems: true, // If set to true, will auto-sort items in ASC order. Optional.
                    body: [

                        {
                            a: "GlossaryPage item 1",
                            b: "Description of the item."
                        },
                        {
                            a: "Another item",
                            b: "Another description of the item."
                        },
                        {
                            a: "Megator",
                            b: "Megator is a leading manufacturer of innovative and unique positive displacement pumps, pumping systems and pollution control solutions."
                        },
                        {
                            a: "Sliding Shoe Pump",
                            b: "Originally developed for mining and marine applications, it is a unique self-priming, positive displacement pump that runs safely under dry suction, has low shear and emulsification characteristics, high suction lift, and is self-compensating for wear."
                        },
                        {
                            a: "Rotary Lobe",
                            b: "Uniquely designed for high-pressure, high-viscosity, and abrasives- and solids-laden stationary pumping applications."
                        },
                        {
                            a: "Weir Skimmers",
                            b: "Designed to remove pollutants and nuisance media from the surface of calm or sheltered waters and shoreline areas."
                        },
                        {
                            a: "Truxor",
                            b: "This Multi-Purpose Amphibious Vehicle is equipped with the Salarollpump and brush skimmer for oil spill cleanup. Its superior amphibious properties make it ideal for working in and around the borders between land and water."
                        },
                        {
                            a: "Recovery Unit",
                            b: "A unique packaged pump and skimmer system designed to recover oil from water in all industrial applications."
                        }
                    ]
                }


            ]
        }

    ]
});










